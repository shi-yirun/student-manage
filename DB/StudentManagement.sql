USE [master]
GO
/****** Object:  Database [StudentManager]    Script Date: 2022/1/15 13:54:27 ******/
CREATE DATABASE [StudentManager] ON  PRIMARY 
( NAME = N'StudentManager', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.MSSQLSERVER\MSSQL\DATA\StudentManager.mdf' , SIZE = 2304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'StudentManager_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.MSSQLSERVER\MSSQL\DATA\StudentManager_log.LDF' , SIZE = 3200KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [StudentManager] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [StudentManager].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [StudentManager] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [StudentManager] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [StudentManager] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [StudentManager] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [StudentManager] SET ARITHABORT OFF 
GO
ALTER DATABASE [StudentManager] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [StudentManager] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [StudentManager] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [StudentManager] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [StudentManager] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [StudentManager] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [StudentManager] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [StudentManager] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [StudentManager] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [StudentManager] SET  ENABLE_BROKER 
GO
ALTER DATABASE [StudentManager] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [StudentManager] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [StudentManager] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [StudentManager] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [StudentManager] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [StudentManager] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [StudentManager] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [StudentManager] SET RECOVERY FULL 
GO
ALTER DATABASE [StudentManager] SET  MULTI_USER 
GO
ALTER DATABASE [StudentManager] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [StudentManager] SET DB_CHAINING OFF 
GO
EXEC sys.sp_db_vardecimal_storage_format N'StudentManager', N'ON'
GO
USE [StudentManager]
GO
/****** Object:  Table [dbo].[ColorSetting]    Script Date: 2022/1/15 13:54:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ColorSetting](
	[UserAccount] [varchar](50) NOT NULL,
	[UserColor] [varchar](50) NULL,
 CONSTRAINT [PK_ColorSetting] PRIMARY KEY CLUSTERED 
(
	[UserAccount] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_admin]    Script Date: 2022/1/15 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_admin](
	[id] [int] IDENTITY(300,1) NOT NULL,
	[adminAccount] [varchar](59) NOT NULL,
	[adminName] [varchar](20) NOT NULL,
	[adminSex] [varchar](4) NULL,
	[RoleId] [int] NULL,
	[adminImage] [varchar](200) NULL,
	[adminTel] [varchar](11) NOT NULL,
	[adminEmail] [varchar](60) NOT NULL,
	[adminPwd] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_class]    Script Date: 2022/1/15 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_class](
	[Id] [int] IDENTITY(10,1) NOT NULL,
	[className] [varchar](200) NOT NULL,
	[classNum] [int] NULL,
	[majorId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_college]    Script Date: 2022/1/15 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_college](
	[Id] [int] IDENTITY(20,1) NOT NULL,
	[collegeName] [varchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_course]    Script Date: 2022/1/15 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_course](
	[Id] [int] IDENTITY(1000,1) NOT NULL,
	[CourseName] [varchar](200) NOT NULL,
	[CourseGrade] [int] NOT NULL,
	[teacherId] [int] NULL,
	[course_start_time] [datetime] NULL,
	[course_end_time] [datetime] NULL,
	[course_desc] [varchar](2000) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_evnet]    Script Date: 2022/1/15 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_evnet](
	[Id] [int] IDENTITY(2000,1) NOT NULL,
	[stuId] [int] NULL,
	[eventDescribe] [varchar](2000) NULL,
	[eventType] [varchar](20) NOT NULL,
	[eventResult] [varchar](200) NOT NULL,
	[eventTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_logs]    Script Date: 2022/1/15 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_logs](
	[logs_id] [int] NOT NULL,
	[logs_operator] [varchar](50) NOT NULL,
	[logs_ip] [varchar](100) NULL,
	[logs_time] [datetime] NULL,
	[logs_desc] [varchar](2048) NULL,
PRIMARY KEY CLUSTERED 
(
	[logs_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_major]    Script Date: 2022/1/15 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_major](
	[Id] [int] IDENTITY(30,1) NOT NULL,
	[majorName] [varchar](200) NOT NULL,
	[collegeId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_menn]    Script Date: 2022/1/15 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_menn](
	[MenuId] [int] IDENTITY(1,1) NOT NULL,
	[MenuName] [nvarchar](50) NULL,
	[MenuIcon] [int] NULL,
	[MenuParent] [int] NULL,
	[MenuUrl] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_role]    Script Date: 2022/1/15 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_role](
	[RoleId] [int] IDENTITY(0,1) NOT NULL,
	[RoleName] [nvarchar](50) NULL,
	[RoleType] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_rolemenu]    Script Date: 2022/1/15 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_rolemenu](
	[RoleMenuId] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[MenuId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleMenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_score]    Script Date: 2022/1/15 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_score](
	[stuId] [int] NOT NULL,
	[courseId] [int] NOT NULL,
	[Grade] [decimal](5, 2) NULL,
 CONSTRAINT [PK__Sys_scor__AC633B72619B8048] PRIMARY KEY CLUSTERED 
(
	[stuId] ASC,
	[courseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_student]    Script Date: 2022/1/15 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_student](
	[Id] [int] IDENTITY(100,1) NOT NULL,
	[studentNo] [varchar](100) NULL,
	[studentName] [varchar](50) NULL,
	[studentSex] [varchar](10) NULL,
	[studentStart_year] [varchar](10) NULL,
	[studentFinish_year] [varchar](10) NULL,
	[studentNation] [varchar](20) NULL,
	[studentBirthday] [datetime] NULL,
	[studentCollegeId] [int] NULL,
	[studentMajorId] [int] NULL,
	[studentDegree] [varchar](10) NULL,
	[RoleId] [int] NULL,
	[classId] [int] NULL,
	[studentImage] [varchar](200) NULL,
	[studentTel] [varchar](11) NULL,
	[studentEmail] [varchar](30) NULL,
	[studentPwd] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_teacher]    Script Date: 2022/1/15 13:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_teacher](
	[Id] [int] IDENTITY(200,1) NOT NULL,
	[teacherNo] [varchar](100) NULL,
	[teacherName] [varchar](50) NOT NULL,
	[teacherSex] [varchar](10) NULL,
	[teacherStart_year] [varchar](10) NULL,
	[teacherNation] [varchar](20) NULL,
	[teacherBirthday] [datetime] NULL,
	[teacherCollegeId] [int] NULL,
	[teacherMajorId] [int] NULL,
	[teacherDegree] [varchar](20) NULL,
	[RoleId] [int] NULL,
	[classId] [int] NULL,
	[teacherImage] [varchar](200) NULL,
	[teacherTel] [varchar](11) NULL,
	[teacherEmail] [varchar](30) NULL,
	[teacherPwd] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ColorSetting] ([UserAccount], [UserColor]) VALUES (N'203010101', N'121, 190, 60')
INSERT [dbo].[ColorSetting] ([UserAccount], [UserColor]) VALUES (N'2133102', N'130, 58, 183')
INSERT [dbo].[ColorSetting] ([UserAccount], [UserColor]) VALUES (N'2133103', N'140, 100, 80')
INSERT [dbo].[ColorSetting] ([UserAccount], [UserColor]) VALUES (N'admin1', N'74, 163, 214')
INSERT [dbo].[ColorSetting] ([UserAccount], [UserColor]) VALUES (N'liuhuang', N'0, 190, 172')
GO
SET IDENTITY_INSERT [dbo].[Sys_admin] ON 

INSERT [dbo].[Sys_admin] ([id], [adminAccount], [adminName], [adminSex], [RoleId], [adminImage], [adminTel], [adminEmail], [adminPwd]) VALUES (300, N'admin1', N'张四', N'女', 0, N'2052254.jpg', N'17771697647', N'862236429@qq.com', N'E10ADC3949BA59ABBE56E057F20F883E')
INSERT [dbo].[Sys_admin] ([id], [adminAccount], [adminName], [adminSex], [RoleId], [adminImage], [adminTel], [adminEmail], [adminPwd]) VALUES (301, N'admin2', N'李四', N'女', 0, N'default.jpg', N'13545115722', N'1694511572@qq.com', N'E10ADC3949BA59ABBE56E057F20F883E')
INSERT [dbo].[Sys_admin] ([id], [adminAccount], [adminName], [adminSex], [RoleId], [adminImage], [adminTel], [adminEmail], [adminPwd]) VALUES (305, N'liuhuang', N'柳欢', N'男', 0, N'default.jpg', N'15586203960', N'1872594125@qq.com', N'E10ADC3949BA59ABBE56E057F20F883E')
SET IDENTITY_INSERT [dbo].[Sys_admin] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_class] ON 

INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (10, N'计科五班', 9, 30)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (11, N'网络二班', 0, 31)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (12, N'信息安全一班', 0, 32)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (13, N'生物科学一班', 0, 33)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (14, N'动力学一班', 0, 34)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (15, N'数学与应用数学一班', 2, 35)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (16, N'经济学一班', 0, 36)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (17, N'药物化学一班', 0, 37)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (18, N'语言信息处理一班', 0, 38)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (19, N'自动化一班', 0, 39)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (20, N'网络三班', 1, 31)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (21, N'计科一班', 0, 30)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (23, N'计科三班', 0, 30)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (24, N'网安一班', 0, 31)
INSERT [dbo].[Sys_class] ([Id], [className], [classNum], [majorId]) VALUES (25, N'数学管理一班', 0, 35)
SET IDENTITY_INSERT [dbo].[Sys_class] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_college] ON 

INSERT [dbo].[Sys_college] ([Id], [collegeName]) VALUES (20, N'计算机学院')
INSERT [dbo].[Sys_college] ([Id], [collegeName]) VALUES (21, N'生命科学学院')
INSERT [dbo].[Sys_college] ([Id], [collegeName]) VALUES (22, N'物理学院')
INSERT [dbo].[Sys_college] ([Id], [collegeName]) VALUES (23, N'数学科学学院')
INSERT [dbo].[Sys_college] ([Id], [collegeName]) VALUES (24, N'经济学院')
INSERT [dbo].[Sys_college] ([Id], [collegeName]) VALUES (25, N'药学院')
INSERT [dbo].[Sys_college] ([Id], [collegeName]) VALUES (26, N'软件与微电子学院')
INSERT [dbo].[Sys_college] ([Id], [collegeName]) VALUES (27, N'信息工程学院')
INSERT [dbo].[Sys_college] ([Id], [collegeName]) VALUES (28, N'化学学院')
INSERT [dbo].[Sys_college] ([Id], [collegeName]) VALUES (29, N'教育与科学学院')
SET IDENTITY_INSERT [dbo].[Sys_college] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_course] ON 

INSERT [dbo].[Sys_course] ([Id], [CourseName], [CourseGrade], [teacherId], [course_start_time], [course_end_time], [course_desc]) VALUES (1016, N'ASP.NET程序设计', 2, 202, CAST(N'2021-08-25T20:48:07.000' AS DateTime), CAST(N'2021-08-25T20:48:11.000' AS DateTime), N'')
INSERT [dbo].[Sys_course] ([Id], [CourseName], [CourseGrade], [teacherId], [course_start_time], [course_end_time], [course_desc]) VALUES (1017, N'JSP程序设计', 3, 202, CAST(N'2021-08-25T20:48:34.000' AS DateTime), CAST(N'2021-08-25T20:48:39.000' AS DateTime), N'')
INSERT [dbo].[Sys_course] ([Id], [CourseName], [CourseGrade], [teacherId], [course_start_time], [course_end_time], [course_desc]) VALUES (1018, N'计算机版', 2, 202, CAST(N'2021-08-25T20:49:15.000' AS DateTime), CAST(N'2021-08-25T20:49:18.000' AS DateTime), N'')
INSERT [dbo].[Sys_course] ([Id], [CourseName], [CourseGrade], [teacherId], [course_start_time], [course_end_time], [course_desc]) VALUES (1022, N'c#编程设计', 2, 207, CAST(N'2021-12-29T00:00:00.000' AS DateTime), CAST(N'2022-01-07T00:00:00.000' AS DateTime), N'')
INSERT [dbo].[Sys_course] ([Id], [CourseName], [CourseGrade], [teacherId], [course_start_time], [course_end_time], [course_desc]) VALUES (1023, N'python课程设计', 2, 204, CAST(N'2021-12-29T18:12:18.057' AS DateTime), CAST(N'2022-01-06T00:00:00.000' AS DateTime), N'')
INSERT [dbo].[Sys_course] ([Id], [CourseName], [CourseGrade], [teacherId], [course_start_time], [course_end_time], [course_desc]) VALUES (1024, N'JAVA语言开发与设计', 4, 206, CAST(N'2021-12-29T18:13:11.733' AS DateTime), CAST(N'2022-01-06T00:00:00.000' AS DateTime), N'')
SET IDENTITY_INSERT [dbo].[Sys_course] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_evnet] ON 

INSERT [dbo].[Sys_evnet] ([Id], [stuId], [eventDescribe], [eventType], [eventResult], [eventTime]) VALUES (2011, 109, N'在2022年乐于助人', N'奖励', N'获得奖状', CAST(N'2021-08-22T16:42:54.607' AS DateTime))
SET IDENTITY_INSERT [dbo].[Sys_evnet] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_major] ON 

INSERT [dbo].[Sys_major] ([Id], [majorName], [collegeId]) VALUES (30, N'计算机科学与技术', 20)
INSERT [dbo].[Sys_major] ([Id], [majorName], [collegeId]) VALUES (31, N'网络安全', 20)
INSERT [dbo].[Sys_major] ([Id], [majorName], [collegeId]) VALUES (32, N'软件工程', 20)
INSERT [dbo].[Sys_major] ([Id], [majorName], [collegeId]) VALUES (33, N'生物科学', 21)
INSERT [dbo].[Sys_major] ([Id], [majorName], [collegeId]) VALUES (34, N'动力学', 22)
INSERT [dbo].[Sys_major] ([Id], [majorName], [collegeId]) VALUES (35, N'数学与应用数学', 23)
INSERT [dbo].[Sys_major] ([Id], [majorName], [collegeId]) VALUES (36, N'经济学', 24)
INSERT [dbo].[Sys_major] ([Id], [majorName], [collegeId]) VALUES (37, N'药物化学', 25)
INSERT [dbo].[Sys_major] ([Id], [majorName], [collegeId]) VALUES (38, N'语言信息处理', 26)
INSERT [dbo].[Sys_major] ([Id], [majorName], [collegeId]) VALUES (39, N'自动化', 27)
INSERT [dbo].[Sys_major] ([Id], [majorName], [collegeId]) VALUES (40, N'物联网专业', 20)
SET IDENTITY_INSERT [dbo].[Sys_major] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_menn] ON 

INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (1, N'学生管理系统', 61732, NULL, NULL)
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (2, N'首页', 57460, 1, NULL)
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (3, N'个人信息管理', 362470, 1, NULL)
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (4, N'系统设置', 361459, 1, NULL)
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (5, N'人员信息管理', 57483, 1, NULL)
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (6, N'教学管理', 61853, 1, NULL)
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (7, N'学院管理', 61852, 1, NULL)
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (8, N'权限管理', 362722, 1, NULL)
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (9, N'首页', 363217, 2, N'StudentManagement.FrmIndex')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (10, N'管理员个人信息', 61979, 3, N'StudentManagement.Admin.FrmAdminInfo')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (11, N'学生信息管理', 362721, 5, N'StudentManagement.Admin.FrmStudentManager')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (12, N'教师信息管理', 362728, 5, N'StudentManagement.Admin.FrmTeacherManagement')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (13, N'班级信息管理', 61869, 6, N'StudentManagement.Admin.FrmClassManagement')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (14, N'课程信息管理', 362744, 6, N'StudentManagement.Admin.FrmCourseManagement')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (15, N'奖惩信息管理', 362882, 6, N'StudentManagement.Admin.FrmEventManagement')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (16, N'专业信息管理', 57566, 6, N'StudentManagement.Admin.FrmMajorManagement')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (17, N'成绩信息管理', 62104, 6, N'StudentManagement.Admin.FrmScoreManagement')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (18, N'学院信息管理', 61852, 7, N'StudentManagement.Admin.FrmCollegeManagement')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (19, N'角色权限设置', 362868, 8, N'StudentManagement.Admin.FrmPowerSetting')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (21, N'主题设置', 62050, 4, N'StudentManagement.FrmSetting')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (23, N'工具箱', 57377, 1, NULL)
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (24, N'天气预报', 363172, 23, N'StudentManagement.Auxiliary.FrmWeather')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (25, N'修改密码', 61475, 4, N'StudentManagement.FrmUpdatePwd')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (27, N'菜单管理', 61499, 8, N'StudentManagement.Admin.FrmMeunManager')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (29, N'学生个人信息', 61979, 3, N'StudentManagement.Student.FrmStudentInfo')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (30, N'教师个人信息', 61979, 3, N'StudentManagement.Teacher.FrmTeacherInfo')
INSERT [dbo].[Sys_menn] ([MenuId], [MenuName], [MenuIcon], [MenuParent], [MenuUrl]) VALUES (31, N'选课信息', 57378, 6, N'StudentManagement.Student.FrmSelectCourse')
SET IDENTITY_INSERT [dbo].[Sys_menn] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_role] ON 

INSERT [dbo].[Sys_role] ([RoleId], [RoleName], [RoleType]) VALUES (0, N'管理员', N'admin')
INSERT [dbo].[Sys_role] ([RoleId], [RoleName], [RoleType]) VALUES (1, N'教师', N'teacher')
INSERT [dbo].[Sys_role] ([RoleId], [RoleName], [RoleType]) VALUES (2, N'学生', N'student')
SET IDENTITY_INSERT [dbo].[Sys_role] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_rolemenu] ON 

INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (149, 0, 1)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (150, 0, 2)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (151, 0, 9)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (152, 0, 3)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (153, 0, 4)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (154, 0, 21)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (155, 0, 25)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (156, 0, 5)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (157, 0, 11)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (158, 0, 12)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (159, 0, 6)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (160, 0, 13)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (161, 0, 14)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (162, 0, 15)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (163, 0, 16)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (164, 0, 17)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (165, 0, 7)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (166, 0, 18)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (167, 0, 8)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (168, 0, 19)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (169, 0, 27)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (170, 0, 23)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (171, 0, 24)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (172, 2, 1)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (173, 2, 2)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (174, 2, 9)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (175, 2, 3)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (176, 2, 29)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (177, 2, 4)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (178, 2, 25)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (179, 2, 6)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (180, 2, 15)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (181, 2, 17)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (182, 2, 31)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (183, 2, 23)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (184, 2, 24)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (200, 1, 2)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (201, 1, 9)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (202, 1, 3)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (203, 1, 30)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (204, 1, 4)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (205, 1, 21)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (206, 1, 25)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (207, 1, 5)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (208, 1, 11)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (209, 1, 6)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (210, 1, 13)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (211, 1, 14)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (212, 1, 17)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (213, 1, 23)
INSERT [dbo].[Sys_rolemenu] ([RoleMenuId], [RoleId], [MenuId]) VALUES (214, 1, 24)
SET IDENTITY_INSERT [dbo].[Sys_rolemenu] OFF
GO
INSERT [dbo].[Sys_score] ([stuId], [courseId], [Grade]) VALUES (104, 1018, CAST(23.00 AS Decimal(5, 2)))
INSERT [dbo].[Sys_score] ([stuId], [courseId], [Grade]) VALUES (105, 1017, CAST(66.00 AS Decimal(5, 2)))
INSERT [dbo].[Sys_score] ([stuId], [courseId], [Grade]) VALUES (106, 1017, CAST(150.00 AS Decimal(5, 2)))
INSERT [dbo].[Sys_score] ([stuId], [courseId], [Grade]) VALUES (116, 1017, CAST(0.00 AS Decimal(5, 2)))
INSERT [dbo].[Sys_score] ([stuId], [courseId], [Grade]) VALUES (116, 1018, CAST(0.00 AS Decimal(5, 2)))
GO
SET IDENTITY_INSERT [dbo].[Sys_student] ON 

INSERT [dbo].[Sys_student] ([Id], [studentNo], [studentName], [studentSex], [studentStart_year], [studentFinish_year], [studentNation], [studentBirthday], [studentCollegeId], [studentMajorId], [studentDegree], [RoleId], [classId], [studentImage], [studentTel], [studentEmail], [studentPwd]) VALUES (104, N'203010102', N'田玉玲', N'女', N'2018', N'2022', N'汉族', CAST(N'2021-08-12T21:16:04.000' AS DateTime), 20, 30, N'本科', 2, 10, N'default.jpg', N'17130833622', N'1234412@qq.com', N'92894219EF3E7B6D752F058D31C5166C')
INSERT [dbo].[Sys_student] ([Id], [studentNo], [studentName], [studentSex], [studentStart_year], [studentFinish_year], [studentNation], [studentBirthday], [studentCollegeId], [studentMajorId], [studentDegree], [RoleId], [classId], [studentImage], [studentTel], [studentEmail], [studentPwd]) VALUES (105, N'203010103', N'蒋文鑫', N'男', N'2018', N'2022', N'汉族', CAST(N'2021-08-12T21:16:04.000' AS DateTime), 20, 30, N'本科', 2, 10, N'default.jpg', N'17123453623', N'5432412@qq.com', N'CF814721358D09942B255746542AD2A4')
INSERT [dbo].[Sys_student] ([Id], [studentNo], [studentName], [studentSex], [studentStart_year], [studentFinish_year], [studentNation], [studentBirthday], [studentCollegeId], [studentMajorId], [studentDegree], [RoleId], [classId], [studentImage], [studentTel], [studentEmail], [studentPwd]) VALUES (106, N'203010104', N'赵志刚', N'男', N'2021', N'2025', N'汉族', CAST(N'2021-08-13T16:32:00.000' AS DateTime), 20, 30, N'专科', 2, 10, N'default.jpg', N'', N'', N'E10ADC3949BA59ABBE56E057F20F883E')
INSERT [dbo].[Sys_student] ([Id], [studentNo], [studentName], [studentSex], [studentStart_year], [studentFinish_year], [studentNation], [studentBirthday], [studentCollegeId], [studentMajorId], [studentDegree], [RoleId], [classId], [studentImage], [studentTel], [studentEmail], [studentPwd]) VALUES (107, N'203010105', N'张晚意', N'男', N'2021', N'2025', N'汉族', CAST(N'2021-08-13T16:32:00.000' AS DateTime), 20, 30, N'专科', 2, 10, N'default.jpg', N'17771654400', N'', N'92894219EF3E7B6D752F058D31C5166C')
INSERT [dbo].[Sys_student] ([Id], [studentNo], [studentName], [studentSex], [studentStart_year], [studentFinish_year], [studentNation], [studentBirthday], [studentCollegeId], [studentMajorId], [studentDegree], [RoleId], [classId], [studentImage], [studentTel], [studentEmail], [studentPwd]) VALUES (109, N'203010107', N'王玲玲', N'女', N'2021', N'2025', N'汉族', CAST(N'2021-08-13T16:32:00.000' AS DateTime), 20, 30, N'专科', 2, 21, N'default.jpg', N'13659630007', N'2287070678@qq.com', N'F59BD65F7EDAFB087A81D4DCA06C4910')
INSERT [dbo].[Sys_student] ([Id], [studentNo], [studentName], [studentSex], [studentStart_year], [studentFinish_year], [studentNation], [studentBirthday], [studentCollegeId], [studentMajorId], [studentDegree], [RoleId], [classId], [studentImage], [studentTel], [studentEmail], [studentPwd]) VALUES (112, N'203010108', N'妮可', N'女', N'2021', N'2025', N'汉族', CAST(N'2021-08-13T16:32:00.000' AS DateTime), 20, 30, N'本科', 2, 10, N'default.jpg', N'13659630005', N'', N'F59BD65F7EDAFB087A81D4DCA06C4910')
INSERT [dbo].[Sys_student] ([Id], [studentNo], [studentName], [studentSex], [studentStart_year], [studentFinish_year], [studentNation], [studentBirthday], [studentCollegeId], [studentMajorId], [studentDegree], [RoleId], [classId], [studentImage], [studentTel], [studentEmail], [studentPwd]) VALUES (113, N'233515100', N'郑蓝琴', N'女', N'2021', N'2025', N'汉族', CAST(N'2021-08-13T16:32:00.000' AS DateTime), 23, 35, N'研究生', 2, 15, N'default.jpg', N'18012345674', N'1872594125@qq.com', N'D9840773233FA6B19FDE8CAF765402F5')
INSERT [dbo].[Sys_student] ([Id], [studentNo], [studentName], [studentSex], [studentStart_year], [studentFinish_year], [studentNation], [studentBirthday], [studentCollegeId], [studentMajorId], [studentDegree], [RoleId], [classId], [studentImage], [studentTel], [studentEmail], [studentPwd]) VALUES (114, N'203120100', N'季小蓝', N'男', N'2021', N'2025', N'汉族', CAST(N'2021-08-13T00:00:00.000' AS DateTime), 20, 31, N'专科', 2, 11, N'default.jpg', N'', N'862236429@qq.com', N'CF814721358D09942B255746542AD2A4')
INSERT [dbo].[Sys_student] ([Id], [studentNo], [studentName], [studentSex], [studentStart_year], [studentFinish_year], [studentNation], [studentBirthday], [studentCollegeId], [studentMajorId], [studentDegree], [RoleId], [classId], [studentImage], [studentTel], [studentEmail], [studentPwd]) VALUES (115, N'213313100', N'尹雨晴', N'男', N'2021', N'2025', N'汉族', CAST(N'2021-08-13T16:32:00.380' AS DateTime), 21, 33, N'专科', 2, 13, N'default.jpg', N'17771697647', N'', N'E10ADC3949BA59ABBE56E057F20F883E')
INSERT [dbo].[Sys_student] ([Id], [studentNo], [studentName], [studentSex], [studentStart_year], [studentFinish_year], [studentNation], [studentBirthday], [studentCollegeId], [studentMajorId], [studentDegree], [RoleId], [classId], [studentImage], [studentTel], [studentEmail], [studentPwd]) VALUES (116, N'233515100', N'郑蓝琴', N'女', N'2021', N'2025', N'汉族', CAST(N'2021-08-13T16:32:00.000' AS DateTime), 23, 35, N'研究生', 2, 15, N'default.jpg', N'18012345674', N'1872594125@qq.com', N'D9840773233FA6B19FDE8CAF765402F5')
INSERT [dbo].[Sys_student] ([Id], [studentNo], [studentName], [studentSex], [studentStart_year], [studentFinish_year], [studentNation], [studentBirthday], [studentCollegeId], [studentMajorId], [studentDegree], [RoleId], [classId], [studentImage], [studentTel], [studentEmail], [studentPwd]) VALUES (117, N'213313101', N'苏愿柳', N'男', N'2021', N'2025', N'汉族', CAST(N'2021-08-13T16:32:00.380' AS DateTime), 21, 33, N'研究生', 2, 13, N'default.jpg', N'13659630004', N'', N'E10ADC3949BA59ABBE56E057F20F883E')
INSERT [dbo].[Sys_student] ([Id], [studentNo], [studentName], [studentSex], [studentStart_year], [studentFinish_year], [studentNation], [studentBirthday], [studentCollegeId], [studentMajorId], [studentDegree], [RoleId], [classId], [studentImage], [studentTel], [studentEmail], [studentPwd]) VALUES (119, N'203010109', N'柳欢', N'男', N'2021', N'2025', N'汉族', CAST(N'2021-08-13T16:32:00.380' AS DateTime), 20, 30, N'本科', 2, 10, N'default.jpg', N'13487067534', N'', N'E10ADC3949BA59ABBE56E057F20F883E')
INSERT [dbo].[Sys_student] ([Id], [studentNo], [studentName], [studentSex], [studentStart_year], [studentFinish_year], [studentNation], [studentBirthday], [studentCollegeId], [studentMajorId], [studentDegree], [RoleId], [classId], [studentImage], [studentTel], [studentEmail], [studentPwd]) VALUES (120, N'203010110', N'刘云', N'女', N'2021', N'2025', N'蒙古族', CAST(N'2021-08-13T16:32:00.380' AS DateTime), 20, 30, N'研究生', 2, 10, N'捕获.PNG', N'15586203960', N'', N'FCEA920F7412B5DA7BE0CF42B8C93759')
SET IDENTITY_INSERT [dbo].[Sys_student] OFF
GO
SET IDENTITY_INSERT [dbo].[Sys_teacher] ON 

INSERT [dbo].[Sys_teacher] ([Id], [teacherNo], [teacherName], [teacherSex], [teacherStart_year], [teacherNation], [teacherBirthday], [teacherCollegeId], [teacherMajorId], [teacherDegree], [RoleId], [classId], [teacherImage], [teacherTel], [teacherEmail], [teacherPwd]) VALUES (202, N'2133102', N'赵菲', N'男', N'2021', N'汉族', CAST(N'2021-08-13T16:32:00.380' AS DateTime), 21, 33, N'本科', 1, 13, N'default.jpg', N'13545115722', N'1694511572@qq.com', N'E10ADC3949BA59ABBE56E057F20F883E')
INSERT [dbo].[Sys_teacher] ([Id], [teacherNo], [teacherName], [teacherSex], [teacherStart_year], [teacherNation], [teacherBirthday], [teacherCollegeId], [teacherMajorId], [teacherDegree], [RoleId], [classId], [teacherImage], [teacherTel], [teacherEmail], [teacherPwd]) VALUES (204, N'2031100', N'张文礼', N'男', N'2021', N'汉族', CAST(N'2021-08-13T16:32:00.000' AS DateTime), 20, 31, N'博士', 1, 11, N'default.jpg', N'15717476793', N'738684555@qq.com', N'F59BD65F7EDAFB087A81D4DCA06C4910')
INSERT [dbo].[Sys_teacher] ([Id], [teacherNo], [teacherName], [teacherSex], [teacherStart_year], [teacherNation], [teacherBirthday], [teacherCollegeId], [teacherMajorId], [teacherDegree], [RoleId], [classId], [teacherImage], [teacherTel], [teacherEmail], [teacherPwd]) VALUES (205, N'2133103', N'陆梁九', N'男', N'2021', N'汉族', CAST(N'2021-08-13T00:00:00.000' AS DateTime), 21, 33, N'研究生', 1, 13, N'default.jpg', N'15715653341', N'635403285@qq.com', N'E10ADC3949BA59ABBE56E057F20F883E')
INSERT [dbo].[Sys_teacher] ([Id], [teacherNo], [teacherName], [teacherSex], [teacherStart_year], [teacherNation], [teacherBirthday], [teacherCollegeId], [teacherMajorId], [teacherDegree], [RoleId], [classId], [teacherImage], [teacherTel], [teacherEmail], [teacherPwd]) VALUES (206, N'2030100', N'文伯天', N'男', N'2021', N'汉族', CAST(N'2021-08-13T16:32:00.380' AS DateTime), 20, 30, N'博士', 1, 10, N'default.jpg', N'13659630007', N'7401404635@qq.com', N'E10ADC3949BA59ABBE56E057F20F883E')
INSERT [dbo].[Sys_teacher] ([Id], [teacherNo], [teacherName], [teacherSex], [teacherStart_year], [teacherNation], [teacherBirthday], [teacherCollegeId], [teacherMajorId], [teacherDegree], [RoleId], [classId], [teacherImage], [teacherTel], [teacherEmail], [teacherPwd]) VALUES (207, N'2031101', N'柳亚子', N'男', N'2021', N'汉族', CAST(N'2021-08-13T16:32:00.380' AS DateTime), 20, 31, N'博士', 1, 20, N'default.jpg', N'13659630000', N'350603@qq.com', N'E10ADC3949BA59ABBE56E057F20F883E')
INSERT [dbo].[Sys_teacher] ([Id], [teacherNo], [teacherName], [teacherSex], [teacherStart_year], [teacherNation], [teacherBirthday], [teacherCollegeId], [teacherMajorId], [teacherDegree], [RoleId], [classId], [teacherImage], [teacherTel], [teacherEmail], [teacherPwd]) VALUES (208, N'2335100', N'李季先', N'女', N'2021', N'汉族', CAST(N'2021-08-13T16:32:00.380' AS DateTime), 23, 35, N'博士', 1, 15, N'default.jpg', N'13659630002', N'401404635@qq.com', N'E10ADC3949BA59ABBE56E057F20F883E')
SET IDENTITY_INSERT [dbo].[Sys_teacher] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Sys_admi__01A352204AB81AF0]    Script Date: 2022/1/15 13:54:30 ******/
ALTER TABLE [dbo].[Sys_admin] ADD UNIQUE NONCLUSTERED 
(
	[adminAccount] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Sys_admi__5EAA9F2547DBAE45]    Script Date: 2022/1/15 13:54:30 ******/
ALTER TABLE [dbo].[Sys_admin] ADD UNIQUE NONCLUSTERED 
(
	[adminTel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Sys_admi__9A3ED53F44FF419A]    Script Date: 2022/1/15 13:54:30 ******/
ALTER TABLE [dbo].[Sys_admin] ADD UNIQUE NONCLUSTERED 
(
	[adminEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ColorSetting] ADD  CONSTRAINT [DF_ColorSetting_Color]  DEFAULT ('80, 160, 255') FOR [UserColor]
GO
ALTER TABLE [dbo].[Sys_admin] ADD  DEFAULT ('男') FOR [adminSex]
GO
ALTER TABLE [dbo].[Sys_admin] ADD  DEFAULT ((0)) FOR [RoleId]
GO
ALTER TABLE [dbo].[Sys_admin] ADD  DEFAULT ('default.jpg') FOR [adminImage]
GO
ALTER TABLE [dbo].[Sys_class] ADD  DEFAULT ((0)) FOR [classNum]
GO
ALTER TABLE [dbo].[Sys_course] ADD  DEFAULT ('1970-01-01 00:00:00') FOR [course_start_time]
GO
ALTER TABLE [dbo].[Sys_course] ADD  DEFAULT ('2100-01-01 00:00:00') FOR [course_end_time]
GO
ALTER TABLE [dbo].[Sys_course] ADD  DEFAULT ('') FOR [course_desc]
GO
ALTER TABLE [dbo].[Sys_evnet] ADD  DEFAULT ('') FOR [eventDescribe]
GO
ALTER TABLE [dbo].[Sys_evnet] ADD  DEFAULT (getdate()) FOR [eventTime]
GO
ALTER TABLE [dbo].[Sys_logs] ADD  DEFAULT ('') FOR [logs_ip]
GO
ALTER TABLE [dbo].[Sys_logs] ADD  DEFAULT ('2100-01-01 00:00:00') FOR [logs_time]
GO
ALTER TABLE [dbo].[Sys_logs] ADD  DEFAULT ('') FOR [logs_desc]
GO
ALTER TABLE [dbo].[Sys_student] ADD  DEFAULT ('男') FOR [studentSex]
GO
ALTER TABLE [dbo].[Sys_student] ADD  DEFAULT ('2018年') FOR [studentStart_year]
GO
ALTER TABLE [dbo].[Sys_student] ADD  DEFAULT ('2022年') FOR [studentFinish_year]
GO
ALTER TABLE [dbo].[Sys_student] ADD  DEFAULT ('汉') FOR [studentNation]
GO
ALTER TABLE [dbo].[Sys_student] ADD  DEFAULT (getdate()) FOR [studentBirthday]
GO
ALTER TABLE [dbo].[Sys_student] ADD  DEFAULT ((20)) FOR [studentCollegeId]
GO
ALTER TABLE [dbo].[Sys_student] ADD  DEFAULT ((30)) FOR [studentMajorId]
GO
ALTER TABLE [dbo].[Sys_student] ADD  DEFAULT ('本科') FOR [studentDegree]
GO
ALTER TABLE [dbo].[Sys_student] ADD  DEFAULT ((2)) FOR [RoleId]
GO
ALTER TABLE [dbo].[Sys_student] ADD  DEFAULT ('default.jpg') FOR [studentImage]
GO
ALTER TABLE [dbo].[Sys_student] ADD  DEFAULT ('') FOR [studentTel]
GO
ALTER TABLE [dbo].[Sys_student] ADD  DEFAULT ('') FOR [studentEmail]
GO
ALTER TABLE [dbo].[Sys_teacher] ADD  DEFAULT ('男') FOR [teacherSex]
GO
ALTER TABLE [dbo].[Sys_teacher] ADD  DEFAULT ('2018年') FOR [teacherStart_year]
GO
ALTER TABLE [dbo].[Sys_teacher] ADD  DEFAULT ('汉') FOR [teacherNation]
GO
ALTER TABLE [dbo].[Sys_teacher] ADD  DEFAULT (getdate()) FOR [teacherBirthday]
GO
ALTER TABLE [dbo].[Sys_teacher] ADD  DEFAULT ((20)) FOR [teacherCollegeId]
GO
ALTER TABLE [dbo].[Sys_teacher] ADD  DEFAULT ((30)) FOR [teacherMajorId]
GO
ALTER TABLE [dbo].[Sys_teacher] ADD  DEFAULT ('研究生') FOR [teacherDegree]
GO
ALTER TABLE [dbo].[Sys_teacher] ADD  DEFAULT ((1)) FOR [RoleId]
GO
ALTER TABLE [dbo].[Sys_teacher] ADD  DEFAULT ('default.jpg') FOR [teacherImage]
GO
ALTER TABLE [dbo].[Sys_teacher] ADD  DEFAULT ('') FOR [teacherTel]
GO
ALTER TABLE [dbo].[Sys_teacher] ADD  DEFAULT ('') FOR [teacherEmail]
GO
ALTER TABLE [dbo].[Sys_admin]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[Sys_role] ([RoleId])
GO
ALTER TABLE [dbo].[Sys_class]  WITH CHECK ADD FOREIGN KEY([majorId])
REFERENCES [dbo].[Sys_major] ([Id])
GO
ALTER TABLE [dbo].[Sys_course]  WITH CHECK ADD FOREIGN KEY([teacherId])
REFERENCES [dbo].[Sys_teacher] ([Id])
GO
ALTER TABLE [dbo].[Sys_evnet]  WITH CHECK ADD FOREIGN KEY([stuId])
REFERENCES [dbo].[Sys_student] ([Id])
GO
ALTER TABLE [dbo].[Sys_major]  WITH CHECK ADD  CONSTRAINT [FK__Sys_major__colle__1273C1CD] FOREIGN KEY([collegeId])
REFERENCES [dbo].[Sys_college] ([Id])
GO
ALTER TABLE [dbo].[Sys_major] CHECK CONSTRAINT [FK__Sys_major__colle__1273C1CD]
GO
ALTER TABLE [dbo].[Sys_rolemenu]  WITH CHECK ADD FOREIGN KEY([MenuId])
REFERENCES [dbo].[Sys_menn] ([MenuId])
GO
ALTER TABLE [dbo].[Sys_rolemenu]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[Sys_role] ([RoleId])
GO
ALTER TABLE [dbo].[Sys_score]  WITH CHECK ADD  CONSTRAINT [FK__Sys_score__cours__6477ECF3] FOREIGN KEY([courseId])
REFERENCES [dbo].[Sys_course] ([Id])
GO
ALTER TABLE [dbo].[Sys_score] CHECK CONSTRAINT [FK__Sys_score__cours__6477ECF3]
GO
ALTER TABLE [dbo].[Sys_score]  WITH CHECK ADD  CONSTRAINT [FK__Sys_score__stuId__6383C8BA] FOREIGN KEY([stuId])
REFERENCES [dbo].[Sys_student] ([Id])
GO
ALTER TABLE [dbo].[Sys_score] CHECK CONSTRAINT [FK__Sys_score__stuId__6383C8BA]
GO
ALTER TABLE [dbo].[Sys_student]  WITH CHECK ADD FOREIGN KEY([classId])
REFERENCES [dbo].[Sys_class] ([Id])
GO
ALTER TABLE [dbo].[Sys_student]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[Sys_role] ([RoleId])
GO
ALTER TABLE [dbo].[Sys_student]  WITH CHECK ADD  CONSTRAINT [FK__Sys_stude__stude__21B6055D] FOREIGN KEY([studentCollegeId])
REFERENCES [dbo].[Sys_college] ([Id])
GO
ALTER TABLE [dbo].[Sys_student] CHECK CONSTRAINT [FK__Sys_stude__stude__21B6055D]
GO
ALTER TABLE [dbo].[Sys_student]  WITH CHECK ADD FOREIGN KEY([studentMajorId])
REFERENCES [dbo].[Sys_major] ([Id])
GO
ALTER TABLE [dbo].[Sys_teacher]  WITH CHECK ADD FOREIGN KEY([classId])
REFERENCES [dbo].[Sys_class] ([Id])
GO
ALTER TABLE [dbo].[Sys_teacher]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[Sys_role] ([RoleId])
GO
ALTER TABLE [dbo].[Sys_teacher]  WITH CHECK ADD  CONSTRAINT [FK__Sys_teach__teach__34C8D9D1] FOREIGN KEY([teacherCollegeId])
REFERENCES [dbo].[Sys_college] ([Id])
GO
ALTER TABLE [dbo].[Sys_teacher] CHECK CONSTRAINT [FK__Sys_teach__teach__34C8D9D1]
GO
ALTER TABLE [dbo].[Sys_teacher]  WITH CHECK ADD FOREIGN KEY([teacherMajorId])
REFERENCES [dbo].[Sys_major] ([Id])
GO
ALTER TABLE [dbo].[Sys_admin]  WITH CHECK ADD CHECK  ((len([adminTel])=(11)))
GO
ALTER TABLE [dbo].[Sys_teacher]  WITH CHECK ADD CHECK  ((len([teacherTel])=(11)))
GO
/****** Object:  StoredProcedure [dbo].[sp_admin_update]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_admin_update]
@Id int,
@adminAccount varchar(59),
@adminName varchar(20),
@adminSex varchar(4),
@adminImage varchar(200),
@adminTel varchar(11),
@adminEmail varchar(50)
as
update Sys_admin set adminAccount=@adminAccount , adminName= @adminName,adminSex=@adminSex, adminImage=@adminImage,
adminTel=@adminTel ,adminEmail=@adminEmail where id=@Id
GO
/****** Object:  StoredProcedure [dbo].[sp_class_add]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_class_add]
@className varchar(200),
@majorId int
as
INSERT INTO [dbo].Sys_class(className,majorId)VALUES(@className,@majorId)
  
GO
/****** Object:  StoredProcedure [dbo].[sp_class_update]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_class_update]
@className varchar(200),
@Id int
as
update Sys_class set className=@className where Id=@Id
GO
/****** Object:  StoredProcedure [dbo].[sp_college_add]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_college_add]
@collegeName varchar(200),
@result int  output  -- 返回插入的结果
as
-- 判断学院名是否存在
if exists(select * from [dbo].[Sys_college] where collegeName=@collegeName)
  set @result=-1
else
begin
  INSERT INTO [dbo].[Sys_college]([collegeName])VALUES(@collegeName)
  set @result=1
end
GO
/****** Object:  StoredProcedure [dbo].[sp_college_update]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_college_update]
@collegeName varchar(200),
@id int,
@result int  output  -- 返回插入的结果
as
-- 判断学院名是否存在
if exists(select * from [dbo].[Sys_college] where collegeName=@collegeName  and Id!=@id)
  set @result=-1
else
begin
  update [dbo].[Sys_college]  set collegeName=@collegeName where Id=@id;
  set @result=1
end
GO
/****** Object:  StoredProcedure [dbo].[sp_color_add]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_color_add]
@UserAccount varchar(50),
@UserColor varchar(50)
 as

INSERT INTO [dbo].[ColorSetting]
           ([UserAccount]
           ,[UserColor])
     VALUES
           (@UserAccount,@UserColor)
GO
/****** Object:  StoredProcedure [dbo].[sp_color_update]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_color_update]
@UserAccount varchar(50),
@UserColor varchar(50)
 as
UPDATE [dbo].[ColorSetting]
   SET 
      [UserColor] = @UserColor
 WHERE UserAccount=@UserAccount
GO
/****** Object:  StoredProcedure [dbo].[sp_course_add]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[sp_course_add]
@CourseName varchar(200),
@CourseGrade int,
@teacherId int,
@course_start_time datetime,
@course_end_time datetime,
@course_desc varchar(2000)
as
INSERT INTO [dbo].Sys_course(CourseName,CourseGrade,teacherId,course_start_time,course_end_time,course_desc)VALUES(@CourseName,@CourseGrade,@teacherId,@course_start_time,@course_end_time,@course_desc)
  
GO
/****** Object:  StoredProcedure [dbo].[sp_course_update]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[sp_course_update]
@Id int,
@CourseName varchar(200),
@CourseGrade int,
@teacherId int,
@course_start_time datetime,
@course_end_time datetime,
@course_desc varchar(2000)
as
update Sys_course set CourseName=@CourseName, CourseGrade=@CourseGrade,teacherId=@teacherId,course_start_time=@course_start_time,course_end_time=@course_end_time,course_desc=@course_desc
where Id=@Id
GO
/****** Object:  StoredProcedure [dbo].[sp_event_add]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_event_add]
@stuId int,
@eventDescribe varchar(2000),
@eventType varchar(20),
@eventResult varchar(200),
@eventTime datetime
as
INSERT INTO [dbo].Sys_evnet(stuId,eventDescribe,eventType,eventResult,eventTime)VALUES(@stuId,@eventDescribe,@eventType,@eventResult,@eventTime)  
GO
/****** Object:  StoredProcedure [dbo].[sp_event_update]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_event_update]
@eventDescribe varchar(2000),
@eventType varchar(20),
@eventResult varchar(200),
@eventTime datetime,
@Id int
as
update Sys_evnet set  eventDescribe=@eventDescribe,eventType=@eventType,eventResult=@eventResult,eventTime=@eventTime
where Id=@Id
GO
/****** Object:  StoredProcedure [dbo].[sp_major_add]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_major_add]
@majorName varchar(200),
@collegeId int
as
INSERT INTO [dbo].Sys_major(majorName,collegeId)VALUES(@majorName,@collegeId)
GO
/****** Object:  StoredProcedure [dbo].[sp_major_update]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_major_update]
@majorName varchar(200),
@Id int,
@collegeId int
as
update Sys_major set majorName=@majorName,collegeId=@collegeId where Id=@Id
GO
/****** Object:  StoredProcedure [dbo].[sp_menu_add]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_menu_add]
@MenuName nvarchar(50),
@MenuIcon int,
@MenuParent int,
@MenuUrl  nvarchar(50)
 as
INSERT INTO [dbo].[Sys_menn]
           ([MenuName]
           ,[MenuIcon]
           ,[MenuParent]
           ,[MenuUrl])
     VALUES
           (@MenuName,@MenuIcon,@MenuParent,@MenuUrl)
GO
/****** Object:  StoredProcedure [dbo].[sp_menu_update]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_menu_update]
@MenuName nvarchar(50),
@MenuIcon int,
@MenuParent int,
@MenuUrl  nvarchar(50),
@MenuId int
 as
UPDATE [dbo].[Sys_menn]
   SET [MenuName] = @MenuName
      ,[MenuIcon] = @MenuIcon
      ,[MenuParent] =@MenuParent
      ,[MenuUrl] =@MenuUrl
 WHERE  MenuId=@MenuId
GO
/****** Object:  StoredProcedure [dbo].[sp_score_add]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_score_add]
@stuId int,
@courseId int,
@Grade decimal(5,2)
as
INSERT INTO [dbo].Sys_score(stuId,courseId,Grade)VALUES(@stuId,@courseId,@Grade)
GO
/****** Object:  StoredProcedure [dbo].[sp_score_update]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_score_update]
@stuId int,
@courseId int,
@Grade decimal(5,2)
as
update Sys_score set Grade=@Grade where stuId=@stuId and courseId=@courseId
GO
/****** Object:  StoredProcedure [dbo].[sp_student_add]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_student_add]
 -- @studentNo varchar(100),
 @studentName varchar(50),
 @studentSex varchar(10),
 @studentStart_year varchar(10),
 @studentFinish_year varchar(10), 
 @studentNation varchar(20),
 @studentBirthday datetime,
 @studentCollegeId int, 
 @studentMajorId int, 
 @studentDegree varchar(10), 
 @classId int,
 @studentImage varchar(200), 
 @studentTel varchar(11),
 @studentEmail varchar(30), 
 @studentPwd varchar(50)
 as
INSERT INTO [dbo].[Sys_student]
           (
           [studentName]
           ,[studentSex]
           ,[studentStart_year]
           ,[studentFinish_year]
           ,[studentNation]
           ,[studentBirthday]
           ,[studentCollegeId]
           ,[studentMajorId]
           ,[studentDegree]
           ,[classId]
           ,[studentImage]
           ,[studentTel]
           ,[studentEmail]
           ,[studentPwd])
     VALUES
           (
            @studentName
           ,@studentSex
           ,@studentStart_year 
           ,@studentFinish_year
           ,@studentNation
           ,@studentBirthday
           ,@studentCollegeId
           ,@studentMajorId
           ,@studentDegree 
           ,@classId 
           ,@studentImage
           ,@studentTel
           ,@studentEmail
           ,@studentPwd)
GO
/****** Object:  StoredProcedure [dbo].[sp_student_update]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_student_update]
 @studentNo varchar(100),
 @studentName varchar(50),
 @studentSex varchar(10),
 @studentStart_year varchar(10),
 @studentFinish_year varchar(10), 
 @studentNation varchar(20),
 @studentBirthday datetime,
 @studentCollegeId int, 
 @studentMajorId int, 
 @studentDegree varchar(10), 
 @classId int,
 @studentImage varchar(200), 
 @studentTel varchar(11),
 @studentEmail varchar(30), 
 @studentPwd varchar(50)
 as

UPDATE [dbo].[Sys_student]
   SET 
       [studentName] = @studentName
      ,[studentSex] = @studentSex
      ,[studentStart_year] =@studentStart_year
      ,[studentFinish_year] = @studentFinish_year
      ,[studentNation] = @studentNation
      ,[studentBirthday] = @studentBirthday
      ,[studentCollegeId] = @studentCollegeId
      ,[studentMajorId] = @studentMajorId
      ,[studentDegree] = @studentDegree
      ,[classId] = @classId
      ,[studentImage] = @studentImage
      ,[studentTel] =@studentTel
      ,[studentEmail] = @studentEmail
      ,[studentPwd] = @studentPwd
 WHERE studentNo=@studentNo
GO
/****** Object:  StoredProcedure [dbo].[sp_student_update1]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_student_update1]
 @studentNo varchar(100),
 @studentName varchar(50),
 @studentSex varchar(10),
 @studentStart_year varchar(10),
 @studentFinish_year varchar(10), 
 @studentNation varchar(20),
 @studentBirthday datetime,
 @studentCollegeId int, 
 @studentMajorId int, 
 @studentDegree varchar(10), 
 @classId int,
 @studentImage varchar(200), 
 @studentTel varchar(11),
 @studentEmail varchar(30)
 as

UPDATE [dbo].[Sys_student]
   SET 
       [studentName] = @studentName
      ,[studentSex] = @studentSex
      ,[studentStart_year] =@studentStart_year
      ,[studentFinish_year] = @studentFinish_year
      ,[studentNation] = @studentNation
      ,[studentBirthday] = @studentBirthday
      ,[studentCollegeId] = @studentCollegeId
      ,[studentMajorId] = @studentMajorId
      ,[studentDegree] = @studentDegree
      ,[classId] = @classId
      ,[studentImage] = @studentImage
      ,[studentTel] =@studentTel
      ,[studentEmail] = @studentEmail
 WHERE studentNo=@studentNo
GO
/****** Object:  StoredProcedure [dbo].[sp_teacher_add]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_teacher_add]
 @teacherName varchar(50),
 @teacherSex varchar(10),
 @teacherStart_year varchar(10),
 @teacherNation varchar(20),
 @teacherBirthday datetime,
 @teacherCollegeId int, 
 @teacherMajorId int, 
 @teacherDegree varchar(10), 
 @classId int,
 @teacherImage varchar(200), 
 @teacherTel varchar(11),
 @teacherEmail varchar(30), 
 @teacherPwd varchar(50)
 as
INSERT INTO [dbo].[Sys_teacher]
           (
            [teacherName]
           ,[teacherSex]
           ,[teacherStart_year]
           ,[teacherNation]
           ,[teacherBirthday]
           ,[teacherCollegeId]
           ,[teacherMajorId]
           ,[teacherDegree]
           ,[classId]
           ,[teacherImage]
           ,[teacherTel]
           ,[teacherEmail]
           ,[teacherPwd])
     VALUES
           (
 @teacherName,
 @teacherSex,
 @teacherStart_year ,
 @teacherNation ,
 @teacherBirthday ,
 @teacherCollegeId , 
 @teacherMajorId , 
 @teacherDegree , 
 @classId ,
 @teacherImage , 
 @teacherTel ,
 @teacherEmail, 
 @teacherPwd 
)
GO
/****** Object:  StoredProcedure [dbo].[sp_teacher_update]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_teacher_update]
  @teacherNo varchar(100),
 @teacherName varchar(50),
 @teacherSex varchar(10),
 @teacherStart_year varchar(10),
 @teacherNation varchar(20),
 @teacherBirthday datetime,
 @teacherCollegeId int, 
 @teacherMajorId int, 
 @teacherDegree varchar(10), 
 @classId int,
 @teacherImage varchar(200), 
 @teacherTel varchar(11),
 @teacherEmail varchar(30), 
 @teacherPwd varchar(50)
 as

UPDATE [dbo].Sys_teacher
   SET 
	 
      [teacherName] =  @teacherName 
      ,[teacherSex] =@teacherSex 
      ,[teacherStart_year] =@teacherStart_year 
      ,[teacherNation] = @teacherNation
      ,[teacherBirthday] =@teacherBirthday
      ,[teacherCollegeId] =@teacherCollegeId
      ,[teacherMajorId] =@teacherMajorId
      ,[teacherDegree] = @teacherDegree
      ,[classId] = @classId
      ,[teacherImage] = @teacherImage
      ,[teacherTel] =@teacherTel 
      ,[teacherEmail] = @teacherEmail
      ,[teacherPwd] = @teacherPwd
 WHERE   [teacherNo] =  @teacherNo 
GO
/****** Object:  StoredProcedure [dbo].[sp_teacher_update1]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create  procedure [dbo].[sp_teacher_update1]
  @teacherNo varchar(100),
 @teacherName varchar(50),
 @teacherSex varchar(10),
 @teacherStart_year varchar(10),
 @teacherNation varchar(20),
 @teacherBirthday datetime,
 @teacherCollegeId int, 
 @teacherMajorId int, 
 @teacherDegree varchar(10), 
 @classId int,
 @teacherImage varchar(200), 
 @teacherTel varchar(11),
 @teacherEmail varchar(30)
as

UPDATE [dbo].Sys_teacher
   SET 
	 
      [teacherName] =  @teacherName 
      ,[teacherSex] =@teacherSex 
      ,[teacherStart_year] =@teacherStart_year 
      ,[teacherNation] = @teacherNation
      ,[teacherBirthday] =@teacherBirthday
      ,[teacherCollegeId] =@teacherCollegeId
      ,[teacherMajorId] =@teacherMajorId
      ,[teacherDegree] = @teacherDegree
      ,[classId] = @classId
      ,[teacherImage] = @teacherImage
      ,[teacherTel] =@teacherTel 
      ,[teacherEmail] = @teacherEmail
 WHERE   [teacherNo] =  @teacherNo 
GO
/****** Object:  Trigger [dbo].[trg_Student_Insert]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[trg_Student_Insert]
on [dbo].[Sys_student]
after insert
as
DECLARE   @num1 int ; --定义变量存储最后数字
DECLARE   @no1 varchar(50); -- 存储查询出来编号
DECLARE   @studentCollegeId int; -- 存储学院编号
DECLARE   @studentMajorId int; -- 存储专业编号
DECLARE   @Id1 int; -- 存储Id
DECLARE   @classid int;
DECLARE   @result1  varchar(200); -- 存储结果
BEGIN
 -- 获取学院Id和专业Id,新增数据的Id
select  @studentCollegeId=studentCollegeId, @studentMajorId=studentMajorId ,@Id1=Id,@classid=classId
from  inserted; 

-- select '新增学院号'+CONVERT(varchar(20),@teacherCollegeId) +'新增'+CONVERT(varchar(20),@teacherMajorId)
-- 获取新增上一条的数据
  select @no1=studentNo  from (select studentNo,ROW_NUMBER()over(order by id desc)
  row_num from Sys_student where studentCollegeId= @studentCollegeId  and studentMajorId=@studentMajorId and @classid=classId )t
  where  row_num=2
  -- select '上次添加的教师号'+@no
if @no1 is null
begin
     set @num1=100
end
else 
begin
  DECLARE @temp1 int;
  set @temp1=CONVERT(int,SUBSTRING(@no1,LEN(@no1)-2,3))
  set @num1= @temp1+1
end	 
	-- 存储结果
	set @result1= Convert(varchar(200),@studentCollegeId)+Convert(varchar(200),@studentMajorId)+Convert(varchar(200),@classid)+Convert(varchar(200),@num1);
	-- 去更新教师表的职工编号字段
	update  Sys_student  set studentNo=@result1 where Id=@Id1;
end
GO
ALTER TABLE [dbo].[Sys_student] ENABLE TRIGGER [trg_Student_Insert]
GO
/****** Object:  Trigger [dbo].[trg_Student1_delete]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create TRIGGER [dbo].[trg_Student1_delete]
on [dbo].[Sys_student]
after delete
as
begin
declare @id int
select @id=classId from Deleted
update Sys_class set classNum=classNum-1 where @id=Id
end
GO
ALTER TABLE [dbo].[Sys_student] ENABLE TRIGGER [trg_Student1_delete]
GO
/****** Object:  Trigger [dbo].[trg_Student1_Insert]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create TRIGGER [dbo].[trg_Student1_Insert]
on [dbo].[Sys_student]
after insert
as
begin
declare @id int
select @id=classId from inserted
update Sys_class set classNum=classNum+1 where @id=Id
end
GO
ALTER TABLE [dbo].[Sys_student] ENABLE TRIGGER [trg_Student1_Insert]
GO
/****** Object:  Trigger [dbo].[trg_Teaher_Insert]    Script Date: 2022/1/15 13:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[trg_Teaher_Insert]
on [dbo].[Sys_teacher]
after insert
as
DECLARE   @num int ; --定义变量存储最后数字
DECLARE   @no varchar(50); -- 存储查询出来编号
DECLARE   @teacherCollegeId int; -- 存储学院编号
DECLARE   @teacherMajorId int; -- 存储专业编号
DECLARE   @Id int; -- 存储Id
DECLARE   @result  varchar(200); -- 存储结果
BEGIN
 -- 获取学院Id和专业Id,新增数据的Id
select  @teacherCollegeId=teacherCollegeId, @teacherMajorId=teacherMajorId ,@Id=Id
from  inserted; 

-- select '新增学院号'+CONVERT(varchar(20),@teacherCollegeId) +'新增'+CONVERT(varchar(20),@teacherMajorId)

-- 获取新增上一条的数据
  select @no=teacherNo  from (select teacherNo,teacherCollegeId,teacherMajorId,ROW_NUMBER()over(order by id desc)
  row_num from Sys_teacher where teacherCollegeId= @teacherCollegeId  and @teacherMajorId=teacherMajorId  )t
  where  row_num=2
  -- select '上次添加的教师号'+@no
if @no is null
begin
     set @num=100
end
else 
begin
  DECLARE @temp int;
  set @temp=CONVERT(int,SUBSTRING(@no,LEN(@no)-2,3))
  set @num= @temp+1
end	 
	-- 存储结果
	set @result= Convert(varchar(200),@teacherCollegeId)+Convert(varchar(200),@teacherMajorId)+Convert(varchar(200),@num);
	-- 去更新教师表的职工编号字段
	update  Sys_teacher  set teacherNo=@result where Id=@Id;
end
GO
ALTER TABLE [dbo].[Sys_teacher] ENABLE TRIGGER [trg_Teaher_Insert]
GO
USE [master]
GO
ALTER DATABASE [StudentManager] SET  READ_WRITE 
GO
