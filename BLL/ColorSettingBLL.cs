﻿using DAL;
using Model;

namespace BLL
{
    public class ColorSettingBLL
    {
        public ColorSetting GetColor(string adminAccount)
        {
            return new ColorSettingDAL().GetColor(adminAccount);
        }

        public void AddColor(string account, string color)
        {
            new ColorSettingDAL().AddColor(account, color);
        }

        public void UpdateColor(string account, string color)
        {
            new ColorSettingDAL().UpdateColor(account, color);
        }
    }
}