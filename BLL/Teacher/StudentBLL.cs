﻿using System.Data;

namespace BLL.Teacher
{
    public class StudentBLL
    {
        public DataTable GetStudentList(int id)
        {
            return new DAL.Teacher.StudentDAL().GetStudentList(id);
        }
    }
}