﻿using System.Data;

namespace BLL.Teacher
{
    public class CourseBLL
    {
        public DataTable GetCourseList(int id)
        {
            return new DAL.Teacher.CourseDAL().GetCourseList(id);
        }

        public object GetScoreList(int begin, int end, out int count, int studentName, int courseName, int id)
        {
            return new DAL.Teacher.CourseDAL().GetScoreList(begin, end, out count, studentName, courseName, id);
        }
    }
}