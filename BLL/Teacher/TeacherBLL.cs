﻿using DAL.Teacher;
using Model;
using System.Collections.Generic;
using System.Data;

namespace BLL.Teacher
{
    public class TeacherBLL
    {
        /// <summary>
        /// 教师使用手机用户名邮箱进行登录
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public Model.Teacher GetTeacherInfo(string userName)
        {
            return new TeacherDAL().GetTeacherInfo(userName);
        }

        public DataTable GetTeacherList()
        {
            return new TeacherDAL().GetTeacherList();
        }

        public bool UpdateTeacherPwd(Model.Teacher teacher)
        {
            return new TeacherDAL().UpdateTeacherPwd(teacher);
        }

        public List<Model.Class> GetTeacherList(int id)
        {
            return new TeacherDAL().GetTeacherList(id);
        }

        public int TeacherUpdate(Model.Teacher teacher)
        {
            return new TeacherDAL().TeacherUpdate(teacher);
        }

        public DataTable GetTeacher(int id)
        {
            return new TeacherDAL().CourseBind(id);
        }

        public List<Course> SelectTeacher(int id, int course, out int count, int begin, int end)
        {
            return new TeacherDAL().SelectTeacher(id, course, out count, begin, end);
        }
    }
}