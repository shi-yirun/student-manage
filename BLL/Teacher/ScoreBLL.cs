﻿using Model;

namespace BLL.Teacher
{
    public class ScoreBLL
    {
        public int UpdateScore(Score score)
        {
            return new DAL.Teacher.ScoreDAL().UpdateScore(score);
        }
    }
}