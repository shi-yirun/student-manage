﻿using DAL.Common;
using Model;
using System.Collections.Generic;

namespace BLL.Common
{
    public class MeunBLL
    {
        /// <summary>
        /// 返回菜单中的根节点
        /// </summary>
        /// <returns></returns>
        public Menu GetMeunRootInfo()
        {
            return new MeunDAL().GetMeunRootInfo();
        }

        /// <summary>
        /// 根据角色Id获取菜单
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public List<Menu> GetMeunListInfo(int roleId)
        {
            return new MeunDAL().GetMeunListInfo(roleId);
        }
    }
}