﻿using DAL.Admin;
using Model;
using System.Collections.Generic;
using System.Data;

namespace BLL.Admin
{
    public class ScoreBLL
    {
        public List<Score> GetScoreList(int begin, int end, out int count, int cId, int sId)
        {
            return new ScoreDAL().GetScoreList(begin, end, out count, cId, sId);
        }

        public DataTable GetScoreList()
        {
            return new ScoreDAL().GetScoreList();
        }

        public bool IsExist(Score score, string text)
        {
            return new ScoreDAL().IsExist(score, text);
        }

        public void AddScore(Score score)
        {
            new ScoreDAL().AddScore(score);
        }

        public void Update(Score score)
        {
            new ScoreDAL().Update(score);
        }

        public int Delete(Score score)
        {
            return new ScoreDAL().Delete(score);
        }
    }
}