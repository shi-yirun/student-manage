﻿using DAL.Admin;
using Model;
using System.Collections.Generic;
using System.Data;

namespace BLL.Admin
{
    public class ClassBLL
    {
        /// <summary>
        /// 根据专业绑定班级信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetClassList(int majorId)
        {
            return new ClassDAL().GetClassList(majorId);
        }

        /// <summary>
        /// 绑定班级信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetClassList()
        {
            return new ClassDAL().GetClassList();
        }

        public List<Class> GetClassList(int begin, int end, out int result, Class class1)
        {
            return new ClassDAL().GetClassList(begin, end, out result, class1);
        }

        public bool isExist(string id, string name)
        {
            return new ClassDAL().isExist(id, name);
        }

        public void AddClass(Class @class)
        {
            new ClassDAL().AddClass(@class);
        }

        public void UpdateClass(Class @class)
        {
            new ClassDAL().UpdateClass(@class);
        }
    }
}