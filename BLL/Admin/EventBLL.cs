﻿using DAL.Admin;
using Model;
using System.Collections.Generic;
using System.Data;

namespace BLL.Admin
{
    public class EventBLL
    {
        public DataTable GetEventList()
        {
            return new EventDAL().GetEventList();
        }

        public List<Event> GetEventList(int begin, int end, out int count, int studentName)
        {
            return new EventDAL().GetEventList(begin, end, out count, studentName);
        }

        public void AddEvent(Event event1)
        {
            new EventDAL().AddEvent(event1);
        }

        public void UpdateEvent(Event event1)
        {
            new EventDAL().UpdateEvent(event1);
        }

        public int DeleteEvent(int id)
        {
            return new EventDAL().DeleteEvent(id);
        }

        public DataTable GetEventList(int begin, int end, out int count, string type, int id)
        {
            return new EventDAL().GetEventList(begin, end, out count, type, id);
        }
    }
}