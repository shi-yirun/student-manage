﻿using DAL.Admin;
using System.Data;

namespace BLL.Admin
{
    public class RoleBLL
    {
        /// <summary>
        /// 获取所有的角色列表
        /// </summary>
        /// <returns></returns>
        public DataTable GetRoleList()
        {
            return new RoleDAL().GetRoleList();
        }
    }
}