﻿using DAL.Admin;
using Model;
using System.Collections.Generic;
using System.Data;

namespace BLL.Admin
{
    public class MajorBLL
    {
        /// <summary>
        /// 绑定专业信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetMajorList(int collegeId)
        {
            return new MajorDAL().GetMajorList(collegeId);
        }

        public DataTable GetMajorList()
        {
            return new MajorDAL().GetMajorList();
        }

        public DataTable GetMajorList(string collegeName)
        {
            return new MajorDAL().GetMajorList(collegeName);
        }

        public List<Major> MajorBindTable(int begin, int end, out int result, Model.Major major)
        {
            return new MajorDAL().MajorBindTable(begin, end, out result, major);
        }

        public bool isExist(Major major)
        {
            return new MajorDAL().isExist(major);
        }

        public int AddMajor(Major major)
        {
            return new MajorDAL().AddMajor(major);
        }

        public int UpdateMajor(Major major)
        {
            return new MajorDAL().UpdateMajor(major);
        }

        public DataTable GetMajorCollegeList()
        {
            return new MajorDAL().GetMajorCollegeList();
        }
    }
}