﻿using System.Collections.Generic;

namespace BLL.Admin
{
    public class RoleMenuBLL
    {
        /// <summary>
        /// 修改对应角色的菜单信息
        /// </summary>
        /// <returns></returns>
        public int UpdateMenuList(List<int> menuIds, int roleId)
        {
            return new DAL.Admin.RoleMenuDAL().UpdateMenuList(menuIds, roleId);
        }
    }
}