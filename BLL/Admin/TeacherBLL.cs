﻿using DAL.Admin;
using System.Collections.Generic;
using System.Data;

namespace BLL.Admin
{
    public class TeacherBLL
    {
        public List<Model.Teacher> GetTeacherList(int begin, int end, out int count, Model.Teacher teacher)
        {
            return new TeacherDAL().GetTeacherList(begin, end, out count, teacher);
        }

        public int AddTeacher(Model.Teacher teacher)
        {
            return new TeacherDAL().AddTeacher(teacher);
        }

        public int TeacherUpdate(Model.Teacher teacher)
        {
            return new TeacherDAL().TeacherUpdate(teacher);
        }

        public DataTable GetTeacherList()
        {
            return new TeacherDAL().GetTeacherList();
        }

        public bool IsExistTeacherEmail(string teacherEmail, string teacherNo)
        {
            return new TeacherDAL().IsExistTeacherEmail(teacherEmail, teacherNo);
        }

        public bool IsExistTeacherTel(string teacherTel, string teacherNo)
        {
            return new TeacherDAL().IsExistTeacherTel(teacherTel, teacherNo);
        }

        public int DeleteTeacher(int id)
        {
            return new TeacherDAL().DeleteTeacher(id);
        }
    }
}