﻿using DAL.Admin;
using Model;
using System.Collections.Generic;
using System.Data;

namespace BLL.Admin
{
    public class CollegeBLL
    {
        /// <summary>
        /// 绑定学院信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetCollegeList()
        {
            return new CollegeDAL().GetCollegeList();
        }

        /// <summary>
        /// 获取学院信息
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <param name="count"></param>
        /// <param name="collegeId"></param>
        /// <returns></returns>
        public List<College> GetCollegeList(int begin, int end, out int count, int collegeId)
        {
            return new CollegeDAL().GetCollegeList(begin, end, out count, collegeId);
        }

        /// <summary>
        /// 添加学院
        /// </summary>
        /// <param name="college"></param>
        /// <returns></returns>
        public int AddCollege(College college)
        {
            return new CollegeDAL().AddCollege(college);
        }

        /// <summary>
        /// 修改学院信息
        /// </summary>
        /// <param name="college"></param>
        /// <returns></returns>

        public int UpdateCollege(College college)
        {
            return new CollegeDAL().UpdateCollege(college);
        }
    }
}