﻿using DAL.Admin;

namespace BLL.Admin
{
    public class AdminBLL
    {
        /// <summary>
        /// 通过用户名/邮箱/手机号码获取管理员信息
        /// </summary>
        /// <param name="userName">用户名/邮箱/手机号码</param>
        /// <returns></returns>
        public Model.Admin GetAdminInfo(string userName)
        {
            return new AdminDAL().GetAdminInfo(userName);
        }

        public bool UpdateAdmin(Model.Admin admin)
        {
            return new AdminDAL().UpdateAdmin(admin);
        }
    }
}