﻿using DAL.Admin;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace BLL.Admin
{
    public class StudentBLL
    {
        /// <summary>
        /// 分页获取信息
        /// </summary>
        /// <param name="activePage"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <param name="student"></param>
        /// <returns></returns>
        public object GetStudentList(int activePage, int pageSize, out int count, Model.Student student)
        {
            return new StudentDAL().GetStudentList(activePage, pageSize, out count, student);
        }

        public int StudnetAdd(Model.Student student)
        {
            return new DAL.Admin.StudentDAL().StudnetAdd(student);
        }

        /// <summary>
        /// 判断学生邮箱是否存在
        /// </summary>
        /// <param name="studentEmail"></param>
        /// <returns></returns>
        public bool IsExistStudentEmail(string studentEmail, string studentNo)
        {
            return new DAL.Admin.StudentDAL().IsExistStudentEmail(studentEmail, studentNo);
        }

        public DataTable GetStudentList1()
        {
            return new DAL.Admin.StudentDAL().GetStudentList1();
        }

        public object SelectStudent(int id, int courese, out int count, int begin, int end)
        {
            return new StudentDAL().SelectStudent(id, courese, out count, begin, end);
        }

        /// 判断电话号码是否存在
        /// </summary>
        /// <param name="studentTel"></param>
        /// <param name="studentNo"></param>
        /// <returns></returns>
        public bool IsExistStudentTel(string studentTel, string studentNo)
        {
            return new DAL.Admin.StudentDAL().IsExistStudentTel(studentTel, studentNo);
        }

        /// <summary>
        /// 学生做修改
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        public int StudnetUpdate(Model.Student student)
        {
            return new DAL.Admin.StudentDAL().StudnetUpdate(student);
        }

        /// <summary>
        /// 学生信息删除
        /// </summary>
        /// <param name="stuId"></param>
        /// <returns></returns>
        public int StudentDelete(int stuId)
        {
            return new DAL.Admin.StudentDAL().StudentDelete(stuId);
        }

        /// <summary>
        /// 从数据库中获取所有的数据进行打印
        /// </summary>
        /// <returns></returns>
        public DataTable GetStudentList()
        {
            return new DAL.Admin.StudentDAL().GetStudentList();
        }

        /// <summary>
        /// 根据指定的列去查询数据
        /// </summary>
        /// <param name="viewColumns"></param>
        /// <returns></returns>
        public DataTable GetStudentList(List<DataGridViewColumn> viewColumns)
        {
            return new DAL.Admin.StudentDAL().GetStudentList(viewColumns);
        }

        public int StudnetUpdate1(Model.Student student1)
        {
            return new StudentDAL().StudnetUpdate1(student1);
        }
    }
}