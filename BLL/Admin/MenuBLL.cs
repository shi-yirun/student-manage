﻿using DAL.Admin;
using Model;
using System.Collections.Generic;
using System.Data;

namespace BLL.Admin
{
    public class MenuBLL
    {
        public DataTable GetMeunList()
        {
            return new MenuDAL().GetMeunList();
        }

        public object GetMeunList(int begin, int end, out int count, Menu menu)
        {
            return new MenuDAL().GetMeunList(begin, end, out count, menu);
        }

        public int MenuAdd(Menu menu)
        {
            return new MenuDAL().MenuAdd(menu);
        }

        public int MenuUpdate(Menu menu)
        {
            return new MenuDAL().MenuUpdate(menu);
        }

        public int MenuDelete(int menuid)
        {
            return new MenuDAL().MenuDelete(menuid);
        }

        public DataTable GetMenuList()
        {
            return new MenuDAL().GetMeunList();
        }

        /// <summary>
        /// 返回所有的菜单信息
        /// </summary>
        /// <returns></returns>
        public List<Menu> GetMenuList2()
        {
            return new MenuDAL().GetMenuList2();
        }

        public DataTable GetMenuList1()
        {
            return new MenuDAL().GetMenuList1();
        }

        /// <summary>
        /// 根据角色获取对应的菜单
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public List<Menu> GetMeunList(int roleId)
        {
            return new MenuDAL().GetMenuList(roleId);
        }
    }
}