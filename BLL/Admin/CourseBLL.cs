﻿using DAL.Admin;
using Model;
using System.Collections.Generic;
using System.Data;

namespace BLL.Admin
{
    public class CourseBLL
    {
        public DataTable GetCourseList()
        {
            return new CourseDAL().GetCourseList();
        }

        public List<Course> GetCourseList(int begin, int end, out int count, string cId)
        {
            return new CourseDAL().GetCourseList(begin, end, out count, cId);
        }

        public DataTable GetCourseTList()
        {
            return new CourseDAL().GetCourseTList();
        }

        public bool IsExist(string couserName, string id)
        {
            return new CourseDAL().IsExist(couserName, id);
        }

        public DataTable GetCourseList1(int id)
        {
            return new CourseDAL().GetCourseList1(id);
        }

        public void AddCourse(Course course)
        {
            new CourseDAL().AddCourse(course);
        }

        public DataTable GetCourseList1(int id, int course, out int count, int begin, int end)
        {
            return new CourseDAL().GetCourseList1(id, course, out count, begin, end);
        }

        public DataTable GetCourseList(int id)
        {
            return new CourseDAL().GetCourseList(id);
        }

        public void UpdateCourse(Course course)
        {
            new CourseDAL().UpdateCourse(course);
        }

        public int DeleteCourse(int id)
        {
            return new CourseDAL().DeleteCourse(id);
        }
    }
}