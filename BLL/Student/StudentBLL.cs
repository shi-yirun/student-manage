﻿using DAL.Student;

namespace BLL.Student
{
    public class StudentBLL
    {
        /// <summary>
        /// 通过手机或用户名或邮箱获取学生信息
        /// </summary>
        /// <param name="userName">手机或用户名或邮箱</param>
        /// <returns></returns>
        public Model.Student GetStudentInfo(string userName)
        {
            return new StudentDAL().GetStudentInfo(userName);
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>

        public bool UpdateStudentPwd(Model.Student student)
        {
            return new StudentDAL().UpdateStudentPwd(student);
        }
    }
}