﻿using Model;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Teacher
{
    public class CourseDAL
    {
        public DataTable GetCourseList(int id)
        {
            string sql = string.Format(@"select distinct a.CourseId,c.CourseName from Sys_score a,Sys_student b,Sys_course c
            where a.courseId=c.Id and a.stuId=b.Id  and c.teacherId ={0}", id);
            return DBHelper.GetDataTable(sql);
        }

        public object GetScoreList(int begin, int end, out int count, int studentName, int courseName, int id)
        {
            List<Score> scores = new List<Score>();
            //SQL语句条件
            string sqlWhere = "";

            ///当值选择学院
            if (studentName != 0)
            {
                sqlWhere += string.Format("  and a.stuId={0}", studentName);
            }

            ///当值选择学院
            if (courseName != 0)
            {
                sqlWhere += string.Format("  and a.courseId={0}", courseName);
            }

            //获取行
            string sql = string.Format(@"select count(*) from Sys_score a,Sys_student b,Sys_course c
where a.courseId = c.Id and a.stuId = b.Id  and c.teacherId = {0} {1}", id, sqlWhere);

            count = (int)DBHelper.GetExecuteScalar(sql, new SqlParameter[0]);

            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = new SqlParameter("@Id", id);
            parameters[1] = new SqlParameter("@begin", begin);
            parameters[2] = new SqlParameter("@end", end);

            string sql1 = string.Format(@"select * from (select a.stuId,b.studentName,a.courseId,c.CourseName,a.Grade,ROW_NUMBER() over(order by a.stuId)row  from Sys_score a,Sys_student b,Sys_course c
            where a.courseId=c.Id and a.stuId=b.Id  and c.teacherId =@Id {0})t
            where row between @begin and @end", sqlWhere);
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql1, parameters);
            if (reader.HasRows)

            {
                while (reader.Read())
                {
                    Score score = new Score()
                    {
                        StuId = reader.GetInt32(0),
                        StudentName = reader.GetString(1),
                        CourseId = reader.GetInt32(2),
                        CourseName = reader.GetString(3),
                        Grade = reader.GetDecimal(4)
                    };
                    scores.Add(score);
                }
            }
            reader.Close();
            return scores;
        }
    }
}