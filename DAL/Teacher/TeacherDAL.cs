﻿using Model;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Teacher
{
    public class TeacherDAL
    {
        /// <summary>
        /// 教师使用用户名/邮箱/手机号进行获取信息
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public Model.Teacher GetTeacherInfo(string userName)
        {
            //声明一个教师对象
            Model.Teacher teacher = null;
            string sql = @"SELECT[Id],[teacherNo],[teacherName],[teacherSex],[teacherStart_year]
                         ,[teacherNation],[teacherBirthday],[teacherCollegeId],[teacherMajorId]
                         ,[teacherDegree],[RoleId],[classId],[teacherImage],[teacherTel],[teacherEmail]
                         ,[teacherPwd] FROM[dbo].[Sys_teacher]
                         where[teacherNo] =@teacherNo or  [teacherTel] = @teacherTel or  [teacherEmail] =@teacherEmail";

            //传入参数
            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@teacherNo", userName);
            paras[1] = new SqlParameter("@teacherTel", userName);
            paras[2] = new SqlParameter("@teacherEmail", userName);
            // 获取DataRead 对象
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql, paras);
            //  判断是否存在数据
            if (!reader.HasRows)
            {
                return teacher;
            }
            // 有数据就进行读取
            while (reader.Read())
            {
                // 初始化对象
                teacher = new Model.Teacher()
                {
                    Id = reader.GetInt32(0),
                    TeacherNo = reader.GetString(1),
                    TeacherName = reader.GetString(2),
                    TeacherSex = reader.GetString(3),
                    TeacherStart_year = reader.GetString(4),
                    TeacherNation = reader.GetString(5),
                    TeacherBirthday = reader.GetDateTime(6),
                    TeacherCollegeId = reader.GetInt32(7),
                    TeacherMajorId = reader.GetInt32(8),
                    TeacherDegree = reader.GetString(9),
                    RoleId = reader.GetInt32(10),
                    ClassId = reader.GetInt32(11),
                    TeacherImage = reader.GetString(12),
                    TeacherTel = reader.GetString(13),
                    TeacherEmail = reader.GetString(14),
                    TeacherPwd = reader.GetString(15),
                };
            }
            // 关闭读写器
            reader.Close();
            return teacher;
        }

        public List<Model.Class> GetTeacherList(int id)
        {
            List<Model.Class> classes = new List<Class>();
            string sql1 = @"SELECT
Sys_class.Id,
	Sys_class.className,
	Sys_class.classNum,
	Sys_major.majorName, Sys_major.Id,
	Sys_college.collegeName
FROM
	dbo.Sys_major
	INNER JOIN
	dbo.Sys_college
	ON
		Sys_major.collegeId = Sys_college.Id AND
		Sys_major.collegeId = Sys_college.Id
	INNER JOIN
	dbo.Sys_class
	ON
		Sys_major.Id = Sys_class.majorId
		where Sys_class.id=@id
";
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = new SqlParameter("@id", id);
            Class @class = null;
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql1, parameters);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    @class = new Class()
                    {
                        Id = reader.GetInt32(0),
                        className = reader.GetString(1),
                        classNum = reader.GetInt32(2),
                        MajorName = reader.GetString(3),
                        majorId = reader.GetInt32(4),
                        collegeName = reader.GetString(5)
                    };
                    classes.Add(@class);
                }
            }
            return classes;
        }

        public List<Course> SelectTeacher(int id, int course, out int count, int begin, int end)
        {
            List<Course> courses = new List<Course>();
            //SQL语句条件
            string sqlWhere = "";

            if (course != 0)
            {
                sqlWhere = @" and Sys_course.Id=" + course;
            }
            string s = @" and teacherId=" + id;
            //获取行
            string sql = string.Format("SELECT count(*) FROM[dbo].[Sys_course] where 1=1{1}{0}", sqlWhere, s);
            count = (int)DBHelper.GetExecuteScalar(sql);
            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = new SqlParameter("@begin", begin);
            parameters[1] = new SqlParameter("@end", end);

            string sql1 = string.Format(@"
                       select  [Id] ,[CourseName],[CourseGrade],[teacherId],[course_start_time] ,[course_end_time] ,[course_desc],teacherName from
                        (SELECT Sys_course.[Id] ,[CourseName],[CourseGrade],[teacherId],[course_start_time] ,[course_end_time] ,[course_desc],teacherName , ROW_NUMBER() over(order by Sys_course.id) row
                        FROM[dbo].[Sys_course],Sys_teacher where Sys_teacher.Id= Sys_course.teacherId{0}{1})t
                        where row between @begin and @end", sqlWhere, s);
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql1, parameters);
            if (reader.HasRows)

            {
                while (reader.Read())
                {
                    Course course1 = new Course()
                    {
                        Id = reader.GetInt32(0),
                        CourseName = reader.GetString(1),
                        CourseGrade = reader.GetInt32(2),
                        TeacherId = reader.GetInt32(3),
                        Course_start_time = reader.GetDateTime(4),
                        Course_end_time = reader.GetDateTime(5),
                        Course_desc = reader.GetString(6),
                        teacherName = reader.GetString(7)
                    };
                    courses.Add(course1);
                }
            }
            reader.Close();
            return courses;
        }

        public DataTable CourseBind(int id)
        {
            string sql = @"SELECT [Id],[CourseName]
          FROM [dbo].[Sys_course]
  where    [teacherId]=" + id;
            return DBHelper.GetDataTable(sql);
        }

        public int TeacherUpdate(Model.Teacher teacher)
        {
            SqlParameter[] sqlParameters = new SqlParameter[13];
            sqlParameters[0] = new SqlParameter("@teacherName", teacher.TeacherName);
            sqlParameters[1] = new SqlParameter("@teacherSex", teacher.TeacherSex);
            sqlParameters[2] = new SqlParameter("@teacherStart_year", teacher.TeacherStart_year);
            sqlParameters[3] = new SqlParameter("@teacherNation", teacher.TeacherNation);
            sqlParameters[4] = new SqlParameter("@teacherBirthday", teacher.TeacherBirthday);
            sqlParameters[5] = new SqlParameter("@teacherMajorId", teacher.TeacherMajorId);
            sqlParameters[6] = new SqlParameter("@teacherDegree", teacher.TeacherDegree);
            sqlParameters[7] = new SqlParameter("@classId", teacher.ClassId);
            sqlParameters[8] = new SqlParameter("@teacherImage", teacher.TeacherImage == "" ? "default.jpg" : teacher.TeacherImage);
            sqlParameters[9] = new SqlParameter("@teacherCollegeId", teacher.TeacherCollegeId);
            sqlParameters[10] = new SqlParameter("@teacherTel", teacher.TeacherTel == "" ? "" : teacher.TeacherTel);
            sqlParameters[11] = new SqlParameter("@teacherNo", teacher.TeacherNo);
            sqlParameters[12] = new SqlParameter("@teacherEmail", teacher.TeacherEmail == "" ? "" : teacher.TeacherEmail);
            return DBHelper.GetExecuteNonQuery("sp_teacher_update1", sqlParameters);
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="teacher"></param>
        /// <returns></returns>
        public bool UpdateTeacherPwd(Model.Teacher teacher)
        {
            string sql = "UPDATE [dbo].[Sys_teacher] SET [teacherPwd] =@teacherPwd WHERE Id = @Id";
            SqlParameter[] parameter = new SqlParameter[2];
            parameter[0] = new SqlParameter("@teacherPwd", teacher.TeacherPwd);
            parameter[1] = new SqlParameter("@Id", teacher.Id);
            return DBHelper.GetExecuteNonQuery(sql, parameter, false) >= 1;
        }

        public DataTable GetTeacherList()
        {
            string sql = @"SELECT [Id],[teacherName]
                            FROM [dbo].[Sys_teacher]";
            return DBHelper.GetDataTable(sql);
        }
    }
}