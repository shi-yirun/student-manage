﻿using System.Data;

namespace DAL.Teacher
{
    public class StudentDAL
    {
        /// <summary>
        /// 获取该教师的教的学生
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataTable GetStudentList(int id)
        {
            string sql = string.Format(@"select distinct a.stuId,b.studentName from Sys_score a,Sys_student b,Sys_course c
            where a.courseId = c.Id and a.stuId = b.Id  and c.teacherId = {0}", id);
            return DBHelper.GetDataTable(sql);
        }
    }
}