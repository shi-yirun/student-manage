﻿using Model;
using System.Data.SqlClient;

namespace DAL.Teacher
{
    public class ScoreDAL
    {
        public int UpdateScore(Score score)
        {
            string sql = string.Format(@"UPDATE [dbo].[Sys_score] SET [Grade] = @Grade  WHERE [stuId] = @stuId and [courseId] = @courseId ");
            SqlParameter[] parameter = new SqlParameter[3];
            parameter[0] = new SqlParameter("@Grade", score.Grade);
            parameter[1] = new SqlParameter("@stuId", score.StuId);
            parameter[2] = new SqlParameter("@courseId", score.CourseId);
            return DBHelper.GetExecuteNonQuery(sql, parameter, false);
        }
    }
}