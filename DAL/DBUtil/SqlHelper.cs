﻿using System;

namespace DAL.DBUtil
{
    public class SqlHelper
    {
        /// <summary>
        /// 根据字符串的值，返回数据库中的null值
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static object SqlNull(string str)
        {
            if (str == "")
            {
                return DBNull.Value;
            }
            return str;
        }
    }
}