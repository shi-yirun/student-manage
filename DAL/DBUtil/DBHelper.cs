﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    internal class DBHelper
    {
        private static string strConn = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        /// <summary>
        /// 获取DataTable 对象
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataTable GetDataTable(string sql)
        {
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                try
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
                    DataTable table = new DataTable();
                    adapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// 执行增删改存储过程
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static int GetExecuteNonQuery(string procedureName, SqlParameter[] parameters, bool isStoredProcedure = true)
        {
            using (SqlConnection cnoo = new SqlConnection(strConn))
            {
                try
                {
                    cnoo.Open();
                    SqlCommand com = new SqlCommand(procedureName, cnoo);
                    if (isStoredProcedure == true)
                    {
                        com.CommandType = CommandType.StoredProcedure;
                    }
                    else
                    {
                        com.CommandType = CommandType.Text;
                    }
                    com.Parameters.AddRange(parameters);
                    return com.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// 执行增删改存储过程
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static int GetExecuteNonQuery(string procedureName, SqlParameter[] parameters, out int result)
        {
            using (SqlConnection cnoo = new SqlConnection(strConn))
            {
                try
                {
                    cnoo.Open();
                    SqlCommand com = new SqlCommand(procedureName, cnoo);
                    com.CommandType = CommandType.StoredProcedure;
                    com.Parameters.AddRange(parameters);

                    int value = com.ExecuteNonQuery();
                    result = Convert.ToInt32(com.Parameters["@result"].Value.ToString());
                    return value;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// 获取第一行第一列的值
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static object GetExecuteScalar(string sql)
        {
            using (SqlConnection cnoo = new SqlConnection(strConn))
            {
                try
                {
                    cnoo.Open();
                    SqlCommand com = new SqlCommand(sql, cnoo);

                    return com.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// 获取第一行第一列的值
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static object GetExecuteScalar(string sql, SqlParameter[] parameter)
        {
            using (SqlConnection cnoo = new SqlConnection(strConn))
            {
                try
                {
                    cnoo.Open();
                    SqlCommand com = new SqlCommand(sql, cnoo);
                    com.Parameters.AddRange(parameter);

                    return com.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// 获取一个SqlDataReader对象
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <param name="paras">参数列表</param>
        /// <returns></returns>
        public static SqlDataReader GetSqlDataReader(string sql, SqlParameter[] paras)
        {
            try
            {
                SqlConnection cnoo = new SqlConnection(strConn);
                cnoo.Open();
                SqlCommand cmd = new SqlCommand(sql, cnoo);
                //生成临时的存储过程，防止 where xx or 1=1;
                cmd.Parameters.AddRange(paras);
                //在关闭SqlDataReader对象时关闭SqlConnection对象
                return cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  执行事务
        /// </summary>
        /// <param name="listSql">sql语句</param>
        /// <param name="parameters">参数</param>
        /// <returns></returns>
        public static int GetTransaction(List<string> listSql, SqlParameter[] parameters)
        {
            using (SqlConnection conn = new SqlConnection(strConn))
            {
                conn.Open();
                using (SqlTransaction tran = conn.BeginTransaction())// 开启事务
                {
                    try
                    {
                        SqlCommand cmd = conn.CreateCommand();

                        cmd.Transaction = tran;  // 设置在Command对象中执行的事务
                        cmd.Parameters.AddRange(parameters);//把参数给事务
                        foreach (string sql in listSql)
                        {
                            cmd.CommandText = sql;

                            cmd.ExecuteNonQuery();
                        }
                        tran.Commit();
                        return 1; // 执行成功
                    }
                    catch (Exception)
                    {
                        tran.Rollback(); // 回滚事务
                        return 0;
                    }
                }
            }
        }
    }
}