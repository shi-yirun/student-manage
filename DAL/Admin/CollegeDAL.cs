﻿using Model;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Admin
{
    public class CollegeDAL
    {
        /// <summary>
        /// 查询学员信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetCollegeList()
        {
            string sql = @"SELECT [Id],[collegeName]
                           FROM [dbo].[Sys_college]";
            return DBHelper.GetDataTable(sql);
        }

        /// <summary>
        /// 添加学院信息
        /// </summary>
        /// <param name="college"></param>
        /// <returns></returns>
        public int AddCollege(College college)
        {
            SqlParameter[] para = new SqlParameter[2];
            para[0] = new SqlParameter("@collegeName", college.collegeName);
            para[1] = new SqlParameter("@result", SqlDbType.Int);
            para[1].Direction = ParameterDirection.Output;
            int result;
            DBHelper.GetExecuteNonQuery("sp_college_add", para, out result);
            return result;
        }

        public int UpdateCollege(College college)
        {
            SqlParameter[] para = new SqlParameter[3];
            para[0] = new SqlParameter("@collegeName", college.collegeName);
            para[1] = new SqlParameter("@Id", college.Id);
            para[2] = new SqlParameter("@result", SqlDbType.Int);
            para[2].Direction = ParameterDirection.Output;
            int result;
            DBHelper.GetExecuteNonQuery("sp_college_update", para, out result);
            return result;
        }

        /// <summary>
        /// 查询学院信息并分页
        /// </summary>
        /// <param name="begin">开始</param>
        /// <param name="end">结束</param>
        /// <param name="count">总行数</param>
        /// <param name="collegeName">学院名</param>
        /// <returns></returns>
        public List<College> GetCollegeList(int begin, int end, out int count, int collegeId)
        {
            List<College> colleges = new List<College>();
            //SQL语句条件
            string sqlWhere = "";

            ///当值选择学院
            if (collegeId != 0)
            {
                sqlWhere = string.Format("  and Id=@Id");
            }
            //获取行
            string sql = string.Format("SELECT count(*) FROM[dbo].[Sys_college] where 1=1{0}", sqlWhere);

            SqlParameter[] para = new SqlParameter[1];
            para[0] = new SqlParameter("@Id", collegeId);

            count = (int)DBHelper.GetExecuteScalar(sql, para);

            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = new SqlParameter("@Id", collegeId);
            parameters[1] = new SqlParameter("@begin", begin);
            parameters[2] = new SqlParameter("@end", end);

            string sql1 = string.Format(@"
                         select * from
                        (SELECT[Id], [collegeName], ROW_NUMBER() over(order by id) row
                        FROM[dbo].[Sys_college] where 1=1 {0})t
                        where row between @begin and @end", sqlWhere);
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql1, parameters);
            if (reader.HasRows)

            {
                while (reader.Read())
                {
                    College college = new College()
                    {
                        Id = reader.GetInt32(0),
                        collegeName = reader.GetString(1)
                    };
                    colleges.Add(college);
                }
            }
            reader.Close();
            return colleges;
        }
    }
}