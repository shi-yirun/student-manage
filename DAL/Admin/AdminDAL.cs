﻿using System.Data.SqlClient;

namespace DAL.Admin
{
    public class AdminDAL
    {
        /// <summary>
        /// 通过用户名/邮箱/手机号码获取用户信息
        /// </summary>
        /// <param name="userName">用户名/邮箱/手机号码</param>
        /// <returns></returns>
        public Model.Admin GetAdminInfo(string userName)
        {
            // 声明一个admin对象
            Model.Admin admin = null;
            // sql语句
            string sql = @"SELECT [id],[adminAccount],[adminName],[adminSex],[RoleId],[adminImage],[adminTel],[adminEmail],[adminPwd] FROM [dbo].[Sys_admin]
            where [adminAccount] =@adminAccount or [adminTel]=@adminTel or [adminEmail]=@adminEmail";
            //参数
            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@adminAccount", userName);
            paras[1] = new SqlParameter("@adminTel", userName);
            paras[2] = new SqlParameter("@adminEmail", userName);
            // 获取SqlDataReader对象
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql, paras);
            //如果没有值就直接返回一个null
            if (!reader.HasRows)
            {
                return null;
            }
            //因为手机号/邮箱/用户名是唯一的，所以只能查出唯一的一条数据
            while (reader.Read())
            {
                admin = new Model.Admin()
                {
                    Id = reader.GetInt32(0),
                    AdminAccount = reader.GetString(1),
                    AdminName = reader.GetString(2),
                    AdminSex = reader.GetString(3),
                    RoleId = reader.GetInt32(4),
                    AdminImage = reader.GetString(5),
                    AdminTel = reader.GetString(6),
                    AdminEmail = reader.GetString(7),
                    AdminPwd = reader.GetString(8)
                };
            }

            //关闭reader 对象
            reader.Close();
            return admin;
        }

        public bool UpdateAdmin(Model.Admin admin)
        {
            SqlParameter[] parameters = new SqlParameter[7];
            parameters[0] = new SqlParameter("@Id", admin.Id);
            parameters[1] = new SqlParameter("@adminAccount", admin.AdminAccount);
            parameters[2] = new SqlParameter("@adminName", admin.AdminName);
            parameters[3] = new SqlParameter("@adminSex", admin.AdminSex);
            parameters[4] = new SqlParameter("@adminImage", admin.AdminImage);
            parameters[5] = new SqlParameter("@adminTel", admin.AdminTel);
            parameters[6] = new SqlParameter("@adminEmail", admin.AdminEmail);
            return DBHelper.GetExecuteNonQuery("sp_admin_update", parameters) == 1;
        }
    }
}