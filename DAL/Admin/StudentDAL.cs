﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DAL.Admin
{
    public class StudentDAL
    {
        public object GetStudentList(int begin, int end, out int count, Model.Student studentWhere)
        {
            string sqlWhere = "";
            string sql = @"select count(*) from Sys_student where 1=1";

            List<Model.Student> students = new List<Model.Student>();

            if (studentWhere.StudentNo != "")
            {
                sqlWhere += string.Format(" and studentNo='{0}' ", studentWhere.StudentNo);
            }
            if (studentWhere.StudentName != "")
            {
                sqlWhere += string.Format(" and StudentName='{0}' ", studentWhere.StudentName);
            }
            //0代表班级没选
            if (studentWhere.ClassId != 0)
            {
                sqlWhere += string.Format(" and ClassId={0} ", studentWhere.ClassId);
            }
            sql += sqlWhere;
            count = (int)DBHelper.GetExecuteScalar(sql);
            // 获取一个DateReader 对象
            sql = string.Format(@"select *from (SELECT
	dbo.Sys_student.*,
	dbo.Sys_class.className,
	dbo.Sys_major.majorName,
	dbo.Sys_college.collegeName,ROW_NUMBER()over(order by Sys_student.id)rowindex
FROM
	dbo.Sys_major
	INNER JOIN
	dbo.Sys_college
	ON
		dbo.Sys_major.collegeId = dbo.Sys_college.Id
	INNER JOIN
	dbo.Sys_class
	ON
		dbo.Sys_major.Id = dbo.Sys_class.majorId
	INNER JOIN
	dbo.Sys_student
	ON
		dbo.Sys_student.classId = dbo.Sys_class.Id{0} )t
              where rowindex between @begin and @end", sqlWhere);

            SqlParameter[] paras = new SqlParameter[2];
            paras[0] = new SqlParameter("@begin", begin);
            paras[1] = new SqlParameter("@end", end);

            SqlDataReader reader = DBHelper.GetSqlDataReader(sql, paras);

            //判断DateReader里有值有,没有值即返回一个null
            if (!reader.HasRows)
            {
                return students;
            }
            //有值就初始化赋值
            while (reader.Read())
            {
                Model.Student student = new Model.Student();
                student.Id = reader.GetInt32(0);
                student.StudentNo = reader.GetString(1);
                student.StudentName = reader.GetString(2);
                student.StudentSex = reader.GetString(3);
                student.StudentStart_year = reader.GetString(4);
                student.StudentFinish_year = reader.GetString(5);
                student.StudentNation = reader.GetString(6);
                student.StudentBirthday = reader.GetDateTime(7);
                student.StudentCollegeId = reader.GetInt32(8);
                student.StudentMajorId = reader.GetInt32(9);
                student.StudentDegree = reader.GetString(10);
                student.RoleId = reader.GetInt32(11);
                student.ClassId = reader.GetInt32(12);
                student.StudentImage = reader.GetString(13);
                student.StudentTel = reader.GetString(14);
                student.StudentEmail = reader.GetString(15);
                student.StudentPwd = reader.GetString(16);
                //把数据加入到list
                student.ClassName = reader.GetString(17);
                student.MajorName = reader.GetString(18);
                student.CollegeName = reader.GetString(19);
                students.Add(student);
            }
            //关闭reader 对象
            reader.Close();
            return students;
        }

        public object SelectStudent(int id, int end1, out int count, int begin, int end)
        {
            string s = "";
            if (end1 != 0)
            {
                s = @" and  dbo.Sys_course.Id=" + end1;
            }

            string sql1 = @"SELECT

    count(*)
FROM

    dbo.Sys_course,
	dbo.Sys_score,
	dbo.Sys_student
WHERE

    dbo.Sys_score.stuId = dbo.Sys_student.Id AND

    dbo.Sys_course.Id = dbo.Sys_score.courseId and stuId = " + id + s;
            count = (int)DBHelper.GetExecuteScalar(sql1);
            string sql = string.Format(@"select
Id,CourseName,teacherName,course_start_time,course_end_time,course_desc from
 (SELECT
	dbo.Sys_course.CourseName,teacherName,course_start_time,course_end_time,course_desc,dbo.Sys_course.Id, ROW_NUMBER()over(order by Sys_course.Id)row
FROM
    dbo.Sys_course,
	dbo.Sys_score,
	dbo.Sys_student,Sys_teacher
WHERE

    dbo.Sys_score.stuId = dbo.Sys_student.Id AND

    dbo.Sys_course.Id = dbo.Sys_score.courseId and   dbo.Sys_course.teacherId=Sys_teacher.Id and stuId ={0}" + s + ")t where row between  {1} and {2}", id, begin, end);

            return DBHelper.GetDataTable(sql);
        }

        public int StudnetUpdate1(Model.Student student1)
        {
            SqlParameter[] sqlParameters = new SqlParameter[14];
            sqlParameters[0] = new SqlParameter("@studentName", student1.StudentName);
            sqlParameters[1] = new SqlParameter("@studentSex", student1.StudentSex);
            sqlParameters[2] = new SqlParameter("@studentStart_year", student1.StudentStart_year);
            sqlParameters[3] = new SqlParameter("@studentFinish_year", student1.StudentFinish_year);
            sqlParameters[4] = new SqlParameter("@studentNation", student1.StudentNation);
            sqlParameters[5] = new SqlParameter("@studentBirthday", student1.StudentBirthday);
            sqlParameters[6] = new SqlParameter("@studentMajorId", student1.StudentMajorId);
            sqlParameters[7] = new SqlParameter("@studentDegree", student1.StudentDegree);
            sqlParameters[8] = new SqlParameter("@classId", student1.ClassId);
            sqlParameters[9] = new SqlParameter("@studentImage", student1.StudentImage == "" ? "default.jpg" : student1.StudentImage);
            sqlParameters[10] = new SqlParameter("@studentCollegeId", student1.StudentCollegeId);
            sqlParameters[11] = new SqlParameter("@studentTel", student1.StudentTel == "" ? "" : student1.StudentTel);
            sqlParameters[12] = new SqlParameter("@studentEmail", student1.StudentEmail == "" ? "" : student1.StudentEmail);
            sqlParameters[13] = new SqlParameter("@studentNo", student1.StudentNo);
            return DBHelper.GetExecuteNonQuery("sp_student_update1", sqlParameters);
        }

        /// <summary>
        /// 根据指定列去查询数据
        /// </summary>
        /// <param name="viewColumns"></param>
        /// <returns></returns>
        public DataTable GetStudentList(List<DataGridViewColumn> viewColumns)
        {
            string sql = "select  ";
            int i = 0;
            foreach (DataGridViewColumn item in viewColumns)
            {
                sql += item.Name;
                if (i < viewColumns.Count - 1)
                {
                    sql += ", ";
                }
                i++;
            }
            sql += @" FROM
	dbo.Sys_major
	INNER JOIN
	dbo.Sys_college
	ON
		dbo.Sys_major.collegeId = dbo.Sys_college.Id
	INNER JOIN
	dbo.Sys_class
	ON
		dbo.Sys_major.Id = dbo.Sys_class.majorId
	INNER JOIN
	dbo.Sys_student
	ON
		dbo.Sys_student.classId = dbo.Sys_class.Id ";
            return DBHelper.GetDataTable(sql);
        }

        /// <summary>
        /// 返回要打印的学生数据
        /// </summary>
        /// <returns></returns>
        public DataTable GetStudentList()
        {
            string sql = @"SELECT
	Sys_student.*, Sys_class.className,

	Sys_major.majorName, 	Sys_college.collegeName

FROM
	dbo.Sys_student
	INNER JOIN
	dbo.Sys_college
	ON
		Sys_student.studentCollegeId = Sys_college.Id
	INNER JOIN
	dbo.Sys_class
	ON
		Sys_student.classId = Sys_class.Id
	INNER JOIN
	dbo.Sys_major
	ON
		Sys_class.majorId = Sys_major.Id AND
		Sys_student.studentMajorId = Sys_major.Id AND
		Sys_college.Id = Sys_major.collegeId";
            return DBHelper.GetDataTable(sql);
        }

        /// <summary>
        /// 返回要打印的学生数据
        /// </summary>
        /// <returns></returns>
        public DataTable GetStudentList1()
        {
            string sql = "select Id,studentName from Sys_student";
            return DBHelper.GetDataTable(sql);
        }

        /// <summary>
        /// 学生信息删除
        /// </summary>
        /// <param name="stuId"></param>
        /// <returns></returns>
        public int StudentDelete(int stuId)
        {
            List<string> listSql = new List<string>();
            string sql = "";
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = new SqlParameter("@stuId", stuId);
            //1.删除成绩表的数据
            sql = string.Format("DELETE FROM [dbo].[Sys_score] WHERE stuId=@stuId");
            listSql.Add(sql);
            //2.删除奖惩表的数据
            sql = string.Format("DELETE FROM [dbo].[Sys_evnet] WHERE stuId=@stuId");
            listSql.Add(sql);
            //3.删除学生表的数据
            sql = string.Format("DELETE FROM [dbo].[Sys_student] WHERE Id=@stuId");
            listSql.Add(sql);
            return DBHelper.GetTransaction(listSql, parameters);
        }

        /// <summary>
        /// 学生信息修改
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        public int StudnetUpdate(Model.Student student)
        {
            SqlParameter[] sqlParameters = new SqlParameter[15];
            sqlParameters[0] = new SqlParameter("@studentName", student.StudentName);
            sqlParameters[1] = new SqlParameter("@studentSex", student.StudentSex);
            sqlParameters[2] = new SqlParameter("@studentStart_year", student.StudentStart_year);
            sqlParameters[3] = new SqlParameter("@studentFinish_year", student.StudentFinish_year);
            sqlParameters[4] = new SqlParameter("@studentNation", student.StudentNation);
            sqlParameters[5] = new SqlParameter("@studentBirthday", student.StudentBirthday);
            sqlParameters[6] = new SqlParameter("@studentMajorId", student.StudentMajorId);
            sqlParameters[7] = new SqlParameter("@studentDegree", student.StudentDegree);
            sqlParameters[8] = new SqlParameter("@classId", student.ClassId);
            sqlParameters[9] = new SqlParameter("@studentImage", student.StudentImage == "" ? "default.jpg" : student.StudentImage);
            sqlParameters[10] = new SqlParameter("@studentCollegeId", student.StudentCollegeId);
            sqlParameters[11] = new SqlParameter("@studentTel", student.StudentTel == "" ? "" : student.StudentTel);
            sqlParameters[12] = new SqlParameter("@studentEmail", student.StudentEmail == "" ? "" : student.StudentEmail);
            sqlParameters[13] = new SqlParameter("@studentPwd", student.StudentPwd);
            sqlParameters[14] = new SqlParameter("@studentNo", student.StudentNo);
            return DBHelper.GetExecuteNonQuery("sp_student_update", sqlParameters);
        }

        public bool IsExistStudentTel(string studentTel, string studentNo)
        {
            string sql = string.Format("select COUNT(*) from Sys_student where  studentTel='{0}'", studentTel);
            if (studentNo != "")
            {
                sql += string.Format("and  studentNo!='{0}'", studentNo);
            }
            return (int)DBHelper.GetExecuteScalar(sql) >= 1;
        }

        /// <summary>
        /// 判断学生邮箱是否存在
        /// </summary>
        /// <param name="studentEmail"></param>
        /// <returns></returns>
        public bool IsExistStudentEmail(string studentEmail, string studentNo)
        {
            string sql = string.Format("select COUNT(*) from Sys_student where  studentEmail='{0}'", studentEmail);
            // 如果学号不为空，那么就是做的编辑
            if (studentNo != "")
            {
                sql += string.Format("and  studentNo!='{0}'", studentNo);
            }
            return (int)DBHelper.GetExecuteScalar(sql) >= 1;
        }

        /// <summary>
        /// 添加学生
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        public int StudnetAdd(Model.Student student)
        {
            SqlParameter[] sqlParameters = new SqlParameter[14];
            sqlParameters[0] = new SqlParameter("@studentName", student.StudentName);
            sqlParameters[1] = new SqlParameter("@studentSex", student.StudentSex);
            sqlParameters[2] = new SqlParameter("@studentStart_year", student.StudentStart_year);
            sqlParameters[3] = new SqlParameter("@studentFinish_year", student.StudentFinish_year);
            sqlParameters[4] = new SqlParameter("@studentNation", student.StudentNation);
            sqlParameters[5] = new SqlParameter("@studentBirthday", student.StudentBirthday);
            sqlParameters[6] = new SqlParameter("@studentMajorId", student.StudentMajorId);
            sqlParameters[7] = new SqlParameter("@studentDegree", student.StudentDegree);
            sqlParameters[8] = new SqlParameter("@classId", student.ClassId);
            sqlParameters[9] = new SqlParameter("@studentImage", student.StudentImage == "" ? "default.jpg" : student.StudentImage);
            sqlParameters[10] = new SqlParameter("@studentCollegeId", student.StudentCollegeId);
            sqlParameters[11] = new SqlParameter("@studentTel", student.StudentTel == "" ? "" : student.StudentTel);
            sqlParameters[12] = new SqlParameter("@studentEmail", student.StudentEmail == "" ? "" : student.StudentEmail);
            sqlParameters[13] = new SqlParameter("@studentPwd", student.StudentPwd);
            return DBHelper.GetExecuteNonQuery("sp_student_add", sqlParameters);
        }
    }
}