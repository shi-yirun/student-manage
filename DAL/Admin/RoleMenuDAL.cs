﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace DAL.Admin
{
    public class RoleMenuDAL
    {
        /// <summary>
        /// 修改对应角色的菜单信息
        /// </summary>
        /// <param name="menuIds"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public int UpdateMenuList(List<int> menuIds, int roleId)
        {
            List<string> sqlList = new List<string>();
            string sql = "";

            //1.删除对应角色的菜单
            sql = string.Format("DELETE FROM [dbo].[Sys_rolemenu] WHERE  RoleId = {0}", roleId);
            sqlList.Add(sql);
            //2.添加对应角色的菜单
            foreach (var item in menuIds)
            {
                sql = string.Format("INSERT INTO [dbo].[Sys_rolemenu] ([RoleId],[MenuId]) VALUES({0}, {1})", roleId, item);
                sqlList.Add(sql);
            }
            return DBHelper.GetTransaction(sqlList, new SqlParameter[0]);
        }
    }
}