﻿using System.Data;

namespace DAL.Admin
{
    public class RoleDAL
    {
        /// <summary>
        /// 获取所有的角色列表
        /// </summary>
        /// <returns></returns>
        public DataTable GetRoleList()
        {
            string sql = "SELECT  [RoleId],[RoleName] FROM [StudentManager].[dbo].[Sys_role]";
            return DBHelper.GetDataTable(sql);
        }
    }
}