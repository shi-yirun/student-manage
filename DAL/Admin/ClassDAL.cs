﻿using Model;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Admin
{
    public class ClassDAL
    {
        /// <summary>
        /// 根据专业绑定班级信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetClassList(int majorId)
        {
            string sql = @"select Id,className from Sys_class where majorId= " + majorId;
            DataTable table = DBHelper.GetDataTable(sql);
            return table;
        }

        /// <summary>
        /// 绑定班级数据
        /// </summary>
        /// <returns></returns>
        public DataTable GetClassList()
        {
            string sql = @"select Sys_class.Id,className,classNum,majorName from Sys_major, Sys_class where Sys_major.id = Sys_class.majorId";
            DataTable table = DBHelper.GetDataTable(sql);
            return table;
        }

        public bool isExist(string id, string name)
        {
            string sql = string.Format(@"select count(*)
                         from Sys_class where className='{0}'", name);

            if (id != "")
            {
                sql += @" and Id!=" + id;
            }
            return (int)DBHelper.GetExecuteScalar(sql) >= 1;
        }

        public void UpdateClass(Class @class)
        {
            SqlParameter[] paras = new SqlParameter[2];
            paras[0] = new SqlParameter("@className", @class.className);
            paras[1] = new SqlParameter("@Id", @class.Id);
            DBHelper.GetExecuteNonQuery("sp_class_update", paras);
        }

        public void AddClass(Class @class)
        {
            SqlParameter[] paras = new SqlParameter[2];
            paras[0] = new SqlParameter("@className", @class.className);
            paras[1] = new SqlParameter("@majorId", @class.majorId);
            DBHelper.GetExecuteNonQuery("sp_class_add", paras);
        }

        public List<Class> GetClassList(int begin, int end, out int result, Class class1)
        {
            List<Class> classes = new List<Class>();
            string sql = @"select count(*) from Sys_major, Sys_class,Sys_college where Sys_major.id = Sys_class.majorId and Sys_major.collegeId = Sys_college.Id";

            string sqlwhere = "";
            if (class1.MajorName != "请选择专业")
            {
                sqlwhere += string.Format(" and majorName='{0}' ", class1.MajorName);
            }
            if (class1.className != "请选择班级")
            {
                sqlwhere += string.Format(" and className='{0}'", class1.className);
            }
            sql += sqlwhere;
            result = (int)DBHelper.GetExecuteScalar(sql);
            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = new SqlParameter("@begin", begin);
            parameters[1] = new SqlParameter("@end", end);
            string sql1 = string.Format(@"select Id,className,classNum,majorName,majorId,collegeName
   from(select  Sys_class.Id, className, classNum, majorName, majorId,collegeName, ROW_NUMBER()over(order by Sys_class.id)row from Sys_major, Sys_class,Sys_college where Sys_major.id = Sys_class.majorId and	Sys_major.collegeId = Sys_college.Id{0})t
      where row between @begin and @end  ", sqlwhere);
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql1, parameters);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Class @class = new Class()
                    {
                        Id = reader.GetInt32(0),
                        className = reader.GetString(1),
                        classNum = reader.GetInt32(2),
                        MajorName = reader.GetString(3),
                        majorId = reader.GetInt32(4),
                        collegeName = reader.GetString(5)
                    };
                    classes.Add(@class);
                }
            }
            return classes;
        }
    }
}