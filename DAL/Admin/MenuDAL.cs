﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Admin
{
    public class MenuDAL
    {
        /// <summary>
        /// 返回所有的父级菜单
        /// </summary>
        /// <returns></returns>
        public DataTable GetMeunList()
        {
            string sql = "select [MenuId],[MenuName] from Sys_menn where  MenuUrl is  null ";
            return DBHelper.GetDataTable(sql);
        }

        /// <summary>
        /// 添加菜单
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public int MenuAdd(Model.Menu menu)
        {
            SqlParameter[] sqlParameters = new SqlParameter[4];
            sqlParameters[0] = new SqlParameter("@MenuName", menu.MenuName);
            sqlParameters[1] = new SqlParameter("@MenuIcon", menu.MenuIcon);
            if (menu.MenuParent == 0)
            {
                sqlParameters[2] = new SqlParameter("@MenuParent", DBNull.Value);
            }
            else
            {
                sqlParameters[2] = new SqlParameter("@MenuParent", menu.MenuParent);
            }

            if (menu.MenuUrl == "")
            {
                sqlParameters[3] = new SqlParameter("@MenuUrl", DBNull.Value);
            }
            else
            {
                sqlParameters[3] = new SqlParameter("@MenuUrl", menu.MenuUrl);
            }

            return DBHelper.GetExecuteNonQuery("sp_menu_add", sqlParameters);
        }

        /// <summary>
        /// 根据角色获取对应的菜单
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public List<Model.Menu> GetMenuList(int roleId)
        {
            List<Model.Menu> meuns = new List<Model.Menu>();
            string sql = string.Format(@"select MenuId,MenuName, isnull(MenuParent,0) MenuParent from Sys_menn  where MenuId in (select MenuId from Sys_rolemenu where RoleId={0})", roleId);
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql, new SqlParameter[0]);
            if (reader.HasRows == false) return meuns;
            while (reader.Read())
            {
                Model.Menu meun = new Model.Menu()
                {
                    MenuId = reader.GetInt32(0),
                    MenuName = reader.GetString(1),
                    MenuParent = reader.GetInt32(2)
                };
                meuns.Add(meun);
            }
            return meuns;
        }

        /// <summary>
        /// 返回所有菜单信息
        /// </summary>
        /// <returns></returns>
        public List<Model.Menu> GetMenuList2()
        {
            List<Model.Menu> menus = new List<Model.Menu>();
            string sql = "select MenuId,MenuName,isnull(MenuParent,0) from Sys_menn";
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql, new SqlParameter[0]);
            if (!reader.HasRows) return menus;

            while (reader.Read())
            {
                Model.Menu menu = new Model.Menu()
                {
                    MenuId = reader.GetInt32(0),
                    MenuName = reader.GetString(1),
                    MenuParent = reader.GetInt32(2)
                };
                menus.Add(menu);
            }
            return menus;
        }

        public DataTable GetMenuList1()
        {
            string sql = @"
  select Sys_menn.MenuName, Sys_menn.MenuIcon,ISNULL(Sys_menn.MenuUrl,'')  MenuUrl,
               ISNULL( b.MenuName,'')
                     ParentName from Sys_menn
					 left join Sys_menn b
                    on Sys_menn.MenuParent=b.MenuId";
            return DBHelper.GetDataTable(sql);
        }

        public int MenuDelete(int menuid)
        {
            List<string> list = new List<string>();
            list.Add(@"DELETE FROM [dbo].[Sys_rolemenu]
      WHERE MenuId=@MenuId");
            list.Add(@"DELETE FROM [dbo].[Sys_menn]
      WHERE MenuId=@MenuId");
            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@MenuId", menuid);
            return DBHelper.GetTransaction(list, paras);
        }

        public int MenuUpdate(Model.Menu menu)
        {
            SqlParameter[] sqlParameters = new SqlParameter[5];
            sqlParameters[0] = new SqlParameter("@MenuName", menu.MenuName);
            sqlParameters[1] = new SqlParameter("@MenuIcon", menu.MenuIcon);
            if (menu.MenuParent == 0)
            {
                sqlParameters[2] = new SqlParameter("@MenuParent", DBNull.Value);
            }
            else
            {
                sqlParameters[2] = new SqlParameter("@MenuParent", menu.MenuParent);
            }

            if (menu.MenuUrl == "")
            {
                sqlParameters[3] = new SqlParameter("@MenuUrl", DBNull.Value);
            }
            else
            {
                sqlParameters[3] = new SqlParameter("@MenuUrl", menu.MenuUrl);
            }
            sqlParameters[4] = new SqlParameter("@MenuId", menu.MenuId);
            return DBHelper.GetExecuteNonQuery("sp_menu_update", sqlParameters);
        }

        /// <summary>
        /// 获取所有的菜单列表
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <param name="count"></param>
        /// <param name="menu"></param>
        /// <returns></returns>
        public object GetMeunList(int begin, int end, out int count, Model.Menu menu)
        {
            List<Model.Menu> menus = new List<Model.Menu>();
            string sql = "";
            string sqlWhere = "";
            // 判断条件
            if (menu.MenuParent != 0)
            {
                sqlWhere += " and Sys_menn.MenuParent = " + menu.MenuParent;
            }
            if (menu.MenuName != "")
            {
                sqlWhere += string.Format(" and Sys_menn.MenuName= '{0}'", menu.MenuName);
            }

            sql = $"select COUNT(*) from Sys_menn where 1 = 1 {sqlWhere}";
            count = (int)DBHelper.GetExecuteScalar(sql);
            // 获取一个DateReader 对象

            sql = $@"select * from(select Sys_menn.MenuId,Sys_menn.MenuIcon,Sys_menn.MenuName,
                     ISNULL( Sys_menn.MenuParent,0) MenuParent ,ISNULL( b.MenuName,'')
                     ParentName, ISNULL(Sys_menn.MenuUrl,'')  MenuUrl ,ROW_NUMBER()over(order by Sys_menn.MenuId) rowindex from Sys_menn
					 left join Sys_menn b
                    on Sys_menn.MenuParent=b.MenuId   where 1=1  {sqlWhere}) t
                    where rowindex between @begin and @end";

            SqlParameter[] paras = new SqlParameter[2];
            paras[0] = new SqlParameter("@begin", begin);
            paras[1] = new SqlParameter("@end", end);

            SqlDataReader reader = DBHelper.GetSqlDataReader(sql, paras);

            //判断DateReader里有值有,没有值即返回一个null
            if (!reader.HasRows)
            {
                return menus;
            }
            //有值就初始化赋值
            while (reader.Read())
            {
                Model.Menu menu1 = new Model.Menu();
                menu1.MenuId = reader.GetInt32(0);
                menu1.MenuIcon = reader.GetInt32(1);
                menu1.MenuName = reader.GetString(2);
                menu1.MenuParent = reader.GetInt32(3);
                menu1.ParentName = reader.GetString(4);
                menu1.MenuUrl = reader.GetString(5);

                //把数据加入到list
                menus.Add(menu1);
            }
            //关闭reader 对象
            reader.Close();
            return menus;
        }
    }
}