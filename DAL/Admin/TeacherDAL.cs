﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Admin
{
    public class TeacherDAL
    {
        public List<Model.Teacher> GetTeacherList(int begin, int end, out int count, Model.Teacher teacher)
        {
            string sqlWhere = "";
            string sql = @"select count(*) from Sys_teacher where 1=1";

            List<Model.Teacher> teachers = new List<Model.Teacher>();

            if (teacher.TeacherNo != "")
            {
                sqlWhere += string.Format(" and teacherNo='{0}' ", teacher.TeacherNo);
            }
            if (teacher.TeacherName != "")
            {
                sqlWhere += string.Format(" and teacherName='{0}' ", teacher.TeacherName);
            }

            sql += sqlWhere;
            count = (int)DBHelper.GetExecuteScalar(sql);
            // 获取一个DateReader 对象
            sql = string.Format(@"select [Id]
      ,[teacherNo]
      ,[teacherName]
      ,[teacherSex]
      ,[teacherStart_year]
      ,[teacherNation]
      ,[teacherBirthday]
      ,[teacherCollegeId]
      ,[teacherMajorId]
      ,[teacherDegree]
      ,[RoleId]
      ,[classId]
      ,[teacherImage]
      ,[teacherTel]
      ,[teacherEmail]
      ,[teacherPwd],collegeName,
	majorName,
	className from (SELECT
	dbo.Sys_teacher.*,  dbo.Sys_college.collegeName,
	dbo.Sys_major.majorName,
	dbo.Sys_class.className,ROW_NUMBER()over(order by Sys_teacher.id)rowindex
    FROM
	dbo.Sys_teacher
	INNER JOIN
	dbo.Sys_class
	ON
		dbo.Sys_teacher.classId = dbo.Sys_class.Id
	INNER JOIN
	dbo.Sys_college
	ON
		dbo.Sys_teacher.teacherCollegeId = dbo.Sys_college.Id
	INNER JOIN
	dbo.Sys_major
	ON
		dbo.Sys_teacher.teacherMajorId = dbo.Sys_major.Id {0} )t
              where rowindex between @begin and @end", sqlWhere);

            SqlParameter[] paras = new SqlParameter[2];
            paras[0] = new SqlParameter("@begin", begin);
            paras[1] = new SqlParameter("@end", end);

            SqlDataReader reader = DBHelper.GetSqlDataReader(sql, paras);

            //判断DateReader里有值有,没有值即返回一个null
            if (!reader.HasRows)
            {
                return teachers;
            }
            //有值就初始化赋值
            while (reader.Read())
            {
                Model.Teacher teacher1 = new Model.Teacher()
                {
                    Id = reader.GetInt32(0),
                    TeacherNo = reader.GetString(1),
                    TeacherName = reader.GetString(2),
                    TeacherSex = reader.GetString(3),
                    TeacherStart_year = reader.GetString(4),
                    TeacherNation = reader.GetString(5),
                    TeacherBirthday = reader.GetDateTime(6),
                    TeacherCollegeId = reader.GetInt32(7),
                    TeacherMajorId = reader.GetInt32(8),
                    TeacherDegree = reader.GetString(9),
                    RoleId = reader.GetInt32(10),
                    ClassId = reader.GetInt32(11),
                    TeacherImage = reader.GetString(12),
                    TeacherTel = reader.GetString(13),
                    TeacherEmail = reader.GetString(14),
                    TeacherPwd = reader.GetString(15),
                    collegeName = reader.GetString(16),
                    MajorName = reader.GetString(17),
                    className = reader.GetString(18)
                };
                teachers.Add(teacher1);
            }
            //关闭reader 对象
            reader.Close();
            return teachers;
        }

        public int DeleteTeacher(int id)
        {
            string sql = @"DELETE FROM [dbo].[Sys_score]
      WHERE  courseId in (SELECT [Id]
  FROM [StudentManager].[dbo].[Sys_course]
  where teacherId=@Id)";
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = new SqlParameter("@Id", id);
            string sql1 = @"delete
  FROM [StudentManager].[dbo].[Sys_course]
  where teacherId=@Id";
            string sql2 = @"DELETE FROM [dbo].[Sys_teacher]
      WHERE Id=@Id";
            List<string> list = new List<string>();
            list.Add(sql);
            list.Add(sql1);
            list.Add(sql2);
            return DBHelper.GetTransaction(list, parameters);
        }

        public bool IsExistTeacherTel(string teacherTel, string teacherNo)
        {
            string sql = string.Format("select COUNT(*) from Sys_teacher where  teacherTel='{0}'", teacherTel);
            // 如果工号不为空，那么就是做的编辑
            if (teacherNo != "")
            {
                sql += string.Format("and  teacherNo!='{0}'", teacherNo);
            }
            return (int)DBHelper.GetExecuteScalar(sql) >= 1;
        }

        public bool IsExistTeacherEmail(string teacherEmail, string teacherNo)
        {
            string sql = string.Format("select COUNT(*) from Sys_teacher where  teacherEmail='{0}'", teacherEmail);
            // 如果学号不为空，那么就是做的编辑
            if (teacherNo != "")
            {
                sql += string.Format("and  teacherNo!='{0}'", teacherNo);
            }
            return (int)DBHelper.GetExecuteScalar(sql) >= 1;
        }

        public DataTable GetTeacherList()
        {
            string sql = @"SELECT
	dbo.Sys_teacher.*,  dbo.Sys_college.collegeName,
	dbo.Sys_major.majorName,
	dbo.Sys_class.className

FROM
	dbo.Sys_teacher
	INNER JOIN
	dbo.Sys_class
	ON
		dbo.Sys_teacher.classId = dbo.Sys_class.Id
	INNER JOIN
	dbo.Sys_college
	ON
		dbo.Sys_teacher.teacherCollegeId = dbo.Sys_college.Id
	INNER JOIN
	dbo.Sys_major
	ON
		dbo.Sys_teacher.teacherMajorId = dbo.Sys_major.Id";
            return DBHelper.GetDataTable(sql);
        }

        public int TeacherUpdate(Model.Teacher teacher)
        {
            SqlParameter[] sqlParameters = new SqlParameter[14];
            sqlParameters[0] = new SqlParameter("@teacherName", teacher.TeacherName);
            sqlParameters[1] = new SqlParameter("@teacherSex", teacher.TeacherSex);
            sqlParameters[2] = new SqlParameter("@teacherStart_year", teacher.TeacherStart_year);
            sqlParameters[3] = new SqlParameter("@teacherPwd", teacher.TeacherPwd);
            sqlParameters[4] = new SqlParameter("@teacherNation", teacher.TeacherNation);
            sqlParameters[5] = new SqlParameter("@teacherBirthday", teacher.TeacherBirthday);
            sqlParameters[6] = new SqlParameter("@teacherMajorId", teacher.TeacherMajorId);
            sqlParameters[7] = new SqlParameter("@teacherDegree", teacher.TeacherDegree);
            sqlParameters[8] = new SqlParameter("@classId", teacher.ClassId);
            sqlParameters[9] = new SqlParameter("@teacherImage", teacher.TeacherImage == "" ? "default.jpg" : teacher.TeacherImage);
            sqlParameters[10] = new SqlParameter("@teacherCollegeId", teacher.TeacherCollegeId);
            sqlParameters[11] = new SqlParameter("@teacherTel", teacher.TeacherTel == "" ? "" : teacher.TeacherTel);
            sqlParameters[13] = new SqlParameter("@teacherNo", teacher.TeacherNo);
            sqlParameters[12] = new SqlParameter("@teacherEmail", teacher.TeacherEmail == "" ? "" : teacher.TeacherEmail);
            return DBHelper.GetExecuteNonQuery("sp_teacher_update1", sqlParameters);
        }

        public int AddTeacher(Model.Teacher teacher)
        {
            SqlParameter[] sqlParameters = new SqlParameter[13];
            sqlParameters[0] = new SqlParameter("@teacherName", teacher.TeacherName);
            sqlParameters[1] = new SqlParameter("@teacherSex", teacher.TeacherSex);
            sqlParameters[2] = new SqlParameter("@teacherStart_year", teacher.TeacherStart_year);
            sqlParameters[3] = new SqlParameter("@teacherPwd", teacher.TeacherPwd);
            sqlParameters[4] = new SqlParameter("@teacherNation", teacher.TeacherNation);
            sqlParameters[5] = new SqlParameter("@teacherBirthday", teacher.TeacherBirthday);
            sqlParameters[6] = new SqlParameter("@teacherMajorId", teacher.TeacherMajorId);
            sqlParameters[7] = new SqlParameter("@teacherDegree", teacher.TeacherDegree);
            sqlParameters[8] = new SqlParameter("@classId", teacher.ClassId);
            sqlParameters[9] = new SqlParameter("@teacherImage", teacher.TeacherImage == "" ? "default.jpg" : teacher.TeacherImage);
            sqlParameters[10] = new SqlParameter("@teacherCollegeId", teacher.TeacherCollegeId);
            sqlParameters[11] = new SqlParameter("@teacherTel", teacher.TeacherTel == "" ? "" : teacher.TeacherTel);
            sqlParameters[12] = new SqlParameter("@teacherEmail", teacher.TeacherEmail == "" ? "" : teacher.TeacherEmail);
            return
                DBHelper.GetExecuteNonQuery("sp_teacher_add", sqlParameters);
        }
    }
}