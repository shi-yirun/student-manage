﻿using Model;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Admin
{
    public class CourseDAL
    {
        public DataTable GetCourseList()
        {
            string sql = @"SELECT  [Id] ,[CourseName] FROM [StudentManager].[dbo].[Sys_course]";
            return DBHelper.GetDataTable(sql);
        }

        public List<Course> GetCourseList(int begin, int end, out int count, string cId)
        {
            List<Course> courses = new List<Course>();
            //SQL语句条件
            string sqlWhere = "";

            if (cId != "0")
            {
                sqlWhere = @" and Sys_course.Id=" + cId;
            }
            //获取行
            string sql = string.Format("SELECT count(*) FROM[dbo].[Sys_course] where 1=1{0}", sqlWhere);
            count = (int)DBHelper.GetExecuteScalar(sql);
            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = new SqlParameter("@begin", begin);
            parameters[1] = new SqlParameter("@end", end);

            string sql1 = string.Format(@"
                       select  [Id] ,[CourseName],[CourseGrade],[teacherId],[course_start_time] ,[course_end_time] ,[course_desc],teacherName from
                        (SELECT Sys_course.[Id] ,[CourseName],[CourseGrade],[teacherId],[course_start_time] ,[course_end_time] ,[course_desc],teacherName , ROW_NUMBER() over(order by Sys_course.id) row
                        FROM[dbo].[Sys_course],Sys_teacher where Sys_teacher.Id= Sys_course.teacherId{0})t
                        where row between @begin and @end", sqlWhere);
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql1, parameters);
            if (reader.HasRows)

            {
                while (reader.Read())
                {
                    Course course = new Course()
                    {
                        Id = reader.GetInt32(0),
                        CourseName = reader.GetString(1),
                        CourseGrade = reader.GetInt32(2),
                        TeacherId = reader.GetInt32(3),
                        Course_start_time = reader.GetDateTime(4),
                        Course_end_time = reader.GetDateTime(5),
                        Course_desc = reader.GetString(6),
                        teacherName = reader.GetString(7)
                    };
                    courses.Add(course);
                }
            }
            reader.Close();
            return courses;
        }

        public DataTable GetCourseList1(int id, int end1, out int count, int begin, int end)
        {
            string s = "";
            if (end1 != 0)
            {
                s = @" and  dbo.Sys_course.Id=" + end1;
            }
            string sql1 = @"select count(*) from dbo.Sys_course,Sys_teacher where CourseName not in(SELECT
  dbo.Sys_course.CourseName
FROM
	dbo.Sys_course,
	dbo.Sys_score,
	dbo.Sys_student
WHERE
	dbo.Sys_score.stuId = dbo.Sys_student.Id AND
	dbo.Sys_course.Id = dbo.Sys_score.courseId and stuId=" + id + ")  and Sys_teacher.Id=dbo.Sys_course.teacherId" + s;
            count = (int)DBHelper.GetExecuteScalar(sql1);
            string sql = @"
  select
	Id,CourseName,course_start_time,course_end_time,course_desc,teacherName from
  (select dbo.Sys_course.Id,
	dbo.Sys_course.CourseName,course_start_time,course_end_time,course_desc,teacherName,ROW_NUMBER()over(order by  dbo.Sys_course.Id)row  from dbo.Sys_course,Sys_teacher where CourseName not in(SELECT
  dbo.Sys_course.CourseName
FROM
	dbo.Sys_course,
	dbo.Sys_score,
	dbo.Sys_student
WHERE
	dbo.Sys_score.stuId = dbo.Sys_student.Id AND
	dbo.Sys_course.Id = dbo.Sys_score.courseId and stuId=" + id + ")" + " and Sys_teacher.Id=dbo.Sys_course.teacherId " + s + ")t where row between " + begin + " and " + end;
            return DBHelper.GetDataTable(sql);
        }

        public DataTable GetCourseList1(int id)
        {
            string sql = @"select dbo.Sys_course.Id,
	dbo.Sys_course.CourseName,course_start_time,course_end_time,course_desc,teacherName  from dbo.Sys_course,Sys_teacher where CourseName not in(SELECT
  dbo.Sys_course.CourseName
FROM
	dbo.Sys_course,
	dbo.Sys_score,
	dbo.Sys_student
WHERE
	dbo.Sys_score.stuId = dbo.Sys_student.Id AND
	dbo.Sys_course.Id = dbo.Sys_score.courseId and stuId=" + id + ")  and Sys_teacher.Id=dbo.Sys_course.teacherId";
            return DBHelper.GetDataTable(sql);
        }

        public DataTable GetCourseList(int id)
        {
            string sql = @"SELECT
	dbo.Sys_course.Id,
	dbo.Sys_course.CourseName
FROM
	dbo.Sys_course,
	dbo.Sys_score,
	dbo.Sys_student
WHERE
	dbo.Sys_score.stuId = dbo.Sys_student.Id AND
	dbo.Sys_course.Id = dbo.Sys_score.courseId and stuId=" + id;
            return DBHelper.GetDataTable(sql);
        }

        public int DeleteCourse(int id)
        {
            List<string> sqls = new List<string>();

            sqls.Add(@"delete
                       from Sys_course
                       where Id=@Id");
            sqls.Add(@"delete from Sys_score where courseId=@Id");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@Id", id);
            int result = DBHelper.GetTransaction(sqls, sqlParameters);
            return result;
        }

        public void UpdateCourse(Course course)
        {
            SqlParameter[] paras = new SqlParameter[7];
            paras[0] = new SqlParameter("@CourseName", course.CourseName);
            paras[1] = new SqlParameter("@CourseGrade", course.CourseGrade);
            paras[2] = new SqlParameter("@teacherId", course.TeacherId);
            paras[3] = new SqlParameter("@course_start_time", course.Course_start_time);
            paras[4] = new SqlParameter("@course_end_time", course.Course_end_time);
            paras[5] = new SqlParameter("@Id", course.Id);
            paras[6] = new SqlParameter("@course_desc", course.Course_desc);
            DBHelper.GetExecuteNonQuery("sp_course_update", paras);
        }

        public void AddCourse(Course course)
        {
            SqlParameter[] paras = new SqlParameter[6];
            paras[0] = new SqlParameter("@CourseName", course.CourseName);
            paras[1] = new SqlParameter("@CourseGrade", course.CourseGrade);
            paras[2] = new SqlParameter("@teacherId", course.TeacherId);
            paras[3] = new SqlParameter("@course_start_time", course.Course_start_time);
            paras[4] = new SqlParameter("@course_end_time", course.Course_end_time);
            paras[5] = new SqlParameter("@course_desc", course.Course_desc);
            DBHelper.GetExecuteNonQuery("sp_course_add", paras);
        }

        public bool IsExist(string couserName, string id)
        {
            string sqlWhere = "";
            // 编辑
            if (!string.IsNullOrEmpty(id))
            {
                sqlWhere = " and Id!=" + id;
            }
            string sql = string.Format(@"select count(*)from Sys_course where CourseName ='{0}' {1}", couserName, sqlWhere);
            return (int)DBHelper.GetExecuteScalar(sql) >= 1;
        }

        public DataTable GetCourseTList()
        {
            string sql = @" SELECT Sys_course.[Id] ,[CourseName],[CourseGrade],[course_start_time] ,[course_end_time] ,[course_desc],teacherName
                            FROM[dbo].[Sys_course],Sys_teacher where Sys_teacher.Id= Sys_course.teacherId";
            return DBHelper.GetDataTable(sql);
        }
    }
}