﻿using Model;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Admin
{
    public class MajorDAL
    {
        /// <summary>
        /// 绑定专业信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetMajorList(int collegeId)
        {
            string sql = @"SELECT [Id],[majorName]
                           FROM[dbo].[Sys_major] where collegeId=" + collegeId;
            return DBHelper.GetDataTable(sql);
        }

        public DataTable GetMajorList()
        {
            string sql = @"select Id,majorName from Sys_major";
            DataTable table = DBHelper.GetDataTable(sql);
            return table;
        }

        public DataTable GetMajorList(string collegeName)
        {
            string sql = string.Format(@"SELECT Sys_major.Id ,majorName
                    FROM [dbo].[Sys_major],Sys_college
                     where Sys_college.Id=Sys_major.collegeId and collegeName='{0}'", collegeName);
            DataTable table = DBHelper.GetDataTable(sql);
            return table;
        }

        /// <summary>
        /// 添加专业
        /// </summary>
        /// <param name="major"></param>
        /// <returns></returns>
        public int AddMajor(Major major)
        {
            SqlParameter[] para = new SqlParameter[2];
            para[0] = new SqlParameter("@majorName", major.MajorName);
            para[1] = new SqlParameter("@collegeId", major.collegeId);
            return DBHelper.GetExecuteNonQuery("sp_major_add", para);
        }

        public DataTable GetMajorCollegeList()
        {
            string sql = @" select Sys_major.[Id],[majorName],collegeName from [dbo].[Sys_major],Sys_college";
            return DBHelper.GetDataTable(sql);
        }

        /// <summary>
        /// 修改专业
        /// </summary>
        /// <param name="major"></param>
        /// <returns></returns>
        public int UpdateMajor(Major major)
        {
            SqlParameter[] para = new SqlParameter[3];
            para[0] = new SqlParameter("@majorName", major.MajorName);
            para[1] = new SqlParameter("@collegeId", major.collegeId);
            para[2] = new SqlParameter("@Id", major.Id);
            return DBHelper.GetExecuteNonQuery("sp_major_update", para);
        }

        /// <summary>
        /// 判断专业是否存在
        /// </summary>
        /// <param name="major"></param>
        /// <returns></returns>
        public bool isExist(Major major)
        {
            string sqlWhere = "";
            // 编辑
            if (major.Id != 0)
            {
                sqlWhere = string.Format(" and Id!={0}", major.Id);
            }
            string sql = string.Format(@"select count(*)from Sys_major where majorName ='{0}' {1}", major.MajorName, sqlWhere);
            return (int)DBHelper.GetExecuteScalar(sql) >= 1;
        }

        /// <summary>
        /// 获取专业信息
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <param name="result"></param>
        /// <param name="major"></param>
        /// <returns></returns>
        public List<Major> MajorBindTable(int begin, int end, out int result, Model.Major major)
        {
            List<Major> majors = new List<Major>();
            string sqlwhere = "";
            if (major.MajorName != "请选择专业")
            {
                sqlwhere += string.Format("  and majorName='{0}'", major.MajorName);
            }
            if (major.collegeName != "请选择学院")
            {
                sqlwhere += string.Format("  and collegeName='{0}'", major.collegeName);
            }

            string sql = string.Format(@"SELECT count(*)
                    FROM[dbo].[Sys_major],Sys_college where Sys_college.Id=Sys_major.collegeId{0}", sqlwhere);
            result = (int)DBHelper.GetExecuteScalar(sql);
            sql = string.Format(@"
                         select [Id] ,[majorName],collegeName,collegeId from
                        (SELECT Sys_major.Id ,majorName, collegeName,collegeId,ROW_NUMBER() over(order by Sys_major.id) row
                    FROM[dbo].[Sys_major],Sys_college
                    where Sys_college.Id = Sys_major.collegeId {0})t
                    where row between @begin and @end", sqlwhere);
            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = new SqlParameter("@begin", begin);
            parameters[1] = new SqlParameter("@end", end);
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql, parameters);
            if (reader.HasRows)

            {
                while (reader.Read())
                {
                    Major major1 = new Major()
                    {
                        Id = reader.GetInt32(0),

                        MajorName = reader.GetString(1),
                        collegeName = reader.GetString(2),
                        collegeId = reader.GetInt32(3)
                    };
                    majors.Add(major1);
                }
            }
            reader.Close();
            return majors;
        }
    }
}