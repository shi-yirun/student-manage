﻿using Model;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Admin
{
    public class EventDAL
    {
        public DataTable GetEventList()
        {
            string sql = @"SELECT  [Sys_evnet].[Id] ,[eventDescribe] ,[eventType],[eventResult],[eventTime] ,[studentName]
           FROM [StudentManager].[dbo].[Sys_student],Sys_evnet
           where [Sys_student].Id=[Sys_evnet].stuId
";
            return DBHelper.GetDataTable(sql);
        }

        public List<Event> GetEventList(int begin, int end, out int count, int studentName)
        {
            List<Event> events = new List<Event>();
            //SQL语句条件
            string sqlWhere = "";
            ///当值选择学院
            if (studentName != 0)
            {
                sqlWhere = string.Format("  and stuId=" + studentName);
            }
            //获取行
            string sql = string.Format("SELECT count(*) FROM[dbo].[Sys_evnet] where 1=1{0}", sqlWhere);
            count = (int)DBHelper.GetExecuteScalar(sql);
            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = new SqlParameter("@begin", begin);
            parameters[1] = new SqlParameter("@end", end);
            string sql1 = string.Format(@"
                         select * from
                        (SELECT [Sys_evnet].[Id] ,[stuId],[eventDescribe] ,[eventType],[eventResult],[eventTime] ,[studentName]
	                      , ROW_NUMBER() over(order by [Sys_evnet].id) row
                         FROM [StudentManager].[dbo].[Sys_evnet],[Sys_student]
                         where [Sys_student].Id=[Sys_evnet].stuId {0})t
                        where row between @begin and @end", sqlWhere);
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql1, parameters);
            if (reader.HasRows)

            {
                while (reader.Read())
                {
                    Event event1 = new Event()
                    {
                        Id = reader.GetInt32(0),
                        StuId = reader.GetInt32(1),
                        EventDescribe = reader.GetString(2),
                        EventType = reader.GetString(3),
                        EventResult = reader.GetString(4),
                        EventTime = reader.GetDateTime(5),
                        StudentName = reader.GetString(6)
                    };
                    events.Add(event1);
                }
            }
            reader.Close();
            return events;
        }

        public DataTable GetEventList(int begin, int end, out int count, string type, int id)
        {
            string sqlWhere = "";
            if (type != "" && type != "请选择")
            {
                sqlWhere = string.Format(@" and eventType='{0}'", type);
            }

            string sql = string.Format(@"SELECT count(*)
                         FROM [dbo].[Sys_evnet]
                         where stuId={0}{1}", id, sqlWhere);
            count = (int)DBHelper.GetExecuteScalar(sql);
            string sql1 = string.Format(@"
                         select [eventDescribe] ,[eventType],[eventResult],[eventTime] from
                        (SELECT [eventDescribe] ,[eventType],[eventResult],[eventTime]
	                      , ROW_NUMBER() over(order by [Sys_evnet].id) row
                         FROM [dbo].[Sys_evnet]
                         where stuId={3}{0})t
                        where row between {1} and {2}", sqlWhere, begin, end, id);
            return DBHelper.GetDataTable(sql1);
        }

        public int DeleteEvent(int id)

        {
            List<string> list = new List<string>();
            string sql = @"delete from Sys_evnet where Id=@id";
            list.Add(sql);
            SqlParameter[] para = new SqlParameter[1];
            para[0] = new SqlParameter("@id", id);

            return DBHelper.GetTransaction(list, para);
        }

        public void UpdateEvent(Event event1)
        {
            SqlParameter[] para = new SqlParameter[5];

            para[0] = new SqlParameter("@eventDescribe", event1.EventDescribe);
            para[1] = new SqlParameter("@eventType", event1.EventType);
            para[2] = new SqlParameter("@eventResult", event1.EventResult);
            para[3] = new SqlParameter("@eventTime", event1.EventTime);
            para[4] = new SqlParameter("@Id", event1.Id);
            DBHelper.GetExecuteNonQuery("sp_event_update", para);
        }

        public void AddEvent(Event event1)
        {
            SqlParameter[] para = new SqlParameter[5];
            para[0] = new SqlParameter("@stuId", event1.StuId);
            para[1] = new SqlParameter("@eventDescribe", event1.EventDescribe);
            para[2] = new SqlParameter("@eventType", event1.EventType);
            para[3] = new SqlParameter("@eventResult", event1.EventResult);
            para[4] = new SqlParameter("@eventTime", event1.EventTime);
            DBHelper.GetExecuteNonQuery("sp_event_add", para);
        }
    }
}