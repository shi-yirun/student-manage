﻿using Model;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Admin
{
    public class ScoreDAL
    {
        public List<Score> GetScoreList(int begin, int end, out int count, int cId, int sId)
        {
            List<Score> scores = new List<Score>();
            string sqlwhere = "";
            if (cId != 0)
            {
                sqlwhere += @" and courseId=" + cId;
            }
            if (sId != 0)
            {
                sqlwhere += @" and stuId=" + sId;
            }

            string sql = string.Format(@"SELECT count(*)
                   FROM
	dbo.Sys_course,
	dbo.Sys_score,
	dbo.Sys_student
WHERE
	dbo.Sys_score.stuId = dbo.Sys_student.Id AND
	dbo.Sys_course.Id = dbo.Sys_score.courseId{0}", sqlwhere);
            count = (int)DBHelper.GetExecuteScalar(sql);

            sql = string.Format(@"
                         select * from

    (SELECT
    Sys_score.stuId,
    Sys_score.courseId,
	Sys_student.studentName,
	Sys_course.CourseName,
	Sys_score.Grade,
	ROW_NUMBER() over(order by stuId) row
FROM
	dbo.Sys_course,
	dbo.Sys_score,
	dbo.Sys_student
WHERE
	Sys_score.stuId = Sys_student.Id AND
	Sys_course.Id = Sys_score.courseId {0})t
                    where row between @begin and @end", sqlwhere);
            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = new SqlParameter("@begin", begin);
            parameters[1] = new SqlParameter("@end", end);
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql, parameters);
            if (reader.HasRows)

            {
                while (reader.Read())
                {
                    Score score = new Score()
                    {
                        StuId = reader.GetInt32(0),
                        CourseId = reader.GetInt32(1),
                        StudentName = reader.GetString(2),
                        CourseName = reader.GetString(3),
                        Grade = reader.GetDecimal(4),
                    };
                    scores.Add(score);
                }
            }
            reader.Close();
            return scores;
        }

        public int Delete(Score score)
        {
            List<string> list = new List<string>();
            string sql = @"delete from Sys_score where stuId=@stuId and courseId=@courseId";
            list.Add(sql);
            SqlParameter[] paras = new SqlParameter[2];
            paras[0] = new SqlParameter("@stuId", score.StuId);
            paras[1] = new SqlParameter("@courseId", score.CourseId);
            return DBHelper.GetTransaction(list, paras);
        }

        public void Update(Score score)
        {
            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@stuId", score.StuId);
            paras[1] = new SqlParameter("@courseId", score.CourseId);
            paras[2] = new SqlParameter("@Grade", score.Grade);
            DBHelper.GetExecuteNonQuery("sp_score_update", paras);
        }

        public void AddScore(Score score)
        {
            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@stuId", score.StuId);
            paras[1] = new SqlParameter("@courseId", score.CourseId);
            paras[2] = new SqlParameter("@Grade", score.Grade);
            DBHelper.GetExecuteNonQuery("sp_score_add", paras);
        }

        public bool IsExist(Score score, string text)
        {
            if (text == "修改成绩信息")
            {
                string sql = string.Format(@"select COUNT(*) from
(SELECT *
  FROM [StudentManager].[dbo].[Sys_score]
  where stuId!={0} and courseId!={1}
 )t where stuId={0} and courseId={1}", score.StuId, score.CourseId);
                return (int)DBHelper.GetExecuteScalar(sql) >= 1;
            }
            else
            {
                string sql = string.Format(@"SELECT  count(*)
  FROM [StudentManager].[dbo].[Sys_score]
  where stuId={0} and courseId={1}", score.StuId, score.CourseId);
                return (int)DBHelper.GetExecuteScalar(sql) >= 1;
            }
        }

        public DataTable GetScoreList()
        {
            string sql = @"SELECT

	Sys_course.CourseName, Sys_student.studentName,
	Sys_score.Grade

FROM
	dbo.Sys_course,
	dbo.Sys_score,
	dbo.Sys_student
WHERE
	Sys_score.stuId = Sys_student.Id AND
	Sys_course.Id = Sys_score.courseId ";
            return DBHelper.GetDataTable(sql);
        }
    }
}