﻿using System.Data.SqlClient;

namespace DAL.Student
{
    public class StudentDAL
    {
        /// <summary>
        /// 通过手机或用户名或邮箱获取学生信息
        /// </summary>
        /// <param name="userName">手机或用户名或邮箱</param>
        /// <returns></returns>
        public Model.Student GetStudentInfo(string userName)
        {
            Model.Student student = new Model.Student();
            //   获取一个DateReader 对象
            string sql = @"SELECT [Id],[studentNo],[studentName],[studentSex],[studentStart_year] ,[studentFinish_year],[studentNation]
                         ,[studentBirthday],[studentCollegeId],[studentMajorId],[studentDegree],[RoleId]
                         ,[classId],[studentImage],[studentTel],[studentEmail],[studentPwd]
                          FROM [dbo].[Sys_student]
                          where [studentNo]=@studentNo or [studentEmail]=@studentEmail or [studentTel]=@studentTel";

            SqlParameter[] paras = new SqlParameter[3];
            paras[0] = new SqlParameter("@studentNo", userName);
            paras[1] = new SqlParameter("@studentEmail", userName);
            paras[2] = new SqlParameter("@studentTel", userName);
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql, paras);

            //判断DateReader里有值有,没有值即返回一个null
            if (!reader.HasRows)
            {
                return null;
            }
            //有值就初始化赋值
            while (reader.Read())
            {
                student.Id = reader.GetInt32(0);
                student.StudentNo = reader.GetString(1);
                student.StudentName = reader.GetString(2);
                student.StudentSex = reader.GetString(3);
                student.StudentStart_year = reader.GetString(4);
                student.StudentFinish_year = reader.GetString(5);
                student.StudentNation = reader.GetString(6);
                student.StudentBirthday = reader.GetDateTime(7);
                student.StudentCollegeId = reader.GetInt32(8);
                student.StudentMajorId = reader.GetInt32(9);
                student.StudentDegree = reader.GetString(10);
                student.RoleId = reader.GetInt32(11);
                student.ClassId = reader.GetInt32(12);
                student.StudentImage = reader.GetString(13);
                student.StudentTel = reader.GetString(14);
                student.StudentEmail = reader.GetString(15);
                student.StudentPwd = reader.GetString(16);
            }
            //关闭reader 对象
            reader.Close();
            return student;
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        public bool UpdateStudentPwd(Model.Student student)
        {
            string sql = "UPDATE [dbo].[Sys_student] SET [studentPwd] = @studentPwd  WHERE Id = @Id";
            SqlParameter[] parameter = new SqlParameter[2];
            parameter[0] = new SqlParameter("@studentPwd", student.StudentPwd);
            parameter[1] = new SqlParameter("@Id", student.Id);
            return DBHelper.GetExecuteNonQuery(sql, parameter, false) >= 1;
        }
    }
}