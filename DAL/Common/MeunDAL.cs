﻿using Model;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DAL.Common
{
    public class MeunDAL
    {
        public Menu GetMeunRootInfo()
        {
            Menu menu = null;
            // 使用SQLDataReader对象读取一条数据
            string sql = string.Format(@"SELECT  [MenuId],[MenuName],[MenuIcon]
                        ,isNull([MenuParent],0),isNULL([MenuUrl],'')
                         FROM[StudentManager].[dbo].[Sys_menn] where[MenuParent] is null");
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql, new SqlParameter[0]);
            //判断是否有数据
            if (reader.HasRows)
            {
                if (reader.Read())
                {
                    menu = new Menu()
                    {
                        MenuId = reader.GetInt32(0),
                        MenuName = reader.GetString(1),
                        MenuIcon = reader.GetInt32(2),
                        MenuParent = reader.GetInt32(3),
                        MenuUrl = reader.GetString(4)
                    };
                }
            }
            //关闭读写器
            reader.Close();
            return menu;
        }

        /// <summary>
        /// 根据角色Id返回所有菜单
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public List<Menu> GetMeunListInfo(int roleId)
        {
            // 要返回的List对象
            List<Menu> menus = new List<Menu>();
            string sql = string.Format(@"SELECT [MenuId],[MenuName],[MenuIcon],isNULL([MenuParent],0),isNULL([MenuUrl],'')
                                       FROM [dbo].[Sys_menn]
                                       WHERE [MenuId] in(SELECT [MenuId]FROM [dbo].[Sys_rolemenu]
                                       where [RoleId]=@RoleId)");
            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@RoleId", roleId);

            SqlDataReader reader = DBHelper.GetSqlDataReader(sql, paras);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Menu menu = new Menu()
                    {
                        MenuId = reader.GetInt32(0),
                        MenuName = reader.GetString(1),
                        MenuIcon = reader.GetInt32(2),
                        MenuParent = reader.GetInt32(3),
                        MenuUrl = reader.GetString(4)
                    };
                    // 把数据加入list集合中
                    menus.Add(menu);
                }
            }
            //关闭读写器
            reader.Close();
            return menus;
        }
    }
}