--添加管理员数据
INSERT INTO [dbo].[Sys_admin]
           ([adminAccount]
           ,[adminName]
           ,[adminSex]
           ,[adminTel]
           ,[adminEmail]
           ,[adminPwd])
     VALUES
           ('admin1','张三','男','14723507270','8535186151@qq.com','123456')
GO

INSERT INTO [dbo].[Sys_admin]
           ([adminAccount]
           ,[adminName]
           ,[adminSex]
           ,[adminTel]
           ,[adminEmail]
           ,[adminPwd])
     VALUES
           ('admin2','李四','女','18756002479','4483005242326@qq.com','123456')
GO

-- 添加学院表
INSERT INTO [dbo].[Sys_college]
           ([collegeName])
     VALUES
           ('计算机学院')
GO

INSERT INTO [dbo].[Sys_college]
           ([collegeName])
     VALUES
           ('生命科学学院')
GO

INSERT INTO [dbo].[Sys_college]
           ([collegeName])
     VALUES
           ('物理学院')
GO

INSERT INTO [dbo].[Sys_college]
           ([collegeName])
     VALUES
           ('数学科学学院')
GO

INSERT INTO [dbo].[Sys_college]
           ([collegeName])
     VALUES
           ('经济学院')
GO

INSERT INTO [dbo].[Sys_college]
           ([collegeName])
     VALUES
           ('药学院')
		   
INSERT INTO [dbo].[Sys_college]
           ([collegeName])
     VALUES
           ('软件与微电子学院')
GO

INSERT INTO [dbo].[Sys_college]
           ([collegeName])
     VALUES
           ('信息工程学院')
GO



-- 添加专业表
INSERT INTO [dbo].[Sys_major]
           ([majorName]
           ,[collegeId])
     VALUES
           ('计算机科学与技术',20)
GO
INSERT INTO [dbo].[Sys_major]
           ([majorName]
           ,[collegeId])
     VALUES
           ('软件工程',20)
GO
INSERT INTO [dbo].[Sys_major]
           ([majorName]
           ,[collegeId])
     VALUES
           ('信息安全',20)
GO

INSERT INTO [dbo].[Sys_major]
           ([majorName]
           ,[collegeId])
     VALUES
           ('生物科学',21)
GO
INSERT INTO [dbo].[Sys_major]
           ([majorName]
           ,[collegeId])
     VALUES
           ('动力学',22)
GO
INSERT INTO [dbo].[Sys_major]
           ([majorName]
           ,[collegeId])
     VALUES
           ('数学与应用数学',23)
GO
INSERT INTO [dbo].[Sys_major]
           ([majorName]
           ,[collegeId])
     VALUES
           ('经济学',24)
GO
INSERT INTO [dbo].[Sys_major]
           ([majorName]
           ,[collegeId])
     VALUES
           ('药物化学',25)
GO
INSERT INTO [dbo].[Sys_major]
           ([majorName]
           ,[collegeId])
     VALUES
           ('语言信息处理',26)
GO
INSERT INTO [dbo].[Sys_major]
           ([majorName]
           ,[collegeId])
     VALUES
           ('自动化',27)
GO

--添加班级数据
INSERT INTO [dbo].[Sys_class]
           ([className]
           ,[majorId])
     VALUES
           ('计科一班',30),  ('软件工程一班',31) , ('信息安全一班',32), ('生物科学一班',33), 
		   ('动力学一班',34), ('数学与应用数学一班',35),  ('经济学一班',36),  ('药物化学一班',37),  ('语言信息处理一班',38),('自动化一班',39)
Go

--创建触发器
if exists(select *from sysobjects where name ='trg_Teaher_Insert')
drop TRIGGER trg_Teaher_Insert
go
CREATE TRIGGER [dbo].[trg_Teaher_Insert]
on Sys_teacher
after insert
as
DECLARE   @num int ; --定义变量存储最后数字
DECLARE   @no varchar(50); -- 存储查询出来编号
DECLARE   @teacherCollegeId int; -- 存储学院编号
DECLARE   @teacherMajorId int; -- 存储专业编号
DECLARE   @Id int; -- 存储Id
DECLARE   @result  varchar(200); -- 存储结果
BEGIN
 -- 获取学院Id和专业Id,新增数据的Id
select  @teacherCollegeId=teacherCollegeId, @teacherMajorId=teacherMajorId ,@Id=Id
from  inserted; 

-- select '新增学院号'+CONVERT(varchar(20),@teacherCollegeId) +'新增'+CONVERT(varchar(20),@teacherMajorId)

-- 获取新增上一条的数据
  select @no=teacherNo  from (select teacherNo,teacherCollegeId,teacherMajorId,ROW_NUMBER()over(order by id desc)
  row_num from Sys_teacher where teacherCollegeId= @teacherCollegeId  and @teacherMajorId=teacherMajorId  )t
  where  row_num=2
  -- select '上次添加的教师号'+@no
if @no is null
begin
     set @num=100
end
else 
begin
  DECLARE @temp int;
  set @temp=CONVERT(int,SUBSTRING(@no,LEN(@no)-2,3))
  set @num= @temp+1
end	 
	-- 存储结果
	set @result= Convert(varchar(200),@teacherCollegeId)+Convert(varchar(200),@teacherMajorId)+Convert(varchar(200),@num);
	-- 去更新教师表的职工编号字段
	update  Sys_teacher  set teacherNo=@result where Id=@Id;
end
go



-- 添加教师数据
INSERT INTO [dbo].[Sys_teacher]
           (
            [teacherName]
           ,[teacherSex]
           ,[teacherBirthday]
           ,[teacherCollegeId]
           ,[teacherMajorId]
           ,[classId]
           ,[teacherTel]
           ,[teacherEmail]
           ,[teacherPwd])
     VALUES
		   ('张无忌','男','1990-07-22',21,33,13,'17106924331','4517440@qq.com','123456')
GO
INSERT INTO [dbo].[Sys_teacher]
           (
            [teacherName]
           ,[teacherSex]
           ,[teacherBirthday]
           ,[teacherCollegeId]
           ,[teacherMajorId]
           ,[classId]
           ,[teacherTel]
           ,[teacherEmail]
           ,[teacherPwd])
     VALUES
           ('是','女','1990-09-22',21,33,10,'18830837266','35018523512@qq.com','123456')
GO


-- 学生学号触发器
if exists(select *from sysobjects where name ='trg_Student_Insert')
drop TRIGGER trg_Student_Insert
go
CREATE TRIGGER [dbo].[trg_Student_Insert]
on Sys_student
after insert
as
DECLARE   @num1 int ; --定义变量存储最后数字
DECLARE   @no1 varchar(50); -- 存储查询出来编号
DECLARE   @studentCollegeId int; -- 存储学院编号
DECLARE   @studentMajorId int; -- 存储专业编号
DECLARE   @Id1 int; -- 存储Id
DECLARE   @classid int;
DECLARE   @result1  varchar(200); -- 存储结果
BEGIN
 -- 获取学院Id和专业Id,新增数据的Id
select  @studentCollegeId=studentCollegeId, @studentMajorId=studentMajorId ,@Id1=Id,@classid=classId
from  inserted; 

-- select '新增学院号'+CONVERT(varchar(20),@teacherCollegeId) +'新增'+CONVERT(varchar(20),@teacherMajorId)
-- 获取新增上一条的数据
  select @no1=studentNo  from (select studentNo,ROW_NUMBER()over(order by id desc)
  row_num from Sys_student where studentCollegeId= @studentCollegeId  and studentMajorId=@studentMajorId and @classid=classId )t
  where  row_num=2
  -- select '上次添加的教师号'+@no
if @no1 is null
begin
     set @num1=100
end
else 
begin
  DECLARE @temp1 int;
  set @temp1=CONVERT(int,SUBSTRING(@no1,LEN(@no1)-2,3))
  set @num1= @temp1+1
end	 
	-- 存储结果
	set @result1= Convert(varchar(200),@studentCollegeId)+Convert(varchar(200),@studentMajorId)+Convert(varchar(200),@classid)+Convert(varchar(200),@num1);
	-- 去更新教师表的职工编号字段
	update  Sys_student  set studentNo=@result1 where Id=@Id1;
end
go
-- 添加学生信息
INSERT INTO [dbo].[Sys_student]
           ([studentName]
           ,[studentSex]
           ,[studentCollegeId]
           ,[studentMajorId]
           ,[classId]
           ,[studentTel]
           ,[studentEmail]
           ,[studentPwd])
     VALUES
           ('史来运','男', 20 ,30,10,'18161489674','7244412@qq.com','123456')
GO
INSERT INTO [dbo].[Sys_student]
           ([studentName]
           ,[studentSex]
           ,[studentCollegeId]
           ,[studentMajorId]
           ,[classId]
           ,[studentTel]
           ,[studentEmail]
           ,[studentPwd])
     VALUES
           ('尹雨晴','女', 20 ,30,10,'17030833621','1753743@qq.com','123456')
GO










