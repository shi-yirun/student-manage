use master
go
if exists (select *from sysdatabases where name ='StudentManager')
drop database StudentManager
go
create database StudentManager
go

use StudentManager
go
--菜单表
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sys_menu]') AND type IN ('U'))
	DROP TABLE [dbo].[Sys_menu]
GO
CREATE TABLE [dbo].[Sys_menn] (
  [MenuId] bigint  NOT NULL primary key identity(1,1),
  [MenuName] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [MenuIcon] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [MenuParent] bigint  NULL
)
GO

--角色表
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sys_role]') AND type IN ('U'))
	DROP TABLE [dbo].[Sys_role]
GO
CREATE TABLE [dbo].[Sys_role] (
  [RoleId] bigint  NOT NULL primary key identity(0,1),                                --
  [RoleName] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NULL,  -- 权限角色名称 /管理员 /教师
  [RoleType] nvarchar(10) COLLATE Chinese_PRC_CI_AS  NULL,  -- admin 、student / Teacher
)
GO
insert into  Sys_role(RoleName,RoleType)values('管理员','admin')
insert into  Sys_role(RoleName,RoleType)values('教师','teacher')
insert into  Sys_role(RoleName,RoleType)values('学生','student')

select *from Sys_role


-- 角色表和菜单表的关系
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sys_rolemenu]') AND type IN ('U'))
	DROP TABLE [dbo].[Sys_rolemenu]
GO
CREATE TABLE [dbo].[Sys_rolemenu] (
  [RoleMenuId] bigint  NOT NULL  primary key  identity(1,1),
  [RoleId] bigint   references Sys_role(RoleId) not null,                  -- 角色Id
  [MenuId] bigint   references Sys_menn(MenuId) not null,                 --菜单Id
)
GO


--学院表
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sys_college]') AND type IN ('U'))
	DROP TABLE [dbo].[Sys_college]
GO
create  table Sys_college
(
Id  int   primary key  identity(20,1)  , --学院Id
collegeName varchar(200) not null, -- 学院名称
)
GO

--专业表
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sys_major]') AND type IN ('U'))
	DROP TABLE [dbo].[Sys_major]
GO
create  table Sys_major
(
Id  int  primary key  identity(30,1)  , --专业表
majorName varchar(200) not null ,  -- 专业名称
collegeId int references Sys_college(Id) --学院表
)
go

-- 班级表
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sys_class]') AND type IN ('U'))
	DROP TABLE [dbo].[Sys_class]
GO
create  table Sys_class
(
Id  int  primary key  identity(10,1)   ,     -- 班级编号        
className varchar(200) not null ,          -- 班级名称
classNum    int default(0)  , -- 班级人数
majorId int references Sys_major(Id) -- 专业表
)
GO

-- 学生表
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sys_student]') AND type IN ('U'))
	DROP TABLE [dbo].[Sys_student]
GO
CREATE TABLE Sys_student 
(
     Id bigint  primary key identity(100,1),
     studentNo  varchar(100)  ,    --'学生学号，要唯一，不能重复',
     studentName  varchar(50)  , -- '学生姓名',
     studentSex  varchar(10)  DEFAULT ('男'), -- '学生性别，如：男/女/未知',
     studentStart_year  varchar(10)  DEFAULT ('2018年') ,-- '入学年份，可以根据此字段是哪一届',
     studentFinish_year  varchar(10)  DEFAULT '2022年' ,-- '毕业年份，可以知道毕业的时间',
     studentNation  varchar(20)  DEFAULT '汉', -- '民族',
     studentBirthday  datetime  default(getdate()), -- '学生生日',
     studentCollegeId  int references Sys_college(Id) default(20)  , -- '学院',
     studentMajorId  int   references Sys_major(Id) default(30)   ,         -- '专业',
     studentDegree   varchar(10) default('本科') ,  -- '学生类型，例如：中专/大专/本科/研究生',
     RoleId bigint  references Sys_role(RoleId) default(2)  , -- 角色Id
     classId int references Sys_class(Id) ,-- 班级编号
	 studentImage varchar(200)  default('default.jpg'),-- 学生图片
	 studentTel varchar(11) check(len(studentTel)=11) default(''), -- 学生电话
	 studentEmail varchar(30) default(''), --邮箱
	 studentPwd	varchar(50) not null   -- 密码
)
go

-- 教师表
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sys_teacher]') AND type IN ('U'))
	DROP TABLE [dbo].[Sys_teacher]
GO
CREATE TABLE Sys_teacher 
(
     Id int  primary key   identity(200,1) ,                      -- Id
     teacherNo  varchar(100)   ,    --'教师学号，要唯一，不能重复',
     teacherName  varchar(50) not null , -- '教师姓名',
     teacherSex  varchar(10)  DEFAULT ('男'), -- '学生性别，如：男/女/未知',
     teacherStart_year  varchar(10)  DEFAULT ('2018年') ,-- '入职年份',
     teacherNation  varchar(20)  DEFAULT '汉', -- '民族',
     teacherBirthday  datetime  default(getdate()), -- '教师生日',
     teacherCollegeId    int references Sys_college(Id) default(20), -- '学院',
     teacherMajorId  int      references Sys_major(Id) default(30),         -- '专业',
     teacherDegree  varchar(20) default('研究生') ,  -- '教师学位，例如：中专/大专/本科/研究生',
	 RoleId int  default(1)  , -- 角色Id
     classId int references Sys_class(Id) ,-- 班级编号
	 teacherImage varchar(200)  default('default.jpg'),-- 教师图片
	 teacherTel varchar(11) check(len(teacherTel)=11) default(''), -- 电话
	 teacherEmail varchar(30) default(''), --邮箱
	 teacherPwd	varchar(50) not null --密码
)
go



-- 管理员
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sys_admin]') AND type IN ('U'))
	DROP TABLE [dbo].[Sys_admin]
GO
create Table Sys_admin
(
id  int  primary key  identity(300,1),  -- 管理员Id
adminAccount varchar(59) not null unique, -- 账号
adminName varchar(20) not null, --管理员姓名
adminSex   varchar(4) default('男'), --性别
RoleId int  default(0) , -- 角色Id
adminImage varchar(200)  default('default.jpg'),-- 管理员图片,
adminTel  varchar(11) check(len(adminTel)=11) not null unique  , -- 管理员手机号
adminEmail varchar(60) not null unique ,-- 管理员邮箱
adminPwd varchar(50)  not null  -- 管理员密码
)
go

--课程表
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sys_course]') AND type IN ('U'))
	DROP TABLE [dbo].[Sys_course]
GO
create  table Sys_course
(
Id  int primary key  identity(1000,1) ,   --- 编号
CourseName varchar(200) not null,   -- 课程名称
CourseGrade int not null, -- 课程时长
teacherId  int references Sys_teacher(Id) , -- 教师
course_start_time  datetime NULL DEFAULT '1970-01-01 00:00:00', -- '课程开始时间',
course_end_time  datetime NULL DEFAULT '2100-01-01 00:00:00', -- '课程结束时间',
course_desc  varchar(2000) default('') -- '课程描述',
)

--奖惩表
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sys_evnet]') AND type IN ('U'))
	DROP TABLE [dbo].[Sys_evnet]
GO
create  table Sys_evnet
(
Id  int  primary key  identity(2000,1),
stuId bigint  references Sys_student(Id),
eventDescribe varchar(2000) default(''), -- 事件描述 
eventType varchar(20) not null ,  -- 奖励 ，惩罚
eventResult varchar(200)  not null, -- 奖励/惩罚信息
eventTime dateTime default(getdate()) -- 事件日期
)
Go

-- 选课表
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sys_score]') AND type IN ('U'))
	DROP TABLE [dbo].[Sys_score]
GO
create  table Sys_score
(
stuId bigint  references Sys_student(Id), -- 学生Id
courseId  int references Sys_course(Id),  --课程Id
Grade  dec(5,2)  not null, -- 成绩
primary key(stuId,courseId)
)
Go
--日志表
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Sys_logs]') AND type IN ('U'))
	DROP TABLE [dbo].[Sys_logs]
GO
CREATE TABLE  Sys_logs  (
     logs_id  int  primary key, --  '日志编号',
     logs_operator  varchar(50) not null, --  '操作者的账号id',
     logs_ip  varchar(100)  default(''), --  '操作人的ip地址',
     logs_time  datetime NULL DEFAULT '2100-01-01 00:00:00',--  '操作的时间',
     logs_desc  varchar(2048) default('') --  '操作的描述',
)  
GO