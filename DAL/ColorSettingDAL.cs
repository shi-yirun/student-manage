﻿using Model;
using System.Data.SqlClient;

namespace DAL
{
    public class ColorSettingDAL
    {
        public ColorSetting GetColor(string adminAccount)
        {
            ColorSetting colorSetting = null;
            string sql = @"SELECT *
  FROM [dbo].[ColorSetting]
  where[dbo].[ColorSetting].UserAccount=@adminAccount";
            SqlParameter[] paras = new SqlParameter[1];
            paras[0] = new SqlParameter("@adminAccount", adminAccount);
            SqlDataReader reader = DBHelper.GetSqlDataReader(sql, paras);
            if (reader.HasRows)
            {
                reader.Read();
                colorSetting = new ColorSetting();
                colorSetting.UserAccount = reader.GetString(0);
                colorSetting.UserColor = reader.GetString(1);
            }
            return colorSetting;
        }

        public void UpdateColor(string account, string color)
        {
            SqlParameter[] paras = new SqlParameter[2];
            paras[0] = new SqlParameter("@UserAccount", account);
            paras[1] = new SqlParameter("@UserColor", color);
            DBHelper.GetExecuteNonQuery("sp_color_update", paras);
        }

        public void AddColor(string account, string color)
        {
            SqlParameter[] paras = new SqlParameter[2];
            paras[0] = new SqlParameter("@UserAccount", account);
            paras[1] = new SqlParameter("@UserColor", color);
            DBHelper.GetExecuteNonQuery("sp_color_add", paras);
        }
    }
}