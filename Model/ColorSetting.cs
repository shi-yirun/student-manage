﻿namespace Model
{
    public class ColorSetting
    {
        public string UserAccount { get; set; }
        public string UserColor { get; set; }
    }
}