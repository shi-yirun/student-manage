﻿namespace Model
{
    public class Major
    {
        public int Id { get; set; }
        public string MajorName { get; set; }
        public int collegeId { get; set; }
        public string collegeName { get; set; }
    }
}