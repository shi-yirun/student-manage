﻿using System;

namespace Model
{
    public class Event
    {
        public int Id { get; set; }
        public int StuId { get; set; }

        /// <summary>
        /// 事情描述
        /// </summary>
        public string EventDescribe { get; set; }

        /// <summary>
        /// 惩罚/奖励
        /// </summary>
        public string EventType { get; set; }

        /// <summary>
        /// 事件结果
        /// </summary>
        public string EventResult { get; set; }

        /// <summary>
        /// 事件发生的时间
        /// </summary>
        public DateTime EventTime { get; set; }

        public string StudentName { get; set; }
    }
}