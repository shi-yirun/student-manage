﻿using System;

namespace Model
{
    public class Course
    {
        public int Id { get; set; }
        public string CourseName { get; set; }

        public int CourseGrade { get; set; }

        public int TeacherId { get; set; }

        public DateTime Course_start_time { get; set; }
        public DateTime Course_end_time { get; set; }

        /// <summary>
        /// 课程描述
        /// </summary>
        public string Course_desc { get; set; }

        public string teacherName { get; set; }
    }
}