﻿namespace Model
{
    public class Score
    {
        /// <summary>
        /// 学生Id
        /// </summary>
        public int StuId { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public int CourseId { get; set; }

        public string StudentName { get; set; }

        public string CourseName { get; set; }

        /// <summary>
        /// 成绩
        /// </summary>
        public decimal Grade { get; set; }
    }
}