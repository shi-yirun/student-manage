﻿namespace Model
{
    public class RoleMenu
    {
        /// <summary>
        /// 关系Id
        /// </summary>
        public int RoleMenuId { get; set; }

        /// <summary>
        /// 角色Id
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        /// 菜单Id
        /// </summary>
        public int MenuId { get; set; }
    }
}