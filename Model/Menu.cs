﻿namespace Model
{
    public class Menu
    {
        public int MenuId { get; set; }
        public string MenuName { get; set; }

        public int MenuIcon { get; set; }

        public int MenuParent { get; set; }

        public string MenuUrl { get; set; }

        public string ParentName { get; set; }
    }
}