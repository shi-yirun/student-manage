﻿namespace Model
{
    public class Role
    {
        public int RoleId { get; set; }
        public int RoleName { get; set; }

        public string RoleType { get; set; }
    }
}