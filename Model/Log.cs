﻿namespace Model
{
    public class Log
    {
        public int Logs_id { get; set; }

        /// <summary>
        /// 操作者账号Id
        /// </summary>
        public int Logs_operatorId { get; set; }

        /// <summary>
        /// ip地址
        /// </summary>
        public int Logs_ip { get; set; }

        /// <summary>
        /// 时间
        /// </summary>
        public int Logs_time { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public int logs_desc { get; set; }
    }
}