﻿using System;

namespace Model
{
    public class Teacher
    {
        public int Id { get; set; }

        public string TeacherNo { get; set; }

        public string TeacherName { get; set; }

        public string TeacherSex { get; set; }

        public string TeacherStart_year { get; set; }
        public string TeacherNation { get; set; }

        public DateTime TeacherBirthday { get; set; }

        public int TeacherCollegeId { get; set; }

        public int TeacherMajorId { get; set; }

        public string TeacherDegree { get; set; }

        public int RoleId { get; set; }

        public int ClassId { get; set; }

        public string TeacherImage { get; set; }

        public string TeacherTel { get; set; }

        public string TeacherEmail { get; set; }

        public string TeacherPwd { get; set; }
        public string collegeName { get; set; }
        public string className { get; set; }
        public string MajorName { get; set; }
    }
}