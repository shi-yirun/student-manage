﻿using System;

namespace Model
{
    public class Student
    {
        public int Id { get; set; }
        public string StudentNo { get; set; }
        public string StudentName { get; set; }
        public string StudentSex { get; set; }
        public string StudentStart_year { get; set; }
        public string StudentFinish_year { get; set; }
        public string StudentNation { get; set; }
        public DateTime StudentBirthday { get; set; }
        public int StudentCollegeId { get; set; }
        public int StudentMajorId { get; set; }
        public string StudentDegree { get; set; }
        public int RoleId { get; set; }
        public int ClassId { get; set; }
        public string StudentImage { get; set; }
        public string StudentTel { get; set; }
        public string StudentEmail { get; set; }
        public string StudentPwd { get; set; }

        public string ClassName { get; set; }
        public string MajorName { get; set; }
        public string CollegeName { get; set; }
    }
}