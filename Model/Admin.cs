﻿namespace Model
{
    public class Admin
    {
        public int Id { get; set; }
        public string AdminAccount { get; set; }
        public string AdminName { get; set; }
        public string AdminSex { get; set; }
        public int RoleId { get; set; }
        public string AdminImage { get; set; }
        public string AdminTel { get; set; }
        public string AdminEmail { get; set; }
        public string AdminPwd { get; set; }
    }
}