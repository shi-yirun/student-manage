﻿using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Threading;
using System.Windows.Forms;

namespace StudentManagement
{
    public partial class FrmUpdatePwd : UIPage
    {
        public FrmUpdatePwd()
        {
            InitializeComponent();

        }
        string email = "";
        string name = "";

        private void btnSendEmailCode_Click(object sender, EventArgs e)
        {
            //检查邮箱
            if (Program.Type == "admin")
            {
                ob = Program.Admin;
                name = Program.Admin.AdminAccount;
                email = Program.Admin.AdminEmail;
                if (string.IsNullOrEmpty(Program.Admin.AdminEmail))
                {
                    Program.Aside.SelectPage(2003);
                    ShowWarningDialog("请先完善邮箱信息");
                    return;
                }
            }
            if (Program.Type == "student")
            {
                ob = Program.Student;
                email = Program.Student.StudentEmail;
                name = Program.Student.StudentNo;

                if (string.IsNullOrEmpty(Program.Student.StudentEmail))
                {
                    Program.Aside.SelectPage(2003);
                    ShowWarningDialog("请先完善邮箱信息");
                    return;
                }

            }
            if (Program.Type == "teacher")
            {
                ob = Program.Teacher;
                email = Program.Teacher.TeacherEmail;
                name = Program.Teacher.TeacherNo;
                if (string.IsNullOrEmpty(Program.Teacher.TeacherEmail))
                {
                    Program.Aside.SelectPage(2003);
                    ShowWarningDialog("请先完善邮箱信息");
                    return;
                }

            }


            //调用封装的类发送验证码
            bool flag = SendEmailCode.GetEmail(email, "修改密码", name);
            if (flag)
            {
                ShowSuccessTip("发送成功");
                //设置按钮为不可选取的状态
                btnSendEmailCode.Enabled = false;
                //设置按钮文本
                btnSendEmailCode.Text = $"({SendEmailCode.Times})后重新获取";
                //设置间隔时间1s
                timer1.Interval = 1000;
                //设置到达间隔要执行的事件
                timer1.Tick += Timer1_Tick;
                //启动计时器
                timer1.Enabled = true;

            }
            if (!flag) ShowErrorTip("发送失败，请检查网络");
        }
        private void Timer1_Tick(object sender, EventArgs e)
        {
            //如果倒计时秒数等于0
            if (SendEmailCode.Times == 0)
            {
                //停止计时器
                timer1.Enabled = false;
                //移除到达间隔要执行的事件
                timer1.Tick -= Timer1_Tick;
                //设置按钮为不可选取的状态
                btnSendEmailCode.Enabled = true;
                //设置按钮文本
                btnSendEmailCode.Text = "发送验证码";
                //重置倒计时的值
                SendEmailCode.Times = 59;

                return;
            }
            //让秒数一直递减
            --SendEmailCode.Times;
            // 不断的重新赋值
            btnSendEmailCode.Text = $"({SendEmailCode.Times})后重新获取";//可以把参数嵌入
        }

        Object ob = null;


        /// <summary>
        /// 执行修改
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool Update(object obj)
        {
            bool result;
            string pwd = MD5Helper.GetMD5Str(txtpwd.Text.Trim());
            if (obj.GetType().ToString() == "Model.Admin")
            {
                Program.Admin.AdminPwd = pwd;
                Model.Admin admin = Program.Admin;
                return result = new BLL.Admin.AdminBLL().UpdateAdmin(admin);

            }
            if (obj.GetType().ToString() == "Model.Student")
            {
                Program.Student.StudentPwd = pwd;
                Model.Student student = Program.Student;
                return result = new BLL.Student.StudentBLL().UpdateStudentPwd(student);


            }
            if (obj.GetType().ToString() == "Model.Teacher")
            {
                Program.Teacher.TeacherPwd = pwd;
                Model.Teacher teacher = Program.Teacher;
                return result = new BLL.Teacher.TeacherBLL().UpdateTeacherPwd(teacher);
            }
            return false;
        }

        /// <summary>
        /// 提交修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiButton1_Click(object sender, EventArgs e)
        {


            if (string.IsNullOrEmpty(txtNewpwd.Text) || string.IsNullOrEmpty(txtpwd.Text))
            {
                ShowWarningDialog("请输入密码");
                return;
            }

            if (string.IsNullOrEmpty(txtEmailCode.Text.Trim()))
            {
                ShowWarningDialog("验证码不能为空");
                return;
            }

            if (!txtEmailCode.Text.Trim().ToUpper().Equals(SendEmailCode.Code.ToUpper()))
            {
                ShowWarningDialog("验证码输入错误");
                return;
            }
            //使用update方法执行修改密码
            bool result = Update(ob);
            if (result == true)
            {
                ShowSuccessDialog("密码修改成功");
                ShowSuccessTip("3秒后将退出系统....");
                //禁用按钮
                btnUpdate.Enabled = false;
                //睡眠3秒
                Thread.Sleep(3000);
                //重启应用
                Application.Restart();
            }
            else
                ShowWarningDialog("修改失败");
        }



        private void txtUserEmail_Click(object sender, EventArgs e)
        {
            if (txtNewpwd.Text != txtpwd.Text)
            {
                ShowWarningDialog("两次密码不一致");
                return;
            }
        }




    }
}
