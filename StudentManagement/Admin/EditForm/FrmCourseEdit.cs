﻿using BLL.Admin;
using Model;
using Sunny.UI;
using System;
using System.Data;
using System.Windows.Forms;

namespace StudentManagement.Admin.EditForm
{
    public partial class FrmCourseEdit : UIEditForm
    {
        public FrmCourseEdit()
        {
            InitializeComponent();
        }

        private void FrmCourseEdit_Load(object sender, EventArgs e)
        {
            TeacherBind();
            ButtonOkClick += FrmCourseEdit_ButtonOkClick;
            //检查提交数据
            CheckedData += FrmCourseEdit_CheckedData;

            //接受tag对象,tag是窗体的属性，编辑时给tag赋值，窗体释放后，tag属性为空，添加时，没有给tag属性赋值
            Course course = (Course)Tag;
            if (Tag != null)
            {
                txtCourseId.Text = course.Id.ToString();
                txtCourseDode.Text = course.CourseGrade.ToString();
                txtCourseName.Text = course.CourseName;
                this.cmbTeacher.SelectedValue = course.TeacherId;
                this.cmbBegin.Value = course.Course_start_time;
                this.cmbEnd.Value = course.Course_end_time;
                this.txtDir.Text = course.Course_desc;
            }
            else
            {
                this.cmbBegin.Value = DateTime.Now;
                this.cmbEnd.Value = DateTime.Now;
            }
        }

        private void TeacherBind()
        {
            this.cmbTeacher.DisplayMember = "teacherName";
            this.cmbTeacher.ValueMember = "Id";
            DataTable table = new TeacherBLL().GetTeacherList();
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["teacherName"] = "请选择教师";
            table.Rows.InsertAt(row, 0);
            this.cmbTeacher.DataSource = table;
        }

        private bool FrmCourseEdit_CheckedData(object sender, EditFormEventArgs e)
        {
            string couserName = this.txtCourseName.Text;
            string id = this.txtCourseId.Text;
            if (string.IsNullOrEmpty(couserName))
            {
                ShowWarningDialog("课程名不能为空");
                return false;
            }
            if (string.IsNullOrEmpty(this.txtCourseDode.Text))
            {
                ShowWarningDialog("学分不能为空");
                return false;
            }
            if (this.cmbTeacher.Text == "请选择教师")
            {
                ShowWarningDialog("请选择教师");
                return false;
            }
            if (this.cmbBegin.Value >= this.cmbEnd.Value)
            {
                ShowWarningDialog("请正确选择时间");

                return false;
            }
            if (new CourseBLL().IsExist(couserName, id) == true)
            {
                ShowWarningDialog("课程已存在");
                return false;
            }
            return true;
        }

        private void FrmCourseEdit_ButtonOkClick(object sender, EventArgs e)
        {
            Course course = new Course();
            course.Id = this.txtCourseId.Text == "" ? 0 : Convert.ToInt32(this.txtCourseId.Text);
            course.CourseGrade = Convert.ToInt32(this.txtCourseDode.Text);
            course.CourseName = this.txtCourseName.Text;
            course.Course_end_time = this.cmbEnd.Value;
            course.Course_start_time = this.cmbBegin.Value;
            course.TeacherId = Convert.ToInt32(this.cmbTeacher.SelectedValue);
            course.Course_desc = this.txtDir.Text;
            if (course.Id == 0)
            {
                new CourseBLL().AddCourse(course);
                ShowSuccessDialog("添加成功");
            }
            //修改
            else
            {
                new CourseBLL().UpdateCourse(course);
                ShowSuccessDialog("修改成功");
            }
            this.DialogResult = DialogResult.Yes;
        }
    }
}