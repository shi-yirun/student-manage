﻿using BLL.Admin;
using Model;
using Sunny.UI;
using System;
using System.Data;
using System.Windows.Forms;

namespace StudentManagement.Admin.EditForm
{
    public partial class FrmMajorEdit : UIEditForm
    {
        public FrmMajorEdit()
        {
            InitializeComponent();
        }

        private void CollegeBind()
        {
            this.cmbCollege.DisplayMember = "collegeName";
            this.cmbCollege.ValueMember = "Id";
            DataTable table = new CollegeBLL().GetCollegeList();
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["collegeName"] = "请选择学院";
            table.Rows.InsertAt(row, 0);
            this.cmbCollege.DataSource = table;
        }

        private void FrmMajorEdit_Load(object sender, EventArgs e)
        {
            CollegeBind();
            Major major = (Major)Tag;
            if (Tag != null)
            {
                this.cmbCollege.SelectedValue = major.collegeId;
                this.txtMajorId.Text = major.Id.ToString();
                this.txtMajorName.Text = major.MajorName;
            }
            ButtonOkClick += FrmMajorEdit_ButtonOkClick;
            //检查提交数据
            CheckedData += FrmMajorEdit_CheckedData; ;
        }

        private bool FrmMajorEdit_CheckedData(object sender, EditFormEventArgs e)
        {
            Major major = new Major();
            major.Id = txtMajorId.Text == "" ? 0 : Convert.ToInt32(txtMajorId.Text.Trim());
            major.collegeName = this.cmbCollege.Text;
            major.MajorName = txtMajorName.Text;
            major.collegeId = (int)this.cmbCollege.SelectedValue;
            if (string.IsNullOrEmpty(major.MajorName))
            {
                ShowWarningDialog("专业名不能为空");
                return false;
            }
            if (major.collegeName == "请选择学院")
            {
                ShowWarningDialog("请选择学院");
                return false;
            }

            if (new MajorBLL().isExist(major))
            {
                ShowWarningDialog("专业已存在");
                return false;
            }

            return true;
        }

        private void FrmMajorEdit_ButtonOkClick(object sender, EventArgs e)
        {
            Major major = new Major();
            major.Id = txtMajorId.Text == "" ? 0 : Convert.ToInt32(txtMajorId.Text.Trim());
            major.collegeName = this.cmbCollege.Text;
            major.MajorName = txtMajorName.Text;
            major.collegeId = (int)this.cmbCollege.SelectedValue;
            //添加
            if (major.Id == 0)
            {
                new MajorBLL().AddMajor(major);
                ShowSuccessDialog("添加成功");
            }
            //修改
            else
            {
                new MajorBLL().UpdateMajor(major);
                ShowSuccessDialog("修改成功");
            }
            this.DialogResult = DialogResult.Yes;
        }
    }
}