﻿
namespace StudentManagement.Admin.EditForm
{
    partial class FrmEditMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.cmbMeunList = new Sunny.UI.UIComboBox();
            this.txtUrl = new Sunny.UI.UITextBox();
            this.txtIcon = new Sunny.UI.UITextBox();
            this.txtName = new Sunny.UI.UITextBox();
            this.txtId = new Sunny.UI.UITextBox();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.pnlBtm.SuspendLayout();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBtm
            // 
            this.pnlBtm.Location = new System.Drawing.Point(1, 437);
            this.pnlBtm.Size = new System.Drawing.Size(492, 54);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(364, 12);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(249, 12);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.cmbMeunList);
            this.uiGroupBox1.Controls.Add(this.txtUrl);
            this.uiGroupBox1.Controls.Add(this.txtIcon);
            this.uiGroupBox1.Controls.Add(this.txtName);
            this.uiGroupBox1.Controls.Add(this.txtId);
            this.uiGroupBox1.Controls.Add(this.uiLabel5);
            this.uiGroupBox1.Controls.Add(this.uiLabel3);
            this.uiGroupBox1.Controls.Add(this.uiLabel4);
            this.uiGroupBox1.Controls.Add(this.uiLabel2);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(1, 40);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(492, 397);
            this.uiGroupBox1.TabIndex = 2;
            this.uiGroupBox1.Text = "菜单管理";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbMeunList
            // 
            this.cmbMeunList.DataSource = null;
            this.cmbMeunList.FillColor = System.Drawing.Color.White;
            this.cmbMeunList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbMeunList.Location = new System.Drawing.Point(182, 240);
            this.cmbMeunList.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbMeunList.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbMeunList.Name = "cmbMeunList";
            this.cmbMeunList.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbMeunList.Size = new System.Drawing.Size(216, 36);
            this.cmbMeunList.TabIndex = 10;
            this.cmbMeunList.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtUrl
            // 
            this.txtUrl.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtUrl.FillColor = System.Drawing.Color.White;
            this.txtUrl.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtUrl.Location = new System.Drawing.Point(182, 303);
            this.txtUrl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUrl.Maximum = 2147483647D;
            this.txtUrl.Minimum = -2147483648D;
            this.txtUrl.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(216, 34);
            this.txtUrl.TabIndex = 9;
            this.txtUrl.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtIcon
            // 
            this.txtIcon.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtIcon.DoubleValue = 162105D;
            this.txtIcon.FillColor = System.Drawing.Color.White;
            this.txtIcon.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtIcon.IntValue = 162105;
            this.txtIcon.Location = new System.Drawing.Point(182, 169);
            this.txtIcon.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIcon.Maximum = 2147483647D;
            this.txtIcon.Minimum = -2147483648D;
            this.txtIcon.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtIcon.Name = "txtIcon";
            this.txtIcon.Size = new System.Drawing.Size(216, 34);
            this.txtIcon.Symbol = 162105;
            this.txtIcon.TabIndex = 7;
            this.txtIcon.Text = "162105";
            this.txtIcon.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtIcon.TextChanged += new System.EventHandler(this.txtIcon_TextChanged);
            // 
            // txtName
            // 
            this.txtName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtName.FillColor = System.Drawing.Color.White;
            this.txtName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtName.Location = new System.Drawing.Point(182, 102);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtName.Maximum = 2147483647D;
            this.txtName.Minimum = -2147483648D;
            this.txtName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(216, 34);
            this.txtName.TabIndex = 6;
            this.txtName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtId
            // 
            this.txtId.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtId.FillColor = System.Drawing.Color.White;
            this.txtId.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtId.Location = new System.Drawing.Point(182, 35);
            this.txtId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtId.Maximum = 2147483647D;
            this.txtId.Minimum = -2147483648D;
            this.txtId.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(216, 34);
            this.txtId.TabIndex = 5;
            this.txtId.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel5.Location = new System.Drawing.Point(36, 314);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(120, 23);
            this.uiLabel5.TabIndex = 4;
            this.uiLabel5.Text = "窗体地址：";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(36, 180);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(100, 23);
            this.uiLabel3.TabIndex = 3;
            this.uiLabel3.Text = "图标：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(36, 247);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(120, 23);
            this.uiLabel4.TabIndex = 2;
            this.uiLabel4.Text = "父级菜单：";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(36, 113);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(120, 23);
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "菜单名称：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(36, 46);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(100, 23);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "编号：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmEditMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 494);
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "FrmEditMenu";
            this.Text = "FrmEditMenu";
            this.Load += new System.EventHandler(this.FrmEditMenu_Load);
            this.Controls.SetChildIndex(this.pnlBtm, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            this.pnlBtm.ResumeLayout(false);
            this.uiGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UITextBox txtUrl;
        private Sunny.UI.UITextBox txtName;
        private Sunny.UI.UITextBox txtId;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIComboBox cmbMeunList;
        private Sunny.UI.UITextBox txtIcon;
    }
}