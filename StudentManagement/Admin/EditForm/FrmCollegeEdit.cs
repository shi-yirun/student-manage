﻿using BLL.Admin;
using Model;
using Sunny.UI;
using System;
using System.Windows.Forms;

namespace StudentManagement.Admin.EditForm
{
    public partial class FrmCollegeEdit : UIEditForm
    {
        public FrmCollegeEdit()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmCollegeEdit_Load(object sender, EventArgs e)
        {
            //给ok按钮添加一个方法
            ButtonOkClick += FrmCollegeEdit_ButtonOkClick;
            //检查提交数据
            CheckedData += FrmCollegeEdit_CheckedData;

            //接受tag对象,tag是窗体的属性，编辑时给tag赋值，窗体释放后，tag属性为空，添加时，没有给tag属性赋值
            College college = (College)Tag;
            if (Tag != null)
            {
                txtCollegeId.Text = college.Id.ToString();
                txtCollegeName.Text = college.collegeName;
            }
        }

        /// <summary>
        /// 检查数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private bool FrmCollegeEdit_CheckedData(object sender, EditFormEventArgs e)
        {
            if (string.IsNullOrEmpty(txtCollegeName.Text))
            {
                ShowWarningDialog("学院名不能为空");
                return false;
            }
            return true;
        }

        /// <summary>
        /// ok按钮的点击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmCollegeEdit_ButtonOkClick(object sender, EventArgs e)
        {
            College college = new College();
            college.Id = txtCollegeId.Text == "" ? 0 : Convert.ToInt32(txtCollegeId.Text.Trim());
            college.collegeName = txtCollegeName.Text;

            //添加
            if (college.Id == 0)
            {
                int result = new CollegeBLL().AddCollege(college);
                if (result == -1)
                {
                    ShowWarningDialog("学院已经存在了");
                }
                else
                {
                    ShowSuccessDialog("添加成功");
                    this.DialogResult = DialogResult.Yes;
                }
            }
            //修改
            else
            {
                int result = new CollegeBLL().UpdateCollege(college);
                if (result == -1)
                {
                    ShowWarningDialog("学院已存在");
                }
                else
                {
                    ShowSuccessDialog("修改成功");
                    this.DialogResult = DialogResult.Yes;
                }
            }
        }
    }
}