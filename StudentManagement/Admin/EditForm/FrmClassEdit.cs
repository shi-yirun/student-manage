﻿using BLL.Admin;
using Model;
using Sunny.UI;
using System;
using System.Data;
using System.Windows.Forms;

namespace StudentManagement.Admin.EditForm
{
    public partial class FrmClassEdit : UIEditForm
    {
        public FrmClassEdit()
        {
            InitializeComponent();
        }

        private void MajorBind()
        {
            this.cmbMajor.DisplayMember = "majorName";
            this.cmbMajor.ValueMember = "Id";
            DataTable table = new MajorBLL().GetMajorList();
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["majorName"] = "请选择专业";
            table.Rows.InsertAt(row, 0);
            this.cmbMajor.DataSource = table;
        }

        private void FrmClassEdit_Load(object sender, EventArgs e)
        {
            MajorBind();
            Class @class = (Class)Tag;
            if (Tag != null)
            {
                this.txtClassId.Text = @class.Id.ToString();
                this.txtClassName.Text = @class.className;
                this.cmbMajor.SelectedValue = @class.majorId;
            }
            ButtonOkClick += FrmClassEdit_ButtonOkClick;
            CheckedData += FrmClassEdit_CheckedData;
        }

        private bool FrmClassEdit_CheckedData(object sender, EditFormEventArgs e)
        {
            string id = this.txtClassId.Text;
            string classname = this.txtClassName.Text;

            if (string.IsNullOrEmpty(this.txtClassName.Text))
            {
                ShowWarningDialog("班级不能为空");
                return false;
            }
            if (this.cmbMajor.Text == "请选择专业")
            {
                ShowWarningDialog("专业不能为空");
                return false;
            }
            if (new ClassBLL().isExist(id, classname) == true)
            {
                ShowWarningDialog("班级已存在");
                return false;
            }
            return true;
        }

        private void FrmClassEdit_ButtonOkClick(object sender, EventArgs e)
        {
            Class @class = new Class();
            @class.Id = this.txtClassId.Text == "" ? 0 : Convert.ToInt32(this.txtClassId.Text);
            @class.className = this.txtClassName.Text;
            @class.majorId = Convert.ToInt32(this.cmbMajor.SelectedValue);
            if (string.IsNullOrEmpty(this.txtClassId.Text))
            {
                new ClassBLL().AddClass(@class);
                ShowSuccessDialog("添加成功");
            }
            else
            {
                new ClassBLL().UpdateClass(@class);
                ShowSuccessDialog("编辑成功");
            }
            this.DialogResult = DialogResult.OK;
        }
    }
}