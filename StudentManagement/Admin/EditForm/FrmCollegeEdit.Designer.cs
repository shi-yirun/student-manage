﻿
namespace StudentManagement.Admin.EditForm
{
    partial class FrmCollegeEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCollegeId = new Sunny.UI.UITextBox();
            this.uiLabel11 = new Sunny.UI.UILabel();
            this.txtCollegeName = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.uiFlowLayoutPanel1 = new Sunny.UI.UIFlowLayoutPanel();
            this.pnlBtm.SuspendLayout();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBtm
            // 
            this.pnlBtm.Controls.Add(this.uiFlowLayoutPanel1);
            this.pnlBtm.Location = new System.Drawing.Point(1, 260);
            this.pnlBtm.Size = new System.Drawing.Size(444, 55);
            this.pnlBtm.Controls.SetChildIndex(this.uiFlowLayoutPanel1, 0);
            // 
            // txtCollegeId
            // 
            this.txtCollegeId.BackColor = System.Drawing.Color.Silver;
            this.txtCollegeId.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCollegeId.FillColor = System.Drawing.Color.Silver;
            this.txtCollegeId.FillDisableColor = System.Drawing.Color.Silver;
            this.txtCollegeId.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtCollegeId.Location = new System.Drawing.Point(120, 50);
            this.txtCollegeId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCollegeId.Maximum = 2147483647D;
            this.txtCollegeId.Minimum = -2147483648D;
            this.txtCollegeId.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtCollegeId.Name = "txtCollegeId";
            this.txtCollegeId.ReadOnly = true;
            this.txtCollegeId.Size = new System.Drawing.Size(219, 34);
            this.txtCollegeId.Style = Sunny.UI.UIStyle.Custom;
            this.txtCollegeId.StyleCustomMode = true;
            this.txtCollegeId.TabIndex = 64;
            this.txtCollegeId.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel11
            // 
            this.uiLabel11.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel11.Location = new System.Drawing.Point(27, 61);
            this.uiLabel11.Name = "uiLabel11";
            this.uiLabel11.Size = new System.Drawing.Size(76, 23);
            this.uiLabel11.TabIndex = 63;
            this.uiLabel11.Text = "编号：";
            this.uiLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCollegeName
            // 
            this.txtCollegeName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCollegeName.FillColor = System.Drawing.Color.White;
            this.txtCollegeName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtCollegeName.Location = new System.Drawing.Point(120, 107);
            this.txtCollegeName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCollegeName.Maximum = 2147483647D;
            this.txtCollegeName.Minimum = -2147483648D;
            this.txtCollegeName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtCollegeName.Name = "txtCollegeName";
            this.txtCollegeName.Size = new System.Drawing.Size(219, 34);
            this.txtCollegeName.TabIndex = 62;
            this.txtCollegeName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(27, 118);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(76, 23);
            this.uiLabel1.TabIndex = 61;
            this.uiLabel1.Text = "名称：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.txtCollegeId);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Controls.Add(this.uiLabel11);
            this.uiGroupBox1.Controls.Add(this.txtCollegeName);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(30, 57);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(378, 180);
            this.uiGroupBox1.TabIndex = 65;
            this.uiGroupBox1.Text = "学院信息";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uiFlowLayoutPanel1
            // 
            this.uiFlowLayoutPanel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiFlowLayoutPanel1.Location = new System.Drawing.Point(85, 24);
            this.uiFlowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiFlowLayoutPanel1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiFlowLayoutPanel1.Name = "uiFlowLayoutPanel1";
            this.uiFlowLayoutPanel1.Padding = new System.Windows.Forms.Padding(2);
            this.uiFlowLayoutPanel1.Size = new System.Drawing.Size(8, 8);
            this.uiFlowLayoutPanel1.TabIndex = 2;
            this.uiFlowLayoutPanel1.Text = "uiFlowLayoutPanel1";
            this.uiFlowLayoutPanel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmCollegeEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 318);
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "FrmCollegeEdit";
            this.Text = "";
            this.Load += new System.EventHandler(this.FrmCollegeEdit_Load);
            this.Controls.SetChildIndex(this.pnlBtm, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            this.pnlBtm.ResumeLayout(false);
            this.uiGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UITextBox txtCollegeId;
        private Sunny.UI.UILabel uiLabel11;
        private Sunny.UI.UITextBox txtCollegeName;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UIFlowLayoutPanel uiFlowLayoutPanel1;
    }
}