﻿
namespace StudentManagement.Admin.EditForm
{
    partial class FrmEventEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.cmbName = new Sunny.UI.UIComboBox();
            this.cmbTime = new Sunny.UI.UIDatePicker();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.cmbEvent = new Sunny.UI.UIComboBox();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.txtEventR = new Sunny.UI.UITextBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.txtEventD = new Sunny.UI.UITextBox();
            this.txtEventId = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiLabel11 = new Sunny.UI.UILabel();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBtm
            // 
            this.pnlBtm.Location = new System.Drawing.Point(1, 529);
            this.pnlBtm.Size = new System.Drawing.Size(652, 55);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.cmbName);
            this.uiGroupBox1.Controls.Add(this.cmbTime);
            this.uiGroupBox1.Controls.Add(this.uiLabel5);
            this.uiGroupBox1.Controls.Add(this.cmbEvent);
            this.uiGroupBox1.Controls.Add(this.uiLabel4);
            this.uiGroupBox1.Controls.Add(this.uiLabel3);
            this.uiGroupBox1.Controls.Add(this.txtEventR);
            this.uiGroupBox1.Controls.Add(this.uiLabel2);
            this.uiGroupBox1.Controls.Add(this.txtEventD);
            this.uiGroupBox1.Controls.Add(this.txtEventId);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Controls.Add(this.uiLabel11);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(25, 60);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(573, 440);
            this.uiGroupBox1.TabIndex = 66;
            this.uiGroupBox1.Text = "奖惩信息";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbName
            // 
            this.cmbName.DataSource = null;
            this.cmbName.FillColor = System.Drawing.Color.White;
            this.cmbName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbName.Items.AddRange(new object[] {
            "奖励",
            "惩罚"});
            this.cmbName.Location = new System.Drawing.Point(151, 118);
            this.cmbName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbName.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbName.Name = "cmbName";
            this.cmbName.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbName.Size = new System.Drawing.Size(219, 36);
            this.cmbName.TabIndex = 71;
            this.cmbName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbTime
            // 
            this.cmbTime.FillColor = System.Drawing.Color.White;
            this.cmbTime.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbTime.Location = new System.Drawing.Point(151, 383);
            this.cmbTime.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbTime.MaxLength = 10;
            this.cmbTime.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbTime.Name = "cmbTime";
            this.cmbTime.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbTime.Size = new System.Drawing.Size(219, 34);
            this.cmbTime.SymbolDropDown = 61555;
            this.cmbTime.SymbolNormal = 61555;
            this.cmbTime.TabIndex = 72;
            this.cmbTime.Text = "2021-08-13";
            this.cmbTime.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmbTime.Value = new System.DateTime(2021, 8, 13, 16, 32, 0, 379);
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel5.Location = new System.Drawing.Point(16, 382);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(121, 35);
            this.uiLabel5.TabIndex = 71;
            this.uiLabel5.Text = "事件时间：";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbEvent
            // 
            this.cmbEvent.DataSource = null;
            this.cmbEvent.FillColor = System.Drawing.Color.White;
            this.cmbEvent.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbEvent.Items.AddRange(new object[] {
            "奖励",
            "惩罚"});
            this.cmbEvent.Location = new System.Drawing.Point(151, 256);
            this.cmbEvent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbEvent.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbEvent.Name = "cmbEvent";
            this.cmbEvent.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbEvent.Size = new System.Drawing.Size(219, 36);
            this.cmbEvent.TabIndex = 70;
            this.cmbEvent.Text = "请选择类型";
            this.cmbEvent.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(16, 256);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(121, 35);
            this.uiLabel4.TabIndex = 69;
            this.uiLabel4.Text = "事件类型：";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(27, 322);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(76, 23);
            this.uiLabel3.TabIndex = 67;
            this.uiLabel3.Text = "结果：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtEventR
            // 
            this.txtEventR.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEventR.FillColor = System.Drawing.Color.White;
            this.txtEventR.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtEventR.Location = new System.Drawing.Point(151, 311);
            this.txtEventR.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEventR.Maximum = 2147483647D;
            this.txtEventR.Minimum = -2147483648D;
            this.txtEventR.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtEventR.Name = "txtEventR";
            this.txtEventR.Size = new System.Drawing.Size(219, 34);
            this.txtEventR.TabIndex = 68;
            this.txtEventR.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(17, 168);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(120, 63);
            this.uiLabel2.TabIndex = 65;
            this.uiLabel2.Text = "事件描述：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtEventD
            // 
            this.txtEventD.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEventD.FillColor = System.Drawing.Color.White;
            this.txtEventD.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtEventD.Location = new System.Drawing.Point(151, 178);
            this.txtEventD.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEventD.Maximum = 2147483647D;
            this.txtEventD.Minimum = -2147483648D;
            this.txtEventD.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtEventD.Multiline = true;
            this.txtEventD.Name = "txtEventD";
            this.txtEventD.Size = new System.Drawing.Size(219, 36);
            this.txtEventD.TabIndex = 66;
            this.txtEventD.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtEventId
            // 
            this.txtEventId.BackColor = System.Drawing.Color.Silver;
            this.txtEventId.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEventId.FillColor = System.Drawing.Color.Silver;
            this.txtEventId.FillDisableColor = System.Drawing.Color.Silver;
            this.txtEventId.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtEventId.Location = new System.Drawing.Point(151, 50);
            this.txtEventId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEventId.Maximum = 2147483647D;
            this.txtEventId.Minimum = -2147483648D;
            this.txtEventId.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtEventId.Name = "txtEventId";
            this.txtEventId.ReadOnly = true;
            this.txtEventId.Size = new System.Drawing.Size(219, 34);
            this.txtEventId.Style = Sunny.UI.UIStyle.Custom;
            this.txtEventId.StyleCustomMode = true;
            this.txtEventId.TabIndex = 64;
            this.txtEventId.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(27, 118);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(76, 23);
            this.uiLabel1.TabIndex = 61;
            this.uiLabel1.Text = "姓名：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel11
            // 
            this.uiLabel11.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel11.Location = new System.Drawing.Point(27, 61);
            this.uiLabel11.Name = "uiLabel11";
            this.uiLabel11.Size = new System.Drawing.Size(76, 23);
            this.uiLabel11.TabIndex = 63;
            this.uiLabel11.Text = "编号：";
            this.uiLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmEventEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 587);
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "FrmEventEdit";
            this.Text = "FrmEventEdit";
            this.Load += new System.EventHandler(this.FrmEventEdit_Load);
            this.Controls.SetChildIndex(this.pnlBtm, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            this.uiGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UIComboBox cmbEvent;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UITextBox txtEventR;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UITextBox txtEventD;
        private Sunny.UI.UITextBox txtEventId;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel11;
        private Sunny.UI.UIDatePicker cmbTime;
        private Sunny.UI.UIComboBox cmbName;
    }
}