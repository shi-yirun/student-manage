﻿using BLL.Admin;
using Model;
using Sunny.UI;
using System;
using System.Data;
using System.Windows.Forms;

namespace StudentManagement.Admin.EditForm
{
    public partial class FrmEventEdit : UIEditForm
    {
        public FrmEventEdit()
        {
            InitializeComponent();
        }

        private void FrmEventEdit_Load(object sender, EventArgs e)
        {
            //给ok按钮添加一个方法
            ButtonOkClick += FrmEventEdit_ButtonOkClick; ;
            //检查提交数据
            CheckedData += FrmEventEdit_CheckedData; ;
            StudentBind();
            //接受tag对象,tag是窗体的属性，编辑时给tag赋值，窗体释放后，tag属性为空，添加时，没有给tag属性赋值
            Event event1 = (Event)Tag;
            if (Tag != null)
            {
                txtEventId.Text = event1.Id.ToString();
                this.cmbName.Text = event1.StudentName;
                cmbEvent.Text = event1.EventType;
                txtEventR.Text = event1.EventResult;
                txtEventD.Text = event1.EventDescribe;
                cmbTime.Value = event1.EventTime;
            }
        }

        private void StudentBind()
        {
            {
                this.cmbName.DisplayMember = "studentName";
                this.cmbName.ValueMember = "Id";
                DataTable table = new EventBLL().GetEventList();
                DataRow row = table.NewRow();
                row["Id"] = 0;
                row["studentName"] = "请选择姓名";
                table.Rows.InsertAt(row, 0);
                this.cmbName.DataSource = table;
            }
        }

        private bool FrmEventEdit_CheckedData(object sender, EditFormEventArgs e)
        {
            if (string.IsNullOrEmpty(this.cmbName.Text))
            {
                ShowWarningDialog("姓名不能为空");
                return false;
            }
            if (cmbEvent.Text == "请选择类型")
            {
                ShowWarningDialog("请选择类型");
                return false;
            }
            if (string.IsNullOrEmpty(txtEventR.Text))
            {
                ShowWarningDialog("结果不能为空");
                return false;
            }
            if (string.IsNullOrEmpty(cmbTime.Text))
            {
                ShowWarningDialog("请选择时间");
                return false;
            }
            return true;
        }

        private void FrmEventEdit_ButtonOkClick(object sender, EventArgs e)
        {
            Event event1 = new Event();
            event1.Id = this.txtEventId.Text == "" ? 0 : Convert.ToInt32(this.txtEventId.Text);
            event1.StudentName = this.cmbName.Text;
            event1.EventDescribe = this.txtEventD.Text;
            event1.EventResult = this.txtEventR.Text;
            event1.EventType = this.cmbEvent.Text;
            event1.EventTime = this.cmbTime.Value;
            event1.StuId = (int)this.cmbName.SelectedValue;
            if (event1.Id == 0)
            {
                new EventBLL().AddEvent(event1);
                ShowSuccessDialog("添加成功");
            }
            //修改
            else
            {
                new EventBLL().UpdateEvent(event1);
                ShowSuccessDialog("修改成功");
            }
            this.DialogResult = DialogResult.Yes;
        }
    }
}