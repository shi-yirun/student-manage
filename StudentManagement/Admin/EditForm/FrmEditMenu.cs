﻿using BLL.Admin;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Data;
using System.Windows.Forms;

namespace StudentManagement.Admin.EditForm
{
    public partial class FrmEditMenu : UIEditForm
    {
        public FrmEditMenu()
        {
            InitializeComponent();
        }

        private void FrmEditMenu_Load(object sender, EventArgs e)
        {
            //绑定下拉框
            BindCmbMenn();
            //检查数据
            CheckedData += FrmEditMenu_CheckedData;
            //点击Ok的事件
            ButtonOkClick += FrmEditMenu_ButtonOkClick;
            if (Tag != null)
            {
                Model.Menu menu = (Model.Menu)Tag;
                //为窗体赋值
                txtIcon.Text = menu.MenuIcon.ToString();
                txtId.Text = menu.MenuId.ToString();
                txtName.Text = menu.MenuName;
                txtUrl.Text = menu.MenuUrl;
                cmbMeunList.SelectedValue = menu.MenuParent;
            }
        }

        /// <summary>
        /// ok按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmEditMenu_ButtonOkClick(object sender, EventArgs e)
        {
            // 获取菜单按钮的值，根据值去区分添加/编辑
            Model.Menu menu = new Model.Menu();
            menu.MenuId = txtId.Text == "" ? 0 : Convert.ToInt32(txtId.Text);
            menu.MenuName = txtName.Text;
            menu.MenuIcon = Convert.ToInt32(txtIcon.Text);
            menu.MenuParent = (int)cmbMeunList.SelectedValue;
            menu.MenuUrl = txtUrl.Text;

            //添加
            if (menu.MenuId == 0)
            {
                int result = new BLL.Admin.MenuBLL().MenuAdd(menu);

                if (result >= 1)
                {
                    ShowSuccessDialog("添加成功");
                    //传递一个字符串告诉学生管理窗体，添加成功了
                    this.TagString = "添加成功";
                }
                else
                {
                    ShowSuccessDialog("添加失败");
                    this.TagString = "添加失败";
                }
            }
            else
            {
                int result = new BLL.Admin.MenuBLL().MenuUpdate(menu);
                if (result >= 1)
                {
                    ShowSuccessDialog("修改成功");

                    this.TagString = "修改成功";
                }
                else
                {
                    ShowSuccessDialog("修改失败");
                    this.TagString = "修改失败";
                }
            }

            this.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// 检查数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private bool FrmEditMenu_CheckedData(object sender, EditFormEventArgs e)
        {
            if (txtName.Text == "")
            {
                ShowWarningDialog("菜单名称不能为空");
                return false;
            }
            if (txtIcon.Text == "")
            {
                ShowWarningDialog("图标不能为空");
                return false;
            }
            if (!RegexHelper.IsNumber(txtIcon.Text))
            {
                ShowWarningDialog("请输入正整数");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 绑定下拉框
        /// </summary>
        private void BindCmbMenn()
        {
            this.cmbMeunList.DisplayMember = "MenuName";
            this.cmbMeunList.ValueMember = "MenuId";
            DataTable table = new MenuBLL().GetMeunList();
            DataRow row = table.NewRow();
            row["MenuId"] = 0;
            row["MenuName"] = "请选择父级菜单";
            table.Rows.InsertAt(row, 0);
            this.cmbMeunList.DataSource = table;
            this.cmbMeunList.SelectedIndex = 0;
        }

        /// <summary>
        /// 文本改变事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIcon_TextChanged(object sender, EventArgs e)
        {
            if (txtIcon.Text == "")
            {
                txtIcon.Symbol = 110;
                return;
            }
            if (Convert.ToDouble(txtIcon.Text) >= 200000 || Convert.ToDouble(txtIcon.Text) <= 100)
            {
                txtIcon.Symbol = 110;
                return;
            }
            txtIcon.Symbol = Convert.ToInt32(txtIcon.Text);
        }
    }
}