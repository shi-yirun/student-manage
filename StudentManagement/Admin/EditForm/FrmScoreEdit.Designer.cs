﻿
namespace StudentManagement.Admin.EditForm
{
    partial class FrmScoreEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbStudent = new Sunny.UI.UIComboBox();
            this.txtGrade = new Sunny.UI.UITextBox();
            this.cmbCourse = new Sunny.UI.UIComboBox();
            this.uiLabel12 = new Sunny.UI.UILabel();
            this.uiLabel6 = new Sunny.UI.UILabel();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.SuspendLayout();
            // 
            // pnlBtm
            // 
            this.pnlBtm.Location = new System.Drawing.Point(1, 335);
            this.pnlBtm.Size = new System.Drawing.Size(524, 55);
            // 
            // cmbStudent
            // 
            this.cmbStudent.DataSource = null;
            this.cmbStudent.FillColor = System.Drawing.Color.White;
            this.cmbStudent.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbStudent.Items.AddRange(new object[] {
            "汉族",
            "蒙古族",
            "满族",
            "朝鲜族",
            "赫哲族",
            "达斡尔族",
            "鄂温克族",
            "鄂伦春族",
            "回族",
            "东乡族",
            "土族",
            "撒拉族",
            "保安族",
            "裕固族",
            "维吾尔族",
            "哈萨克族",
            "柯尔克孜族",
            "锡伯族",
            "塔吉克族",
            "乌孜别克族",
            "俄罗斯族",
            "塔塔尔族",
            "藏族",
            "门巴族",
            "珞巴族",
            "羌族",
            "彝族",
            "白族",
            "哈尼族",
            "傣族",
            "僳僳族",
            "佤族",
            "拉祜族",
            "纳西族",
            "景颇族",
            "布朗族",
            "阿昌族",
            "普米族",
            "怒族",
            "德昂族",
            "独龙族",
            "基诺族",
            "苗族",
            "布依族",
            "侗族",
            "水族",
            "仡佬族",
            "壮族",
            "瑶族",
            "仫佬族",
            "毛南族",
            "京族",
            "土家族",
            "黎族",
            "畲族",
            "高山族"});
            this.cmbStudent.Location = new System.Drawing.Point(163, 63);
            this.cmbStudent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbStudent.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbStudent.Name = "cmbStudent";
            this.cmbStudent.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbStudent.Size = new System.Drawing.Size(219, 29);
            this.cmbStudent.TabIndex = 67;
            this.cmbStudent.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
      
            // 
            // txtGrade
            // 
            this.txtGrade.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtGrade.FillColor = System.Drawing.Color.White;
            this.txtGrade.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtGrade.Location = new System.Drawing.Point(163, 193);
            this.txtGrade.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtGrade.Maximum = 2147483647D;
            this.txtGrade.Minimum = -2147483648D;
            this.txtGrade.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtGrade.Name = "txtGrade";
            this.txtGrade.Size = new System.Drawing.Size(219, 34);
            this.txtGrade.TabIndex = 66;
            this.txtGrade.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbCourse
            // 
            this.cmbCourse.DataSource = null;
            this.cmbCourse.FillColor = System.Drawing.Color.White;
            this.cmbCourse.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbCourse.Location = new System.Drawing.Point(163, 119);
            this.cmbCourse.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCourse.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbCourse.Name = "cmbCourse";
            this.cmbCourse.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbCourse.Size = new System.Drawing.Size(219, 29);
            this.cmbCourse.TabIndex = 65;
            this.cmbCourse.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel12
            // 
            this.uiLabel12.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel12.Location = new System.Drawing.Point(93, 198);
            this.uiLabel12.Name = "uiLabel12";
            this.uiLabel12.Size = new System.Drawing.Size(76, 23);
            this.uiLabel12.TabIndex = 64;
            this.uiLabel12.Text = "成绩:";
            this.uiLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel6
            // 
            this.uiLabel6.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel6.Location = new System.Drawing.Point(93, 126);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Size = new System.Drawing.Size(76, 23);
            this.uiLabel6.TabIndex = 63;
            this.uiLabel6.Text = "课程：";
            this.uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(93, 69);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(76, 23);
            this.uiLabel4.TabIndex = 62;
            this.uiLabel4.Text = "姓名：";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmScoreEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 393);
            this.Controls.Add(this.cmbStudent);
            this.Controls.Add(this.txtGrade);
            this.Controls.Add(this.cmbCourse);
            this.Controls.Add(this.uiLabel12);
            this.Controls.Add(this.uiLabel6);
            this.Controls.Add(this.uiLabel4);
            this.Name = "FrmScoreEdit";
            this.Text = "FrmScore";
            this.Load += new System.EventHandler(this.FrmScoreEdit_Load);
            this.Controls.SetChildIndex(this.pnlBtm, 0);
            this.Controls.SetChildIndex(this.uiLabel4, 0);
            this.Controls.SetChildIndex(this.uiLabel6, 0);
            this.Controls.SetChildIndex(this.uiLabel12, 0);
            this.Controls.SetChildIndex(this.cmbCourse, 0);
            this.Controls.SetChildIndex(this.txtGrade, 0);
            this.Controls.SetChildIndex(this.cmbStudent, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIComboBox cmbStudent;
        private Sunny.UI.UITextBox txtGrade;
        private Sunny.UI.UIComboBox cmbCourse;
        private Sunny.UI.UILabel uiLabel12;
        private Sunny.UI.UILabel uiLabel6;
        private Sunny.UI.UILabel uiLabel4;
    }
}