﻿
namespace StudentManagement.Admin.EditForm
{
    partial class FrmCourseEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.txtDir = new Sunny.UI.UITextBox();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.uiLabel6 = new Sunny.UI.UILabel();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.cmbBegin = new Sunny.UI.UIDatePicker();
            this.cmbEnd = new Sunny.UI.UIDatePicker();
            this.cmbTeacher = new Sunny.UI.UIComboBox();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.txtCourseDode = new Sunny.UI.UITextBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.txtCourseId = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiLabel11 = new Sunny.UI.UILabel();
            this.txtCourseName = new Sunny.UI.UITextBox();
            this.pnlBtm.SuspendLayout();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBtm
            // 
            this.pnlBtm.Location = new System.Drawing.Point(1, 699);
            this.pnlBtm.Size = new System.Drawing.Size(580, 55);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(452, 12);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(337, 12);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.txtDir);
            this.uiGroupBox1.Controls.Add(this.uiLabel4);
            this.uiGroupBox1.Controls.Add(this.uiLabel6);
            this.uiGroupBox1.Controls.Add(this.uiLabel5);
            this.uiGroupBox1.Controls.Add(this.cmbBegin);
            this.uiGroupBox1.Controls.Add(this.cmbEnd);
            this.uiGroupBox1.Controls.Add(this.cmbTeacher);
            this.uiGroupBox1.Controls.Add(this.uiLabel3);
            this.uiGroupBox1.Controls.Add(this.txtCourseDode);
            this.uiGroupBox1.Controls.Add(this.uiLabel2);
            this.uiGroupBox1.Controls.Add(this.txtCourseId);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Controls.Add(this.uiLabel11);
            this.uiGroupBox1.Controls.Add(this.txtCourseName);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(17, 64);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(522, 608);
            this.uiGroupBox1.TabIndex = 66;
            this.uiGroupBox1.Text = "课程信息";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDir
            // 
            this.txtDir.AutoSize = true;
            this.txtDir.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtDir.FillColor = System.Drawing.Color.White;
            this.txtDir.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtDir.Location = new System.Drawing.Point(148, 454);
            this.txtDir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDir.Maximum = 2147483647D;
            this.txtDir.Minimum = -2147483648D;
            this.txtDir.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtDir.Multiline = true;
            this.txtDir.Name = "txtDir";
            this.txtDir.Size = new System.Drawing.Size(348, 120);
            this.txtDir.TabIndex = 73;
            this.txtDir.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(25, 454);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(125, 23);
            this.uiLabel4.TabIndex = 72;
            this.uiLabel4.Text = "课程描述：";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel6
            // 
            this.uiLabel6.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel6.Location = new System.Drawing.Point(25, 376);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Size = new System.Drawing.Size(125, 23);
            this.uiLabel6.TabIndex = 71;
            this.uiLabel6.Text = "结束时间：";
            this.uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel5.Location = new System.Drawing.Point(35, 312);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(125, 23);
            this.uiLabel5.TabIndex = 70;
            this.uiLabel5.Text = "开始时间：";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbBegin
            // 
            this.cmbBegin.FillColor = System.Drawing.Color.White;
            this.cmbBegin.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbBegin.Location = new System.Drawing.Point(167, 303);
            this.cmbBegin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbBegin.MaxLength = 10;
            this.cmbBegin.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbBegin.Name = "cmbBegin";
            this.cmbBegin.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbBegin.Size = new System.Drawing.Size(219, 34);
            this.cmbBegin.SymbolDropDown = 61555;
            this.cmbBegin.SymbolNormal = 61555;
            this.cmbBegin.TabIndex = 67;
            this.cmbBegin.Text = "2021-08-13";
            this.cmbBegin.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmbBegin.Value = new System.DateTime(2021, 8, 13, 16, 32, 0, 379);
            // 
            // cmbEnd
            // 
            this.cmbEnd.FillColor = System.Drawing.Color.White;
            this.cmbEnd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbEnd.Location = new System.Drawing.Point(167, 365);
            this.cmbEnd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbEnd.MaxLength = 10;
            this.cmbEnd.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbEnd.Name = "cmbEnd";
            this.cmbEnd.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbEnd.Size = new System.Drawing.Size(219, 34);
            this.cmbEnd.SymbolDropDown = 61555;
            this.cmbEnd.SymbolNormal = 61555;
            this.cmbEnd.TabIndex = 56;
            this.cmbEnd.Text = "2021-08-13";
            this.cmbEnd.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmbEnd.Value = new System.DateTime(2021, 8, 13, 16, 32, 0, 379);
            // 
            // cmbTeacher
            // 
            this.cmbTeacher.DataSource = null;
            this.cmbTeacher.FillColor = System.Drawing.Color.White;
            this.cmbTeacher.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbTeacher.Items.AddRange(new object[] {
            "汉族",
            "蒙古族",
            "满族",
            "朝鲜族",
            "赫哲族",
            "达斡尔族",
            "鄂温克族",
            "鄂伦春族",
            "回族",
            "东乡族",
            "土族",
            "撒拉族",
            "保安族",
            "裕固族",
            "维吾尔族",
            "哈萨克族",
            "柯尔克孜族",
            "锡伯族",
            "塔吉克族",
            "乌孜别克族",
            "俄罗斯族",
            "塔塔尔族",
            "藏族",
            "门巴族",
            "珞巴族",
            "羌族",
            "彝族",
            "白族",
            "哈尼族",
            "傣族",
            "僳僳族",
            "佤族",
            "拉祜族",
            "纳西族",
            "景颇族",
            "布朗族",
            "阿昌族",
            "普米族",
            "怒族",
            "德昂族",
            "独龙族",
            "基诺族",
            "苗族",
            "布依族",
            "侗族",
            "水族",
            "仡佬族",
            "壮族",
            "瑶族",
            "仫佬族",
            "毛南族",
            "京族",
            "土家族",
            "黎族",
            "畲族",
            "高山族"});
            this.cmbTeacher.Location = new System.Drawing.Point(167, 248);
            this.cmbTeacher.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbTeacher.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbTeacher.Name = "cmbTeacher";
            this.cmbTeacher.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbTeacher.Size = new System.Drawing.Size(219, 34);
            this.cmbTeacher.TabIndex = 68;
            this.cmbTeacher.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(74, 259);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(76, 23);
            this.uiLabel3.TabIndex = 67;
            this.uiLabel3.Text = "教师：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCourseDode
            // 
            this.txtCourseDode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCourseDode.FillColor = System.Drawing.Color.White;
            this.txtCourseDode.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtCourseDode.Location = new System.Drawing.Point(167, 188);
            this.txtCourseDode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCourseDode.Maximum = 2147483647D;
            this.txtCourseDode.Minimum = -2147483648D;
            this.txtCourseDode.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtCourseDode.Name = "txtCourseDode";
            this.txtCourseDode.Size = new System.Drawing.Size(219, 34);
            this.txtCourseDode.TabIndex = 66;
            this.txtCourseDode.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(74, 196);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(76, 23);
            this.uiLabel2.TabIndex = 65;
            this.uiLabel2.Text = "学分：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCourseId
            // 
            this.txtCourseId.BackColor = System.Drawing.Color.Silver;
            this.txtCourseId.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCourseId.FillColor = System.Drawing.Color.Silver;
            this.txtCourseId.FillDisableColor = System.Drawing.Color.Silver;
            this.txtCourseId.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtCourseId.Location = new System.Drawing.Point(167, 68);
            this.txtCourseId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCourseId.Maximum = 2147483647D;
            this.txtCourseId.Minimum = -2147483648D;
            this.txtCourseId.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtCourseId.Name = "txtCourseId";
            this.txtCourseId.ReadOnly = true;
            this.txtCourseId.Size = new System.Drawing.Size(219, 34);
            this.txtCourseId.Style = Sunny.UI.UIStyle.Custom;
            this.txtCourseId.StyleCustomMode = true;
            this.txtCourseId.TabIndex = 64;
            this.txtCourseId.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(74, 133);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(76, 23);
            this.uiLabel1.TabIndex = 61;
            this.uiLabel1.Text = "课程：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel11
            // 
            this.uiLabel11.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel11.Location = new System.Drawing.Point(74, 76);
            this.uiLabel11.Name = "uiLabel11";
            this.uiLabel11.Size = new System.Drawing.Size(76, 23);
            this.uiLabel11.TabIndex = 63;
            this.uiLabel11.Text = "编号：";
            this.uiLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCourseName
            // 
            this.txtCourseName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCourseName.FillColor = System.Drawing.Color.White;
            this.txtCourseName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtCourseName.Location = new System.Drawing.Point(167, 128);
            this.txtCourseName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCourseName.Maximum = 2147483647D;
            this.txtCourseName.Minimum = -2147483648D;
            this.txtCourseName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtCourseName.Name = "txtCourseName";
            this.txtCourseName.Size = new System.Drawing.Size(219, 34);
            this.txtCourseName.TabIndex = 62;
            this.txtCourseName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmCourseEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 757);
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "FrmCourseEdit";
            this.Text = "FrmCourseEdit";
            this.Load += new System.EventHandler(this.FrmCourseEdit_Load);
            this.Controls.SetChildIndex(this.pnlBtm, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            this.pnlBtm.ResumeLayout(false);
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UITextBox txtCourseId;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel11;
        private Sunny.UI.UITextBox txtCourseName;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UITextBox txtCourseDode;
        private Sunny.UI.UIComboBox cmbTeacher;
        private Sunny.UI.UILabel uiLabel6;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UIDatePicker cmbBegin;
        private Sunny.UI.UIDatePicker cmbEnd;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UITextBox txtDir;
    }
}