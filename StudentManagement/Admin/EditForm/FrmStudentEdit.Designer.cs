﻿
namespace StudentManagement.Admin.EditForm
{
    partial class FrmStudentEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.cmbNation = new Sunny.UI.UIComboBox();
            this.txtStudentNo = new Sunny.UI.UITextBox();
            this.uiLabel11 = new Sunny.UI.UILabel();
            this.uiGroupBox2 = new Sunny.UI.UIGroupBox();
            this.userImage = new System.Windows.Forms.PictureBox();
            this.btnFileUpload = new Sunny.UI.UISymbolButton();
            this.cmbEndYear = new Sunny.UI.UIComboboxEx();
            this.cmbStartYear = new Sunny.UI.UIComboboxEx();
            this.cmbDate = new Sunny.UI.UIDatePicker();
            this.txtPwd = new Sunny.UI.UITextBox();
            this.txtEmail = new Sunny.UI.UITextBox();
            this.txtTel = new Sunny.UI.UITextBox();
            this.cmbClass = new Sunny.UI.UIComboBox();
            this.cmbDegree = new Sunny.UI.UIComboBox();
            this.cmbMajor = new Sunny.UI.UIComboBox();
            this.cmbCollege = new Sunny.UI.UIComboBox();
            this.radbGirl = new Sunny.UI.UIRadioButton();
            this.radbBoy = new Sunny.UI.UIRadioButton();
            this.uiLabel14 = new Sunny.UI.UILabel();
            this.uiLabel13 = new Sunny.UI.UILabel();
            this.uiLabel12 = new Sunny.UI.UILabel();
            this.uiLabel10 = new Sunny.UI.UILabel();
            this.uiLabel9 = new Sunny.UI.UILabel();
            this.uiLabel8 = new Sunny.UI.UILabel();
            this.uiLabel6 = new Sunny.UI.UILabel();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel7 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.txtName = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.pnlBtm.SuspendLayout();
            this.uiGroupBox1.SuspendLayout();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userImage)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlBtm
            // 
            this.pnlBtm.Location = new System.Drawing.Point(1, 737);
            this.pnlBtm.Size = new System.Drawing.Size(811, 55);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(683, 12);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(568, 12);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.cmbNation);
            this.uiGroupBox1.Controls.Add(this.txtStudentNo);
            this.uiGroupBox1.Controls.Add(this.uiLabel11);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.cmbEndYear);
            this.uiGroupBox1.Controls.Add(this.cmbStartYear);
            this.uiGroupBox1.Controls.Add(this.cmbDate);
            this.uiGroupBox1.Controls.Add(this.txtPwd);
            this.uiGroupBox1.Controls.Add(this.txtEmail);
            this.uiGroupBox1.Controls.Add(this.txtTel);
            this.uiGroupBox1.Controls.Add(this.cmbClass);
            this.uiGroupBox1.Controls.Add(this.cmbDegree);
            this.uiGroupBox1.Controls.Add(this.cmbMajor);
            this.uiGroupBox1.Controls.Add(this.cmbCollege);
            this.uiGroupBox1.Controls.Add(this.radbGirl);
            this.uiGroupBox1.Controls.Add(this.radbBoy);
            this.uiGroupBox1.Controls.Add(this.uiLabel14);
            this.uiGroupBox1.Controls.Add(this.uiLabel13);
            this.uiGroupBox1.Controls.Add(this.uiLabel12);
            this.uiGroupBox1.Controls.Add(this.uiLabel10);
            this.uiGroupBox1.Controls.Add(this.uiLabel9);
            this.uiGroupBox1.Controls.Add(this.uiLabel8);
            this.uiGroupBox1.Controls.Add(this.uiLabel6);
            this.uiGroupBox1.Controls.Add(this.uiLabel5);
            this.uiGroupBox1.Controls.Add(this.uiLabel4);
            this.uiGroupBox1.Controls.Add(this.uiLabel3);
            this.uiGroupBox1.Controls.Add(this.uiLabel7);
            this.uiGroupBox1.Controls.Add(this.uiLabel2);
            this.uiGroupBox1.Controls.Add(this.txtName);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(14, 55);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(781, 672);
            this.uiGroupBox1.TabIndex = 2;
            this.uiGroupBox1.Text = "个人信息";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbNation
            // 
            this.cmbNation.DataSource = null;
            this.cmbNation.FillColor = System.Drawing.Color.White;
            this.cmbNation.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbNation.Items.AddRange(new object[] {
            "汉族",
            "蒙古族",
            "满族",
            "朝鲜族",
            "赫哲族",
            "达斡尔族",
            "鄂温克族",
            "鄂伦春族",
            "回族",
            "东乡族",
            "土族",
            "撒拉族",
            "保安族",
            "裕固族",
            "维吾尔族",
            "哈萨克族",
            "柯尔克孜族",
            "锡伯族",
            "塔吉克族",
            "乌孜别克族",
            "俄罗斯族",
            "塔塔尔族",
            "藏族",
            "门巴族",
            "珞巴族",
            "羌族",
            "彝族",
            "白族",
            "哈尼族",
            "傣族",
            "僳僳族",
            "佤族",
            "拉祜族",
            "纳西族",
            "景颇族",
            "布朗族",
            "阿昌族",
            "普米族",
            "怒族",
            "德昂族",
            "独龙族",
            "基诺族",
            "苗族",
            "布依族",
            "侗族",
            "水族",
            "仡佬族",
            "壮族",
            "瑶族",
            "仫佬族",
            "毛南族",
            "京族",
            "土家族",
            "黎族",
            "畲族",
            "高山族"});
            this.cmbNation.Location = new System.Drawing.Point(87, 217);
            this.cmbNation.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbNation.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbNation.Name = "cmbNation";
            this.cmbNation.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbNation.Size = new System.Drawing.Size(219, 29);
            this.cmbNation.TabIndex = 61;
            this.cmbNation.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtStudentNo
            // 
            this.txtStudentNo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtStudentNo.FillColor = System.Drawing.Color.White;
            this.txtStudentNo.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtStudentNo.Location = new System.Drawing.Point(87, 41);
            this.txtStudentNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtStudentNo.Maximum = 2147483647D;
            this.txtStudentNo.Minimum = -2147483648D;
            this.txtStudentNo.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtStudentNo.Name = "txtStudentNo";
            this.txtStudentNo.ReadOnly = true;
            this.txtStudentNo.Size = new System.Drawing.Size(219, 34);
            this.txtStudentNo.TabIndex = 60;
            this.txtStudentNo.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel11
            // 
            this.uiLabel11.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel11.Location = new System.Drawing.Point(17, 52);
            this.uiLabel11.Name = "uiLabel11";
            this.uiLabel11.Size = new System.Drawing.Size(76, 23);
            this.uiLabel11.TabIndex = 59;
            this.uiLabel11.Text = "学号：";
            this.uiLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Controls.Add(this.userImage);
            this.uiGroupBox2.Controls.Add(this.btnFileUpload);
            this.uiGroupBox2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox2.Location = new System.Drawing.Point(392, 41);
            this.uiGroupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox2.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox2.Size = new System.Drawing.Size(365, 345);
            this.uiGroupBox2.TabIndex = 58;
            this.uiGroupBox2.Text = "头像";
            this.uiGroupBox2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBox2.Click += new System.EventHandler(this.uiGroupBox2_Click);
            // 
            // userImage
            // 
            this.userImage.Location = new System.Drawing.Point(32, 35);
            this.userImage.Name = "userImage";
            this.userImage.Size = new System.Drawing.Size(286, 226);
            this.userImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.userImage.TabIndex = 74;
            this.userImage.TabStop = false;
            this.userImage.Click += new System.EventHandler(this.userImage_Click);
            // 
            // btnFileUpload
            // 
            this.btnFileUpload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFileUpload.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnFileUpload.Location = new System.Drawing.Point(51, 293);
            this.btnFileUpload.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnFileUpload.Name = "btnFileUpload";
            this.btnFileUpload.Padding = new System.Windows.Forms.Padding(28, 0, 0, 0);
            this.btnFileUpload.Size = new System.Drawing.Size(242, 35);
            this.btnFileUpload.Symbol = 61717;
            this.btnFileUpload.TabIndex = 73;
            this.btnFileUpload.Text = "选择文件夹";
            this.btnFileUpload.Click += new System.EventHandler(this.btnFileUpload_Click);
            // 
            // cmbEndYear
            // 
            this.cmbEndYear.BackColor = System.Drawing.Color.White;
            this.cmbEndYear.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbEndYear.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbEndYear.FormattingEnabled = true;
            this.cmbEndYear.ItemSelectForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.cmbEndYear.Location = new System.Drawing.Point(501, 595);
            this.cmbEndYear.Name = "cmbEndYear";
            this.cmbEndYear.Size = new System.Drawing.Size(226, 35);
            this.cmbEndYear.TabIndex = 57;
            // 
            // cmbStartYear
            // 
            this.cmbStartYear.BackColor = System.Drawing.Color.White;
            this.cmbStartYear.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmbStartYear.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbStartYear.FormattingEnabled = true;
            this.cmbStartYear.ItemSelectForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.cmbStartYear.Location = new System.Drawing.Point(501, 517);
            this.cmbStartYear.Name = "cmbStartYear";
            this.cmbStartYear.Size = new System.Drawing.Size(226, 35);
            this.cmbStartYear.TabIndex = 56;
            // 
            // cmbDate
            // 
            this.cmbDate.FillColor = System.Drawing.Color.White;
            this.cmbDate.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbDate.Location = new System.Drawing.Point(501, 439);
            this.cmbDate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbDate.MaxLength = 10;
            this.cmbDate.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbDate.Name = "cmbDate";
            this.cmbDate.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbDate.Size = new System.Drawing.Size(226, 35);
            this.cmbDate.SymbolDropDown = 61555;
            this.cmbDate.SymbolNormal = 61555;
            this.cmbDate.TabIndex = 55;
            this.cmbDate.Text = "2021-08-13";
            this.cmbDate.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmbDate.Value = new System.DateTime(2021, 8, 13, 16, 32, 0, 379);
            // 
            // txtPwd
            // 
            this.txtPwd.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtPwd.FillColor = System.Drawing.Color.White;
            this.txtPwd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPwd.Location = new System.Drawing.Point(87, 608);
            this.txtPwd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPwd.Maximum = 2147483647D;
            this.txtPwd.Minimum = -2147483648D;
            this.txtPwd.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.Size = new System.Drawing.Size(219, 34);
            this.txtPwd.TabIndex = 54;
            this.txtPwd.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtEmail
            // 
            this.txtEmail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEmail.FillColor = System.Drawing.Color.White;
            this.txtEmail.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtEmail.Location = new System.Drawing.Point(87, 560);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEmail.Maximum = 2147483647D;
            this.txtEmail.Minimum = -2147483648D;
            this.txtEmail.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(219, 34);
            this.txtEmail.TabIndex = 53;
            this.txtEmail.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTel
            // 
            this.txtTel.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtTel.FillColor = System.Drawing.Color.White;
            this.txtTel.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtTel.Location = new System.Drawing.Point(87, 503);
            this.txtTel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTel.Maximum = 2147483647D;
            this.txtTel.Minimum = -2147483648D;
            this.txtTel.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(219, 34);
            this.txtTel.TabIndex = 52;
            this.txtTel.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbClass
            // 
            this.cmbClass.DataSource = null;
            this.cmbClass.FillColor = System.Drawing.Color.White;
            this.cmbClass.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbClass.Location = new System.Drawing.Point(87, 394);
            this.cmbClass.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbClass.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbClass.Name = "cmbClass";
            this.cmbClass.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbClass.Size = new System.Drawing.Size(219, 29);
            this.cmbClass.TabIndex = 51;
            this.cmbClass.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbDegree
            // 
            this.cmbDegree.DataSource = null;
            this.cmbDegree.FillColor = System.Drawing.Color.White;
            this.cmbDegree.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbDegree.Items.AddRange(new object[] {
            "专科",
            "本科",
            "研究生",
            "博士"});
            this.cmbDegree.Location = new System.Drawing.Point(87, 453);
            this.cmbDegree.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbDegree.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbDegree.Name = "cmbDegree";
            this.cmbDegree.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbDegree.Size = new System.Drawing.Size(219, 29);
            this.cmbDegree.TabIndex = 50;
            this.cmbDegree.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbMajor
            // 
            this.cmbMajor.DataSource = null;
            this.cmbMajor.FillColor = System.Drawing.Color.White;
            this.cmbMajor.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbMajor.Location = new System.Drawing.Point(87, 331);
            this.cmbMajor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbMajor.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbMajor.Name = "cmbMajor";
            this.cmbMajor.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbMajor.Size = new System.Drawing.Size(219, 29);
            this.cmbMajor.TabIndex = 49;
            this.cmbMajor.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmbMajor.SelectedIndexChanged += new System.EventHandler(this.cmbMajor_SelectedIndexChanged);
            // 
            // cmbCollege
            // 
            this.cmbCollege.DataSource = null;
            this.cmbCollege.FillColor = System.Drawing.Color.White;
            this.cmbCollege.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbCollege.Location = new System.Drawing.Point(87, 273);
            this.cmbCollege.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCollege.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbCollege.Name = "cmbCollege";
            this.cmbCollege.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbCollege.Size = new System.Drawing.Size(219, 29);
            this.cmbCollege.TabIndex = 48;
            this.cmbCollege.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmbCollege.SelectedIndexChanged += new System.EventHandler(this.cmbCollege_SelectedIndexChanged);
            // 
            // radbGirl
            // 
            this.radbGirl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radbGirl.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.radbGirl.Location = new System.Drawing.Point(243, 160);
            this.radbGirl.MinimumSize = new System.Drawing.Size(1, 1);
            this.radbGirl.Name = "radbGirl";
            this.radbGirl.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.radbGirl.Size = new System.Drawing.Size(63, 29);
            this.radbGirl.TabIndex = 46;
            this.radbGirl.Text = "女";
            // 
            // radbBoy
            // 
            this.radbBoy.Checked = true;
            this.radbBoy.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radbBoy.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.radbBoy.Location = new System.Drawing.Point(87, 160);
            this.radbBoy.MinimumSize = new System.Drawing.Size(1, 1);
            this.radbBoy.Name = "radbBoy";
            this.radbBoy.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.radbBoy.Size = new System.Drawing.Size(83, 29);
            this.radbBoy.TabIndex = 45;
            this.radbBoy.Text = "男";
            // 
            // uiLabel14
            // 
            this.uiLabel14.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel14.Location = new System.Drawing.Point(17, 619);
            this.uiLabel14.Name = "uiLabel14";
            this.uiLabel14.Size = new System.Drawing.Size(76, 23);
            this.uiLabel14.TabIndex = 44;
            this.uiLabel14.Text = "密码:";
            this.uiLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel13
            // 
            this.uiLabel13.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel13.Location = new System.Drawing.Point(17, 560);
            this.uiLabel13.Name = "uiLabel13";
            this.uiLabel13.Size = new System.Drawing.Size(76, 23);
            this.uiLabel13.TabIndex = 43;
            this.uiLabel13.Text = "邮箱:";
            this.uiLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel12
            // 
            this.uiLabel12.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel12.Location = new System.Drawing.Point(17, 508);
            this.uiLabel12.Name = "uiLabel12";
            this.uiLabel12.Size = new System.Drawing.Size(76, 23);
            this.uiLabel12.TabIndex = 42;
            this.uiLabel12.Text = "电话:";
            this.uiLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel10
            // 
            this.uiLabel10.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel10.Location = new System.Drawing.Point(17, 394);
            this.uiLabel10.Name = "uiLabel10";
            this.uiLabel10.Size = new System.Drawing.Size(76, 23);
            this.uiLabel10.TabIndex = 41;
            this.uiLabel10.Text = "班级:";
            this.uiLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel9
            // 
            this.uiLabel9.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel9.Location = new System.Drawing.Point(17, 451);
            this.uiLabel9.Name = "uiLabel9";
            this.uiLabel9.Size = new System.Drawing.Size(76, 23);
            this.uiLabel9.TabIndex = 40;
            this.uiLabel9.Text = "学位:";
            this.uiLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel8
            // 
            this.uiLabel8.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel8.Location = new System.Drawing.Point(17, 337);
            this.uiLabel8.Name = "uiLabel8";
            this.uiLabel8.Size = new System.Drawing.Size(76, 23);
            this.uiLabel8.TabIndex = 39;
            this.uiLabel8.Text = "专业：";
            this.uiLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel6
            // 
            this.uiLabel6.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel6.Location = new System.Drawing.Point(17, 280);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Size = new System.Drawing.Size(76, 23);
            this.uiLabel6.TabIndex = 38;
            this.uiLabel6.Text = "学院：";
            this.uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel5.Location = new System.Drawing.Point(356, 451);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(113, 23);
            this.uiLabel5.TabIndex = 37;
            this.uiLabel5.Text = "出生日期：";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(17, 223);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(76, 23);
            this.uiLabel4.TabIndex = 36;
            this.uiLabel4.Text = "民族：";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(356, 607);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(113, 23);
            this.uiLabel3.TabIndex = 35;
            this.uiLabel3.Text = "毕业年份：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel7
            // 
            this.uiLabel7.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel7.Location = new System.Drawing.Point(17, 166);
            this.uiLabel7.Name = "uiLabel7";
            this.uiLabel7.Size = new System.Drawing.Size(76, 23);
            this.uiLabel7.TabIndex = 34;
            this.uiLabel7.Text = "性别：";
            this.uiLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(356, 529);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(113, 23);
            this.uiLabel2.TabIndex = 33;
            this.uiLabel2.Text = "入学年份：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtName
            // 
            this.txtName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtName.FillColor = System.Drawing.Color.White;
            this.txtName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtName.Location = new System.Drawing.Point(87, 98);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtName.Maximum = 2147483647D;
            this.txtName.Minimum = -2147483648D;
            this.txtName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(219, 34);
            this.txtName.TabIndex = 32;
            this.txtName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(17, 109);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(76, 23);
            this.uiLabel1.TabIndex = 31;
            this.uiLabel1.Text = "姓名：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // FrmStudentEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 795);
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "FrmStudentEdit";
            this.Text = "";
            this.Load += new System.EventHandler(this.FrmStudentEdit_Load);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            this.Controls.SetChildIndex(this.pnlBtm, 0);
            this.pnlBtm.ResumeLayout(false);
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.userImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UIGroupBox uiGroupBox2;
        private Sunny.UI.UISymbolButton btnFileUpload;
        private Sunny.UI.UIComboboxEx cmbEndYear;
        private Sunny.UI.UIComboboxEx cmbStartYear;
        private Sunny.UI.UIDatePicker cmbDate;
        private Sunny.UI.UITextBox txtPwd;
        private Sunny.UI.UITextBox txtEmail;
        private Sunny.UI.UITextBox txtTel;
        private Sunny.UI.UIComboBox cmbClass;
        private Sunny.UI.UIComboBox cmbDegree;
        private Sunny.UI.UIComboBox cmbMajor;
        private Sunny.UI.UIComboBox cmbCollege;
        private Sunny.UI.UIRadioButton radbGirl;
        private Sunny.UI.UIRadioButton radbBoy;
        private Sunny.UI.UILabel uiLabel14;
        private Sunny.UI.UILabel uiLabel13;
        private Sunny.UI.UILabel uiLabel12;
        private Sunny.UI.UILabel uiLabel10;
        private Sunny.UI.UILabel uiLabel9;
        private Sunny.UI.UILabel uiLabel8;
        private Sunny.UI.UILabel uiLabel6;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel uiLabel7;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UITextBox txtName;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UITextBox txtStudentNo;
        private Sunny.UI.UILabel uiLabel11;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox userImage;
        private Sunny.UI.UIComboBox cmbNation;
    }
}