﻿
namespace StudentManagement.Admin.EditForm
{
    partial class FrmMajorEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.cmbCollege = new Sunny.UI.UIComboBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.txtMajorId = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiLabel11 = new Sunny.UI.UILabel();
            this.txtMajorName = new Sunny.UI.UITextBox();
            this.uiTextBox1 = new Sunny.UI.UITextBox();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.uiTextBox2 = new Sunny.UI.UITextBox();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBtm
            // 
            this.pnlBtm.Location = new System.Drawing.Point(1, 277);
            this.pnlBtm.Size = new System.Drawing.Size(474, 55);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.cmbCollege);
            this.uiGroupBox1.Controls.Add(this.uiLabel5);
            this.uiGroupBox1.Controls.Add(this.uiLabel2);
            this.uiGroupBox1.Controls.Add(this.uiTextBox2);
            this.uiGroupBox1.Controls.Add(this.uiLabel4);
            this.uiGroupBox1.Controls.Add(this.txtMajorId);
            this.uiGroupBox1.Controls.Add(this.uiLabel3);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Controls.Add(this.uiTextBox1);
            this.uiGroupBox1.Controls.Add(this.uiLabel11);
            this.uiGroupBox1.Controls.Add(this.txtMajorName);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(16, 50);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(419, 222);
            this.uiGroupBox1.TabIndex = 66;
            this.uiGroupBox1.Text = "专业信息";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbCollege
            // 
            this.cmbCollege.DataSource = null;
            this.cmbCollege.FillColor = System.Drawing.Color.White;
            this.cmbCollege.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbCollege.Location = new System.Drawing.Point(116, 178);
            this.cmbCollege.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCollege.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbCollege.Name = "cmbCollege";
            this.cmbCollege.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbCollege.Size = new System.Drawing.Size(219, 29);
            this.cmbCollege.TabIndex = 66;
            this.cmbCollege.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(27, 184);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(76, 23);
            this.uiLabel2.TabIndex = 65;
            this.uiLabel2.Text = "学院：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMajorId
            // 
            this.txtMajorId.BackColor = System.Drawing.Color.Silver;
            this.txtMajorId.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtMajorId.FillColor = System.Drawing.Color.Silver;
            this.txtMajorId.FillDisableColor = System.Drawing.Color.Silver;
            this.txtMajorId.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtMajorId.Location = new System.Drawing.Point(116, 61);
            this.txtMajorId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtMajorId.Maximum = 2147483647D;
            this.txtMajorId.Minimum = -2147483648D;
            this.txtMajorId.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtMajorId.Name = "txtMajorId";
            this.txtMajorId.ReadOnly = true;
            this.txtMajorId.Size = new System.Drawing.Size(219, 34);
            this.txtMajorId.Style = Sunny.UI.UIStyle.Custom;
            this.txtMajorId.StyleCustomMode = true;
            this.txtMajorId.TabIndex = 64;
            this.txtMajorId.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(27, 129);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(76, 23);
            this.uiLabel1.TabIndex = 61;
            this.uiLabel1.Text = "专业：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel11
            // 
            this.uiLabel11.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel11.Location = new System.Drawing.Point(27, 61);
            this.uiLabel11.Name = "uiLabel11";
            this.uiLabel11.Size = new System.Drawing.Size(76, 23);
            this.uiLabel11.TabIndex = 63;
            this.uiLabel11.Text = "编号：";
            this.uiLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMajorName
            // 
            this.txtMajorName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtMajorName.FillColor = System.Drawing.Color.White;
            this.txtMajorName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtMajorName.Location = new System.Drawing.Point(116, 118);
            this.txtMajorName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtMajorName.Maximum = 2147483647D;
            this.txtMajorName.Minimum = -2147483648D;
            this.txtMajorName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtMajorName.Name = "txtMajorName";
            this.txtMajorName.Size = new System.Drawing.Size(219, 34);
            this.txtMajorName.TabIndex = 62;
            this.txtMajorName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiTextBox1
            // 
            this.uiTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox1.FillColor = System.Drawing.Color.White;
            this.uiTextBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox1.Location = new System.Drawing.Point(116, 118);
            this.uiTextBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox1.Maximum = 2147483647D;
            this.uiTextBox1.Minimum = -2147483648D;
            this.uiTextBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiTextBox1.Name = "uiTextBox1";
            this.uiTextBox1.Size = new System.Drawing.Size(219, 34);
            this.uiTextBox1.TabIndex = 62;
            this.uiTextBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(27, 61);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(76, 23);
            this.uiLabel3.TabIndex = 63;
            this.uiLabel3.Text = "编号：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(27, 129);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(76, 23);
            this.uiLabel4.TabIndex = 61;
            this.uiLabel4.Text = "专业：";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiTextBox2
            // 
            this.uiTextBox2.BackColor = System.Drawing.Color.Silver;
            this.uiTextBox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox2.FillColor = System.Drawing.Color.Silver;
            this.uiTextBox2.FillDisableColor = System.Drawing.Color.Silver;
            this.uiTextBox2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox2.Location = new System.Drawing.Point(116, 61);
            this.uiTextBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox2.Maximum = 2147483647D;
            this.uiTextBox2.Minimum = -2147483648D;
            this.uiTextBox2.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiTextBox2.Name = "uiTextBox2";
            this.uiTextBox2.ReadOnly = true;
            this.uiTextBox2.Size = new System.Drawing.Size(219, 34);
            this.uiTextBox2.Style = Sunny.UI.UIStyle.Custom;
            this.uiTextBox2.StyleCustomMode = true;
            this.uiTextBox2.TabIndex = 64;
            this.uiTextBox2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel5.Location = new System.Drawing.Point(27, 184);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(76, 23);
            this.uiLabel5.TabIndex = 65;
            this.uiLabel5.Text = "学院：";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmMajorEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 335);
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "FrmMajorEdit";
            this.Text = "FrmMajorEdit";
            this.Load += new System.EventHandler(this.FrmMajorEdit_Load);
            this.Controls.SetChildIndex(this.pnlBtm, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            this.uiGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UITextBox txtMajorId;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel11;
        private Sunny.UI.UITextBox txtMajorName;
        private Sunny.UI.UIComboBox cmbCollege;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UITextBox uiTextBox2;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UITextBox uiTextBox1;
    }
}