﻿using BLL.Admin;
using Model;
using Sunny.UI;
using System;
using System.Data;
using System.Windows.Forms;

namespace StudentManagement.Admin.EditForm
{
    public partial class FrmScoreEdit : UIEditForm
    {
        public FrmScoreEdit()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 窗体加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmScoreEdit_Load(object sender, EventArgs e)
        {
            ButtonOkClick += FrmScoreEdit_ButtonOkClick; ;
            //检查提交数据
            CheckedData += FrmScoreEdit_CheckedData; ;
            StudentBind();
            CourseBind();
            //接受tag对象,tag是窗体的属性，编辑时给tag赋值，窗体释放后，tag属性为空，添加时，没有给tag属性赋值
            Score score = (Score)Tag;
            if (Tag != null)
            {
                cmbCourse.SelectedValue = score.CourseId;
                cmbStudent.SelectedValue = score.StuId;
                txtGrade.Text = score.Grade.ToString();
            }
        }

        /// <summary>
        /// 学生绑定
        /// </summary>
        private void StudentBind()
        {
            {
                this.cmbStudent.DisplayMember = "studentName";
                this.cmbStudent.ValueMember = "Id";
                DataTable table = new StudentBLL().GetStudentList1();
                DataRow row = table.NewRow();
                row["Id"] = 0;
                row["studentName"] = "请选择姓名";
                table.Rows.InsertAt(row, 0);
                this.cmbStudent.DataSource = table;
            }
        }

        private void CourseBind()
        {
            this.cmbCourse.DisplayMember = "CourseName";
            this.cmbCourse.ValueMember = "Id";
            DataTable table = new CourseBLL().GetCourseList();
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["CourseName"] = "请选择课程";
            table.Rows.InsertAt(row, 0);
            this.cmbCourse.DataSource = table;
        }

        private bool FrmScoreEdit_CheckedData(object sender, EditFormEventArgs e)
        {
            Score score = new Score();
            score.StuId = (int)this.cmbStudent.SelectedValue;
            score.CourseId = (int)this.cmbCourse.SelectedValue;
            if (cmbStudent.Text == "请选择学生")
            {
                ShowWarningDialog("请选择学生");
                return false;
            }
            if (cmbCourse.Text == "请选择课程")
            {
                ShowWarningDialog("请选择课程");
                return false;
            }

            if (string.IsNullOrEmpty(txtGrade.Text))
            {
                ShowWarningDialog("成绩不能为空");
                return false;
            }
            string text = this.Text;
            if (new ScoreBLL().IsExist(score, text) == true)
            {
                ShowWarningDialog("成绩已存在");
                return false;
            }
            return true;
        }

        private void FrmScoreEdit_ButtonOkClick(object sender, EventArgs e)
        {
            Score score = new Score();
            score.StuId = (int)this.cmbStudent.SelectedValue;
            score.CourseId = (int)this.cmbCourse.SelectedValue;
            score.Grade = Convert.ToDecimal(txtGrade.Text);
            if (this.Text == "添加成绩信息")
            {
                new ScoreBLL().AddScore(score);
                ShowSuccessDialog("添加成功");
            }
            else
            {
                new ScoreBLL().Update(score);
                ShowSuccessDialog("修改成功");
            }

            this.DialogResult = DialogResult.Yes;
        }
    }
}