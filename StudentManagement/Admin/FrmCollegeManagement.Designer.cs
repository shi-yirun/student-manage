﻿
namespace StudentManagement.Admin
{
    partial class FrmCollegeManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btnPrint = new Sunny.UI.UISymbolButton();
            this.btnExport = new Sunny.UI.UISymbolButton();
            this.btnCollegeDelete = new Sunny.UI.UISymbolButton();
            this.btnCollegeEdit = new Sunny.UI.UISymbolButton();
            this.btnCollegeAdd = new Sunny.UI.UISymbolButton();
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.cmbCollege = new Sunny.UI.UIComboBox();
            this.btnFind = new Sunny.UI.UIButton();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.btnRefresh = new Sunny.UI.UISymbolButton();
            this.dgvCollegeList = new Sunny.UI.UIDataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collegeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uiPagination1 = new Sunny.UI.UIPagination();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCollegeList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrint.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnPrint.Location = new System.Drawing.Point(1005, 113);
            this.btnPrint.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnPrint.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnPrint.Size = new System.Drawing.Size(46, 35);
            this.btnPrint.Symbol = 57594;
            this.btnPrint.SymbolSize = 35;
            this.btnPrint.TabIndex = 118;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExport
            // 
            this.btnExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExport.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnExport.Location = new System.Drawing.Point(959, 113);
            this.btnExport.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnExport.Name = "btnExport";
            this.btnExport.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnExport.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnExport.Size = new System.Drawing.Size(46, 35);
            this.btnExport.Symbol = 61584;
            this.btnExport.SymbolSize = 35;
            this.btnExport.TabIndex = 117;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnCollegeDelete
            // 
            this.btnCollegeDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCollegeDelete.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnCollegeDelete.Location = new System.Drawing.Point(114, 113);
            this.btnCollegeDelete.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnCollegeDelete.Name = "btnCollegeDelete";
            this.btnCollegeDelete.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnCollegeDelete.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnCollegeDelete.Size = new System.Drawing.Size(46, 35);
            this.btnCollegeDelete.Symbol = 61544;
            this.btnCollegeDelete.SymbolSize = 35;
            this.btnCollegeDelete.TabIndex = 115;
            this.btnCollegeDelete.Click += new System.EventHandler(this.btnCollegeDelete_Click);
            // 
            // btnCollegeEdit
            // 
            this.btnCollegeEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCollegeEdit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnCollegeEdit.Location = new System.Drawing.Point(68, 113);
            this.btnCollegeEdit.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnCollegeEdit.Name = "btnCollegeEdit";
            this.btnCollegeEdit.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnCollegeEdit.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnCollegeEdit.Size = new System.Drawing.Size(46, 35);
            this.btnCollegeEdit.Symbol = 61508;
            this.btnCollegeEdit.SymbolSize = 35;
            this.btnCollegeEdit.TabIndex = 114;
            this.btnCollegeEdit.Click += new System.EventHandler(this.btnCollegeEdit_Click);
            // 
            // btnCollegeAdd
            // 
            this.btnCollegeAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCollegeAdd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnCollegeAdd.Location = new System.Drawing.Point(22, 113);
            this.btnCollegeAdd.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnCollegeAdd.Name = "btnCollegeAdd";
            this.btnCollegeAdd.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnCollegeAdd.Size = new System.Drawing.Size(46, 35);
            this.btnCollegeAdd.Symbol = 61543;
            this.btnCollegeAdd.SymbolSize = 35;
            this.btnCollegeAdd.TabIndex = 113;
            this.btnCollegeAdd.Click += new System.EventHandler(this.btnCollegeAdd_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.cmbCollege);
            this.uiGroupBox1.Controls.Add(this.btnFind);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(22, 14);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(1032, 81);
            this.uiGroupBox1.TabIndex = 110;
            this.uiGroupBox1.Text = "查询";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbCollege
            // 
            this.cmbCollege.DataSource = null;
            this.cmbCollege.FillColor = System.Drawing.Color.White;
            this.cmbCollege.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbCollege.Location = new System.Drawing.Point(92, 37);
            this.cmbCollege.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCollege.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbCollege.Name = "cmbCollege";
            this.cmbCollege.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbCollege.Size = new System.Drawing.Size(150, 29);
            this.cmbCollege.TabIndex = 8;
            this.cmbCollege.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnFind
            // 
            this.btnFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFind.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnFind.Location = new System.Drawing.Point(901, 31);
            this.btnFind.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(100, 35);
            this.btnFind.TabIndex = 7;
            this.btnFind.Text = "查询";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(24, 31);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(80, 35);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "学院：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnRefresh.Location = new System.Drawing.Point(913, 113);
            this.btnRefresh.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnRefresh.Size = new System.Drawing.Size(46, 35);
            this.btnRefresh.Symbol = 61473;
            this.btnRefresh.SymbolSize = 35;
            this.btnRefresh.TabIndex = 116;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dgvCollegeList
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.dgvCollegeList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCollegeList.BackgroundColor = System.Drawing.Color.White;
            this.dgvCollegeList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCollegeList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCollegeList.ColumnHeadersHeight = 32;
            this.dgvCollegeList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvCollegeList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.collegeName});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCollegeList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvCollegeList.EnableHeadersVisualStyles = false;
            this.dgvCollegeList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.dgvCollegeList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.dgvCollegeList.Location = new System.Drawing.Point(22, 154);
            this.dgvCollegeList.Name = "dgvCollegeList";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCollegeList.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCollegeList.RowHeadersWidth = 51;
            this.dgvCollegeList.RowHeight = 27;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.dgvCollegeList.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvCollegeList.RowTemplate.Height = 27;
            this.dgvCollegeList.SelectedIndex = -1;
            this.dgvCollegeList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCollegeList.ShowGridLine = true;
            this.dgvCollegeList.Size = new System.Drawing.Size(1032, 404);
            this.dgvCollegeList.TabIndex = 112;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "序号";
            this.Id.MinimumWidth = 6;
            this.Id.Name = "Id";
            this.Id.Width = 125;
            // 
            // collegeName
            // 
            this.collegeName.DataPropertyName = "collegeName";
            this.collegeName.HeaderText = "学院";
            this.collegeName.MinimumWidth = 6;
            this.collegeName.Name = "collegeName";
            this.collegeName.Width = 125;
            // 
            // uiPagination1
            // 
            this.uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPagination1.Location = new System.Drawing.Point(228, 576);
            this.uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPagination1.Name = "uiPagination1";
            this.uiPagination1.PageSize = 5;
            this.uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiPagination1.Size = new System.Drawing.Size(689, 43);
            this.uiPagination1.TabIndex = 111;
            this.uiPagination1.Text = "uiPagination1";
            this.uiPagination1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPagination1.PageChanged += new Sunny.UI.UIPagination.OnPageChangeEventHandler(this.uiPagination1_PageChanged);
            // 
            // FrmCollegeManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1079, 629);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnCollegeDelete);
            this.Controls.Add(this.btnCollegeEdit);
            this.Controls.Add(this.btnCollegeAdd);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.dgvCollegeList);
            this.Controls.Add(this.uiPagination1);
            this.Name = "FrmCollegeManagement";
            this.Text = "学院管理";
            this.Load += new System.EventHandler(this.FrmCollegeManagement_Load);
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCollegeList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Sunny.UI.UISymbolButton btnPrint;
        private Sunny.UI.UISymbolButton btnExport;
        private Sunny.UI.UISymbolButton btnCollegeDelete;
        private Sunny.UI.UISymbolButton btnCollegeEdit;
        private Sunny.UI.UISymbolButton btnCollegeAdd;
        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UIButton btnFind;
        private Sunny.UI.UISymbolButton btnRefresh;
        private Sunny.UI.UIDataGridView dgvCollegeList;
        private Sunny.UI.UIPagination uiPagination1;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIComboBox cmbCollege;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn collegeName;
    }
}