﻿
namespace StudentManagement.Admin
{
    partial class FrmMajorManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnPrint = new Sunny.UI.UISymbolButton();
            this.btnExport = new Sunny.UI.UISymbolButton();
            this.btnCollegeDelete = new Sunny.UI.UISymbolButton();
            this.btnCollegeEdit = new Sunny.UI.UISymbolButton();
            this.btnCollegeAdd = new Sunny.UI.UISymbolButton();
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.cmbMajor = new Sunny.UI.UIComboBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.cmbCollege = new Sunny.UI.UIComboBox();
            this.btnFind = new Sunny.UI.UIButton();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.btnRefresh = new Sunny.UI.UISymbolButton();
            this.dgvMajorList = new Sunny.UI.UIDataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collegeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.majorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CollegeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uiPagination1 = new Sunny.UI.UIPagination();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMajorList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrint.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnPrint.Location = new System.Drawing.Point(1017, 127);
            this.btnPrint.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnPrint.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnPrint.Size = new System.Drawing.Size(46, 35);
            this.btnPrint.Symbol = 57594;
            this.btnPrint.SymbolSize = 35;
            this.btnPrint.TabIndex = 127;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExport
            // 
            this.btnExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExport.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnExport.Location = new System.Drawing.Point(971, 127);
            this.btnExport.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnExport.Name = "btnExport";
            this.btnExport.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnExport.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnExport.Size = new System.Drawing.Size(46, 35);
            this.btnExport.Symbol = 61584;
            this.btnExport.SymbolSize = 35;
            this.btnExport.TabIndex = 126;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnCollegeDelete
            // 
            this.btnCollegeDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCollegeDelete.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnCollegeDelete.Location = new System.Drawing.Point(126, 127);
            this.btnCollegeDelete.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnCollegeDelete.Name = "btnCollegeDelete";
            this.btnCollegeDelete.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnCollegeDelete.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnCollegeDelete.Size = new System.Drawing.Size(46, 35);
            this.btnCollegeDelete.Symbol = 61544;
            this.btnCollegeDelete.SymbolSize = 35;
            this.btnCollegeDelete.TabIndex = 124;
            this.btnCollegeDelete.Click += new System.EventHandler(this.btnCollegeDelete_Click);
            // 
            // btnCollegeEdit
            // 
            this.btnCollegeEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCollegeEdit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnCollegeEdit.Location = new System.Drawing.Point(80, 127);
            this.btnCollegeEdit.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnCollegeEdit.Name = "btnCollegeEdit";
            this.btnCollegeEdit.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnCollegeEdit.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnCollegeEdit.Size = new System.Drawing.Size(46, 35);
            this.btnCollegeEdit.Symbol = 61508;
            this.btnCollegeEdit.SymbolSize = 35;
            this.btnCollegeEdit.TabIndex = 123;
            this.btnCollegeEdit.Click += new System.EventHandler(this.btnCollegeEdit_Click);
            // 
            // btnCollegeAdd
            // 
            this.btnCollegeAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCollegeAdd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnCollegeAdd.Location = new System.Drawing.Point(34, 127);
            this.btnCollegeAdd.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnCollegeAdd.Name = "btnCollegeAdd";
            this.btnCollegeAdd.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnCollegeAdd.Size = new System.Drawing.Size(46, 35);
            this.btnCollegeAdd.Symbol = 61543;
            this.btnCollegeAdd.SymbolSize = 35;
            this.btnCollegeAdd.TabIndex = 122;
            this.btnCollegeAdd.Click += new System.EventHandler(this.btnCollegeAdd_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.cmbMajor);
            this.uiGroupBox1.Controls.Add(this.uiLabel2);
            this.uiGroupBox1.Controls.Add(this.cmbCollege);
            this.uiGroupBox1.Controls.Add(this.btnFind);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(34, 28);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(1032, 81);
            this.uiGroupBox1.TabIndex = 119;
            this.uiGroupBox1.Text = "查询";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbMajor
            // 
            this.cmbMajor.DataSource = null;
            this.cmbMajor.FillColor = System.Drawing.Color.White;
            this.cmbMajor.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbMajor.Location = new System.Drawing.Point(480, 37);
            this.cmbMajor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbMajor.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbMajor.Name = "cmbMajor";
            this.cmbMajor.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbMajor.Size = new System.Drawing.Size(150, 29);
            this.cmbMajor.TabIndex = 10;
            this.cmbMajor.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(408, 32);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(80, 35);
            this.uiLabel2.TabIndex = 9;
            this.uiLabel2.Text = "专业：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbCollege
            // 
            this.cmbCollege.DataSource = null;
            this.cmbCollege.FillColor = System.Drawing.Color.White;
            this.cmbCollege.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbCollege.Location = new System.Drawing.Point(92, 37);
            this.cmbCollege.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCollege.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbCollege.Name = "cmbCollege";
            this.cmbCollege.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbCollege.Size = new System.Drawing.Size(150, 29);
            this.cmbCollege.TabIndex = 8;
            this.cmbCollege.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmbCollege.SelectedIndexChanged += new System.EventHandler(this.cmbCollege_SelectedIndexChanged);
            // 
            // btnFind
            // 
            this.btnFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFind.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnFind.Location = new System.Drawing.Point(901, 31);
            this.btnFind.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(100, 35);
            this.btnFind.TabIndex = 7;
            this.btnFind.Text = "查询";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(24, 31);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(80, 35);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "学院：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnRefresh.Location = new System.Drawing.Point(925, 127);
            this.btnRefresh.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnRefresh.Size = new System.Drawing.Size(46, 35);
            this.btnRefresh.Symbol = 61473;
            this.btnRefresh.SymbolSize = 35;
            this.btnRefresh.TabIndex = 125;
            // 
            // dgvMajorList
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.dgvMajorList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMajorList.BackgroundColor = System.Drawing.Color.White;
            this.dgvMajorList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMajorList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvMajorList.ColumnHeadersHeight = 32;
            this.dgvMajorList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvMajorList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.collegeName,
            this.majorName,
            this.CollegeId});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMajorList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvMajorList.EnableHeadersVisualStyles = false;
            this.dgvMajorList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.dgvMajorList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.dgvMajorList.Location = new System.Drawing.Point(34, 168);
            this.dgvMajorList.Name = "dgvMajorList";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMajorList.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvMajorList.RowHeadersWidth = 51;
            this.dgvMajorList.RowHeight = 27;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.dgvMajorList.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvMajorList.RowTemplate.Height = 27;
            this.dgvMajorList.SelectedIndex = -1;
            this.dgvMajorList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMajorList.ShowGridLine = true;
            this.dgvMajorList.Size = new System.Drawing.Size(1032, 404);
            this.dgvMajorList.TabIndex = 121;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "序号";
            this.Id.MinimumWidth = 6;
            this.Id.Name = "Id";
            this.Id.Width = 125;
            // 
            // collegeName
            // 
            this.collegeName.DataPropertyName = "collegeName";
            this.collegeName.HeaderText = "学院";
            this.collegeName.MinimumWidth = 6;
            this.collegeName.Name = "collegeName";
            this.collegeName.Width = 125;
            // 
            // majorName
            // 
            this.majorName.DataPropertyName = "majorName";
            this.majorName.HeaderText = "专业";
            this.majorName.MinimumWidth = 6;
            this.majorName.Name = "majorName";
            this.majorName.Width = 125;
            // 
            // CollegeId
            // 
            this.CollegeId.DataPropertyName = "CollegeId";
            this.CollegeId.HeaderText = "学院号";
            this.CollegeId.MinimumWidth = 6;
            this.CollegeId.Name = "CollegeId";
            this.CollegeId.Visible = false;
            this.CollegeId.Width = 125;
            // 
            // uiPagination1
            // 
            this.uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPagination1.Location = new System.Drawing.Point(240, 590);
            this.uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPagination1.Name = "uiPagination1";
            this.uiPagination1.PageSize = 5;
            this.uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiPagination1.Size = new System.Drawing.Size(689, 43);
            this.uiPagination1.TabIndex = 120;
            this.uiPagination1.Text = "uiPagination1";
            this.uiPagination1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPagination1.PageChanged += new Sunny.UI.UIPagination.OnPageChangeEventHandler(this.uiPagination1_PageChanged);
            // 
            // FrmMajorManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1100, 661);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnCollegeDelete);
            this.Controls.Add(this.btnCollegeEdit);
            this.Controls.Add(this.btnCollegeAdd);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.dgvMajorList);
            this.Controls.Add(this.uiPagination1);
            this.Name = "FrmMajorManagement";
            this.Text = "专业管理";
            this.Load += new System.EventHandler(this.FrmMajorManagement_Load);
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMajorList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UISymbolButton btnPrint;
        private Sunny.UI.UISymbolButton btnExport;
        private Sunny.UI.UISymbolButton btnCollegeDelete;
        private Sunny.UI.UISymbolButton btnCollegeEdit;
        private Sunny.UI.UISymbolButton btnCollegeAdd;
        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UIComboBox cmbMajor;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UIComboBox cmbCollege;
        private Sunny.UI.UIButton btnFind;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UISymbolButton btnRefresh;
        private Sunny.UI.UIDataGridView dgvMajorList;
        private Sunny.UI.UIPagination uiPagination1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn collegeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn majorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CollegeId;
    }
}