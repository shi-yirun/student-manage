﻿using BLL.Admin;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace StudentManagement.Admin
{
    public partial class FrmAdminInfo : UIPage
    {
        private string filename;
        public FrmAdminInfo()
        {
            InitializeComponent();
        }


        private void FrmAdminInfo_Load(object sender, EventArgs e)
        {
            txtAdminAcount.Text = Program.Admin.AdminAccount;
            txtAdiminName.Text = Program.Admin.AdminName;
            txtEmail.Text = Program.Admin.AdminEmail;
            txtTel.Text = Program.Admin.AdminTel;
            filename = Program.Admin.AdminImage;
            //userImage.Image = new Bitmap(@"../../images/" + Program.Admin.AdminImage);
            userImage.Image = new Bitmap(Application.StartupPath + "/images/" + Program.Admin.AdminImage);

            if (Program.Admin.AdminSex == "男") radbBoy.Checked = true;
            else radbGirl.Checked = true;
        }

        private void btnFileUpload_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "选择上传头像";
            openFileDialog1.Filter = "图片类型|*.jpg;*.png;*.gif;|所有文件|*.*;";
            openFileDialog1.Multiselect = false;
            //如果点了ok按钮就获取文件信息
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //获取文件所有在的位置
                string filePath = openFileDialog1.FileName;
                //获取文件名
                string fileName = openFileDialog1.SafeFileName;
                //目标路径
                //string destFileName = @"../../images/" + fileName;
                string destFileName = Application.StartupPath + "/images/" + fileName;
                filename = fileName;
                //判断文件是否存在
                if (File.Exists(destFileName) == false)
                {
                    File.Copy(filePath, destFileName);
                }
                //设置用户头像的image属性
                userImage.Image = new Bitmap(destFileName);

            }
        }

        private void uiButton11_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtAdminAcount.Text))
            {
                ShowWarningDialog("账号不能为空");
                return;

            }
            if (string.IsNullOrEmpty(txtAdiminName.Text))
            {
                ShowWarningDialog("姓名不能为空");
                return;
            }
            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                ShowWarningDialog("邮箱不能为空");
                return;

            }
            if (string.IsNullOrEmpty(txtTel.Text))
            {
                ShowWarningDialog("电话不能为空");
                return;
            }
            if (!RegexHelper.IsEmail(txtEmail.Text))
            {
                ShowWarningDialog("请输入正确邮箱");
                return;
            }
            if (!RegexHelper.IsMobilePhone(txtTel.Text))
            {
                ShowWarningDialog("请输入正确电话");
                return;
            }
            Program.Admin.AdminAccount = txtAdminAcount.Text;
            Program.Admin.AdminName = txtAdiminName.Text;
            Program.Admin.AdminEmail = txtEmail.Text;
            Program.Admin.AdminTel = txtTel.Text;
            Program.Admin.AdminImage = filename;
            if (radbBoy.Checked == true) Program.Admin.AdminSex = "男";
            else Program.Admin.AdminSex = "女";
            Model.Admin admin = Program.Admin;
            bool result = new AdminBLL().UpdateAdmin(admin);
            if (result == true)
            {
                ShowSuccessDialog("资料更新成功");

                //跳转到首页
                Program.Aside.SelectPage(2002);

            }
            else
            {
                ShowSuccessDialog("资料更新失败");
            }
        }
    }
}
