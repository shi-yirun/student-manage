﻿using BLL.Admin;
using Model;
using StudentManagement.Admin.EditForm;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace StudentManagement.Admin
{
    public partial class FrmScoreManagement : UIPage
    {
        public FrmScoreManagement()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 窗体加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmScoreManagement_Load(object sender, EventArgs e)
        {

            StudentBind();
            CourseBind();
            this.uiPagination1.ActivePage = 1;
        }
        /// <summary>
        /// 学生绑定
        /// </summary>
        private void StudentBind()
        {
            {
                this.cmbStudent.DisplayMember = "studentName";
                this.cmbStudent.ValueMember = "Id";
                DataTable table = new StudentBLL().GetStudentList1();
                DataRow row = table.NewRow();
                row["Id"] = 0;
                row["studentName"] = "请选择姓名";
                table.Rows.InsertAt(row, 0);
                this.cmbStudent.DataSource = table;

            }
        }
        /// <summary>
        /// 课程绑定
        /// </summary>
        private void CourseBind()
        {

            this.cmbCourse.DisplayMember = "CourseName";
            this.cmbCourse.ValueMember = "Id";
            DataTable table = new CourseBLL().GetCourseList();
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["CourseName"] = "请选择课程";
            table.Rows.InsertAt(row, 0);
            this.cmbCourse.DataSource = table;

        }
        /// <summary>
        /// 成绩绑定
        /// </summary>
        private void ScoreTableBind()
        {
            //获取当前页面的index
            int index = this.uiPagination1.ActivePage - 1;
            int pageIndex = this.uiPagination1.ActivePage;
            int size = this.uiPagination1.PageSize;
            int begin = (index * size) + 1;
            int end = (index + 1) * size;
            int count;
            int cId = (int)this.cmbCourse.SelectedValue;
            int sId = (int)this.cmbStudent.SelectedValue;
            this.dgvScoreList.DataSource = new ScoreBLL().GetScoreList(begin, end, out count, cId, sId);
            uiPagination1.TotalCount = count;
        }


        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFind_Click(object sender, EventArgs e)
        {
            ScoreTableBind();
        }
        /// <summary>
        /// 刷新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ScoreTableBind();
        }
        /// <summary>
        /// 页面改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="pagingSource"></param>
        /// <param name="pageIndex"></param>
        /// <param name="count"></param>
        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            ScoreTableBind();
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnScoreAdd_Click(object sender, EventArgs e)
        {

            FrmScoreEdit frmScoreEdit = new FrmScoreEdit();
            frmScoreEdit.Text = "添加成绩信息";
            DialogResult result = frmScoreEdit.ShowDialog();
            if (result == DialogResult.Yes)
            {
                frmScoreEdit.Dispose();
                ScoreTableBind();
            }
        }
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnScoreEdit_Click(object sender, EventArgs e)
        {
            if (this.dgvScoreList.CurrentRow == null || this.dgvScoreList.RowCount == 0)
            {
                ShowWarningDialog("没有可编辑的数据");
                return;
            }

            Score score = new Score();
            score.CourseId = (int)this.dgvScoreList.SelectedRows[0].Cells["courseId"].Value;
            score.StuId = (int)this.dgvScoreList.SelectedRows[0].Cells["stuId"].Value;
            score.Grade = (decimal)this.dgvScoreList.SelectedRows[0].Cells["Grade"].Value;

            FrmScoreEdit frmScoreEdit = new FrmScoreEdit();
            frmScoreEdit.Text = "修改成绩信息";
            frmScoreEdit.Tag = score;
            DialogResult result = frmScoreEdit.ShowDialog();
            if (result == DialogResult.Yes)
            {
                frmScoreEdit.Dispose();
                ScoreTableBind();
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnScoreDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvScoreList.CurrentRow == null || this.dgvScoreList.RowCount == 0)
            {
                ShowWarningDialog("没有可删除的数据");
                return;
            }
            Score score = new Score();
            score.CourseId = (int)this.dgvScoreList.SelectedRows[0].Cells["CourseId"].Value;
            score.StuId = (int)this.dgvScoreList.SelectedRows[0].Cells["StuId"].Value;
            int result = new ScoreBLL().Delete(score);
            if (result == 0)
            {
                ShowWarningDialog("删除失败");
            }
            else
            {

                ShowSuccessDialog("删除成功");
                ScoreTableBind();
            }
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExport_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Excel文件|*.xlsx;*.xls";
            saveFileDialog1.FileName = "成绩信息";
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                //获取数据源
                DataTable table = new ScoreBLL().GetScoreList();
                //获取表格的标题
                List<string> title = new List<string>();
                foreach (DataGridViewColumn t in this.dgvScoreList.Columns)
                {
                    if (t.HeaderText == "学号" || t.HeaderText == "课程号")
                        continue;
                    title.Add(t.HeaderText);
                }
                //获取文件路径
                string path = saveFileDialog1.FileName;
                //获取文件的名字、
                int i = path.LastIndexOf('\\');
                string name = path.Substring(i + 1);

                int m = NpoiHelper.ExportExcel(table, title, path, name);
                if (m == -1)
                {
                    ShowWarningDialog("数据为空");

                }
                else
                {
                    ShowSuccessDialog("写入成功");
                }
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //显示选择面板
            FrmSelection frmSelection = new FrmSelection();
            frmSelection.Tag = this.dgvScoreList.Columns;
            DialogResult dialogResult = frmSelection.ShowDialog();
            if (DialogResult.OK == dialogResult)
            {
                GridPrinter.StartPrint(this.dgvScoreList, true, "成绩信息");
            }
            //恢复原样
            foreach (DataGridViewColumn item in this.dgvScoreList.Columns)
            {
                item.Visible = true;
            }

        }
    }
}
