﻿
namespace StudentManagement.Admin
{
    partial class FrmTeacherManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btnPrint = new Sunny.UI.UISymbolButton();
            this.btnExport = new Sunny.UI.UISymbolButton();
            this.btnTeacherDelete = new Sunny.UI.UISymbolButton();
            this.btnTeacherEdit = new Sunny.UI.UISymbolButton();
            this.btnTeacherAdd = new Sunny.UI.UISymbolButton();
            this.dgvTeacherList = new Sunny.UI.UIDataGridView();
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.btnFind = new Sunny.UI.UIButton();
            this.txtName = new Sunny.UI.UITextBox();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.txtTeacherNo = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.btnRefresh = new Sunny.UI.UISymbolButton();
            this.uiPagination1 = new Sunny.UI.UIPagination();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherSex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherStart_year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherNation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherBirthday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherCollegeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherMajorId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherDegree = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoleId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherImage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherTel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherPwd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collegeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.majorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.className = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTeacherList)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrint.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnPrint.Location = new System.Drawing.Point(996, 113);
            this.btnPrint.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnPrint.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnPrint.Size = new System.Drawing.Size(46, 35);
            this.btnPrint.Symbol = 57594;
            this.btnPrint.SymbolSize = 35;
            this.btnPrint.TabIndex = 118;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExport
            // 
            this.btnExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExport.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnExport.Location = new System.Drawing.Point(950, 113);
            this.btnExport.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnExport.Name = "btnExport";
            this.btnExport.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnExport.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnExport.Size = new System.Drawing.Size(46, 35);
            this.btnExport.Symbol = 61584;
            this.btnExport.SymbolSize = 35;
            this.btnExport.TabIndex = 117;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnTeacherDelete
            // 
            this.btnTeacherDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTeacherDelete.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnTeacherDelete.Location = new System.Drawing.Point(105, 113);
            this.btnTeacherDelete.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnTeacherDelete.Name = "btnTeacherDelete";
            this.btnTeacherDelete.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnTeacherDelete.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnTeacherDelete.Size = new System.Drawing.Size(46, 35);
            this.btnTeacherDelete.Symbol = 61544;
            this.btnTeacherDelete.SymbolSize = 35;
            this.btnTeacherDelete.TabIndex = 115;
            this.btnTeacherDelete.Click += new System.EventHandler(this.btnTeacherDelete_Click);
            // 
            // btnTeacherEdit
            // 
            this.btnTeacherEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTeacherEdit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnTeacherEdit.Location = new System.Drawing.Point(59, 113);
            this.btnTeacherEdit.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnTeacherEdit.Name = "btnTeacherEdit";
            this.btnTeacherEdit.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnTeacherEdit.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnTeacherEdit.Size = new System.Drawing.Size(46, 35);
            this.btnTeacherEdit.Symbol = 61508;
            this.btnTeacherEdit.SymbolSize = 35;
            this.btnTeacherEdit.TabIndex = 114;
            this.btnTeacherEdit.Click += new System.EventHandler(this.btnTeacherEdit_Click);
            // 
            // btnTeacherAdd
            // 
            this.btnTeacherAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTeacherAdd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnTeacherAdd.Location = new System.Drawing.Point(13, 113);
            this.btnTeacherAdd.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnTeacherAdd.Name = "btnTeacherAdd";
            this.btnTeacherAdd.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnTeacherAdd.Size = new System.Drawing.Size(46, 35);
            this.btnTeacherAdd.Symbol = 61543;
            this.btnTeacherAdd.SymbolSize = 35;
            this.btnTeacherAdd.TabIndex = 113;
            this.btnTeacherAdd.Click += new System.EventHandler(this.btnTeacherAdd_Click);
            // 
            // dgvTeacherList
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.dgvTeacherList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTeacherList.BackgroundColor = System.Drawing.Color.White;
            this.dgvTeacherList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTeacherList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvTeacherList.ColumnHeadersHeight = 32;
            this.dgvTeacherList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvTeacherList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.teacherNo,
            this.teacherName,
            this.teacherSex,
            this.teacherStart_year,
            this.teacherNation,
            this.teacherBirthday,
            this.teacherCollegeId,
            this.teacherMajorId,
            this.teacherDegree,
            this.RoleId,
            this.classId,
            this.teacherImage,
            this.teacherTel,
            this.teacherEmail,
            this.teacherPwd,
            this.collegeName,
            this.majorName,
            this.className});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTeacherList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvTeacherList.EnableHeadersVisualStyles = false;
            this.dgvTeacherList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.dgvTeacherList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.dgvTeacherList.Location = new System.Drawing.Point(10, 154);
            this.dgvTeacherList.Name = "dgvTeacherList";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTeacherList.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvTeacherList.RowHeadersWidth = 51;
            this.dgvTeacherList.RowHeight = 27;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.dgvTeacherList.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvTeacherList.RowTemplate.Height = 27;
            this.dgvTeacherList.SelectedIndex = -1;
            this.dgvTeacherList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTeacherList.ShowGridLine = true;
            this.dgvTeacherList.Size = new System.Drawing.Size(1032, 404);
            this.dgvTeacherList.TabIndex = 112;
           
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.btnFind);
            this.uiGroupBox1.Controls.Add(this.txtName);
            this.uiGroupBox1.Controls.Add(this.uiLabel3);
            this.uiGroupBox1.Controls.Add(this.txtTeacherNo);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(13, 14);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(1032, 81);
            this.uiGroupBox1.TabIndex = 110;
            this.uiGroupBox1.Text = "查询";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnFind
            // 
            this.btnFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFind.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnFind.Location = new System.Drawing.Point(901, 31);
            this.btnFind.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(100, 35);
            this.btnFind.TabIndex = 7;
            this.btnFind.Text = "查询";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // txtName
            // 
            this.txtName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtName.FillColor = System.Drawing.Color.White;
            this.txtName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtName.Location = new System.Drawing.Point(433, 32);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtName.Maximum = 2147483647D;
            this.txtName.Minimum = -2147483648D;
            this.txtName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(148, 35);
            this.txtName.TabIndex = 5;
            this.txtName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(328, 31);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(76, 35);
            this.uiLabel3.TabIndex = 4;
            this.uiLabel3.Text = "姓名：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTeacherNo
            // 
            this.txtTeacherNo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtTeacherNo.FillColor = System.Drawing.Color.White;
            this.txtTeacherNo.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtTeacherNo.Location = new System.Drawing.Point(111, 32);
            this.txtTeacherNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTeacherNo.Maximum = 2147483647D;
            this.txtTeacherNo.Minimum = -2147483648D;
            this.txtTeacherNo.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtTeacherNo.Name = "txtTeacherNo";
            this.txtTeacherNo.Size = new System.Drawing.Size(188, 35);
            this.txtTeacherNo.TabIndex = 1;
            this.txtTeacherNo.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(24, 31);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(80, 35);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "教师号：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnRefresh.Location = new System.Drawing.Point(904, 113);
            this.btnRefresh.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnRefresh.Size = new System.Drawing.Size(46, 35);
            this.btnRefresh.Symbol = 61473;
            this.btnRefresh.SymbolSize = 35;
            this.btnRefresh.TabIndex = 116;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // uiPagination1
            // 
            this.uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPagination1.Location = new System.Drawing.Point(219, 576);
            this.uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPagination1.Name = "uiPagination1";
            this.uiPagination1.PageSize = 5;
            this.uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiPagination1.Size = new System.Drawing.Size(689, 43);
            this.uiPagination1.TabIndex = 111;
            this.uiPagination1.Text = "uiPagination1";
            this.uiPagination1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPagination1.ParentChanged += new System.EventHandler(this.uiPagination1_ParentChanged);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "序号";
            this.Id.MinimumWidth = 6;
            this.Id.Name = "Id";
            this.Id.Width = 125;
            // 
            // teacherNo
            // 
            this.teacherNo.DataPropertyName = "TeacherNo";
            this.teacherNo.HeaderText = "教师号";
            this.teacherNo.MinimumWidth = 6;
            this.teacherNo.Name = "teacherNo";
            this.teacherNo.Width = 125;
            // 
            // teacherName
            // 
            this.teacherName.DataPropertyName = "TeacherName";
            this.teacherName.HeaderText = "名字";
            this.teacherName.MinimumWidth = 6;
            this.teacherName.Name = "teacherName";
            this.teacherName.Width = 125;
            // 
            // teacherSex
            // 
            this.teacherSex.DataPropertyName = "TeacherSex";
            this.teacherSex.HeaderText = "性别";
            this.teacherSex.MinimumWidth = 6;
            this.teacherSex.Name = "teacherSex";
            this.teacherSex.Width = 125;
            // 
            // teacherStart_year
            // 
            this.teacherStart_year.DataPropertyName = "TeacherStart_year";
            this.teacherStart_year.HeaderText = "入职年份";
            this.teacherStart_year.MinimumWidth = 6;
            this.teacherStart_year.Name = "teacherStart_year";
            this.teacherStart_year.Width = 125;
            // 
            // teacherNation
            // 
            this.teacherNation.DataPropertyName = "TeacherNation";
            this.teacherNation.HeaderText = "民族";
            this.teacherNation.MinimumWidth = 6;
            this.teacherNation.Name = "teacherNation";
            this.teacherNation.Width = 125;
            // 
            // teacherBirthday
            // 
            this.teacherBirthday.DataPropertyName = "TeacherBirthday";
            this.teacherBirthday.HeaderText = "出生日期";
            this.teacherBirthday.MinimumWidth = 6;
            this.teacherBirthday.Name = "teacherBirthday";
            this.teacherBirthday.Width = 125;
            // 
            // teacherCollegeId
            // 
            this.teacherCollegeId.DataPropertyName = "TeacherCollegeId";
            this.teacherCollegeId.HeaderText = "学院号";
            this.teacherCollegeId.MinimumWidth = 6;
            this.teacherCollegeId.Name = "teacherCollegeId";
            this.teacherCollegeId.Visible = false;
            this.teacherCollegeId.Width = 125;
            // 
            // teacherMajorId
            // 
            this.teacherMajorId.DataPropertyName = "TeacherMajorId";
            this.teacherMajorId.HeaderText = "专业号";
            this.teacherMajorId.MinimumWidth = 6;
            this.teacherMajorId.Name = "teacherMajorId";
            this.teacherMajorId.Visible = false;
            this.teacherMajorId.Width = 125;
            // 
            // teacherDegree
            // 
            this.teacherDegree.DataPropertyName = "TeacherDegree";
            this.teacherDegree.HeaderText = "学历";
            this.teacherDegree.MinimumWidth = 6;
            this.teacherDegree.Name = "teacherDegree";
            this.teacherDegree.Width = 125;
            // 
            // RoleId
            // 
            this.RoleId.DataPropertyName = "RoleId";
            this.RoleId.HeaderText = "角色号";
            this.RoleId.MinimumWidth = 6;
            this.RoleId.Name = "RoleId";
            this.RoleId.Visible = false;
            this.RoleId.Width = 125;
            // 
            // classId
            // 
            this.classId.DataPropertyName = "ClassId";
            this.classId.HeaderText = "班级号";
            this.classId.MinimumWidth = 6;
            this.classId.Name = "classId";
            this.classId.Visible = false;
            this.classId.Width = 125;
            // 
            // teacherImage
            // 
            this.teacherImage.DataPropertyName = "TeacherImage";
            this.teacherImage.HeaderText = "教师照片";
            this.teacherImage.MinimumWidth = 6;
            this.teacherImage.Name = "teacherImage";
            this.teacherImage.Width = 125;
            // 
            // teacherTel
            // 
            this.teacherTel.DataPropertyName = "TeacherTel";
            this.teacherTel.HeaderText = "电话号码";
            this.teacherTel.MinimumWidth = 6;
            this.teacherTel.Name = "teacherTel";
            this.teacherTel.Width = 125;
            // 
            // teacherEmail
            // 
            this.teacherEmail.DataPropertyName = "TeacherEmail";
            this.teacherEmail.HeaderText = "邮件";
            this.teacherEmail.MinimumWidth = 6;
            this.teacherEmail.Name = "teacherEmail";
            this.teacherEmail.Width = 125;
            // 
            // teacherPwd
            // 
            this.teacherPwd.DataPropertyName = "TeacherPwd";
            this.teacherPwd.HeaderText = "密码";
            this.teacherPwd.MinimumWidth = 6;
            this.teacherPwd.Name = "teacherPwd";
            this.teacherPwd.Visible = false;
            this.teacherPwd.Width = 125;
            // 
            // collegeName
            // 
            this.collegeName.DataPropertyName = "collegeName";
            this.collegeName.HeaderText = "学院";
            this.collegeName.MinimumWidth = 6;
            this.collegeName.Name = "collegeName";
            this.collegeName.Width = 125;
            // 
            // majorName
            // 
            this.majorName.DataPropertyName = "MajorName";
            this.majorName.HeaderText = "专业";
            this.majorName.MinimumWidth = 6;
            this.majorName.Name = "majorName";
            this.majorName.Width = 125;
            // 
            // className
            // 
            this.className.DataPropertyName = "className";
            this.className.HeaderText = "班级";
            this.className.MinimumWidth = 6;
            this.className.Name = "className";
            this.className.Width = 125;
            // 
            // FrmTeacherManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1077, 656);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnTeacherDelete);
            this.Controls.Add(this.btnTeacherEdit);
            this.Controls.Add(this.btnTeacherAdd);
            this.Controls.Add(this.dgvTeacherList);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.uiPagination1);
            this.Name = "FrmTeacherManagement";
            this.Text = "教师信息管理";
            this.Load += new System.EventHandler(this.FrmTeacherManagement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTeacherList)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Sunny.UI.UISymbolButton btnPrint;
        private Sunny.UI.UISymbolButton btnExport;
        private Sunny.UI.UISymbolButton btnTeacherDelete;
        private Sunny.UI.UISymbolButton btnTeacherEdit;
        private Sunny.UI.UISymbolButton btnTeacherAdd;
        private Sunny.UI.UIDataGridView dgvTeacherList;
        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UIButton btnFind;
        private Sunny.UI.UITextBox txtName;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UITextBox txtTeacherNo;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UISymbolButton btnRefresh;
        private Sunny.UI.UIPagination uiPagination1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherName;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherSex;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherStart_year;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherNation;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherBirthday;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherCollegeId;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherMajorId;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherDegree;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoleId;
        private System.Windows.Forms.DataGridViewTextBoxColumn classId;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherTel;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherPwd;
        private System.Windows.Forms.DataGridViewTextBoxColumn collegeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn majorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn className;
    }
}