﻿using BLL.Admin;
using StudentManagement.Admin.EditForm;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Menu = Model.Menu;

namespace StudentManagement.Admin
{

    public partial class FrmMeunManager : UIPage
    {

        public FrmMeunManager()
        {
            InitializeComponent();
        }

        private void FrmMeunManager_Initialize(object sender, EventArgs e)
        {
            BindCmbMenn();

            //设置默认页面
            uiPagination1.ActivePage = 1;
        }

        private void BindMenuList()
        {
            //获取当前页面的index
            int index = this.uiPagination1.ActivePage - 1;
            int pageIndex = this.uiPagination1.ActivePage;
            int size = this.uiPagination1.PageSize;
            int begin = (index * size) + 1;
            int end = (index + 1) * size;
            int count;
            //获取2个参数
            Model.Menu menu = new Model.Menu();
            menu.MenuParent = (int)cmbMeunList.SelectedValue;
            menu.MenuName = txtMenuName.Text;

            this.dgvMenuList.DataSource = new MenuBLL().GetMeunList(begin, end, out count, menu);
            //设置分页控件总数
            uiPagination1.TotalCount = count;

        }

        /// <summary>
        /// 绑定下拉框
        /// </summary>
        private void BindCmbMenn()
        {
            this.cmbMeunList.DisplayMember = "MenuName";
            this.cmbMeunList.ValueMember = "MenuId";
            DataTable table = new MenuBLL().GetMeunList();
            DataRow row = table.NewRow();
            row["MenuId"] = 0;
            row["MenuName"] = "请选择父级菜单";
            table.Rows.InsertAt(row, 0);
            this.cmbMeunList.DataSource = table;
            this.cmbMeunList.SelectedIndex = 0;
        }

        /// <summary>
        /// 页面改变事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="pagingSource"></param>
        /// <param name="pageIndex"></param>
        /// <param name="count"></param>
        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {

            BindMenuList();
        }

        /// <summary>
        /// 添加菜单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenuAdd_Click(object sender, EventArgs e)
        {
            //设置标题
            FrmEditMenu frmEditMenu = new FrmEditMenu();
            frmEditMenu.Text = "添加菜单信息";
            DialogResult result = frmEditMenu.ShowDialog();
            //如果接收到的返回值为ok，那么就关闭窗体
            if (DialogResult.OK == result)
            {
                //关闭窗体 
                frmEditMenu.Dispose();
                //接受一下tagString 的值，如果为添加成功，那么就刷新
                if (frmEditMenu.TagString == "添加成功")
                {
                    BindMenuList(); //刷新菜单信息
                }
            }
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            BindMenuList();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            BindMenuList();
        }

        private void btnMenuEdit_Click(object sender, EventArgs e)
        {
            if (this.dgvMenuList.Rows.Count <= 0)
            {
                ShowWarningDialog("没有可以选则的数据");
                return;
            }
            if (this.dgvMenuList.CurrentRow == null)
            {
                ShowWarningDialog("没有选中任何数据");
                return;
            }
            Menu menu = new Menu();
            menu.MenuId = Convert.ToInt32(this.dgvMenuList.SelectedRows[0].Cells["MenuId"].Value.ToString());
            menu.MenuName = this.dgvMenuList.SelectedRows[0].Cells["MenuName"].Value.ToString();
            menu.MenuIcon = Convert.ToInt32(this.dgvMenuList.SelectedRows[0].Cells["MenuIcon"].Value.ToString());
            menu.MenuParent = Convert.ToInt32(this.dgvMenuList.SelectedRows[0].Cells["MenuParent"].Value.ToString());
            menu.MenuUrl = this.dgvMenuList.SelectedRows[0].Cells["MenuUrl"].Value.ToString();
            FrmEditMenu frmEditMenu = new FrmEditMenu();
            frmEditMenu.Text = "修改学生信息";
            //把获取的对象传递到菜单的修改窗体
            frmEditMenu.Tag = menu;
            DialogResult result = frmEditMenu.ShowDialog();
            //如果接收到的返回值为ok，那么就关闭窗体
            if (DialogResult.OK == result)
            {
                //关闭窗体 
                frmEditMenu.Dispose();
                //接受一下tagString 的值，如果为添加成功，那么就刷新
                if (frmEditMenu.TagString == "修改成功")
                {
                    BindMenuList(); //刷新菜单信息
                }
            }
        }

        private void btnMenuDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvMenuList.Rows.Count <= 0)
            {
                ShowWarningDialog("没有可以选则的数据");
                return;
            }
            if (this.dgvMenuList.CurrentRow == null)
            {
                ShowWarningDialog("没有选中任何数据");
                return;
            }
            //获取Id删除信息
            int menuid = (int)this.dgvMenuList.SelectedRows[0].Cells[0].Value;
            int result = new BLL.Admin.MenuBLL().MenuDelete(menuid);
            if (result >= 1)
            {
                ShowSuccessDialog("删除成功");
                BindMenuList(); // 刷新
            }
            else
            {
                ShowSuccessDialog("删除失败");
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            List<string> headTextList = new List<string>();
            foreach (DataGridViewColumn t in this.dgvMenuList.Columns)
            {
                if (t.HeaderText == "编号" || t.HeaderText == "父级菜单编号")
                    continue;

                headTextList.Add(t.HeaderText);
            }
            // 2.从数据库获取要打印的数据
            DataTable table = new BLL.Admin.MenuBLL().GetMenuList1();
            //3.打印数据
            saveFileDialog1.Filter = "excel文件|*.xlsx;*.xls;";
            saveFileDialog1.Title = "请选择保存的路径";
            saveFileDialog1.FileName = "Menu";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //获取存储路径
                string filePath = saveFileDialog1.FileName;
                //获取文件名字
                int index = filePath.LastIndexOf(@"\");
                string fileName = filePath.Substring(index + 1);
                int result = NpoiHelper.ExportExcel(table, headTextList, filePath, fileName);
                switch (result)
                {
                    case 1:
                        ShowSuccessDialog("导出成功");
                        break;
                    case -1:
                        ShowSuccessDialog("数据源为空");
                        break;
                    default:
                        ShowSuccessDialog("导出失败");
                        break;
                }
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //显示选择面板
            FrmSelection frmSelection = new FrmSelection();
            frmSelection.Tag = this.dgvMenuList.Columns;
            DialogResult dialogResult = frmSelection.ShowDialog();
            if (DialogResult.OK == dialogResult)
            {
                GridPrinter.StartPrint(this.dgvMenuList, true, "菜单信息");
            }
            //恢复原样
            foreach (DataGridViewColumn item in this.dgvMenuList.Columns)
            {
                item.Visible = true;
            }

        }
    }
}
