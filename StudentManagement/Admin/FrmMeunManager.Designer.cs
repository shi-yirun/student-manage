﻿
namespace StudentManagement.Admin
{
    partial class FrmMeunManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btnPrint = new Sunny.UI.UISymbolButton();
            this.btnExport = new Sunny.UI.UISymbolButton();
            this.btnMenuDelete = new Sunny.UI.UISymbolButton();
            this.btnMenuEdit = new Sunny.UI.UISymbolButton();
            this.btnMenuAdd = new Sunny.UI.UISymbolButton();
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.cmbMeunList = new Sunny.UI.UIComboBox();
            this.btnFind = new Sunny.UI.UIButton();
            this.txtMenuName = new Sunny.UI.UITextBox();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.btnRefresh = new Sunny.UI.UISymbolButton();
            this.dgvMenuList = new Sunny.UI.UIDataGridView();
            this.MenuId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MenuName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MenuIcon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MenuParent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MenuUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uiPagination1 = new Sunny.UI.UIPagination();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMenuList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrint.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnPrint.Location = new System.Drawing.Point(1017, 178);
            this.btnPrint.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnPrint.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnPrint.Size = new System.Drawing.Size(46, 35);
            this.btnPrint.Symbol = 57594;
            this.btnPrint.SymbolSize = 35;
            this.btnPrint.TabIndex = 118;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExport
            // 
            this.btnExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExport.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnExport.Location = new System.Drawing.Point(971, 178);
            this.btnExport.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnExport.Name = "btnExport";
            this.btnExport.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnExport.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnExport.Size = new System.Drawing.Size(46, 35);
            this.btnExport.Symbol = 61584;
            this.btnExport.SymbolSize = 35;
            this.btnExport.TabIndex = 117;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnMenuDelete
            // 
            this.btnMenuDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenuDelete.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnMenuDelete.Location = new System.Drawing.Point(126, 178);
            this.btnMenuDelete.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnMenuDelete.Name = "btnMenuDelete";
            this.btnMenuDelete.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnMenuDelete.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnMenuDelete.Size = new System.Drawing.Size(46, 35);
            this.btnMenuDelete.Symbol = 61544;
            this.btnMenuDelete.SymbolSize = 35;
            this.btnMenuDelete.TabIndex = 115;
            this.btnMenuDelete.Click += new System.EventHandler(this.btnMenuDelete_Click);
            // 
            // btnMenuEdit
            // 
            this.btnMenuEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenuEdit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnMenuEdit.Location = new System.Drawing.Point(80, 178);
            this.btnMenuEdit.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnMenuEdit.Name = "btnMenuEdit";
            this.btnMenuEdit.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnMenuEdit.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnMenuEdit.Size = new System.Drawing.Size(46, 35);
            this.btnMenuEdit.Symbol = 61508;
            this.btnMenuEdit.SymbolSize = 35;
            this.btnMenuEdit.TabIndex = 114;
            this.btnMenuEdit.Click += new System.EventHandler(this.btnMenuEdit_Click);
            // 
            // btnMenuAdd
            // 
            this.btnMenuAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMenuAdd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnMenuAdd.Location = new System.Drawing.Point(34, 178);
            this.btnMenuAdd.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnMenuAdd.Name = "btnMenuAdd";
            this.btnMenuAdd.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnMenuAdd.Size = new System.Drawing.Size(46, 35);
            this.btnMenuAdd.Symbol = 61543;
            this.btnMenuAdd.SymbolSize = 35;
            this.btnMenuAdd.TabIndex = 113;
            this.btnMenuAdd.Click += new System.EventHandler(this.btnMenuAdd_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.cmbMeunList);
            this.uiGroupBox1.Controls.Add(this.btnFind);
            this.uiGroupBox1.Controls.Add(this.txtMenuName);
            this.uiGroupBox1.Controls.Add(this.uiLabel3);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(34, 79);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(1032, 81);
            this.uiGroupBox1.TabIndex = 110;
            this.uiGroupBox1.Text = "查询";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbMeunList
            // 
            this.cmbMeunList.DataSource = null;
            this.cmbMeunList.FillColor = System.Drawing.Color.White;
            this.cmbMeunList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbMeunList.Location = new System.Drawing.Point(154, 38);
            this.cmbMeunList.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbMeunList.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbMeunList.Name = "cmbMeunList";
            this.cmbMeunList.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbMeunList.Size = new System.Drawing.Size(150, 29);
            this.cmbMeunList.TabIndex = 8;
            this.cmbMeunList.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnFind
            // 
            this.btnFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFind.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnFind.Location = new System.Drawing.Point(901, 31);
            this.btnFind.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(100, 35);
            this.btnFind.TabIndex = 7;
            this.btnFind.Text = "查询";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // txtMenuName
            // 
            this.txtMenuName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtMenuName.FillColor = System.Drawing.Color.White;
            this.txtMenuName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtMenuName.Location = new System.Drawing.Point(522, 32);
            this.txtMenuName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtMenuName.Maximum = 2147483647D;
            this.txtMenuName.Minimum = -2147483648D;
            this.txtMenuName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtMenuName.Name = "txtMenuName";
            this.txtMenuName.Size = new System.Drawing.Size(148, 35);
            this.txtMenuName.TabIndex = 5;
            this.txtMenuName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(388, 32);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(114, 35);
            this.uiLabel3.TabIndex = 4;
            this.uiLabel3.Text = "菜单名称：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(24, 32);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(114, 35);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "父级菜单：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnRefresh.Location = new System.Drawing.Point(925, 178);
            this.btnRefresh.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnRefresh.Size = new System.Drawing.Size(46, 35);
            this.btnRefresh.Symbol = 61473;
            this.btnRefresh.SymbolSize = 35;
            this.btnRefresh.TabIndex = 116;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dgvMenuList
            // 
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.dgvMenuList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvMenuList.BackgroundColor = System.Drawing.Color.White;
            this.dgvMenuList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMenuList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvMenuList.ColumnHeadersHeight = 32;
            this.dgvMenuList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvMenuList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MenuId,
            this.MenuName,
            this.MenuIcon,
            this.MenuParent,
            this.MenuUrl,
            this.ParentName});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMenuList.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgvMenuList.EnableHeadersVisualStyles = false;
            this.dgvMenuList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.dgvMenuList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.dgvMenuList.Location = new System.Drawing.Point(34, 219);
            this.dgvMenuList.Name = "dgvMenuList";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMenuList.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvMenuList.RowHeadersWidth = 51;
            this.dgvMenuList.RowHeight = 27;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            this.dgvMenuList.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvMenuList.RowTemplate.Height = 27;
            this.dgvMenuList.SelectedIndex = -1;
            this.dgvMenuList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMenuList.ShowGridLine = true;
            this.dgvMenuList.Size = new System.Drawing.Size(1032, 404);
            this.dgvMenuList.TabIndex = 112;
            // 
            // MenuId
            // 
            this.MenuId.DataPropertyName = "MenuId";
            this.MenuId.HeaderText = "编号";
            this.MenuId.MinimumWidth = 6;
            this.MenuId.Name = "MenuId";
            this.MenuId.Width = 125;
            // 
            // MenuName
            // 
            this.MenuName.DataPropertyName = "MenuName";
            this.MenuName.HeaderText = "菜单名称";
            this.MenuName.MinimumWidth = 6;
            this.MenuName.Name = "MenuName";
            this.MenuName.Width = 125;
            // 
            // MenuIcon
            // 
            this.MenuIcon.DataPropertyName = "MenuIcon";
            this.MenuIcon.HeaderText = "图标";
            this.MenuIcon.MinimumWidth = 6;
            this.MenuIcon.Name = "MenuIcon";
            this.MenuIcon.Width = 125;
            // 
            // MenuParent
            // 
            this.MenuParent.DataPropertyName = "MenuParent";
            this.MenuParent.HeaderText = "父级菜单编号";
            this.MenuParent.MinimumWidth = 6;
            this.MenuParent.Name = "MenuParent";
            this.MenuParent.Visible = false;
            this.MenuParent.Width = 125;
            // 
            // MenuUrl
            // 
            this.MenuUrl.DataPropertyName = "MenuUrl";
            this.MenuUrl.HeaderText = "窗体地址";
            this.MenuUrl.MinimumWidth = 6;
            this.MenuUrl.Name = "MenuUrl";
            this.MenuUrl.Width = 125;
            // 
            // ParentName
            // 
            this.ParentName.DataPropertyName = "ParentName";
            this.ParentName.HeaderText = "父级菜单名";
            this.ParentName.MinimumWidth = 6;
            this.ParentName.Name = "ParentName";
            this.ParentName.Width = 125;
            // 
            // uiPagination1
            // 
            this.uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPagination1.Location = new System.Drawing.Point(240, 641);
            this.uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPagination1.Name = "uiPagination1";
            this.uiPagination1.PageSize = 5;
            this.uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiPagination1.Size = new System.Drawing.Size(689, 43);
            this.uiPagination1.TabIndex = 111;
            this.uiPagination1.Text = "uiPagination1";
            this.uiPagination1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPagination1.PageChanged += new Sunny.UI.UIPagination.OnPageChangeEventHandler(this.uiPagination1_PageChanged);
            // 
            // FrmMeunManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1101, 763);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnMenuDelete);
            this.Controls.Add(this.btnMenuEdit);
            this.Controls.Add(this.btnMenuAdd);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.dgvMenuList);
            this.Controls.Add(this.uiPagination1);
            this.Name = "FrmMeunManager";
            this.Text = "菜单管理";
            this.Initialize += new System.EventHandler(this.FrmMeunManager_Initialize);
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMenuList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Sunny.UI.UISymbolButton btnPrint;
        private Sunny.UI.UISymbolButton btnExport;
        private Sunny.UI.UISymbolButton btnMenuDelete;
        private Sunny.UI.UISymbolButton btnMenuEdit;
        private Sunny.UI.UISymbolButton btnMenuAdd;
        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UIButton btnFind;
        private Sunny.UI.UITextBox txtMenuName;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UISymbolButton btnRefresh;
        private Sunny.UI.UIDataGridView dgvMenuList;
        private Sunny.UI.UIPagination uiPagination1;
        private Sunny.UI.UIComboBox cmbMeunList;
        private System.Windows.Forms.DataGridViewTextBoxColumn MenuId;
        private System.Windows.Forms.DataGridViewTextBoxColumn MenuName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MenuIcon;
        private System.Windows.Forms.DataGridViewTextBoxColumn MenuParent;
        private System.Windows.Forms.DataGridViewTextBoxColumn MenuUrl;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParentName;
    }
}