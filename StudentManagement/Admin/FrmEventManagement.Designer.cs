﻿
namespace StudentManagement.Admin
{
    partial class FrmEventManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvEventList = new Sunny.UI.UIDataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventDescribe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stuId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btnPrint = new Sunny.UI.UISymbolButton();
            this.btnExport = new Sunny.UI.UISymbolButton();
            this.btnStudentDelete = new Sunny.UI.UISymbolButton();
            this.btnStudentEdit = new Sunny.UI.UISymbolButton();
            this.btnStudentAdd = new Sunny.UI.UISymbolButton();
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.cmbStudent = new Sunny.UI.UIComboBox();
            this.btnFind = new Sunny.UI.UIButton();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.btnRefresh = new Sunny.UI.UISymbolButton();
            this.uiPagination1 = new Sunny.UI.UIPagination();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEventList)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvEventList
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.dgvEventList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEventList.BackgroundColor = System.Drawing.Color.White;
            this.dgvEventList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEventList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvEventList.ColumnHeadersHeight = 32;
            this.dgvEventList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvEventList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.studentName,
            this.eventDescribe,
            this.eventType,
            this.eventResult,
            this.eventTime,
            this.stuId});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEventList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvEventList.EnableHeadersVisualStyles = false;
            this.dgvEventList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.dgvEventList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.dgvEventList.Location = new System.Drawing.Point(22, 169);
            this.dgvEventList.Name = "dgvEventList";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEventList.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvEventList.RowHeadersWidth = 51;
            this.dgvEventList.RowHeight = 27;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.dgvEventList.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvEventList.RowTemplate.Height = 27;
            this.dgvEventList.SelectedIndex = -1;
            this.dgvEventList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEventList.ShowGridLine = true;
            this.dgvEventList.Size = new System.Drawing.Size(1032, 404);
            this.dgvEventList.TabIndex = 121;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "序号";
            this.Id.MinimumWidth = 6;
            this.Id.Name = "Id";
            this.Id.Width = 125;
            // 
            // studentName
            // 
            this.studentName.DataPropertyName = "studentName";
            this.studentName.HeaderText = "姓名";
            this.studentName.MinimumWidth = 6;
            this.studentName.Name = "studentName";
            this.studentName.Width = 125;
            // 
            // eventDescribe
            // 
            this.eventDescribe.DataPropertyName = "eventDescribe";
            this.eventDescribe.HeaderText = "事件描述";
            this.eventDescribe.MinimumWidth = 6;
            this.eventDescribe.Name = "eventDescribe";
            this.eventDescribe.Width = 125;
            // 
            // eventType
            // 
            this.eventType.DataPropertyName = "eventType";
            this.eventType.HeaderText = "奖惩";
            this.eventType.MinimumWidth = 6;
            this.eventType.Name = "eventType";
            this.eventType.Width = 125;
            // 
            // eventResult
            // 
            this.eventResult.DataPropertyName = "eventResult";
            this.eventResult.HeaderText = "结果";
            this.eventResult.MinimumWidth = 6;
            this.eventResult.Name = "eventResult";
            this.eventResult.Width = 125;
            // 
            // eventTime
            // 
            this.eventTime.DataPropertyName = "eventTime";
            this.eventTime.HeaderText = "时间";
            this.eventTime.MinimumWidth = 6;
            this.eventTime.Name = "eventTime";
            this.eventTime.Width = 125;
            // 
            // stuId
            // 
            this.stuId.DataPropertyName = "stuId";
            this.stuId.HeaderText = "学号";
            this.stuId.MinimumWidth = 6;
            this.stuId.Name = "stuId";
            this.stuId.Visible = false;
            this.stuId.Width = 125;
            // 
            // btnPrint
            // 
            this.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrint.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnPrint.Location = new System.Drawing.Point(1005, 128);
            this.btnPrint.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnPrint.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnPrint.Size = new System.Drawing.Size(46, 35);
            this.btnPrint.Symbol = 57594;
            this.btnPrint.SymbolSize = 35;
            this.btnPrint.TabIndex = 127;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExport
            // 
            this.btnExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExport.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnExport.Location = new System.Drawing.Point(959, 128);
            this.btnExport.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnExport.Name = "btnExport";
            this.btnExport.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnExport.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnExport.Size = new System.Drawing.Size(46, 35);
            this.btnExport.Symbol = 61584;
            this.btnExport.SymbolSize = 35;
            this.btnExport.TabIndex = 126;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnStudentDelete
            // 
            this.btnStudentDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStudentDelete.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnStudentDelete.Location = new System.Drawing.Point(114, 128);
            this.btnStudentDelete.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnStudentDelete.Name = "btnStudentDelete";
            this.btnStudentDelete.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnStudentDelete.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnStudentDelete.Size = new System.Drawing.Size(46, 35);
            this.btnStudentDelete.Symbol = 61544;
            this.btnStudentDelete.SymbolSize = 35;
            this.btnStudentDelete.TabIndex = 124;
            this.btnStudentDelete.Click += new System.EventHandler(this.btnStudentDelete_Click);
            // 
            // btnStudentEdit
            // 
            this.btnStudentEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStudentEdit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnStudentEdit.Location = new System.Drawing.Point(68, 128);
            this.btnStudentEdit.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnStudentEdit.Name = "btnStudentEdit";
            this.btnStudentEdit.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnStudentEdit.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnStudentEdit.Size = new System.Drawing.Size(46, 35);
            this.btnStudentEdit.Symbol = 61508;
            this.btnStudentEdit.SymbolSize = 35;
            this.btnStudentEdit.TabIndex = 123;
            this.btnStudentEdit.Click += new System.EventHandler(this.btnStudentEdit_Click);
            // 
            // btnStudentAdd
            // 
            this.btnStudentAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStudentAdd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnStudentAdd.Location = new System.Drawing.Point(22, 128);
            this.btnStudentAdd.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnStudentAdd.Name = "btnStudentAdd";
            this.btnStudentAdd.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnStudentAdd.Size = new System.Drawing.Size(46, 35);
            this.btnStudentAdd.Symbol = 61543;
            this.btnStudentAdd.SymbolSize = 35;
            this.btnStudentAdd.TabIndex = 122;
            this.btnStudentAdd.Click += new System.EventHandler(this.btnStudentAdd_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.cmbStudent);
            this.uiGroupBox1.Controls.Add(this.btnFind);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(22, 29);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(1032, 81);
            this.uiGroupBox1.TabIndex = 119;
            this.uiGroupBox1.Text = "查询";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbStudent
            // 
            this.cmbStudent.DataSource = null;
            this.cmbStudent.FillColor = System.Drawing.Color.White;
            this.cmbStudent.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbStudent.Location = new System.Drawing.Point(92, 37);
            this.cmbStudent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbStudent.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbStudent.Name = "cmbStudent";
            this.cmbStudent.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbStudent.Size = new System.Drawing.Size(150, 29);
            this.cmbStudent.TabIndex = 8;
            this.cmbStudent.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnFind
            // 
            this.btnFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFind.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnFind.Location = new System.Drawing.Point(901, 31);
            this.btnFind.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(100, 35);
            this.btnFind.TabIndex = 7;
            this.btnFind.Text = "查询";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(24, 31);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(80, 35);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "姓名：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnRefresh.Location = new System.Drawing.Point(913, 128);
            this.btnRefresh.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnRefresh.Size = new System.Drawing.Size(46, 35);
            this.btnRefresh.Symbol = 61473;
            this.btnRefresh.SymbolSize = 35;
            this.btnRefresh.TabIndex = 125;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // uiPagination1
            // 
            this.uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPagination1.Location = new System.Drawing.Point(228, 591);
            this.uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPagination1.Name = "uiPagination1";
            this.uiPagination1.PageSize = 5;
            this.uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiPagination1.Size = new System.Drawing.Size(689, 43);
            this.uiPagination1.TabIndex = 120;
            this.uiPagination1.Text = "uiPagination1";
            this.uiPagination1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPagination1.PageChanged += new Sunny.UI.UIPagination.OnPageChangeEventHandler(this.uiPagination1_PageChanged);
            // 
            // FrmEventManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1077, 647);
            this.Controls.Add(this.dgvEventList);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnStudentDelete);
            this.Controls.Add(this.btnStudentEdit);
            this.Controls.Add(this.btnStudentAdd);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.uiPagination1);
            this.Name = "FrmEventManagement";
            this.Text = "学生奖惩管理";
            this.Load += new System.EventHandler(this.FrmEventManagement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEventList)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIDataGridView dgvEventList;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Sunny.UI.UISymbolButton btnPrint;
        private Sunny.UI.UISymbolButton btnExport;
        private Sunny.UI.UISymbolButton btnStudentDelete;
        private Sunny.UI.UISymbolButton btnStudentEdit;
        private Sunny.UI.UISymbolButton btnStudentAdd;
        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UIComboBox cmbStudent;
        private Sunny.UI.UIButton btnFind;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UISymbolButton btnRefresh;
        private Sunny.UI.UIPagination uiPagination1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn eventDescribe;
        private System.Windows.Forms.DataGridViewTextBoxColumn eventType;
        private System.Windows.Forms.DataGridViewTextBoxColumn eventResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn eventTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn stuId;
    }
}