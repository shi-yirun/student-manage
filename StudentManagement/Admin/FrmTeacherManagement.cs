﻿using BLL.Admin;
using StudentManagement.Admin.EditForm;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace StudentManagement.Admin
{
    public partial class FrmTeacherManagement : UIPage
    {
        public FrmTeacherManagement()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 教师事件绑定
        /// </summary>
        private void TeacherBind()
        {
            //获取当前页面的index
            int index = this.uiPagination1.ActivePage - 1;
            int pageIndex = this.uiPagination1.ActivePage;
            int size = this.uiPagination1.PageSize;
            int begin = (index * size) + 1;
            int end = (index + 1) * size;
            int count;
            //获取3个参数
            Model.Teacher teacher = new Model.Teacher();
            teacher.TeacherNo = this.txtTeacherNo.Text;
            teacher.TeacherName = this.txtName.Text;
            this.dgvTeacherList.DataSource = new TeacherBLL().GetTeacherList(begin, end, out count, teacher);
            //设置分页控件总数
            uiPagination1.TotalCount = count;
        }
        /// <summary>
        /// 页面改变事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiPagination1_ParentChanged(object sender, EventArgs e)
        {
            TeacherBind();
        }
        /// <summary>
        /// 添加教师信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTeacherAdd_Click(object sender, EventArgs e)
        {
            FrmTeacherEdit frmTeacherEdit = new FrmTeacherEdit();
            frmTeacherEdit.Text = "添加教师信息";
            DialogResult result = frmTeacherEdit.ShowDialog();
            //如果接收到的返回值为ok，那么就关闭窗体
            if (DialogResult.OK == result)
            {
                //关闭窗体 
                frmTeacherEdit.Dispose();
                //接受一下tagString 的值，如果为添加成功，那么就刷新
                ShowSuccessDialog("添加成功");
                TeacherBind();
            }
        }
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTeacherEdit_Click(object sender, EventArgs e)
        {
            //获取选中行的数据
            if (this.dgvTeacherList.Rows.Count <= 0)
            {
                ShowWarningDialog("没有可以选则的数据");
                return;
            }
            if (this.dgvTeacherList.CurrentRow == null)
            {
                ShowWarningDialog("没有选中任何数据");
                return;
            }
            //存储学生值
            Model.Teacher teacher = new Model.Teacher();
            teacher.TeacherNo = this.dgvTeacherList.SelectedRows[0].Cells["teacherNo"].Value.ToString();
            teacher.TeacherName = this.dgvTeacherList.SelectedRows[0].Cells["teacherName"].Value.ToString();
            teacher.TeacherSex = this.dgvTeacherList.SelectedRows[0].Cells["teacherSex"].Value.ToString();
            teacher.TeacherStart_year = this.dgvTeacherList.SelectedRows[0].Cells["teacherStart_year"].Value.ToString();
            teacher.TeacherNation = this.dgvTeacherList.SelectedRows[0].Cells["teacherNation"].Value.ToString();
            teacher.TeacherBirthday = Convert.ToDateTime(this.dgvTeacherList.SelectedRows[0].Cells["teacherBirthday"].Value.ToString());
            teacher.TeacherCollegeId = Convert.ToInt32(this.dgvTeacherList.SelectedRows[0].Cells["teacherCollegeId"].Value.ToString());
            teacher.TeacherMajorId = Convert.ToInt32(this.dgvTeacherList.SelectedRows[0].Cells["teacherMajorId"].Value.ToString());
            teacher.ClassId = Convert.ToInt32(this.dgvTeacherList.SelectedRows[0].Cells["classId"].Value.ToString());
            teacher.TeacherDegree = this.dgvTeacherList.SelectedRows[0].Cells["teacherDegree"].Value.ToString();
            teacher.TeacherImage = this.dgvTeacherList.SelectedRows[0].Cells["teacherImage"].Value.ToString();
            teacher.TeacherTel = this.dgvTeacherList.SelectedRows[0].Cells["teacherTel"].Value.ToString();
            teacher.TeacherEmail = this.dgvTeacherList.SelectedRows[0].Cells["teacherEmail"].Value.ToString();
            teacher.TeacherPwd = this.dgvTeacherList.SelectedRows[0].Cells["teacherPwd"].Value.ToString();
            //设置标题
            FrmTeacherEdit frmTeacherEdit = new FrmTeacherEdit();
            frmTeacherEdit.Text = "修改教师信息";
            //把获取的对象传递到学生的修改窗体
            frmTeacherEdit.Tag = teacher;
            DialogResult result = frmTeacherEdit.ShowDialog();
            //如果接收到的返回值为ok，那么就关闭窗体
            if (DialogResult.OK == result)
            {
                //关闭窗体 
                frmTeacherEdit.Dispose();
                //接受一下tagString 的值，如果为添加成功，那么就刷新
                ShowSuccessDialog("修改成功");
                TeacherBind(); //刷新学生信息

            }


        }
        /// <summary>
        /// 刷新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            TeacherBind();

        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFind_Click(object sender, EventArgs e)
        {
            TeacherBind();


        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExport_Click(object sender, EventArgs e)
        {
            List<string> headTextList = new List<string>();
            for (int i = 0; i < this.dgvTeacherList.ColumnCount; i++)
            {
                headTextList.Add(dgvTeacherList.Columns[i].HeaderText);
            }
            // 2.从数据库获取要打印的数据
            DataTable table = new BLL.Admin.TeacherBLL().GetTeacherList();
            //3.打印数据
            saveFileDialog1.Filter = "excel文件|*.xlsx;*.xls;";
            saveFileDialog1.Title = "请选择保存的路径";
            saveFileDialog1.FileName = "teacher";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //获取存储路径
                string filePath = saveFileDialog1.FileName;
                //获取文件名字
                int index = filePath.LastIndexOf(@"\");
                string fileName = filePath.Substring(index + 1);
                int result = NpoiHelper.ExportExcel(table, headTextList, filePath, fileName);
                switch (result)
                {
                    case 1:
                        ShowSuccessDialog("导出成功");
                        break;
                    case -1:
                        ShowSuccessDialog("数据源为空");
                        break;
                    default:
                        ShowSuccessDialog("导出失败");
                        break;
                }
            }

        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTeacherDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvTeacherList.Rows.Count <= 0)
            {
                ShowWarningDialog("没有可以选则的数据");
                return;

            }
            if (this.dgvTeacherList.CurrentRow == null)
            {
                ShowWarningDialog("没有选中任何数据");
                return;
            }
            //获取Id删除信息
            int Id = (int)this.dgvTeacherList.SelectedRows[0].Cells[0].Value;
            int result = new BLL.Admin.TeacherBLL().DeleteTeacher(Id);
            if (result == 1)
            {
                ShowSuccessDialog("删除成功");
                TeacherBind(); // 刷新
            }
            else
            {
                ShowSuccessDialog("删除失败");
            }
        }

        private void FrmTeacherManagement_Load(object sender, EventArgs e)
        {
            TeacherBind();
        }

       

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //显示选择面板
            FrmSelection frmSelection = new FrmSelection();
            frmSelection.Tag = this.dgvTeacherList.Columns;
            DialogResult dialogResult = frmSelection.ShowDialog();
            if (DialogResult.OK == dialogResult)
            {
                GridPrinter.StartPrint(this.dgvTeacherList, true, "教师信息");
            }
            //恢复原样
            foreach (DataGridViewColumn item in this.dgvTeacherList.Columns)
            {
                item.Visible = true;
            }


        }
    }
}
