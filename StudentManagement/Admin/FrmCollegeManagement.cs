﻿using BLL.Admin;
using Model;
using StudentManagement.Admin.EditForm;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
namespace StudentManagement.Admin
{
    public partial class FrmCollegeManagement : UIPage
    {
        public FrmCollegeManagement()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 窗体加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmCollegeManagement_Load(object sender, EventArgs e)
        {
            CollegeBind();
            //设置页面会触发页面改变事件
            this.uiPagination1.ActivePage = 1;
        }
        /// <summary>
        /// 绑定表格
        /// </summary>
        private void CollegeBindTable()
        {
            int index = this.uiPagination1.ActivePage - 1;
            int size = this.uiPagination1.PageSize;
            int begin = index * size + 1;
            int end = (index + 1) * size;
            //总行数
            int count;
            int collegeName = (int)this.cmbCollege.SelectedValue;
            this.dgvCollegeList.DataSource = new CollegeBLL().GetCollegeList(begin, end, out count, collegeName);

            //设置行数
            this.uiPagination1.TotalCount = count;
        }
        /// <summary>
        /// 绑定下拉框
        /// </summary>
        private void CollegeBind()
        {
            this.cmbCollege.DisplayMember = "collegeName";
            this.cmbCollege.ValueMember = "Id";
            DataTable table = new CollegeBLL().GetCollegeList();
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["collegeName"] = "请选择学院";
            table.Rows.InsertAt(row, 0);
            this.cmbCollege.DataSource = table;

        }
        /// <summary>
        /// 查询按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFind_Click(object sender, EventArgs e)
        {
            CollegeBindTable();
        }

        /// <summary>
        /// 页面改变事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="pagingSource"></param>
        /// <param name="pageIndex"></param>
        /// <param name="count"></param>
        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            CollegeBindTable();
        }
        /// <summary>
        /// 学院添加按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCollegeAdd_Click(object sender, EventArgs e)
        {
            FrmCollegeEdit frmCollegeEdit = new FrmCollegeEdit();
            frmCollegeEdit.Text = "添加学院信息";
            DialogResult result = frmCollegeEdit.ShowDialog();
            if (result == DialogResult.Yes)
            {
                frmCollegeEdit.Dispose();
                CollegeBindTable();
            }

        }
        /// <summary>
        /// 学院编辑事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCollegeEdit_Click(object sender, EventArgs e)
        {
            if (this.dgvCollegeList.CurrentRow == null || this.dgvCollegeList.RowCount == 0)
            {
                ShowWarningDialog("没有可编辑的数据");
                return;
            }
            College college = new College();
            college.Id = (int)this.dgvCollegeList.SelectedRows[0].Cells[0].Value;
            college.collegeName = this.dgvCollegeList.SelectedRows[0].Cells[1].Value.ToString();
            FrmCollegeEdit frmCollegeEdit = new FrmCollegeEdit();
            frmCollegeEdit.Text = "修改学院信息";
            frmCollegeEdit.Tag = college;
            DialogResult result = frmCollegeEdit.ShowDialog();
            if (result == DialogResult.Yes)
            {
                frmCollegeEdit.Dispose();
                CollegeBindTable();
            }

        }

        /// <summary>
        /// 删除事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCollegeDelete_Click(object sender, EventArgs e)
        {
            ShowSuccessDialog("暂不支持删除");
        }

        /// <summary>
        /// 刷新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            CollegeBindTable();
        }

        /// <summary>
        /// 导出数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExport_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Excel文件|*.xlsx;*.xls";
            saveFileDialog1.FileName = "学院信息";
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                //获取数据源
                DataTable table = new CollegeBLL().GetCollegeList();
                //获取表格的标题
                List<string> title = new List<string>();
                foreach (DataGridViewColumn t in this.dgvCollegeList.Columns)
                {
                    title.Add(t.HeaderText);
                }
                //获取文件路径
                string path = saveFileDialog1.FileName;
                //获取文件的名字、
                int i = path.LastIndexOf('\\');
                string name = path.Substring(i + 1);

                int m = NpoiHelper.ExportExcel(table, title, path, name);
                if (m == -1)
                {
                    ShowWarningDialog("数据为空");

                }
                else
                {
                    ShowSuccessDialog("写入成功");
                }
            }


        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //显示选择面板
            FrmSelection frmSelection = new FrmSelection();
            frmSelection.Tag = this.dgvCollegeList.Columns;
            DialogResult dialogResult = frmSelection.ShowDialog();
            if (DialogResult.OK == dialogResult)
            {
                GridPrinter.StartPrint(this.dgvCollegeList, true, "学院信息");
            }
            //恢复原样
            foreach (DataGridViewColumn item in this.dgvCollegeList.Columns)
            {
                item.Visible = true;
            }


        }
    }
}
