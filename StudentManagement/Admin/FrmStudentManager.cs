﻿using BLL.Admin;
using StudentManagement.Admin.EditForm;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;


namespace StudentManagement.Admin
{
    public partial class FrmStudentManager : UIPage
    {
        public FrmStudentManager()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 绑定班级数据
        /// </summary>
        private void ClassBind()
        {
            this.cmbClass.DisplayMember = "className";
            this.cmbClass.ValueMember = "Id";
            DataTable table = new ClassBLL().GetClassList();
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["className"] = "请选择班级";
            table.Rows.InsertAt(row, 0);
            this.cmbClass.DataSource = table;
            this.cmbClass.SelectedIndex = 0;
        }

        /// <summary>
        /// 查询所有学生信息
        /// </summary>
        private void StudentBind()
        {
            //获取当前页面的index
            int index = this.uiPagination1.ActivePage - 1;
            int pageIndex = this.uiPagination1.ActivePage;
            int size = this.uiPagination1.PageSize;
            int begin = (index * size) + 1;
            int end = (index + 1) * size;
            int count;
            //获取3个参数
            Model.Student student = new Model.Student();
            student.ClassId = cmbClass.SelectedIndex == 0 ? 0 : (int)cmbClass.SelectedValue;
            student.StudentNo = txtStuNo.Text;
            student.StudentName = txtName.Text;

            this.dgvStudentList.DataSource = new StudentBLL().GetStudentList(begin, end, out count, student);
            //设置分页控件总数
            uiPagination1.TotalCount = count;
        }

        /// <summary>
        /// 页面改变事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="pagingSource"></param>
        /// <param name="pageIndex"></param>
        /// <param name="count"></param>
        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            StudentBind();
        }
        /// <summary>
        /// 按钮查询事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFind_Click(object sender, EventArgs e)
        {
            StudentBind();
        }
        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmStudentManager_Load(object sender, EventArgs e)
        {
            //绑定班级信息
            ClassBind();
            //设置选中页数，触发页面改变事件
            this.uiPagination1.ActivePage = 1;
        }

        /// <summary>
        /// 添加按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStudentAdd_Click(object sender, EventArgs e)
        {
            //设置标题
            FrmStudentEdit frmStudentEdit = new FrmStudentEdit();
            frmStudentEdit.Text = "添加学生信息";
            DialogResult result = frmStudentEdit.ShowDialog();
            //如果接收到的返回值为ok，那么就关闭窗体
            if (DialogResult.OK == result)
            {
                //关闭窗体 
                frmStudentEdit.Dispose();
                //接受一下tagString 的值，如果为添加成功，那么就刷新
                if (frmStudentEdit.TagString == "添加成功")
                {
                    StudentBind(); //刷新学生信息
                }
            }
        }

        /// <summary>
        /// 编辑按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStudentEdit_Click(object sender, EventArgs e)
        {
            //获取选中行的数据
            if (this.dgvStudentList.Rows.Count <= 0)
            {
                ShowWarningDialog("没有可以选则的数据");
                return;
            }
            if (this.dgvStudentList.CurrentRow == null)
            {
                ShowWarningDialog("没有选中任何数据");
                return;
            }
            //存储学生值
            Model.Student student = new Model.Student();
            student.StudentNo = this.dgvStudentList.SelectedRows[0].Cells["StudentNo"].Value.ToString();
            student.StudentName = this.dgvStudentList.SelectedRows[0].Cells["StudentName"].Value.ToString();
            student.StudentSex = this.dgvStudentList.SelectedRows[0].Cells["StudentSex"].Value.ToString();
            student.StudentStart_year = this.dgvStudentList.SelectedRows[0].Cells["StudentStart_year"].Value.ToString();
            student.StudentFinish_year = this.dgvStudentList.SelectedRows[0].Cells["StudentFinish_year"].Value.ToString();
            student.StudentNation = this.dgvStudentList.SelectedRows[0].Cells["StudentNation"].Value.ToString();
            student.StudentBirthday = Convert.ToDateTime(this.dgvStudentList.SelectedRows[0].Cells["StudentBirthday"].Value.ToString());
            student.StudentCollegeId = Convert.ToInt32(this.dgvStudentList.SelectedRows[0].Cells["StudentCollegeId"].Value.ToString());
            student.StudentMajorId = Convert.ToInt32(this.dgvStudentList.SelectedRows[0].Cells["StudentMajorId"].Value.ToString());
            student.ClassId = Convert.ToInt32(this.dgvStudentList.SelectedRows[0].Cells["ClassId"].Value.ToString());
            student.StudentDegree = this.dgvStudentList.SelectedRows[0].Cells["StudentDegree"].Value.ToString();
            student.StudentImage = this.dgvStudentList.SelectedRows[0].Cells["StudentImage"].Value.ToString();
            student.StudentTel = this.dgvStudentList.SelectedRows[0].Cells["StudentTel"].Value.ToString();
            student.StudentEmail = this.dgvStudentList.SelectedRows[0].Cells["StudentEmail"].Value.ToString();
            student.StudentPwd = this.dgvStudentList.SelectedRows[0].Cells["StudentPwd"].Value.ToString();
            //设置标题
            FrmStudentEdit frmStudentEdit = new FrmStudentEdit();
            frmStudentEdit.Text = "修改学生信息";
            //把获取的对象传递到学生的修改窗体
            frmStudentEdit.Tag = student;
            DialogResult result = frmStudentEdit.ShowDialog();
            //如果接收到的返回值为ok，那么就关闭窗体
            if (DialogResult.OK == result)
            {
                //关闭窗体 
                frmStudentEdit.Dispose();
                //接受一下tagString 的值，如果为添加成功，那么就刷新
                if (frmStudentEdit.TagString == "修改成功")
                {
                    StudentBind(); //刷新学生信息
                }
            }
        }

        /// <summary>
        /// 删除按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStudentDelete_Click(object sender, EventArgs e)
        {
            //获取选中行的数据
            if (this.dgvStudentList.Rows.Count <= 0)
            {
                ShowWarningDialog("没有可以选则的数据");
                return;
            }
            if (this.dgvStudentList.CurrentRow == null)
            {
                ShowWarningDialog("没有选中任何数据");
                return;
            }
            //获取Id删除信息
            int stuId = (int)this.dgvStudentList.SelectedRows[0].Cells[0].Value;
            int result = new BLL.Admin.StudentBLL().StudentDelete(stuId);
            if (result >= 1)
            {
                ShowSuccessDialog("删除成功");
                StudentBind(); // 刷新
            }
            else
            {
                ShowSuccessDialog("删除失败");
            }
        }

        /// <summary>
        /// 刷新按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            //设置页数为第一页，触发页数改变事件
            this.uiPagination1.ActivePage = 1;
        }
        /// <summary>
        /// 导出Excel数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExport_Click(object sender, EventArgs e)
        {
            //1.从dataGridView里面获取标题
            List<string> headTextList = new List<string>();
            for (int i = 0; i < this.dgvStudentList.ColumnCount; i++)
            {
                headTextList.Add(dgvStudentList.Columns[i].HeaderText);
            }
            // 2.从数据库获取要打印的数据
            DataTable table = new BLL.Admin.StudentBLL().GetStudentList();
            //3.打印数据
            saveFileDialog1.Filter = "excel文件|*.xlsx;*.xls;";
            saveFileDialog1.Title = "请选择保存的路径";
            saveFileDialog1.FileName = "student";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //获取存储路径
                string filePath = saveFileDialog1.FileName;
                //获取文件名字
                int index = filePath.LastIndexOf(@"\");
                string fileName = filePath.Substring(index + 1);
                int result = NpoiHelper.ExportExcel(table, headTextList, filePath, fileName);
                switch (result)
                {
                    case 1:
                        ShowSuccessDialog("导出成功");
                        break;
                    case -1:
                        ShowSuccessDialog("数据源为空");
                        break;
                    default:
                        ShowSuccessDialog("导出失败");
                        break;
                }
            }
        }


        /// <summary>
        /// 打印按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            //显示选择面板
            FrmSelection frmSelection = new FrmSelection();
            frmSelection.Tag = this.dgvStudentList.Columns;
            DialogResult dialogResult= frmSelection.ShowDialog();
            if (DialogResult.OK==dialogResult) 
            {
                GridPrinter.StartPrint(this.dgvStudentList, true, "学生信息");
            }
            //恢复原样
            foreach (DataGridViewColumn item in this.dgvStudentList.Columns)
            {
                item.Visible = true;
            }


        }
    }
}
