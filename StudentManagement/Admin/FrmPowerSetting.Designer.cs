﻿
namespace StudentManagement.Admin
{
    partial class FrmPowerSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiPanel1 = new Sunny.UI.UIPanel();
            this.btnClose = new Sunny.UI.UIButton();
            this.btnSubmit = new Sunny.UI.UIButton();
            this.cmbRoleList = new Sunny.UI.UIComboBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiTreeView1 = new Sunny.UI.UITreeView();
            this.chkSelectAll = new Sunny.UI.UICheckBox();
            this.uiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiPanel1
            // 
            this.uiPanel1.Controls.Add(this.btnClose);
            this.uiPanel1.Controls.Add(this.btnSubmit);
            this.uiPanel1.Controls.Add(this.cmbRoleList);
            this.uiPanel1.Controls.Add(this.uiLabel1);
            this.uiPanel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPanel1.Location = new System.Drawing.Point(13, 14);
            this.uiPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(1062, 133);
            this.uiPanel1.TabIndex = 0;
            this.uiPanel1.Text = null;
            this.uiPanel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnClose
            // 
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnClose.Location = new System.Drawing.Point(910, 61);
            this.btnClose.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(124, 35);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "关闭";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSubmit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnSubmit.Location = new System.Drawing.Point(682, 61);
            this.btnSubmit.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(145, 35);
            this.btnSubmit.TabIndex = 2;
            this.btnSubmit.Text = "提交设置";
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // cmbRoleList
            // 
            this.cmbRoleList.DataSource = null;
            this.cmbRoleList.FillColor = System.Drawing.Color.White;
            this.cmbRoleList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbRoleList.Location = new System.Drawing.Point(168, 61);
            this.cmbRoleList.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbRoleList.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbRoleList.Name = "cmbRoleList";
            this.cmbRoleList.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbRoleList.Size = new System.Drawing.Size(242, 29);
            this.cmbRoleList.TabIndex = 1;
            this.cmbRoleList.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmbRoleList.SelectedIndexChanged += new System.EventHandler(this.cmbRoleList_SelectedIndexChanged);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(45, 61);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(130, 23);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "角色列表：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiTreeView1
            // 
            this.uiTreeView1.FillColor = System.Drawing.Color.White;
            this.uiTreeView1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTreeView1.Location = new System.Drawing.Point(13, 185);
            this.uiTreeView1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTreeView1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiTreeView1.Name = "uiTreeView1";
            this.uiTreeView1.SelectedNode = null;
            this.uiTreeView1.Size = new System.Drawing.Size(1062, 477);
            this.uiTreeView1.TabIndex = 1;
            this.uiTreeView1.Text = "uiTreeView1";
            this.uiTreeView1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiTreeView1.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.uiTreeView1_AfterCheck);
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSelectAll.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.chkSelectAll.Location = new System.Drawing.Point(13, 155);
            this.chkSelectAll.MinimumSize = new System.Drawing.Size(1, 1);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.chkSelectAll.Size = new System.Drawing.Size(150, 29);
            this.chkSelectAll.TabIndex = 3;
            this.chkSelectAll.Text = "全选";
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // FrmPowerSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1088, 676);
            this.Controls.Add(this.chkSelectAll);
            this.Controls.Add(this.uiTreeView1);
            this.Controls.Add(this.uiPanel1);
            this.Name = "FrmPowerSetting";
            this.Text = "权限设置";
            this.Load += new System.EventHandler(this.FrmPowerSetting_Load);
            this.uiPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIPanel uiPanel1;
        private Sunny.UI.UIComboBox cmbRoleList;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIButton btnClose;
        private Sunny.UI.UIButton btnSubmit;
        private Sunny.UI.UITreeView uiTreeView1;
        private Sunny.UI.UICheckBox chkSelectAll;
    }
}