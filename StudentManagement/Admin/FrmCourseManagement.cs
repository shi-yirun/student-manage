﻿using BLL.Admin;
using Model;
using StudentManagement.Admin.EditForm;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace StudentManagement.Admin
{
    public partial class FrmCourseManagement : UIPage
    {
        public FrmCourseManagement()
        {
            InitializeComponent();
        }

        private void FrmCourseManagement_Load(object sender, EventArgs e)
        {
            CourseBind();
            this.uiPagination1.ActivePage = 1;
        }

        private void CourseBind()
        {
            this.cmbCourse.DisplayMember = "CourseName";
            this.cmbCourse.ValueMember = "Id";
            DataTable table = new CourseBLL().GetCourseList();
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["CourseName"] = "请选择课程";
            table.Rows.InsertAt(row, 0);
            this.cmbCourse.DataSource = table;
        }

        private void CourseTableBind()
        {
            //获取当前页面的index
            int index = this.uiPagination1.ActivePage - 1;
            int pageIndex = this.uiPagination1.ActivePage;
            int size = this.uiPagination1.PageSize;
            int begin = (index * size) + 1;
            int end = (index + 1) * size;
            int count;
            string cId = this.cmbCourse.SelectedValue.ToString();
            this.dgvCourseList.DataSource = new CourseBLL().GetCourseList(begin, end, out count, cId);
            uiPagination1.TotalCount = count;
        }

        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            CourseTableBind();
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {
            CourseTableBind();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {

            CourseTableBind();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            CourseTableBind();
        }

        private void btnCourseAdd_Click(object sender, EventArgs e)
        {
            FrmCourseEdit frmCourseEdit = new FrmCourseEdit();
            frmCourseEdit.Text = "添加课程信息";
            DialogResult result = frmCourseEdit.ShowDialog();
            if (result == DialogResult.Yes)
            {
                frmCourseEdit.Dispose();
                CourseTableBind();
            }

        }

        private void btnCourseEdit_Click(object sender, EventArgs e)
        {

            if (this.dgvCourseList.CurrentRow == null || this.dgvCourseList.RowCount == 0)
            {
                ShowWarningDialog("没有可编辑的数据");
                return;
            }
            Course course = new Course();
            course.Id = (int)this.dgvCourseList.SelectedRows[0].Cells["Id"].Value;
            course.CourseGrade = (int)this.dgvCourseList.SelectedRows[0].Cells["CourseGrade"].Value;
            course.TeacherId = (int)this.dgvCourseList.SelectedRows[0].Cells["teacherId"].Value;
            course.CourseName = this.dgvCourseList.SelectedRows[0].Cells["CourseName"].Value.ToString();
            course.teacherName = this.dgvCourseList.SelectedRows[0].Cells["teacherName"].Value.ToString();
            course.Course_start_time = Convert.ToDateTime(this.dgvCourseList.SelectedRows[0].Cells["course_start_time"].Value);
            course.Course_end_time = Convert.ToDateTime(this.dgvCourseList.SelectedRows[0].Cells["course_end_time"].Value.ToString());
            course.Course_desc = this.dgvCourseList.SelectedRows[0].Cells["course_desc"].Value.ToString();
            FrmCourseEdit frmCourseEdit = new FrmCourseEdit();
            frmCourseEdit.Text = "修改课程信息";
            frmCourseEdit.Tag = course;
            DialogResult result = frmCourseEdit.ShowDialog();
            if (result == DialogResult.Yes)
            {
                frmCourseEdit.Dispose();
                CourseTableBind();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Excel文件|*.xlsx;*.xls";
            saveFileDialog1.FileName = "课程信息";
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                DataTable table = new CourseBLL().GetCourseTList();
                string path = saveFileDialog1.FileName;
                int index = path.LastIndexOf("\\");
                string name = path.Substring(index + 1);
                List<string> title = new List<string>();
                foreach (DataGridViewColumn t in this.dgvCourseList.Columns)
                {
                    if (t.HeaderText == "教师号") continue;
                    title.Add(t.HeaderText);

                }
                NpoiHelper.ExportExcel(table, title, path, name);

            }


        }

        private void btnCourseDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvCourseList.CurrentRow == null || this.dgvCourseList.RowCount == 0)
            {
                ShowWarningDialog("没有可删除的数据");
                return;
            }
            int Id = (int)this.dgvCourseList.SelectedRows[0].Cells["Id"].Value;
            int result = new CourseBLL().DeleteCourse(Id);
            if (result == 0)
            {
                ShowWarningDialog("删除失败");
            }
            else
            {

                ShowSuccessDialog("删除成功");
                CourseTableBind();
            }
        }

        private void dgvCourseList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //显示选择面板
            FrmSelection frmSelection = new FrmSelection();
            frmSelection.Tag = this.dgvCourseList.Columns;
            DialogResult dialogResult = frmSelection.ShowDialog();
            if (DialogResult.OK == dialogResult)
            {
                GridPrinter.StartPrint(this.dgvCourseList, true, "课程信息");
            }
            //恢复原样
            foreach (DataGridViewColumn item in this.dgvCourseList.Columns)
            {
                item.Visible = true;
            }

        }
    }
}
