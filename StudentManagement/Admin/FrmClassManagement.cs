﻿using BLL.Admin;
using Model;
using StudentManagement.Admin.EditForm;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace StudentManagement.Admin
{
    public partial class FrmClassManagement : UIPage
    {
        public FrmClassManagement()
        {
            InitializeComponent();
        }
        private void ClassBind()
        {
            int id = (int)this.cmbMajor.SelectedValue;
            this.cmbClass.DisplayMember = "className";
            this.cmbClass.ValueMember = "Id";
            DataTable table = new ClassBLL().GetClassList(id);
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["className"] = "请选择班级";
            table.Rows.InsertAt(row, 0);
            this.cmbClass.DataSource = table;

        }

        private void MajorBind()
        {
            this.cmbMajor.DisplayMember = "majorName";
            this.cmbMajor.ValueMember = "Id";
            DataTable table = new MajorBLL().GetMajorList();
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["majorName"] = "请选择专业";
            table.Rows.InsertAt(row, 0);
            this.cmbMajor.DataSource = table;



        }

        private void FrmClassManagement_Load(object sender, EventArgs e)
        {
            MajorBind();
            this.uiPagination1.ActivePage = 1;

        }

        private void ClassTableBind()
        {
            Class class1 = new Class();
            class1.MajorName = this.cmbMajor.Text;
            class1.className = this.cmbClass.Text;
            int index = this.uiPagination1.ActivePage - 1;
            int size = this.uiPagination1.PageSize;
            int begin = (index * size) + 1;
            int end = (index + 1) * size;
            int result;

            this.dvgClassList.DataSource = new ClassBLL().GetClassList(begin, end, out result, class1);
            this.uiPagination1.TotalCount = result;
        }



        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            ClassTableBind();
        }

        private void cmbMajor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbMajor.SelectedIndex == 0)
            {
                this.cmbClass.DataSource = null;
                this.cmbClass.Items.Clear();
                this.cmbClass.Items.Insert(0, "请选择班级");
                this.cmbClass.SelectedIndex = 0;
                return;

            }
            ClassBind();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ClassTableBind();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            ClassTableBind();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Excel文件|*.xlsx;*.xls";
            saveFileDialog1.FileName = "班级信息";
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                //获取数据源
                DataTable table = new ClassBLL().GetClassList();
                //获取表格的标题
                List<string> title = new List<string>();
                foreach (DataGridViewColumn t in this.dvgClassList.Columns)
                {
                    if (t.HeaderText == "专业号") continue;
                    title.Add(t.HeaderText);
                }
                //获取文件路径
                string path = saveFileDialog1.FileName;
                //获取文件的名字、
                int i = path.LastIndexOf('\\');
                string name = path.Substring(i + 1);

                int m = NpoiHelper.ExportExcel(table, title, path, name);
                if (m == -1)
                {
                    ShowWarningDialog("数据为空");

                }
                else
                {
                    ShowSuccessDialog("写入成功");
                }
            }
        }




        private void btnClassAdd_Click(object sender, EventArgs e)
        {
            FrmClassEdit frmClassEdit = new FrmClassEdit();
            frmClassEdit.Text = "添加班级信息";
            DialogResult result = frmClassEdit.ShowDialog();
            if (result == DialogResult.OK)
            {
                frmClassEdit.Dispose();
                ClassTableBind();
            }
        }

        private void btnClassEdit_Click(object sender, EventArgs e)
        {
            if (this.dvgClassList.CurrentRow == null || this.dvgClassList.RowCount == 0)
            {
                ShowWarningDialog("没有可编辑的行");
                return;

            }
            Class class1 = new Class();
            class1.Id = (int)this.dvgClassList.SelectedRows[0].Cells["Id"].Value;
            class1.majorId = (int)this.dvgClassList.SelectedRows[0].Cells["majorId"].Value;
            class1.className = this.dvgClassList.SelectedRows[0].Cells["className"].Value.ToString();
            FrmClassEdit frmClassEdit = new FrmClassEdit();
            frmClassEdit.Text = "修改班级信息";
            frmClassEdit.Tag = class1;
            DialogResult result = frmClassEdit.ShowDialog();
            if (result == DialogResult.OK)
            {
                frmClassEdit.Dispose();
                ClassTableBind();
            }
        }

        private void btnClassDelete_Click(object sender, EventArgs e)
        {

            ShowWarningDialog("不支持删除");
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //显示选择面板
            FrmSelection frmSelection = new FrmSelection();
            frmSelection.Tag = this.dvgClassList.Columns;
            DialogResult dialogResult = frmSelection.ShowDialog();
            if (DialogResult.OK == dialogResult)
            {
                GridPrinter.StartPrint(this.dvgClassList, true, "班级信息");
            }
            //恢复原样
            foreach (DataGridViewColumn item in this.dvgClassList.Columns)
            {
                item.Visible = true;
            }

        }
    }
}
