﻿using BLL.Admin;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
namespace StudentManagement.Admin
{
    public partial class FrmPowerSetting : UIPage
    {
        public FrmPowerSetting()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 是否是第一次加载
        /// </summary>
        bool isLoad = false;

        /// <summary>
        /// 是否是第一次执行索引改变事件
        /// </summary>
        bool isFrist = false;

        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmPowerSetting_Load(object sender, EventArgs e)
        {
            if (isLoad == false)
            {
                // 1. 加载所有的角色
                RoleListLoad();
                //2.加载所有的菜单
                MeunListLoad();
                //3.修改isload状态
                isLoad = true;
                //4.修改提交按钮的状态
                this.btnSubmit.Enabled = false;
                //5.修改isFrist
                isFrist = true;
            }

        }
        /// <summary>
        /// 加载所有的菜单
        /// </summary>
        private void MeunListLoad()
        {
            // 1.获取所有的菜单信息
            List<Model.Menu> menus = new MenuBLL().GetMenuList2();
            //2。获取根菜单的信息
            Model.Menu menu = menus.Where(m => m.MenuParent == 0).ToList()[0];
            //设置显示checkBox框
            uiTreeView1.CheckBoxes = true;
            //3.给TreeView添加根,并接收返回来的rootNode对象
            TreeNode rootNode = uiTreeView1.Nodes.Add(menu.MenuId.ToString(), menu.MenuName);
            //4.加载当前节点所有的子节点
            CreateTreeNode(menus, rootNode, menu.MenuId);
            //5.展开所有的节点
            uiTreeView1.ExpandAll();
        }

        /// <summary>
        /// 加载当前节点下面的所有的子菜单
        /// </summary>
        /// <param name="menus"></param>
        /// <param name="rootNode"></param>
        /// <param name="menuId"></param>
        private void CreateTreeNode(List<Model.Menu> menus, TreeNode rootNode, int menuId)
        {
            //1.获取当前节点下面所有的子菜单
            var childMenu = menus.Where(m => m.MenuParent == menuId).ToList();
            //2,遍历所有的子菜单
            foreach (var item in childMenu)
            {
                if (rootNode == null)
                {
                    continue;
                }
                TreeNode treeNode = rootNode.Nodes.Add(item.MenuId.ToString(), item.MenuName);
                //使用递归，逐级递归，去寻找子节点并把它们加入到根节点中
                CreateTreeNode(menus, treeNode, item.MenuId);
            }
        }

        /// <summary>
        ///  加载所有的角色信息
        /// </summary>
        private void RoleListLoad()
        {
            //加载角色列表
            this.cmbRoleList.DisplayMember = "RoleName";
            this.cmbRoleList.ValueMember = "RoleId";
            DataTable dataTable = new RoleBLL().GetRoleList();
            DataRow row = dataTable.NewRow();
            row["RoleId"] = -1;
            row["RoleName"] = "请选择角色";
            dataTable.Rows.InsertAt(row, 0);
            this.cmbRoleList.DataSource = dataTable;
            //设置下拉框被选中为第一项
            this.cmbRoleList.SelectedIndex = 0;
        }


        /// <summary>
        /// 索引改变事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbRoleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //第一次执行不执行
            if (isFrist == false)
            {

                return;
            }

            //获取角色Id
            int roleId = (int)cmbRoleList.SelectedValue;
            //如果是请选择角色就不执行任何操作
            if (roleId == -1)
            {
                //全部不勾选
                CheckBoxSelectAll(uiTreeView1.Nodes, false);
                //设置按钮为不可选取
                btnSubmit.Enabled = false;
                return;
            }

            /*--------------根据roleId去获取对应的菜单信息------------------*/
            //1.获取对应角色的菜单信息
            List<Model.Menu> menus = new BLL.Admin.MenuBLL().GetMeunList(roleId);
            //2.先清空勾选
            CheckBoxSelectAll(uiTreeView1.Nodes, false);
            // 如果绑定了菜单项就执行，就去吧存在的菜单进行勾选
            if (menus.Count > 0)
            {
                //2.获取角色菜单中的Id
                List<int> menuIds = menus.Select(m => m.MenuId).ToList();
                //3.选中已经存在的菜单标题
                SelectCheckedNodes(uiTreeView1.Nodes[0].Nodes, menuIds);

            }
            //4.设置按钮为可选取的状态
            btnSubmit.Enabled = true;
        }

        /// <summary>
        /// 选中已经存在的菜单
        /// </summary>
        /// <param name="nodes">学生管理系统下面所有的节点</param>
        /// <param name="menuIds">所属角色的节点的Id集合</param>
        private void SelectCheckedNodes(TreeNodeCollection nodes, List<int> menuIds)
        {
            foreach (TreeNode node in nodes)
            {
                //如果对应角色的列表包含对应的字节，将这个节点设置为选中状态，node.name==key值
                if (menuIds.Contains(Convert.ToInt32(node.Name)))
                {
                    node.Checked = true;//设置为选中状态
                }
                //通过递归，把当前节点下面的节点进行勾选
                SelectCheckedNodes(node.Nodes, menuIds);
            }
        }

        /// <summary>
        /// 选中状态改变事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBoxSelectAll(uiTreeView1.Nodes, chkSelectAll.Checked);
        }
        /// <summary>
        /// 全选或者全部不选
        /// </summary>
        /// <param name="nodes">uiTreeView下面的节点</param>
        /// <param name="checked">复选框的选中状态</param>
        private void CheckBoxSelectAll(TreeNodeCollection nodes, bool @checked)
        {
            foreach (TreeNode item in nodes)
            {
                item.Checked = @checked;

                //使用递归去便利节点下面节点，让其选中或者不选中
                CheckBoxSelectAll(item.Nodes, @checked);
            }
        }

        /// <summary>
        /// uiTreeView 节点被选中事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiTreeView1_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Action == TreeViewAction.ByKeyboard || e.Action == TreeViewAction.ByMouse)
            {
                /*------------当选中的节点是子节点时，那么应该将它的父节点进行勾选-----------*/
                SetParentNodeCheckedState(e.Node);
                /*------------当选中的节点是父节点时，那么应该将它的子节点都进行勾选-----------*/
                SetChildNodeCheckedState(e.Node);

            }
        }

        /// <summary>
        /// 设置当前节点的子节点的状态
        /// </summary>
        /// <param name="node"></param>
        private void SetChildNodeCheckedState(TreeNode node)
        {
            //遍历当前节点的子节点的集合
            foreach (TreeNode treeNode in node.Nodes)
            {
                //设置子节点的勾选状态与父节点的状态一致
                treeNode.Checked = node.Checked;

                //使用递归，去让treenode下面的子字节和父节点保持一致
                SetChildNodeCheckedState(treeNode);
            }

        }

        /// <summary>
        /// 设置当前节点的父节点的状态
        /// </summary>
        /// <param name="node"></param>
        /// <param name="checked"></param>
        private void SetParentNodeCheckedState(TreeNode node)
        {
            //获取父节点
            TreeNode parentNode = node.Parent;
            //判断父节点是否为null
            if (parentNode != null)
            {
                //遍历当前父节点下的子节点，只要有一个被选中，那么就把父节点进行选中
                bool flag = false;

                foreach (TreeNode treeNode in parentNode.Nodes)
                {
                    if (treeNode.Checked == true)
                    {
                        flag = true;
                        break;
                    }
                }
                parentNode.Checked = flag;

                //使用递归，去递归当前父节点的父节点，让其也被选中或者取消选中
                SetParentNodeCheckedState(parentNode);

            }

        }

        /// <summary>
        /// 关闭按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2002);
        }

        /// <summary>
        /// 提交设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            //获取对应的角色
            int roleId = (int)cmbRoleList.SelectedValue;
            // 获取勾选的菜单的Id
            List<int> menuIds = new List<int>();
            GetCheckedMenuIds(uiTreeView1.Nodes, menuIds);
            if (roleId == -1) return;

            //判断管理员
            if (roleId == 0)
            {
                if (menuIds.Contains(29))
                {
                    menuIds.Remove(29);
                    ShowWarningDialog("学生个人信息是属于学生的专属功能，其它角色不能添加 ");
                }
                if (menuIds.Contains(30))
                {
                    menuIds.Remove(30);
                    ShowWarningDialog("教师个人信息是属于教师的专属功能，其它角色不能添加 ");
                }
                if (menuIds.Contains(31))
                {
                    menuIds.Remove(31);
                    ShowWarningDialog("学生选课功能是属于学生的专属功能，其它角色不能添加 ");
                }
            }

            //判断教师
            if (roleId == 1)
            {
                if (menuIds.Contains(29))
                {
                    menuIds.Remove(29);
                    ShowWarningDialog("学生个人信息是属于学生的专属功能，其它角色不能添加 ");
                }
                if (menuIds.Contains(10))
                {
                    menuIds.Remove(10);
                    ShowWarningDialog("管理员个人信息是属于管理员的专属功能，其它角色不能添加 ");
                }
                if (menuIds.Contains(31))
                {
                    menuIds.Remove(31);
                    ShowWarningDialog("学生选课功能是属于学生的专属功能，其它角色不能添加 ");
                }
            }

            //判断学生
            if (roleId == 2)
            {
                if (menuIds.Contains(10))
                {
                    menuIds.Remove(10);
                    ShowWarningDialog("管理员个人信息是属于管理员的专属功能，其它角色不能添加 ");
                }
                if (menuIds.Contains(30))
                {
                    menuIds.Remove(30);
                    ShowWarningDialog("教师个人信息是属于教师的专属功能，其它角色不能添加 ");
                }
            }

            //提交设置
            int result = new BLL.Admin.RoleMenuBLL().UpdateMenuList(menuIds, roleId);
            if (result > 0)
            {
                ShowSuccessDialog("设置成功");
            }
            else
            {
                ShowSuccessDialog("设置失败");
            }
        }


        /// <summary>
        /// 获取勾选的菜单Id
        /// </summary>
        /// <param name="nodes"></param>
        /// <returns></returns>
        private void GetCheckedMenuIds(TreeNodeCollection nodes, List<int> menuIds)
        {
            foreach (TreeNode node in nodes)
            {
                // 节点被选中
                if (node.Checked)
                {
                    int menuId = Convert.ToInt32(node.Name);
                    if (!menuIds.Contains(menuId))
                    {
                        menuIds.Add(menuId);
                    }
                }
                //使用递归获取当前节点的子节点的Id
                GetCheckedMenuIds(node.Nodes, menuIds);
            }

        }
    }
}
