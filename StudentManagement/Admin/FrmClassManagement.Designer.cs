﻿
namespace StudentManagement.Admin
{
    partial class FrmClassManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btnPrint = new Sunny.UI.UISymbolButton();
            this.btnExport = new Sunny.UI.UISymbolButton();
            this.btnClassDelete = new Sunny.UI.UISymbolButton();
            this.btnClassEdit = new Sunny.UI.UISymbolButton();
            this.btnClassAdd = new Sunny.UI.UISymbolButton();
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.cmbMajor = new Sunny.UI.UIComboBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.btnFind = new Sunny.UI.UIButton();
            this.cmbClass = new Sunny.UI.UIComboBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.btnRefresh = new Sunny.UI.UISymbolButton();
            this.dvgClassList = new Sunny.UI.UIDataGridView();
            this.uiPagination1 = new Sunny.UI.UIPagination();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.className = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.majorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collegeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.majorId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgClassList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrint.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnPrint.Location = new System.Drawing.Point(1014, 124);
            this.btnPrint.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnPrint.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnPrint.Size = new System.Drawing.Size(46, 35);
            this.btnPrint.Symbol = 57594;
            this.btnPrint.SymbolSize = 35;
            this.btnPrint.TabIndex = 118;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExport
            // 
            this.btnExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExport.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnExport.Location = new System.Drawing.Point(968, 124);
            this.btnExport.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnExport.Name = "btnExport";
            this.btnExport.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnExport.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnExport.Size = new System.Drawing.Size(46, 35);
            this.btnExport.Symbol = 61584;
            this.btnExport.SymbolSize = 35;
            this.btnExport.TabIndex = 117;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnClassDelete
            // 
            this.btnClassDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClassDelete.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnClassDelete.Location = new System.Drawing.Point(123, 124);
            this.btnClassDelete.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnClassDelete.Name = "btnClassDelete";
            this.btnClassDelete.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnClassDelete.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnClassDelete.Size = new System.Drawing.Size(46, 35);
            this.btnClassDelete.Symbol = 61544;
            this.btnClassDelete.SymbolSize = 35;
            this.btnClassDelete.TabIndex = 115;
            this.btnClassDelete.Click += new System.EventHandler(this.btnClassDelete_Click);
            // 
            // btnClassEdit
            // 
            this.btnClassEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClassEdit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnClassEdit.Location = new System.Drawing.Point(77, 124);
            this.btnClassEdit.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnClassEdit.Name = "btnClassEdit";
            this.btnClassEdit.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnClassEdit.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnClassEdit.Size = new System.Drawing.Size(46, 35);
            this.btnClassEdit.Symbol = 61508;
            this.btnClassEdit.SymbolSize = 35;
            this.btnClassEdit.TabIndex = 114;
            this.btnClassEdit.Click += new System.EventHandler(this.btnClassEdit_Click);
            // 
            // btnClassAdd
            // 
            this.btnClassAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClassAdd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnClassAdd.Location = new System.Drawing.Point(31, 124);
            this.btnClassAdd.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnClassAdd.Name = "btnClassAdd";
            this.btnClassAdd.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnClassAdd.Size = new System.Drawing.Size(46, 35);
            this.btnClassAdd.Symbol = 61543;
            this.btnClassAdd.SymbolSize = 35;
            this.btnClassAdd.TabIndex = 113;
            this.btnClassAdd.Click += new System.EventHandler(this.btnClassAdd_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.cmbMajor);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Controls.Add(this.btnFind);
            this.uiGroupBox1.Controls.Add(this.cmbClass);
            this.uiGroupBox1.Controls.Add(this.uiLabel2);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(31, 25);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(1032, 81);
            this.uiGroupBox1.TabIndex = 110;
            this.uiGroupBox1.Text = "查询";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbMajor
            // 
            this.cmbMajor.DataSource = null;
            this.cmbMajor.FillColor = System.Drawing.Color.White;
            this.cmbMajor.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbMajor.Location = new System.Drawing.Point(185, 31);
            this.cmbMajor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbMajor.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbMajor.Name = "cmbMajor";
            this.cmbMajor.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbMajor.Size = new System.Drawing.Size(150, 35);
            this.cmbMajor.TabIndex = 9;
            this.cmbMajor.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmbMajor.SelectedIndexChanged += new System.EventHandler(this.cmbMajor_SelectedIndexChanged);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(66, 31);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(83, 35);
            this.uiLabel1.TabIndex = 8;
            this.uiLabel1.Text = "专业:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnFind
            // 
            this.btnFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFind.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnFind.Location = new System.Drawing.Point(901, 31);
            this.btnFind.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(100, 35);
            this.btnFind.TabIndex = 7;
            this.btnFind.Text = "查询";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // cmbClass
            // 
            this.cmbClass.DataSource = null;
            this.cmbClass.FillColor = System.Drawing.Color.White;
            this.cmbClass.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbClass.Location = new System.Drawing.Point(596, 31);
            this.cmbClass.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbClass.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbClass.Name = "cmbClass";
            this.cmbClass.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbClass.Size = new System.Drawing.Size(150, 35);
            this.cmbClass.TabIndex = 6;
            this.cmbClass.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(522, 32);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(83, 35);
            this.uiLabel2.TabIndex = 2;
            this.uiLabel2.Text = "班级：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnRefresh.Location = new System.Drawing.Point(922, 124);
            this.btnRefresh.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnRefresh.Size = new System.Drawing.Size(46, 35);
            this.btnRefresh.Symbol = 61473;
            this.btnRefresh.SymbolSize = 35;
            this.btnRefresh.TabIndex = 116;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dvgClassList
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.dvgClassList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dvgClassList.BackgroundColor = System.Drawing.Color.White;
            this.dvgClassList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvgClassList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dvgClassList.ColumnHeadersHeight = 32;
            this.dvgClassList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dvgClassList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.className,
            this.classNum,
            this.majorName,
            this.collegeName,
            this.majorId});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvgClassList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dvgClassList.EnableHeadersVisualStyles = false;
            this.dvgClassList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.dvgClassList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.dvgClassList.Location = new System.Drawing.Point(31, 165);
            this.dvgClassList.Name = "dvgClassList";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvgClassList.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dvgClassList.RowHeadersWidth = 51;
            this.dvgClassList.RowHeight = 27;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.dvgClassList.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dvgClassList.RowTemplate.Height = 27;
            this.dvgClassList.SelectedIndex = -1;
            this.dvgClassList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvgClassList.ShowGridLine = true;
            this.dvgClassList.Size = new System.Drawing.Size(1032, 404);
            this.dvgClassList.TabIndex = 112;
            // 
            // uiPagination1
            // 
            this.uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPagination1.Location = new System.Drawing.Point(237, 587);
            this.uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPagination1.Name = "uiPagination1";
            this.uiPagination1.PageSize = 5;
            this.uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiPagination1.Size = new System.Drawing.Size(689, 43);
            this.uiPagination1.TabIndex = 111;
            this.uiPagination1.Text = "uiPagination1";
            this.uiPagination1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPagination1.PageChanged += new Sunny.UI.UIPagination.OnPageChangeEventHandler(this.uiPagination1_PageChanged);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "班级号";
            this.Id.MinimumWidth = 6;
            this.Id.Name = "Id";
            this.Id.Width = 125;
            // 
            // className
            // 
            this.className.DataPropertyName = "className";
            this.className.HeaderText = "班级";
            this.className.MinimumWidth = 6;
            this.className.Name = "className";
            this.className.Width = 125;
            // 
            // classNum
            // 
            this.classNum.DataPropertyName = "classNum";
            this.classNum.HeaderText = "人数";
            this.classNum.MinimumWidth = 6;
            this.classNum.Name = "classNum";
            this.classNum.Width = 125;
            // 
            // majorName
            // 
            this.majorName.DataPropertyName = "majorName";
            this.majorName.HeaderText = "专业";
            this.majorName.MinimumWidth = 6;
            this.majorName.Name = "majorName";
            this.majorName.Width = 125;
            // 
            // collegeName
            // 
            this.collegeName.DataPropertyName = "collegeName";
            this.collegeName.HeaderText = "学院";
            this.collegeName.MinimumWidth = 6;
            this.collegeName.Name = "collegeName";
            this.collegeName.Width = 125;
            // 
            // majorId
            // 
            this.majorId.DataPropertyName = "majorId";
            this.majorId.HeaderText = "专业号";
            this.majorId.MinimumWidth = 6;
            this.majorId.Name = "majorId";
            this.majorId.Visible = false;
            this.majorId.Width = 125;
            // 
            // FrmClassManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 655);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnClassDelete);
            this.Controls.Add(this.btnClassEdit);
            this.Controls.Add(this.btnClassAdd);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.dvgClassList);
            this.Controls.Add(this.uiPagination1);
            this.Name = "FrmClassManagement";
            this.Text = "班级信息管理";
            this.Load += new System.EventHandler(this.FrmClassManagement_Load);
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvgClassList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Sunny.UI.UISymbolButton btnPrint;
        private Sunny.UI.UISymbolButton btnExport;
        private Sunny.UI.UISymbolButton btnClassDelete;
        private Sunny.UI.UISymbolButton btnClassEdit;
        private Sunny.UI.UISymbolButton btnClassAdd;
        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UIButton btnFind;
        private Sunny.UI.UIComboBox cmbClass;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UISymbolButton btnRefresh;
        private Sunny.UI.UIDataGridView dvgClassList;
        private Sunny.UI.UIPagination uiPagination1;
        private Sunny.UI.UIComboBox cmbMajor;
        private Sunny.UI.UILabel uiLabel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn className;
        private System.Windows.Forms.DataGridViewTextBoxColumn classNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn majorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn collegeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn majorId;
    }
}