﻿using BLL.Admin;
using Model;
using StudentManagement.Admin.EditForm;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace StudentManagement.Admin
{
    public partial class FrmEventManagement : UIPage
    {
        public FrmEventManagement()
        {
            InitializeComponent();
        }

        private void FrmEventManagement_Load(object sender, EventArgs e)
        {
            StudentBind();
            this.uiPagination1.ActivePage = 1;
        }

        private void StudentBind()
        {
            {
                this.cmbStudent.DisplayMember = "studentName";
                this.cmbStudent.ValueMember = "Id";
                DataTable table = new StudentBLL().GetStudentList1();
                DataRow row = table.NewRow();
                row["Id"] = 0;
                row["studentName"] = "请选择姓名";
                table.Rows.InsertAt(row, 0);
                this.cmbStudent.DataSource = table;
            }
        }
        private void EventBindTable()
        {
            int index = this.uiPagination1.ActivePage - 1;
            int size = this.uiPagination1.PageSize;
            int begin = index * size + 1;
            int end = (index + 1) * size;
            //总行数
            int count;
            int studentName = (int)this.cmbStudent.SelectedValue;
            this.dgvEventList.DataSource = new EventBLL().GetEventList(begin, end, out count, studentName);
            //设置行数
            this.uiPagination1.TotalCount = count;
        }

        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            EventBindTable();
        }

        private void btnStudentAdd_Click(object sender, EventArgs e)
        {
            FrmEventEdit frmEventEdit = new FrmEventEdit();
            frmEventEdit.Text = "添加奖惩信息";
            DialogResult result = frmEventEdit.ShowDialog();
            if (result == DialogResult.Yes)
            {
                frmEventEdit.Dispose();
                EventBindTable();
            }

        }

        private void btnStudentEdit_Click(object sender, EventArgs e)
        {
            if (this.dgvEventList.CurrentRow == null || this.dgvEventList.RowCount == 0)
            {
                ShowWarningDialog("没有可编辑的数据");
                return;
            }

            Event event1 = new Event();
            event1.Id = (int)this.dgvEventList.SelectedRows[0].Cells[0].Value;
            event1.StuId = (int)this.dgvEventList.SelectedRows[0].Cells["stuId"].Value;
            event1.EventType = this.dgvEventList.SelectedRows[0].Cells["eventType"].Value.ToString();
            event1.StudentName = this.dgvEventList.SelectedRows[0].Cells["studentName"].Value.ToString();
            event1.EventDescribe = this.dgvEventList.SelectedRows[0].Cells["eventDescribe"].Value.ToString();
            event1.EventResult = this.dgvEventList.SelectedRows[0].Cells["eventResult"].Value.ToString();
            event1.EventTime = Convert.ToDateTime(this.dgvEventList.SelectedRows[0].Cells["eventTime"].Value);
            FrmEventEdit frmEventEdit = new FrmEventEdit();
            frmEventEdit.Text = "修改奖惩信息";
            frmEventEdit.Tag = event1;
            DialogResult result = frmEventEdit.ShowDialog();
            if (result == DialogResult.Yes)
            {
                frmEventEdit.Dispose();
                EventBindTable();
            }
        }

        private void btnStudentDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvEventList.CurrentRow == null || this.dgvEventList.RowCount == 0)
            {
                ShowWarningDialog("没有可删除的数据");
                return;
            }
            int Id = (int)this.dgvEventList.SelectedRows[0].Cells["Id"].Value;
            int result = new EventBLL().DeleteEvent(Id);
            if (result == 0)
            {
                ShowWarningDialog("删除失败");
            }
            else
            {

                ShowSuccessDialog("删除成功");
                EventBindTable();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Excel文件|*.xlsx;*.xls";
            saveFileDialog1.FileName = "奖惩信息";
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                //获取数据源
                DataTable table = new EventBLL().GetEventList();
                //获取表格的标题
                List<string> title = new List<string>();
                foreach (DataGridViewColumn t in this.dgvEventList.Columns)
                {
                    if (t.HeaderText == "学号") continue;

                    title.Add(t.HeaderText);
                }
                //获取文件路径
                string path = saveFileDialog1.FileName;
                //获取文件的名字、
                int i = path.LastIndexOf('\\');
                string name = path.Substring(i + 1);
                int m = NpoiHelper.ExportExcel(table, title, path, name);
                if (m == -1)
                {
                    ShowWarningDialog("数据为空");

                }
                else
                {
                    ShowSuccessDialog("写入成功");
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            EventBindTable();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            EventBindTable();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //显示选择面板
            FrmSelection frmSelection = new FrmSelection();
            frmSelection.Tag = this.dgvEventList.Columns;
            DialogResult dialogResult = frmSelection.ShowDialog();
            if (DialogResult.OK == dialogResult)
            {
                GridPrinter.StartPrint(this.dgvEventList, true, "奖惩信息");
            }
            //恢复原样
            foreach (DataGridViewColumn item in this.dgvEventList.Columns)
            {
                item.Visible = true;
            }

        }
    }
}
