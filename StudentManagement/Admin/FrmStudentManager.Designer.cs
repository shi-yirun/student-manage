﻿
namespace StudentManagement.Admin
{
    partial class FrmStudentManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.btnFind = new Sunny.UI.UIButton();
            this.cmbClass = new Sunny.UI.UIComboBox();
            this.txtName = new Sunny.UI.UITextBox();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.txtStuNo = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiPagination1 = new Sunny.UI.UIPagination();
            this.dgvStudentList = new Sunny.UI.UIDataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentSex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentStart_year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentFinish_year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentNation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentBirthday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentDegree = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentImage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentTel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentPwd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collegeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.majorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.className = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentMajorId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoleId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentCollegeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnStudentDelete = new Sunny.UI.UISymbolButton();
            this.btnStudentEdit = new Sunny.UI.UISymbolButton();
            this.btnStudentAdd = new Sunny.UI.UISymbolButton();
            this.btnPrint = new Sunny.UI.UISymbolButton();
            this.btnExport = new Sunny.UI.UISymbolButton();
            this.btnRefresh = new Sunny.UI.UISymbolButton();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudentList)).BeginInit();
            this.SuspendLayout();
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.btnFind);
            this.uiGroupBox1.Controls.Add(this.cmbClass);
            this.uiGroupBox1.Controls.Add(this.txtName);
            this.uiGroupBox1.Controls.Add(this.uiLabel3);
            this.uiGroupBox1.Controls.Add(this.uiLabel2);
            this.uiGroupBox1.Controls.Add(this.txtStuNo);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(34, 14);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(1032, 81);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.Text = "查询";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnFind
            // 
            this.btnFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFind.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnFind.Location = new System.Drawing.Point(901, 31);
            this.btnFind.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(100, 35);
            this.btnFind.TabIndex = 7;
            this.btnFind.Text = "查询";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // cmbClass
            // 
            this.cmbClass.DataSource = null;
            this.cmbClass.FillColor = System.Drawing.Color.White;
            this.cmbClass.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbClass.Location = new System.Drawing.Point(722, 31);
            this.cmbClass.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbClass.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbClass.Name = "cmbClass";
            this.cmbClass.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbClass.Size = new System.Drawing.Size(150, 35);
            this.cmbClass.TabIndex = 6;
            this.cmbClass.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtName
            // 
            this.txtName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtName.FillColor = System.Drawing.Color.White;
            this.txtName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtName.Location = new System.Drawing.Point(433, 32);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtName.Maximum = 2147483647D;
            this.txtName.Minimum = -2147483648D;
            this.txtName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(148, 35);
            this.txtName.TabIndex = 5;
            this.txtName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(328, 31);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(76, 35);
            this.uiLabel3.TabIndex = 4;
            this.uiLabel3.Text = "姓名：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(610, 32);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(83, 35);
            this.uiLabel2.TabIndex = 2;
            this.uiLabel2.Text = "班级：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtStuNo
            // 
            this.txtStuNo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtStuNo.FillColor = System.Drawing.Color.White;
            this.txtStuNo.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtStuNo.Location = new System.Drawing.Point(111, 32);
            this.txtStuNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtStuNo.Maximum = 2147483647D;
            this.txtStuNo.Minimum = -2147483648D;
            this.txtStuNo.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtStuNo.Name = "txtStuNo";
            this.txtStuNo.Size = new System.Drawing.Size(188, 35);
            this.txtStuNo.TabIndex = 1;
            this.txtStuNo.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(24, 31);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(80, 35);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "学号：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiPagination1
            // 
            this.uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPagination1.Location = new System.Drawing.Point(240, 576);
            this.uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPagination1.Name = "uiPagination1";
            this.uiPagination1.PageSize = 5;
            this.uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiPagination1.Size = new System.Drawing.Size(689, 43);
            this.uiPagination1.TabIndex = 3;
            this.uiPagination1.Text = "uiPagination1";
            this.uiPagination1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPagination1.PageChanged += new Sunny.UI.UIPagination.OnPageChangeEventHandler(this.uiPagination1_PageChanged);
            // 
            // dgvStudentList
            // 
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.dgvStudentList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvStudentList.BackgroundColor = System.Drawing.Color.White;
            this.dgvStudentList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStudentList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvStudentList.ColumnHeadersHeight = 32;
            this.dgvStudentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvStudentList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.studentNo,
            this.studentName,
            this.studentSex,
            this.studentStart_year,
            this.studentFinish_year,
            this.studentNation,
            this.studentBirthday,
            this.studentDegree,
            this.studentImage,
            this.studentTel,
            this.studentEmail,
            this.studentPwd,
            this.collegeName,
            this.majorName,
            this.className,
            this.classId,
            this.studentMajorId,
            this.RoleId,
            this.studentCollegeId});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvStudentList.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgvStudentList.EnableHeadersVisualStyles = false;
            this.dgvStudentList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.dgvStudentList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.dgvStudentList.Location = new System.Drawing.Point(34, 154);
            this.dgvStudentList.Name = "dgvStudentList";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStudentList.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgvStudentList.RowHeadersWidth = 51;
            this.dgvStudentList.RowHeight = 27;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            this.dgvStudentList.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvStudentList.RowTemplate.Height = 27;
            this.dgvStudentList.SelectedIndex = -1;
            this.dgvStudentList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStudentList.ShowGridLine = true;
            this.dgvStudentList.Size = new System.Drawing.Size(1032, 404);
            this.dgvStudentList.TabIndex = 4;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "序号";
            this.Id.MinimumWidth = 6;
            this.Id.Name = "Id";
            this.Id.Width = 125;
            // 
            // studentNo
            // 
            this.studentNo.DataPropertyName = "StudentNo";
            this.studentNo.HeaderText = "学号";
            this.studentNo.MinimumWidth = 6;
            this.studentNo.Name = "studentNo";
            this.studentNo.Width = 125;
            // 
            // studentName
            // 
            this.studentName.DataPropertyName = "StudentName";
            this.studentName.HeaderText = "名字";
            this.studentName.MinimumWidth = 6;
            this.studentName.Name = "studentName";
            this.studentName.Width = 125;
            // 
            // studentSex
            // 
            this.studentSex.DataPropertyName = "StudentSex";
            this.studentSex.HeaderText = "性别";
            this.studentSex.MinimumWidth = 6;
            this.studentSex.Name = "studentSex";
            this.studentSex.Width = 125;
            // 
            // studentStart_year
            // 
            this.studentStart_year.DataPropertyName = "StudentStart_year";
            this.studentStart_year.HeaderText = "入学年份";
            this.studentStart_year.MinimumWidth = 6;
            this.studentStart_year.Name = "studentStart_year";
            this.studentStart_year.Width = 125;
            // 
            // studentFinish_year
            // 
            this.studentFinish_year.DataPropertyName = "StudentFinish_year";
            this.studentFinish_year.HeaderText = "毕业年份";
            this.studentFinish_year.MinimumWidth = 6;
            this.studentFinish_year.Name = "studentFinish_year";
            this.studentFinish_year.Width = 125;
            // 
            // studentNation
            // 
            this.studentNation.DataPropertyName = "StudentNation";
            this.studentNation.HeaderText = "民族";
            this.studentNation.MinimumWidth = 6;
            this.studentNation.Name = "studentNation";
            this.studentNation.Width = 125;
            // 
            // studentBirthday
            // 
            this.studentBirthday.DataPropertyName = "StudentBirthday";
            this.studentBirthday.HeaderText = "出生日期";
            this.studentBirthday.MinimumWidth = 6;
            this.studentBirthday.Name = "studentBirthday";
            this.studentBirthday.Width = 125;
            // 
            // studentDegree
            // 
            this.studentDegree.DataPropertyName = "StudentDegree";
            this.studentDegree.HeaderText = "学历";
            this.studentDegree.MinimumWidth = 6;
            this.studentDegree.Name = "studentDegree";
            this.studentDegree.Width = 125;
            // 
            // studentImage
            // 
            this.studentImage.DataPropertyName = "StudentImage";
            this.studentImage.HeaderText = "学生照片";
            this.studentImage.MinimumWidth = 6;
            this.studentImage.Name = "studentImage";
            this.studentImage.Width = 125;
            // 
            // studentTel
            // 
            this.studentTel.DataPropertyName = "studentTel";
            this.studentTel.HeaderText = "电话号码";
            this.studentTel.MinimumWidth = 6;
            this.studentTel.Name = "studentTel";
            this.studentTel.Width = 125;
            // 
            // studentEmail
            // 
            this.studentEmail.DataPropertyName = "StudentEmail";
            this.studentEmail.HeaderText = "邮件";
            this.studentEmail.MinimumWidth = 6;
            this.studentEmail.Name = "studentEmail";
            this.studentEmail.Width = 125;
            // 
            // studentPwd
            // 
            this.studentPwd.DataPropertyName = "StudentPwd";
            this.studentPwd.HeaderText = "密码";
            this.studentPwd.MinimumWidth = 6;
            this.studentPwd.Name = "studentPwd";
            this.studentPwd.Width = 125;
            // 
            // collegeName
            // 
            this.collegeName.DataPropertyName = "CollegeName";
            this.collegeName.HeaderText = "学院";
            this.collegeName.MinimumWidth = 6;
            this.collegeName.Name = "collegeName";
            this.collegeName.Width = 125;
            // 
            // majorName
            // 
            this.majorName.DataPropertyName = "MajorName";
            this.majorName.HeaderText = "专业";
            this.majorName.MinimumWidth = 6;
            this.majorName.Name = "majorName";
            this.majorName.Width = 125;
            // 
            // className
            // 
            this.className.DataPropertyName = "ClassName";
            this.className.HeaderText = "班级";
            this.className.MinimumWidth = 6;
            this.className.Name = "className";
            this.className.Width = 125;
            // 
            // classId
            // 
            this.classId.DataPropertyName = "ClassId";
            this.classId.HeaderText = "班级号";
            this.classId.MinimumWidth = 6;
            this.classId.Name = "classId";
            this.classId.Visible = false;
            this.classId.Width = 125;
            // 
            // studentMajorId
            // 
            this.studentMajorId.DataPropertyName = "StudentMajorId";
            this.studentMajorId.HeaderText = "专业号";
            this.studentMajorId.MinimumWidth = 6;
            this.studentMajorId.Name = "studentMajorId";
            this.studentMajorId.Visible = false;
            this.studentMajorId.Width = 125;
            // 
            // RoleId
            // 
            this.RoleId.DataPropertyName = "RoleId";
            this.RoleId.HeaderText = "角色号";
            this.RoleId.MinimumWidth = 6;
            this.RoleId.Name = "RoleId";
            this.RoleId.Visible = false;
            this.RoleId.Width = 125;
            // 
            // studentCollegeId
            // 
            this.studentCollegeId.DataPropertyName = "StudentCollegeId";
            this.studentCollegeId.HeaderText = "学院号";
            this.studentCollegeId.MinimumWidth = 6;
            this.studentCollegeId.Name = "studentCollegeId";
            this.studentCollegeId.Visible = false;
            this.studentCollegeId.Width = 125;
            // 
            // btnStudentDelete
            // 
            this.btnStudentDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStudentDelete.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnStudentDelete.Location = new System.Drawing.Point(126, 113);
            this.btnStudentDelete.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnStudentDelete.Name = "btnStudentDelete";
            this.btnStudentDelete.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnStudentDelete.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnStudentDelete.Size = new System.Drawing.Size(46, 35);
            this.btnStudentDelete.Symbol = 61544;
            this.btnStudentDelete.SymbolSize = 35;
            this.btnStudentDelete.TabIndex = 106;
            this.btnStudentDelete.Click += new System.EventHandler(this.btnStudentDelete_Click);
            // 
            // btnStudentEdit
            // 
            this.btnStudentEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStudentEdit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnStudentEdit.Location = new System.Drawing.Point(80, 113);
            this.btnStudentEdit.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnStudentEdit.Name = "btnStudentEdit";
            this.btnStudentEdit.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnStudentEdit.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnStudentEdit.Size = new System.Drawing.Size(46, 35);
            this.btnStudentEdit.Symbol = 61508;
            this.btnStudentEdit.SymbolSize = 35;
            this.btnStudentEdit.TabIndex = 105;
            this.btnStudentEdit.Click += new System.EventHandler(this.btnStudentEdit_Click);
            // 
            // btnStudentAdd
            // 
            this.btnStudentAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStudentAdd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnStudentAdd.Location = new System.Drawing.Point(34, 113);
            this.btnStudentAdd.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnStudentAdd.Name = "btnStudentAdd";
            this.btnStudentAdd.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnStudentAdd.Size = new System.Drawing.Size(46, 35);
            this.btnStudentAdd.Symbol = 61543;
            this.btnStudentAdd.SymbolSize = 35;
            this.btnStudentAdd.TabIndex = 104;
            this.btnStudentAdd.Click += new System.EventHandler(this.btnStudentAdd_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrint.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnPrint.Location = new System.Drawing.Point(1017, 113);
            this.btnPrint.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnPrint.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnPrint.Size = new System.Drawing.Size(46, 35);
            this.btnPrint.Symbol = 57594;
            this.btnPrint.SymbolSize = 35;
            this.btnPrint.TabIndex = 109;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExport
            // 
            this.btnExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExport.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnExport.Location = new System.Drawing.Point(971, 113);
            this.btnExport.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnExport.Name = "btnExport";
            this.btnExport.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnExport.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnExport.Size = new System.Drawing.Size(46, 35);
            this.btnExport.Symbol = 61584;
            this.btnExport.SymbolSize = 35;
            this.btnExport.TabIndex = 108;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnRefresh.Location = new System.Drawing.Point(925, 113);
            this.btnRefresh.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnRefresh.Size = new System.Drawing.Size(46, 35);
            this.btnRefresh.Symbol = 61473;
            this.btnRefresh.SymbolSize = 35;
            this.btnRefresh.TabIndex = 107;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // FrmStudentManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 633);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnStudentDelete);
            this.Controls.Add(this.btnStudentEdit);
            this.Controls.Add(this.btnStudentAdd);
            this.Controls.Add(this.dgvStudentList);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.uiPagination1);
            this.Name = "FrmStudentManager";
            this.Text = "学生信息管理";
            this.Load += new System.EventHandler(this.FrmStudentManager_Load);
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudentList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UIPagination uiPagination1;
        private Sunny.UI.UIDataGridView dgvStudentList;
        private Sunny.UI.UIButton btnFind;
        private Sunny.UI.UIComboBox cmbClass;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UISymbolButton btnStudentDelete;
        private Sunny.UI.UISymbolButton btnStudentEdit;
        private Sunny.UI.UISymbolButton btnStudentAdd;
        private Sunny.UI.UISymbolButton btnPrint;
        private Sunny.UI.UISymbolButton btnExport;
        private Sunny.UI.UISymbolButton btnRefresh;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Sunny.UI.UITextBox txtName;
        private Sunny.UI.UITextBox txtStuNo;
        private Sunny.UI.UILabel uiLabel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentSex;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentStart_year;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentFinish_year;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentNation;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentBirthday;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentDegree;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentTel;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentPwd;
        private System.Windows.Forms.DataGridViewTextBoxColumn collegeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn majorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn className;
        private System.Windows.Forms.DataGridViewTextBoxColumn classId;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentMajorId;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoleId;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentCollegeId;
    }
}