﻿
namespace StudentManagement.Admin
{
    partial class FrmAdminInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtAdminAcount = new Sunny.UI.UITextBox();
            this.uiLabel11 = new Sunny.UI.UILabel();
            this.uiGroupBox2 = new Sunny.UI.UIGroupBox();
            this.userImage = new System.Windows.Forms.PictureBox();
            this.btnFileUpload = new Sunny.UI.UISymbolButton();
            this.txtEmail = new Sunny.UI.UITextBox();
            this.txtTel = new Sunny.UI.UITextBox();
            this.radbGirl = new Sunny.UI.UIRadioButton();
            this.radbBoy = new Sunny.UI.UIRadioButton();
            this.uiLabel13 = new Sunny.UI.UILabel();
            this.uiLabel12 = new Sunny.UI.UILabel();
            this.uiLabel7 = new Sunny.UI.UILabel();
            this.txtAdiminName = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnSubmit = new Sunny.UI.UIButton();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userImage)).BeginInit();
            this.SuspendLayout();
            // 
            // txtAdminAcount
            // 
            this.txtAdminAcount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtAdminAcount.FillColor = System.Drawing.Color.White;
            this.txtAdminAcount.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtAdminAcount.Location = new System.Drawing.Point(446, 142);
            this.txtAdminAcount.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtAdminAcount.Maximum = 2147483647D;
            this.txtAdminAcount.Minimum = -2147483648D;
            this.txtAdminAcount.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtAdminAcount.Name = "txtAdminAcount";
            this.txtAdminAcount.ReadOnly = true;
            this.txtAdminAcount.Size = new System.Drawing.Size(219, 34);
            this.txtAdminAcount.TabIndex = 72;
            this.txtAdminAcount.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel11
            // 
            this.uiLabel11.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel11.Location = new System.Drawing.Point(308, 153);
            this.uiLabel11.Name = "uiLabel11";
            this.uiLabel11.Size = new System.Drawing.Size(76, 23);
            this.uiLabel11.TabIndex = 71;
            this.uiLabel11.Text = "账号：";
            this.uiLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Controls.Add(this.userImage);
            this.uiGroupBox2.Controls.Add(this.btnFileUpload);
            this.uiGroupBox2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox2.Location = new System.Drawing.Point(721, 126);
            this.uiGroupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox2.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox2.Size = new System.Drawing.Size(241, 254);
            this.uiGroupBox2.TabIndex = 70;
            this.uiGroupBox2.Text = "头像";
            this.uiGroupBox2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // userImage
            // 
            this.userImage.Location = new System.Drawing.Point(12, 35);
            this.userImage.Name = "userImage";
            this.userImage.Size = new System.Drawing.Size(210, 153);
            this.userImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.userImage.TabIndex = 74;
            this.userImage.TabStop = false;
            // 
            // btnFileUpload
            // 
            this.btnFileUpload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFileUpload.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnFileUpload.Location = new System.Drawing.Point(12, 194);
            this.btnFileUpload.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnFileUpload.Name = "btnFileUpload";
            this.btnFileUpload.Padding = new System.Windows.Forms.Padding(28, 0, 0, 0);
            this.btnFileUpload.Size = new System.Drawing.Size(210, 35);
            this.btnFileUpload.Symbol = 61717;
            this.btnFileUpload.TabIndex = 73;
            this.btnFileUpload.Text = "选择文件夹";
            this.btnFileUpload.Click += new System.EventHandler(this.btnFileUpload_Click);
            // 
            // txtEmail
            // 
            this.txtEmail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEmail.FillColor = System.Drawing.Color.White;
            this.txtEmail.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtEmail.Location = new System.Drawing.Point(446, 466);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEmail.Maximum = 2147483647D;
            this.txtEmail.Minimum = -2147483648D;
            this.txtEmail.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(219, 34);
            this.txtEmail.TabIndex = 69;
            this.txtEmail.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTel
            // 
            this.txtTel.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtTel.FillColor = System.Drawing.Color.White;
            this.txtTel.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtTel.Location = new System.Drawing.Point(446, 385);
            this.txtTel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtTel.Maximum = 2147483647D;
            this.txtTel.Minimum = -2147483648D;
            this.txtTel.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(219, 34);
            this.txtTel.TabIndex = 68;
            this.txtTel.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radbGirl
            // 
            this.radbGirl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radbGirl.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.radbGirl.Location = new System.Drawing.Point(602, 309);
            this.radbGirl.MinimumSize = new System.Drawing.Size(1, 1);
            this.radbGirl.Name = "radbGirl";
            this.radbGirl.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.radbGirl.Size = new System.Drawing.Size(63, 29);
            this.radbGirl.TabIndex = 67;
            this.radbGirl.Text = "女";
            // 
            // radbBoy
            // 
            this.radbBoy.Checked = true;
            this.radbBoy.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radbBoy.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.radbBoy.Location = new System.Drawing.Point(446, 309);
            this.radbBoy.MinimumSize = new System.Drawing.Size(1, 1);
            this.radbBoy.Name = "radbBoy";
            this.radbBoy.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.radbBoy.Size = new System.Drawing.Size(83, 29);
            this.radbBoy.TabIndex = 66;
            this.radbBoy.Text = "男";
            // 
            // uiLabel13
            // 
            this.uiLabel13.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel13.Location = new System.Drawing.Point(308, 477);
            this.uiLabel13.Name = "uiLabel13";
            this.uiLabel13.Size = new System.Drawing.Size(76, 23);
            this.uiLabel13.TabIndex = 65;
            this.uiLabel13.Text = "邮箱:";
            this.uiLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel12
            // 
            this.uiLabel12.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel12.Location = new System.Drawing.Point(308, 396);
            this.uiLabel12.Name = "uiLabel12";
            this.uiLabel12.Size = new System.Drawing.Size(76, 23);
            this.uiLabel12.TabIndex = 64;
            this.uiLabel12.Text = "电话:";
            this.uiLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel7
            // 
            this.uiLabel7.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel7.Location = new System.Drawing.Point(308, 315);
            this.uiLabel7.Name = "uiLabel7";
            this.uiLabel7.Size = new System.Drawing.Size(76, 23);
            this.uiLabel7.TabIndex = 63;
            this.uiLabel7.Text = "性别：";
            this.uiLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAdiminName
            // 
            this.txtAdiminName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtAdiminName.FillColor = System.Drawing.Color.White;
            this.txtAdiminName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtAdiminName.Location = new System.Drawing.Point(446, 223);
            this.txtAdiminName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtAdiminName.Maximum = 2147483647D;
            this.txtAdiminName.Minimum = -2147483648D;
            this.txtAdiminName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtAdiminName.Name = "txtAdiminName";
            this.txtAdiminName.Size = new System.Drawing.Size(219, 34);
            this.txtAdiminName.TabIndex = 62;
            this.txtAdiminName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(308, 234);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(76, 23);
            this.uiLabel1.TabIndex = 61;
            this.uiLabel1.Text = "姓名：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSubmit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnSubmit.ForeSelectedColor = System.Drawing.Color.Empty;
            this.btnSubmit.Location = new System.Drawing.Point(313, 574);
            this.btnSubmit.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.RectSelectedColor = System.Drawing.Color.Empty;
            this.btnSubmit.Size = new System.Drawing.Size(603, 35);
            this.btnSubmit.StyleCustomMode = true;
            this.btnSubmit.TabIndex = 73;
            this.btnSubmit.Text = "提交";
            this.btnSubmit.Click += new System.EventHandler(this.uiButton11_Click);
            // 
            // FrmAdminInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1172, 835);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.txtAdminAcount);
            this.Controls.Add(this.uiLabel11);
            this.Controls.Add(this.uiGroupBox2);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtTel);
            this.Controls.Add(this.radbGirl);
            this.Controls.Add(this.radbBoy);
            this.Controls.Add(this.uiLabel13);
            this.Controls.Add(this.uiLabel12);
            this.Controls.Add(this.uiLabel7);
            this.Controls.Add(this.txtAdiminName);
            this.Controls.Add(this.uiLabel1);
            this.Name = "FrmAdminInfo";
            this.Text = "管理员个人信息";
            this.Load += new System.EventHandler(this.FrmAdminInfo_Load);
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.userImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UITextBox txtAdminAcount;
        private Sunny.UI.UILabel uiLabel11;
        private Sunny.UI.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.PictureBox userImage;
        private Sunny.UI.UISymbolButton btnFileUpload;
        private Sunny.UI.UITextBox txtEmail;
        private Sunny.UI.UITextBox txtTel;
        private Sunny.UI.UIRadioButton radbGirl;
        private Sunny.UI.UIRadioButton radbBoy;
        private Sunny.UI.UILabel uiLabel13;
        private Sunny.UI.UILabel uiLabel12;
        private Sunny.UI.UILabel uiLabel7;
        private Sunny.UI.UITextBox txtAdiminName;
        private Sunny.UI.UILabel uiLabel1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private Sunny.UI.UIButton btnSubmit;
    }
}