﻿
namespace StudentManagement.Admin
{
    partial class FrmCourseManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btnPrint = new Sunny.UI.UISymbolButton();
            this.btnExport = new Sunny.UI.UISymbolButton();
            this.btnCourseDelete = new Sunny.UI.UISymbolButton();
            this.btnCourseEdit = new Sunny.UI.UISymbolButton();
            this.btnCourseAdd = new Sunny.UI.UISymbolButton();
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.btnFind = new Sunny.UI.UIButton();
            this.cmbCourse = new Sunny.UI.UIComboBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.btnRefresh = new Sunny.UI.UISymbolButton();
            this.dgvCourseList = new Sunny.UI.UIDataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CourseName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CourseGrade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.course_start_time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.course_end_time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.course_desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uiPagination1 = new Sunny.UI.UIPagination();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCourseList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            this.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrint.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnPrint.Location = new System.Drawing.Point(1007, 122);
            this.btnPrint.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnPrint.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnPrint.Size = new System.Drawing.Size(46, 35);
            this.btnPrint.Symbol = 57594;
            this.btnPrint.SymbolSize = 35;
            this.btnPrint.TabIndex = 118;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnExport
            // 
            this.btnExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExport.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnExport.Location = new System.Drawing.Point(961, 122);
            this.btnExport.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnExport.Name = "btnExport";
            this.btnExport.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnExport.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnExport.Size = new System.Drawing.Size(46, 35);
            this.btnExport.Symbol = 61584;
            this.btnExport.SymbolSize = 35;
            this.btnExport.TabIndex = 117;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnCourseDelete
            // 
            this.btnCourseDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCourseDelete.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnCourseDelete.Location = new System.Drawing.Point(116, 122);
            this.btnCourseDelete.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnCourseDelete.Name = "btnCourseDelete";
            this.btnCourseDelete.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnCourseDelete.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnCourseDelete.Size = new System.Drawing.Size(46, 35);
            this.btnCourseDelete.Symbol = 61544;
            this.btnCourseDelete.SymbolSize = 35;
            this.btnCourseDelete.TabIndex = 115;
            this.btnCourseDelete.Click += new System.EventHandler(this.btnCourseDelete_Click);
            // 
            // btnCourseEdit
            // 
            this.btnCourseEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCourseEdit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnCourseEdit.Location = new System.Drawing.Point(70, 122);
            this.btnCourseEdit.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnCourseEdit.Name = "btnCourseEdit";
            this.btnCourseEdit.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnCourseEdit.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnCourseEdit.Size = new System.Drawing.Size(46, 35);
            this.btnCourseEdit.Symbol = 61508;
            this.btnCourseEdit.SymbolSize = 35;
            this.btnCourseEdit.TabIndex = 114;
            this.btnCourseEdit.Click += new System.EventHandler(this.btnCourseEdit_Click);
            // 
            // btnCourseAdd
            // 
            this.btnCourseAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCourseAdd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnCourseAdd.Location = new System.Drawing.Point(24, 122);
            this.btnCourseAdd.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnCourseAdd.Name = "btnCourseAdd";
            this.btnCourseAdd.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnCourseAdd.Size = new System.Drawing.Size(46, 35);
            this.btnCourseAdd.Symbol = 61543;
            this.btnCourseAdd.SymbolSize = 35;
            this.btnCourseAdd.TabIndex = 113;
            this.btnCourseAdd.Click += new System.EventHandler(this.btnCourseAdd_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.btnFind);
            this.uiGroupBox1.Controls.Add(this.cmbCourse);
            this.uiGroupBox1.Controls.Add(this.uiLabel2);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(24, 23);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(1032, 81);
            this.uiGroupBox1.TabIndex = 110;
            this.uiGroupBox1.Text = "查询";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBox1.Click += new System.EventHandler(this.uiGroupBox1_Click);
            // 
            // btnFind
            // 
            this.btnFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFind.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnFind.Location = new System.Drawing.Point(901, 31);
            this.btnFind.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(100, 35);
            this.btnFind.TabIndex = 7;
            this.btnFind.Text = "查询";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // cmbCourse
            // 
            this.cmbCourse.DataSource = null;
            this.cmbCourse.FillColor = System.Drawing.Color.White;
            this.cmbCourse.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbCourse.Location = new System.Drawing.Point(609, 31);
            this.cmbCourse.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCourse.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbCourse.Name = "cmbCourse";
            this.cmbCourse.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbCourse.Size = new System.Drawing.Size(150, 35);
            this.cmbCourse.TabIndex = 6;
            this.cmbCourse.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(519, 31);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(83, 35);
            this.uiLabel2.TabIndex = 2;
            this.uiLabel2.Text = "课程：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnRefresh.Location = new System.Drawing.Point(915, 122);
            this.btnRefresh.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.RadiusSides = ((Sunny.UI.UICornerRadiusSides)((Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom)));
            this.btnRefresh.Size = new System.Drawing.Size(46, 35);
            this.btnRefresh.Symbol = 61473;
            this.btnRefresh.SymbolSize = 35;
            this.btnRefresh.TabIndex = 116;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dgvCourseList
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.dgvCourseList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCourseList.BackgroundColor = System.Drawing.Color.White;
            this.dgvCourseList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCourseList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCourseList.ColumnHeadersHeight = 32;
            this.dgvCourseList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvCourseList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.CourseName,
            this.CourseGrade,
            this.course_start_time,
            this.course_end_time,
            this.teacherName,
            this.teacherId,
            this.course_desc});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCourseList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvCourseList.EnableHeadersVisualStyles = false;
            this.dgvCourseList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.dgvCourseList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.dgvCourseList.Location = new System.Drawing.Point(24, 163);
            this.dgvCourseList.Name = "dgvCourseList";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCourseList.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCourseList.RowHeadersWidth = 51;
            this.dgvCourseList.RowHeight = 27;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.dgvCourseList.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvCourseList.RowTemplate.Height = 27;
            this.dgvCourseList.SelectedIndex = -1;
            this.dgvCourseList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCourseList.ShowGridLine = true;
            this.dgvCourseList.Size = new System.Drawing.Size(1032, 404);
            this.dgvCourseList.TabIndex = 112;
            this.dgvCourseList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCourseList_CellContentClick);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "序号";
            this.Id.MinimumWidth = 6;
            this.Id.Name = "Id";
            this.Id.Width = 125;
            // 
            // CourseName
            // 
            this.CourseName.DataPropertyName = "CourseName";
            this.CourseName.HeaderText = "课程名";
            this.CourseName.MinimumWidth = 6;
            this.CourseName.Name = "CourseName";
            this.CourseName.Width = 125;
            // 
            // CourseGrade
            // 
            this.CourseGrade.DataPropertyName = "CourseGrade";
            this.CourseGrade.HeaderText = "学分";
            this.CourseGrade.MinimumWidth = 6;
            this.CourseGrade.Name = "CourseGrade";
            this.CourseGrade.Width = 125;
            // 
            // course_start_time
            // 
            this.course_start_time.DataPropertyName = "course_start_time";
            this.course_start_time.HeaderText = "开始时间";
            this.course_start_time.MinimumWidth = 6;
            this.course_start_time.Name = "course_start_time";
            this.course_start_time.Width = 125;
            // 
            // course_end_time
            // 
            this.course_end_time.DataPropertyName = "course_end_time";
            this.course_end_time.HeaderText = "结课时间";
            this.course_end_time.MinimumWidth = 6;
            this.course_end_time.Name = "course_end_time";
            this.course_end_time.Width = 125;
            // 
            // teacherName
            // 
            this.teacherName.DataPropertyName = "teacherName";
            this.teacherName.HeaderText = "授课老师";
            this.teacherName.MinimumWidth = 6;
            this.teacherName.Name = "teacherName";
            this.teacherName.Width = 125;
            // 
            // teacherId
            // 
            this.teacherId.DataPropertyName = "teacherId";
            this.teacherId.HeaderText = "教师号";
            this.teacherId.MinimumWidth = 6;
            this.teacherId.Name = "teacherId";
            this.teacherId.Visible = false;
            this.teacherId.Width = 125;
            // 
            // course_desc
            // 
            this.course_desc.DataPropertyName = "course_desc";
            this.course_desc.HeaderText = "课程描述";
            this.course_desc.MinimumWidth = 6;
            this.course_desc.Name = "course_desc";
            this.course_desc.Visible = false;
            this.course_desc.Width = 125;
            // 
            // uiPagination1
            // 
            this.uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPagination1.Location = new System.Drawing.Point(230, 585);
            this.uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPagination1.Name = "uiPagination1";
            this.uiPagination1.PageSize = 5;
            this.uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiPagination1.Size = new System.Drawing.Size(689, 43);
            this.uiPagination1.TabIndex = 111;
            this.uiPagination1.Text = "uiPagination1";
            this.uiPagination1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPagination1.PageChanged += new Sunny.UI.UIPagination.OnPageChangeEventHandler(this.uiPagination1_PageChanged);
            // 
            // FrmCourseManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 646);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnCourseDelete);
            this.Controls.Add(this.btnCourseEdit);
            this.Controls.Add(this.btnCourseAdd);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.dgvCourseList);
            this.Controls.Add(this.uiPagination1);
            this.Name = "FrmCourseManagement";
            this.Text = "课程管理";
            this.Load += new System.EventHandler(this.FrmCourseManagement_Load);
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCourseList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Sunny.UI.UISymbolButton btnPrint;
        private Sunny.UI.UISymbolButton btnExport;
        private Sunny.UI.UISymbolButton btnCourseDelete;
        private Sunny.UI.UISymbolButton btnCourseEdit;
        private Sunny.UI.UISymbolButton btnCourseAdd;
        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UIButton btnFind;
        private Sunny.UI.UIComboBox cmbCourse;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UISymbolButton btnRefresh;
        private Sunny.UI.UIDataGridView dgvCourseList;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseGrade;
        private System.Windows.Forms.DataGridViewTextBoxColumn course_start_time;
        private System.Windows.Forms.DataGridViewTextBoxColumn course_end_time;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherName;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherId;
        private System.Windows.Forms.DataGridViewTextBoxColumn course_desc;
        private Sunny.UI.UIPagination uiPagination1;
    }
}