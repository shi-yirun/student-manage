﻿using BLL.Admin;
using Model;
using StudentManagement.Admin.EditForm;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace StudentManagement.Admin

{
    public partial class FrmMajorManagement : UIPage
    {
        public FrmMajorManagement()
        {
            InitializeComponent();
        }

        private void FrmMajorManagement_Load(object sender, EventArgs e)
        {
            CollegeBind();
            this.uiPagination1.ActivePage = 1;
        }

        private void MajorBindTable()
        {
            Major major = new Major();
            major.MajorName = this.cmbMajor.Text;
            major.collegeName = this.cmbCollege.Text;
            int index = this.uiPagination1.ActivePage - 1;
            int size = this.uiPagination1.PageSize;
            int begin = (index * size) + 1;
            int end = (index + 1) * size;
            int result;
            this.dgvMajorList.DataSource = new MajorBLL().MajorBindTable(begin, end, out result, major);
            this.uiPagination1.TotalCount = result;
        }

        private void CollegeBind()
        {

            this.cmbCollege.DisplayMember = "collegeName";
            this.cmbCollege.ValueMember = "Id";
            DataTable table = new CollegeBLL().GetCollegeList();
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["collegeName"] = "请选择学院";
            table.Rows.InsertAt(row, 0);
            this.cmbCollege.DataSource = table;

        }
        private void MarjorBind(string collegeName)
        {
            this.cmbMajor.DisplayMember = "majorName";
            this.cmbMajor.ValueMember = "Id";
            DataTable table = new MajorBLL().GetMajorList(collegeName);
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["majorName"] = "请选择专业";
            table.Rows.InsertAt(row, 0);
            this.cmbMajor.DataSource = table;

        }
        private void cmbCollege_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbCollege.SelectedIndex == 0)
            {
                this.cmbMajor.DataSource = null;

                this.cmbMajor.Items.Insert(0, "请选择专业");
                this.cmbMajor.SelectedIndex = 0;
                return;
            }
            string collegeName = this.cmbCollege.Text;
            MarjorBind(collegeName);


        }

        private void btnCollegeAdd_Click(object sender, EventArgs e)
        {
            FrmMajorEdit frmMajorEdit = new FrmMajorEdit();
            frmMajorEdit.Text = "添加专业信息";
            DialogResult result = frmMajorEdit.ShowDialog();
            if (result == DialogResult.Yes)
            {
                frmMajorEdit.Dispose();
                MajorBindTable();
            }
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            MajorBindTable();
        }

        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            MajorBindTable();
        }

        private void btnCollegeEdit_Click(object sender, EventArgs e)
        {
            if (this.dgvMajorList.CurrentRow == null || this.dgvMajorList.RowCount == 0)
            {
                ShowWarningDialog("没有可编辑的数据");
                return;
            }
            Major major = new Major();
            major.Id = (int)this.dgvMajorList.SelectedRows[0].Cells["Id"].Value;
            major.collegeName = this.dgvMajorList.SelectedRows[0].Cells["collegeName"].Value.ToString();
            major.MajorName = this.dgvMajorList.SelectedRows[0].Cells["majorName"].Value.ToString();
            major.collegeId = (int)this.dgvMajorList.SelectedRows[0].Cells["collegeId"].Value;
            FrmMajorEdit frmMajorEdit = new FrmMajorEdit();
            frmMajorEdit.Text = "修改专业信息";
            frmMajorEdit.Tag = major;
            DialogResult result = frmMajorEdit.ShowDialog();
            if (result == DialogResult.Yes)
            {
                frmMajorEdit.Dispose();
                MajorBindTable();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Excel文件|*.xlsx;*.xls";
            saveFileDialog1.FileName = "专业信息";
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                //获取数据源
                DataTable table = new MajorBLL().GetMajorCollegeList();
                //获取表格的标题
                List<string> title = new List<string>();

                foreach (DataGridViewColumn t in this.dgvMajorList.Columns)
                {
                    if (t.HeaderText == "学院号")
                        continue;

                    title.Add(t.HeaderText);
                }
                //获取文件路径
                string path = saveFileDialog1.FileName;
                //获取文件的名字、
                int i = path.LastIndexOf('\\');
                string name = path.Substring(i + 1);

                int m = NpoiHelper.ExportExcel(table, title, path, name);
                if (m == -1)
                {
                    ShowWarningDialog("数据为空");

                }
                else
                {
                    ShowSuccessDialog("写入成功");
                }
            }

        }

        private void btnCollegeDelete_Click(object sender, EventArgs e)
        {
            ShowWarningDialog("不支持删除");
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //显示选择面板
            FrmSelection frmSelection = new FrmSelection();
            frmSelection.Tag = this.dgvMajorList.Columns;
            DialogResult dialogResult = frmSelection.ShowDialog();
            if (DialogResult.OK == dialogResult)
            {
                GridPrinter.StartPrint(this.dgvMajorList, true, "专业信息");
            }
            //恢复原样
            foreach (DataGridViewColumn item in this.dgvMajorList.Columns)
            {
                item.Visible = true;
            }

        }
    }
}
