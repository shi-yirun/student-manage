﻿using Sunny.UI;
using System.Windows.Documents;
using System.Windows.Forms;
namespace StudentManagement
{
    public partial class FrmSelection : UIForm
    {

        DataGridViewColumnCollection obj;
        public FrmSelection()
        {
            InitializeComponent();
            this.checkedListBox1.MultiColumn = true;
           
        }
      

        /// <summary>
        /// 确认
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiButton1_Click(object sender, System.EventArgs e)
        {
            for (int n = 0; n < checkedListBox1.Items.Count; n++)
            {
                //如果没有选中
                if (checkedListBox1.GetItemChecked(n) == false)
                {
                    //把对应的列变成隐藏
                    foreach (DataGridViewColumn item in obj)
                    {
                        if (checkedListBox1.Items[n].ToString().Equals(item.HeaderText))
                        {
                           
                            item.Visible = false;
                            break;
                        }
                    }

                }
            }
            this.DialogResult =DialogResult.OK;
        }

        private void uiButton2_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void FrmSelection_Load(object sender, System.EventArgs e)
        {
            obj = (DataGridViewColumnCollection)this.Tag;
            if (this.Tag != null)
            {
                
                foreach (DataGridViewColumn item in obj)
                {
                    if (item.Visible)
                    {
                        this.checkedListBox1.Items.Add(item.HeaderText);
                    }
                }
            }
        }
    }
}
