﻿using BLL.Admin;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Windows.Forms;

namespace StudentManagement
{
    public partial class FrmLogin : UIForm
    {
        public FrmLogin()
        {
            InitializeComponent();
            this.picCode.Image = DrawingCode.GetBitmapCode();
        }

        /// <summary>
        /// 点击生成验证码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picCode_Click(object sender, EventArgs e)
        {
            this.picCode.Image = DrawingCode.GetBitmapCode();
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogin_Click(object sender, EventArgs e)
        {
            #region 获取公共值
            //获取用户点击的是一个方法进行登录的
            int index = this.uiTabControl1.SelectedIndex;
            // 获取用户登录的身份
            string type = "";
            //获取RadioButtonGroupBox里面的全部RadioButton按钮
            Control.ControlCollection items = this.uiRadioButtonGroup1.Controls;
            // 遍历所有的控件，判断那个控件是被选中了，获取值
            foreach (Control item in items)
            {
                UIRadioButton temp = ((UIRadioButton)item);
                if (temp.Checked == true)
                {
                    type = temp.Text;
                }
            }
            //存储用户的类型
            Program.Type = type;
            #endregion
            #region 使用用户和密码进行登录
            if (index == 0)
            {
                // 获取用户名、密码和验证码
                string userName = txtUserName.Text.Trim();
                string userPwd = MD5Helper.GetMD5Str(txtUserPwd.Text.Trim());
                string code = txtUserCode.Text.Trim();

                // 判断用户名是否为空
                if (string.IsNullOrEmpty(userName))
                {
                    ShowWarningDialog("用户名不许为空");
                    return;
                }
                //判断密码是否为空
                if (string.IsNullOrEmpty(userPwd))
                {
                    ShowWarningDialog("密码不许为空");
                    return;
                }
                //判断验证码是否为空
                if (string.IsNullOrEmpty(code))
                {
                    ShowWarningDialog("验证码不许为空");
                    return;
                }
                //验证码是否正确
                if (!DrawingCode.Code.ToUpper().Equals(code.ToUpper()))
                {
                    ShowWarningDialog("验证码输入错误");
                    this.picCode.Image = DrawingCode.GetBitmapCode();
                    return;
                }
                // 判断用户类型
                switch (type)
                {
                    case "管理员":
                        //调用管理员使用用户名和密码登录
                        AdminNameLogin(userName, userPwd);
                        break;
                    case "学生":
                        //调用管理员使用用户名和密码登录
                        StudentNameLogin(userName, userPwd);
                        break;
                    case "教师":
                        //调用管理员使用用户名和密码登录
                        TeacherNameLogin(userName, userPwd);
                        break;
                }
            }
            #endregion

            #region 使用邮箱的方式进行登录
            else if (index == 1)
            {
                //获取信息
                string userName = txtUserEmail.Text.Trim();
                string code = txtEmailCode.Text.Trim();
                if (string.IsNullOrEmpty(userName))
                {
                    ShowWarningDialog("邮箱不能为空");
                    return;
                }
                if (string.IsNullOrEmpty(code))
                {
                    ShowWarningDialog("验证码不许为空");
                    return;
                }


                // 判断用户类型
                switch (type)
                {
                    case "管理员":
                        //调用管理员使用用户名和密码登录
                        AdminEmailLogin(userName, code);
                        break;
                    case "学生":
                        //调用管理员使用用户名和密码登录
                        StudentEmailLogin(userName, code);
                        break;
                    case "教师":
                        //调用管理员使用用户名和密码登录
                        TeacherEmailLogin(userName, code);
                        break;
                }
            }
            #endregion

            #region 使用手机号进行登录
            else
            {
                //获取信息
                string phone = txtUserPhone.Text.Trim();
                string code = txtPhoneCode.Text.Trim();
                //判断手机号是否为空
                if (string.IsNullOrEmpty(phone))
                {
                    ShowWarningDialog("手机号为空");
                    return;
                }
                //判断验证码是否为空
                if (string.IsNullOrEmpty(code))
                {
                    ShowWarningDialog("验证码为空");
                    return;
                }
                switch (type)
                {
                    case "管理员":
                        //调用管理员使用用户名和密码登录
                        AdminPhoneLogin(phone, code);
                        break;
                    case "学生":
                        //调用管理员使用用户名和密码登录
                        StudentPhoneLogin(phone, code);
                        break;
                    case "教师":
                        //调用管理员使用用户名和密码登录
                        TeacherPhoneLogin(phone, code);
                        break;
                }


            }
            #endregion

            #region 释放资源
            //如果计时器还在工作，那么就释放资源
            if (timer1.Enabled)
            {
                timer1.Enabled = false;
                timer1.Dispose();
            }
            #endregion

        }

        #region 处理手机登录
        /// <summary>
        /// 教师手机登录
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="code"></param>
        private void TeacherPhoneLogin(string phone, string code)
        {
            Model.Teacher teacher = new BLL.Teacher.TeacherBLL().GetTeacherInfo(phone);
            //判断对象是否为空
            if (teacher == null)
            {
                ShowWarningDialog("电话不存在");
                return;
            }
            //判断验证码是否正确
            if (!code.ToUpper().Equals(SendPhoneCode.Code.ToString().ToUpper()))
            {
                ShowWarningDialog("验证码输入错误");
                return;
            }
            //设置登录成功
            this.DialogResult = DialogResult.OK;
            //存储teacher对象
            Program.Teacher = teacher;
            Program.Type = "teacher";
        }
        /// <summary>
        /// 学生手机登录
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="code"></param>
        private void StudentPhoneLogin(string phone, string code)
        {
            Model.Student student = new BLL.Student.StudentBLL().GetStudentInfo(phone);
            //判断对象是否为空
            if (student == null)
            {
                ShowWarningDialog("电话不存在");
                return;
            }
            //判断验证码是否正确
            if (!code.ToUpper().Equals(SendPhoneCode.Code.ToString().ToUpper()))
            {
                ShowWarningDialog("验证码输入错误");
                return;
            }
            //设置登录成功
            this.DialogResult = DialogResult.OK;
            Program.Student = student;
            Program.Type = "student";
        }

        /// <summary>
        /// 管理员手机登录
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="code"></param>
        private void AdminPhoneLogin(string phone, string code)
        {
            Model.Admin admin = new AdminBLL().GetAdminInfo(phone);
            //判断对象是否为空
            if (admin == null)
            {
                ShowWarningDialog("电话不存在");
                return;
            }
            //判断验证码是否正确
            if (!code.ToUpper().Equals(SendPhoneCode.Code.ToString().ToUpper()))
            {
                ShowWarningDialog("验证码输入错误");
                return;
            }
            //设置登录成功
            this.DialogResult = DialogResult.OK;
            //存储管理员
            Program.Admin = admin;
            Program.Type = "admin";
        }

        #endregion

        #region 处理用户邮箱登录
        /// <summary>
        /// 教师邮箱登录
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="code"></param>
        private void TeacherEmailLogin(string userName, string code)
        {
            // 获取教师对象
            Model.Teacher teacher = new BLL.Teacher.TeacherBLL().GetTeacherInfo(userName);
            //判断对象是否为空
            if (teacher == null)
            {
                ShowWarningDialog("邮箱不存在");
                return;
            }
            //判断验证码是否正确
            if (!code.ToUpper().Equals(SendEmailCode.Code.ToUpper()))
            {
                ShowWarningDialog("验证码输入错误");
                return;
            }
            //设置登录成功
            this.DialogResult = DialogResult.OK;
            //存储教师
            Program.Teacher = teacher;
            Program.Type = "teacher";
        }
        /// <summary>
        /// 学生邮箱登录
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="code"></param>
        private void StudentEmailLogin(string userName, string code)
        {
            //获取学生对象
            Model.Student student = new BLL.Student.StudentBLL().GetStudentInfo(userName);
            //判断对象是否为空
            if (student == null)
            {
                ShowWarningDialog("邮箱不存在");
                return;
            }
            //判断验证码是否正确
            if (!code.ToUpper().Equals(SendEmailCode.Code.ToUpper()))
            {
                ShowWarningDialog("验证码输入错误");
                return;
            }
            //设置登录成功
            this.DialogResult = DialogResult.OK;
            //存储学生对象
            Program.Student = student;
            Program.Type = "student";
        }

        /// <summary>
        /// 管理员邮箱登录
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="code"></param>
        private void AdminEmailLogin(string userName, string code)
        {
            //获取管理员对象
            Model.Admin admin = new AdminBLL().GetAdminInfo(userName);
            //判断对象是否为空
            if (admin == null)
            {
                ShowWarningDialog("邮箱不存在");
                return;
            }
            //判断验证码是否正确
            if (!code.ToUpper().Equals(SendEmailCode.Code.ToUpper()))
            {
                ShowWarningDialog("验证码输入错误");
                return;
            }
            //设置登录成功
            this.DialogResult = DialogResult.OK;
            //存储管理员信息
            Program.Admin = admin;
            Program.Type = "admin";
        }
        #endregion

        #region 处理用户密码登录
        /// <summary>
        /// 教师用户密码登录
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userPwd"></param>
        private void TeacherNameLogin(string userName, string userPwd)
        {
            Model.Teacher teacher = new BLL.Teacher.TeacherBLL().GetTeacherInfo(userName);
            //判断获取的teaher对象是否为空
            if (teacher == null)
            {
                ShowWarningDialog("用户名/手机号/邮箱不存在");
                return;
            }
            // 判断密码是否正确
            if (!teacher.TeacherPwd.Equals(userPwd))
            {
                ShowWarningDialog("密码输入错误");
                return;
            }
            //上述都没有问题那么就登录成功，跳转到主界面
            this.DialogResult = DialogResult.OK;
            //存储教师
            Program.Teacher = teacher;
            Program.Type = "teacher";

        }
        /// <summary>
        /// 学生用户密码登录
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userPwd"></param>
        private void StudentNameLogin(string userName, string userPwd)
        {


            Model.Student student = new BLL.Student.StudentBLL().GetStudentInfo(userName);
            //判断获取的student对象是否为空
            if (student == null)
            {
                ShowWarningDialog("用户名/手机号/邮箱不存在");
                return;
            }
            // 判断密码是否正确
            if (!student.StudentPwd.Equals(userPwd))
            {
                ShowWarningDialog("密码输入错误");
                return;
            }
            //上述都没有问题那么就登录成功，跳转到主界面
            this.DialogResult = DialogResult.OK;
            //存储学生信息
            Program.Student = student;
            Program.Type = "student";

        }
        /// <summary>
        /// 管理员用户名密码登录
        /// </summary>
        private void AdminNameLogin(string userName, string userPwd)
        {
            // 使用用户名/邮箱/手机号去获取对应的信息
            Model.Admin admin = new AdminBLL().GetAdminInfo(userName);
            //判断获取的admin对象是否为空
            if (admin == null)
            {
                ShowWarningDialog("用户名/手机号/邮箱不存在");
                return;
            }
            // 判断密码是否正确
            if (!admin.AdminPwd.Equals(userPwd))
            {
                ShowWarningDialog("密码输入错误");
                return;
            }
            //上述都没有问题那么就登录成功，跳转到主界面
            this.DialogResult = DialogResult.OK;
            //存储管理员信息
            Program.Admin = admin;
            Program.Type = "admin";
        }
        #endregion


        /// <summary>
        /// 窗体退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            if (ShowAskDialog("确认退出程序?"))
            {
                Application.Exit();
            }
        }

        /// <summary>
        /// 发送邮箱验证码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSendEmailCode_Click(object sender, EventArgs e)
        {
            // 获取邮箱
            string email = txtUserEmail.Text.Trim();
            //判断邮箱是否为空
            if (string.IsNullOrEmpty(email))
            {
                ShowWarningDialog("邮箱不能为空");
                return;
            }
            // 判断是否为合格邮箱
            if (!RegexHelper.IsEmail(email))
            {
                ShowWarningDialog("请输入正确的邮箱号");
                return;
            }
            //调用封装的类发送验证码
            bool flag = SendEmailCode.GetEmail(email);
            if (flag)
            {
                ShowSuccessTip("发送成功");
                //设置按钮为不可选取的状态
                btnSendEmailCode.Enabled = false;
                //设置按钮文本
                btnSendEmailCode.Text = $"({SendEmailCode.Times})后重新获取";
                //设置间隔时间1s
                timer1.Interval = 1000;
                //设置到达间隔要执行的事件
                timer1.Tick += Timer1_Tick;
                //启动计时器
                timer1.Enabled = true;

            }
            if (!flag) ShowErrorTip("发送失败，请检查网络");

        }


        /// <summary>
        /// 发送邮箱验证码的计时器事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer1_Tick(object sender, EventArgs e)
        {
            //如果倒计时秒数等于0
            if (SendEmailCode.Times == 0)
            {
                //停止计时器
                timer1.Enabled = false;
                //移除到达间隔要执行的事件
                timer1.Tick -= Timer1_Tick;
                //设置按钮为不可选取的状态
                btnSendEmailCode.Enabled = true;
                //设置按钮文本
                btnSendEmailCode.Text = "发送验证码";
                //重置倒计时的值
                SendEmailCode.Times = 59;

                return;
            }
            //让秒数一直递减
            --SendEmailCode.Times;
            // 不断的重新赋值
            btnSendEmailCode.Text = $"({SendEmailCode.Times})后重新获取";//可以把参数嵌入
        }

        /// <summary>
        /// 发送手机验证码
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSendPhoneCode_Click(object sender, EventArgs e)
        {
            string phone = txtUserPhone.Text.Trim();
            //判断手机号是否为空
            if (string.IsNullOrEmpty(phone))
            {
                ShowWarningDialog("手机号为空");
                return;
            }
            //判断手机号是否合法
            if (!RegexHelper.IsMobilePhone(phone))
            {
                ShowWarningDialog("手机号不合法");
                return;
            }
            //调用发送验证码的方法
            int flag = SendPhoneCode.SendCode(phone);
            //发送成功
            if (flag == 2)
            {
                ShowSuccessTip("发送成功");
                //设置间隔
                timer1.Interval = 1000;
                //添加事件
                timer1.Tick += Timer1_Tick1;
                //打开计时器
                timer1.Enabled = true;
                //使按钮失效
                btnSendPhoneCode.Enabled = false;
                //设置按钮文本
                btnSendPhoneCode.Text = $"({SendPhoneCode.Times})后重新获取";//@屏蔽转义字符
            }
            else
            {
                ShowSuccessTip("发送失败，请检查网络");
            }
        }

        /// <summary>
        /// 手机发送验证码处理事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer1_Tick1(object sender, EventArgs e)
        {
            //当时间为0
            if (SendEmailCode.Times == 0)
            {
                //   关闭计时器
                timer1.Enabled = false;
                //移除事件
                timer1.Tick -= Timer1_Tick1;
                //使按钮启用
                btnSendPhoneCode.Enabled = true;
                //将按钮复原吧
                btnSendPhoneCode.Text = "发送验证码";
                //时间重置
                SendPhoneCode.Times = 59;
                return;
            }
            //时间递减
            --SendPhoneCode.Times;
            //改变文本
            btnSendPhoneCode.Text = $"({SendPhoneCode.Times})后重新获取";
        }

        /// <summary>
        /// tabControl选中事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiTabControl1_Selected(object sender, TabControlEventArgs e)
        {
            //选中第一项,清空第二项第三项
            if (e.TabPageIndex == 0)
            {
                //清空邮箱登录的信息和计时器
                EmailClear();
                //清空手机登录的信息和计时器
                PhoneClear();
            }
            //选中第二项,清空第一项第三项
            if (e.TabPageIndex == 1)
            {
                //清空用户名密码登录
                UserInfoClear();
                //清空手机登录的信息和计时器
                PhoneClear();
            }
            //选中第三项,清空第一项第二项
            if (e.TabPageIndex == 2)
            {
                //清空用户名密码登录
                UserInfoClear();
                //清空邮箱登录的信息和计时器
                EmailClear();
            }

        }

        /// <summary>
        /// 清空用户登录信息
        /// </summary>
        private void UserInfoClear()
        {
            txtUserName.Text = "";
            txtUserPwd.Text = "";
            txtUserCode.Text = "";
        }

        /// <summary>
        /// 清空手机登录的信息和计时器
        /// </summary>
        private void PhoneClear()
        {
            txtUserPhone.Text = "";
            txtPhoneCode.Text = "";
            //清空计时器
            timer1.Enabled = false;
            timer1.Tick -= Timer1_Tick1;
            //复原按钮文本
            btnSendPhoneCode.Text = "发送验证码";
            btnSendPhoneCode.Enabled = true;
            SendPhoneCode.Times = 59;
        }

        /// <summary>
        /// 清空邮箱登录的信息和计时器
        /// </summary>
        private void EmailClear()
        {
            txtUserEmail.Text = "";
            txtEmailCode.Text = "";
            //清空计时器
            timer1.Enabled = false;
            timer1.Tick -= Timer1_Tick;
            //复原按钮文本
            btnSendEmailCode.Text = "发送验证码";
            btnSendEmailCode.Enabled = true;
            SendEmailCode.Times = 59;
        }

        /// <summary>
        /// 用户名文本框按下回车键
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            //当用户按下enter键
            if (e.KeyChar == 13)
            {
                //转到密码框
                txtUserPwd.Focus();
            }
        }
        /// <summary>
        /// 密码框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUserPwd_KeyPress(object sender, KeyPressEventArgs e)
        {
            //当用户按下enter键
            if (e.KeyChar == 13)
            {
                //转到验证码框
                txtUserCode.Focus();
            }
        }
        /// <summary>
        /// 验证码按键按下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUserCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            //当用户按下enter键
            if (e.KeyChar == 13)
            {
                //执行登录事件的方法
                btnLogin_Click(null, null);
            }
        }
        /// <summary>
        /// 邮箱框按下回车键
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUserEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtEmailCode.Focus();
            }
        }
        /// <summary>
        /// 邮箱验证码按下事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtEmailCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnLogin_Click(null, null);
            }
        }

        /// <summary>
        /// 电话款按下事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUserPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtPhoneCode.Focus();
            }

        }
        /// <summary>
        /// 电话验证码按下事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPhoneCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnLogin_Click(null, null);
            }
        }

        private void uiPanel1_Click(object sender, EventArgs e)
        {

        }
    }
}
