﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace StudentManagement.Util
{
    public class GridPrinter
    {
        /// <summary>
        /// the grid to print
        /// </summary>
        private DataGridView dataGridView;

        /// <summary>
        /// the PrintDocument
        /// </summary>
        private PrintDocument printDocument;

        /// <summary>
        /// 居中打印
        /// </summary>
        private bool centerOnPage;

        /// <summary>
        /// 是否有标题
        /// </summary>
        private bool hasTitle;

        /// <summary>
        /// 标题名称
        /// </summary>
        private string title;

        /// <summary>
        /// 标题字体
        /// </summary>
        private Font titleFont;

        /// <summary>
        /// 标题颜色
        /// </summary>
        private Color titleColor;

        /// <summary>
        /// 是否分页
        /// </summary>
        private bool paging;

        /// <summary>
        /// 当前行
        /// </summary>
        static int currentRow;

        /// <summary>
        /// 打印的总页数
        /// </summary>
        static int pageNumber;

        /// <summary>
        /// 页面宽度
        /// </summary>
        private int pageWidth;

        /// <summary>
        /// 页面高度
        /// </summary>
        private int pageHeight;

        /// <summary>
        /// 左边距
        /// </summary>
        private int leftMargin;

        /// <summary>
        /// 上边距
        /// </summary>
        private int topMargin;

        /// <summary>
        /// 右边距
        /// </summary>
        private int rightMargin;

        /// <summary>
        /// 下边距
        /// </summary>
        private int bottomMargin;

        /// <summary>
        /// Y坐标
        /// </summary>
        private float currentY;

        /// <summary>
        /// 表格的设置
        /// </summary>
        private float rowHeaderHeight;
        private List<float> rowsHeight;
        private List<float> columnsWidth;
        private float dataGridViewWidth;

        /// <summary>
        /// 列的设置
        /// </summary>
        private List<int[]> mColumnPoints;
        private List<float> mColumnPointsWidth;
        private int mColumnPoint;





        /// <summary>
        /// 打印表格
        /// </summary>
        /// <param name="objDataGridView">数据源</param>
        /// <param name="objPrintDocument">打印文档对象</param>
        /// <param name="bCenterOnPage">是否居中</param>
        /// <param name="bHasTitle">是不是有标题</param>
        /// <param name="sTitle">标题名称</param>
        /// <param name="objTitleFont">标题字体</param>
        /// <param name="objTitleColor"><标题颜色/param>
        /// <param name="bPaging">是否分页</param>
        public GridPrinter(DataGridView objDataGridView, PrintDocument objPrintDocument, bool bHasTitle, string sTitle, Font objTitleFont, Color objTitleColor, bool bPaging = true, bool bCenterOnPage = true)
        {
            dataGridView = objDataGridView;
            printDocument = objPrintDocument;
            centerOnPage = bCenterOnPage;
            hasTitle = bHasTitle;
            title = sTitle;
            titleFont = objTitleFont;
            titleColor = objTitleColor;
            paging = bPaging;

            pageNumber = 0;

            rowsHeight = new List<float>();
            columnsWidth = new List<float>();

            mColumnPoints = new List<int[]>();
            mColumnPointsWidth = new List<float>();

            //绑定打印事件
            printDocument.PrintPage += new PrintPageEventHandler(printDoc_PrintPage);

            //竖着打印文档
            if (!printDocument.DefaultPageSettings.Landscape)
            {
                pageWidth = printDocument.DefaultPageSettings.PaperSize.Width;
                pageHeight = printDocument.DefaultPageSettings.PaperSize.Height;
            }
            // 横着打印文档
            else
            {
                pageHeight = printDocument.DefaultPageSettings.PaperSize.Width;
                pageWidth = printDocument.DefaultPageSettings.PaperSize.Height;
            }

            //设置文档边距
            leftMargin = printDocument.DefaultPageSettings.Margins.Left;
            topMargin = printDocument.DefaultPageSettings.Margins.Top;
            rightMargin = printDocument.DefaultPageSettings.Margins.Right;
            bottomMargin = printDocument.DefaultPageSettings.Margins.Bottom;

            //当前行
            currentRow = 0;
        }

        /// <summary>
        /// 每一页打印事件
        /// </summary>
        /// <param name="sender"></param>
        private void printDoc_PrintPage(object sender, PrintPageEventArgs e)
        {
            bool more = DrawDataGridView(e.Graphics);
            if (more == true)
                e.HasMorePages = true;
        }


        /// <summary>
        ///  计算打印度量
        /// </summary>
        /// <param name="g"></param>
        private void Calculate(Graphics g)
        {
            if (pageNumber == 0)
            {
                SizeF tmpSize = new SizeF();
                Font tmpFont;
                float tmpWidth;

                dataGridViewWidth = 0;

                for (int i = 0; i < dataGridView.Columns.Count; i++)
                {
                    tmpFont = dataGridView.ColumnHeadersDefaultCellStyle.Font;
                    if (tmpFont == null)
                        tmpFont = dataGridView.DefaultCellStyle.Font;

                    tmpSize = g.MeasureString(dataGridView.Columns[i].HeaderText, tmpFont);
                    tmpWidth = tmpSize.Width;
                    rowHeaderHeight = tmpSize.Height;

                    for (int j = 0; j < dataGridView.Rows.Count; j++)
                    {
                        tmpFont = dataGridView.Rows[j].DefaultCellStyle.Font;
                        if (tmpFont == null)
                            tmpFont = dataGridView.DefaultCellStyle.Font;

                        tmpSize = g.MeasureString("Anything", tmpFont);
                        rowsHeight.Add(tmpSize.Height);

                        tmpSize = g.MeasureString(dataGridView.Rows[j].Cells[i].EditedFormattedValue.ToString(), tmpFont);
                        if (tmpSize.Width > tmpWidth)
                            tmpWidth = tmpSize.Width;
                    }
                    if (dataGridView.Columns[i].Visible)
                        dataGridViewWidth += tmpWidth;
                    columnsWidth.Add(tmpWidth);
                }

                int k;

                int mStartPoint = 0;
                for (k = 0; k < dataGridView.Columns.Count; k++)
                    if (dataGridView.Columns[k].Visible)
                    {
                        mStartPoint = k;
                        break;
                    }

                int mEndPoint = dataGridView.Columns.Count;
                for (k = dataGridView.Columns.Count - 1; k >= 0; k--)
                    if (dataGridView.Columns[k].Visible)
                    {
                        mEndPoint = k + 1;
                        break;
                    }

                float mTempWidth = dataGridViewWidth;
                float mTempPrintArea = (float)pageWidth - (float)leftMargin - (float)rightMargin;

                if (dataGridViewWidth > mTempPrintArea)
                {
                    mTempWidth = 0.0F;
                    for (k = 0; k < dataGridView.Columns.Count; k++)
                    {
                        if (dataGridView.Columns[k].Visible)
                        {
                            mTempWidth += columnsWidth[k];
                            if (mTempWidth > mTempPrintArea)
                            {
                                mTempWidth -= columnsWidth[k];
                                mColumnPoints.Add(new int[] { mStartPoint, mEndPoint });
                                mColumnPointsWidth.Add(mTempWidth);
                                mStartPoint = k;
                                mTempWidth = columnsWidth[k];
                            }
                        }
                        mEndPoint = k + 1;
                    }
                }

                mColumnPoints.Add(new int[] { mStartPoint, mEndPoint });
                mColumnPointsWidth.Add(mTempWidth);
                mColumnPoint = 0;
            }
        }

        /// <summary>
        /// 打印标题
        /// </summary>
        /// <param name="g"></param>
        private void DrawHeader(Graphics g)
        {
            currentY = (float)topMargin;

            if (paging)
            {
                pageNumber++;
                string PageString = "Page " + pageNumber.ToString();

                StringFormat PageStringFormat = new StringFormat();
                PageStringFormat.Trimming = StringTrimming.Word;
                PageStringFormat.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
                PageStringFormat.Alignment = StringAlignment.Far;

                // 设置打印字体
                Font PageStringFont = new Font("Arial", 8, FontStyle.Regular, GraphicsUnit.Point);

                //计算打印范围
                RectangleF PageStringRectangle = new RectangleF((float)leftMargin, currentY, (float)pageWidth - (float)rightMargin - (float)leftMargin, g.MeasureString(PageString, PageStringFont).Height);

                g.DrawString(PageString, PageStringFont, new SolidBrush(Color.Black), PageStringRectangle, PageStringFormat);

                currentY += g.MeasureString(PageString, PageStringFont).Height;
            }

            if (hasTitle)
            {
                StringFormat TitleFormat = new StringFormat();
                TitleFormat.Trimming = StringTrimming.Word;
                TitleFormat.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
                if (centerOnPage)
                    TitleFormat.Alignment = StringAlignment.Center;
                else
                    TitleFormat.Alignment = StringAlignment.Near;

                RectangleF TitleRectangle = new RectangleF((float)leftMargin, currentY, (float)pageWidth - (float)rightMargin - (float)leftMargin, g.MeasureString(title, titleFont).Height);

                g.DrawString(title, titleFont, new SolidBrush(titleColor), TitleRectangle, TitleFormat);

                currentY += g.MeasureString(title, titleFont).Height;
            }

            float CurrentX = (float)leftMargin;
            if (centerOnPage)
                CurrentX += (((float)pageWidth - (float)rightMargin - (float)leftMargin) - mColumnPointsWidth[mColumnPoint]) / 2.0F;

            Color HeaderForeColor = dataGridView.ColumnHeadersDefaultCellStyle.ForeColor;
            if (HeaderForeColor.IsEmpty)
                HeaderForeColor = dataGridView.DefaultCellStyle.ForeColor;
            SolidBrush HeaderForeBrush = new SolidBrush(HeaderForeColor);

            Color HeaderBackColor = dataGridView.ColumnHeadersDefaultCellStyle.BackColor;
            if (HeaderBackColor.IsEmpty)
                HeaderBackColor = dataGridView.DefaultCellStyle.BackColor;
            SolidBrush HeaderBackBrush = new SolidBrush(HeaderBackColor);

            Pen TheLinePen = new Pen(dataGridView.GridColor, 1);

            Font HeaderFont = dataGridView.ColumnHeadersDefaultCellStyle.Font;
            if (HeaderFont == null)
                HeaderFont = dataGridView.DefaultCellStyle.Font;

            RectangleF HeaderBounds = new RectangleF(CurrentX, currentY, mColumnPointsWidth[mColumnPoint], rowHeaderHeight);
            g.FillRectangle(HeaderBackBrush, HeaderBounds);

            StringFormat CellFormat = new StringFormat();
            CellFormat.Trimming = StringTrimming.Word;
            CellFormat.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.LineLimit | StringFormatFlags.NoClip;

            RectangleF CellBounds;
            float ColumnWidth;
            for (int i = (int)mColumnPoints[mColumnPoint].GetValue(0); i < (int)mColumnPoints[mColumnPoint].GetValue(1); i++)
            {
                if (!dataGridView.Columns[i].Visible) continue;

                ColumnWidth = columnsWidth[i];

                if (dataGridView.ColumnHeadersDefaultCellStyle.Alignment.ToString().Contains("Right"))
                    CellFormat.Alignment = StringAlignment.Far;
                else if (dataGridView.ColumnHeadersDefaultCellStyle.Alignment.ToString().Contains("Center"))
                    CellFormat.Alignment = StringAlignment.Center;
                else
                    CellFormat.Alignment = StringAlignment.Near;

                CellBounds = new RectangleF(CurrentX, currentY, ColumnWidth, rowHeaderHeight);

                g.DrawString(dataGridView.Columns[i].HeaderText, HeaderFont, HeaderForeBrush, CellBounds, CellFormat);

                if (dataGridView.RowHeadersBorderStyle != DataGridViewHeaderBorderStyle.None)
                    g.DrawRectangle(TheLinePen, CurrentX, currentY, ColumnWidth, rowHeaderHeight);

                CurrentX += ColumnWidth;
            }

            currentY += rowHeaderHeight;
        }

        // common row printing function
        private bool DrawRows(Graphics g)
        {
            Pen TheLinePen = new Pen(dataGridView.GridColor, 1);

            Font RowFont;
            Color RowForeColor;
            Color RowBackColor;
            SolidBrush RowForeBrush;
            SolidBrush RowBackBrush;
            SolidBrush RowAlternatingBackBrush;

            StringFormat CellFormat = new StringFormat();
            CellFormat.Trimming = StringTrimming.Word;
            CellFormat.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.LineLimit;

            RectangleF RowBounds;
            float CurrentX;
            float ColumnWidth;
            while (currentRow < dataGridView.Rows.Count)
            {
                if (dataGridView.Rows[currentRow].Visible)
                {
                    RowFont = dataGridView.Rows[currentRow].DefaultCellStyle.Font;
                    if (RowFont == null)
                        RowFont = dataGridView.DefaultCellStyle.Font;

                    RowForeColor = dataGridView.Rows[currentRow].DefaultCellStyle.ForeColor;
                    if (RowForeColor.IsEmpty)
                        RowForeColor = dataGridView.DefaultCellStyle.ForeColor;
                    RowForeBrush = new SolidBrush(RowForeColor);

                    RowBackColor = dataGridView.Rows[currentRow].DefaultCellStyle.BackColor;
                    if (RowBackColor.IsEmpty)
                    {
                        RowBackBrush = new SolidBrush(dataGridView.DefaultCellStyle.BackColor);
                        RowAlternatingBackBrush = new SolidBrush(dataGridView.AlternatingRowsDefaultCellStyle.BackColor);
                    }
                    else
                    {
                        RowBackBrush = new SolidBrush(RowBackColor);
                        RowAlternatingBackBrush = new SolidBrush(RowBackColor);
                    }

                    CurrentX = (float)leftMargin;
                    if (centerOnPage)
                        CurrentX += (((float)pageWidth - (float)rightMargin - (float)leftMargin) - mColumnPointsWidth[mColumnPoint]) / 2.0F;

                    RowBounds = new RectangleF(CurrentX, currentY, mColumnPointsWidth[mColumnPoint], rowsHeight[currentRow]);

                    if (currentRow % 2 == 0)
                        g.FillRectangle(RowBackBrush, RowBounds);
                    else
                        g.FillRectangle(RowAlternatingBackBrush, RowBounds);

                    for (int CurrentCell = (int)mColumnPoints[mColumnPoint].GetValue(0); CurrentCell < (int)mColumnPoints[mColumnPoint].GetValue(1); CurrentCell++)
                    {
                        if (!dataGridView.Columns[CurrentCell].Visible) continue;

                        if (dataGridView.Columns[CurrentCell].DefaultCellStyle.Alignment.ToString().Contains("Right"))
                            CellFormat.Alignment = StringAlignment.Far;
                        else if (dataGridView.Columns[CurrentCell].DefaultCellStyle.Alignment.ToString().Contains("Center"))
                            CellFormat.Alignment = StringAlignment.Center;
                        else
                            CellFormat.Alignment = StringAlignment.Near;

                        ColumnWidth = columnsWidth[CurrentCell];
                        RectangleF CellBounds = new RectangleF(CurrentX, currentY, ColumnWidth, rowsHeight[currentRow]);

                        g.DrawString(dataGridView.Rows[currentRow].Cells[CurrentCell].EditedFormattedValue.ToString(), RowFont, RowForeBrush, CellBounds, CellFormat);

                        if (dataGridView.CellBorderStyle != DataGridViewCellBorderStyle.None)
                            g.DrawRectangle(TheLinePen, CurrentX, currentY, ColumnWidth, rowsHeight[currentRow]);

                        CurrentX += ColumnWidth;
                    }
                    currentY += rowsHeight[currentRow];

                    if ((int)currentY > (pageHeight - topMargin - bottomMargin))
                    {
                        currentRow++;
                        return true;
                    }
                }
                currentRow++;
            }

            currentRow = 0;
            mColumnPoint++;

            if (mColumnPoint == mColumnPoints.Count)
            {
                mColumnPoint = 0;
                return false;
            }
            else
                return true;
        }

        /// <summary>
        /// 打印DataGridView 的
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public bool DrawDataGridView(Graphics g)
        {
            try
            {
                Calculate(g);
                DrawHeader(g);
                bool bContinue = DrawRows(g);
                return bContinue;
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: " + ex.Message.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        /// <summary>
        /// 开始打印
        /// </summary>
        /// <param name="dataGridView"></param>
        /// <param name="hasTile"></param>
        /// <param name="title"></param>
        public static void StartPrint(DataGridView dataGridView, bool hasTitle, string title)
        {
            //打印文档对象
            PrintDocument printDocument = null;
            //打印设置控件
            PrintDialog printDialog = null;
            //打印预览控件
            PrintPreviewDialog printPreviewDialog = null;

            try
            {
                //打印设置控件
                printDialog = new PrintDialog();
                // 设置当前页选项按钮
                printDialog.AllowCurrentPage = true;
                // 启用打印到文件复选框
                printDialog.AllowPrintToFile = true;
                //启用 选择 选项按钮
                printDialog.AllowSelection = true;
                //启用 页 选项按钮
                printDialog.AllowSomePages = true;
                // 选中打印到文件复选框
                printDialog.PrintToFile = true;
                // 启用帮助按钮
                printDialog.ShowHelp = true;
                // 启用网络按钮
                printDialog.ShowNetwork = true;
                if (printDialog.ShowDialog() != DialogResult.OK)
                    return;

                printDocument = new PrintDocument();

                printDocument.PrinterSettings = printDialog.PrinterSettings;
                printDocument.DefaultPageSettings = printDialog.PrinterSettings.DefaultPageSettings;
                printDocument.DefaultPageSettings.Margins = new Margins(40, 40, 40, 40);


                //实例化一个对象
                GridPrinter gridPrinter = new GridPrinter(dataGridView, printDocument, hasTitle, title, new Font("黑体", 18, FontStyle.Bold, GraphicsUnit.Point), Color.Blue);


                //打印预览对象
                printPreviewDialog = new PrintPreviewDialog();
                printPreviewDialog.Document = printDocument;
                //设置控件宽度
                printPreviewDialog.Width = 600;
                //设置控件高度
                printPreviewDialog.Height = 450;
                printPreviewDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                //打印设置控件
                if (printDialog != null) printDialog.Dispose();
                //打印预览控件
                if (printPreviewDialog != null) printPreviewDialog.Dispose();
                if (printDocument != null) printDocument.Dispose(); ;
            }
        }

    }
}
