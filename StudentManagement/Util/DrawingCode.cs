﻿using System;
using System.Drawing;

namespace StudentManagement.Util
{
    public class DrawingCode
    {
        /// <summary>
        /// Code 存储产生的验证码
        /// </summary>
        public static string Code { get; set; }
        public static Bitmap GetBitmapCode()
        {
            Random r = new Random();
            string str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string code = "";
            for (int i = 0; i < 4; i++)
            {
                int rNumber = r.Next(0, str.Length - 1);
                code += str[rNumber].ToString();
            }
            //存储产生的Code值
            DrawingCode.Code = code;
            Bitmap bmp = new Bitmap(80, 20);//设置长度和宽度分别为80和20
            Graphics g = Graphics.FromImage(bmp);//Bitmap是Image的子类，所以可以替换Image放置在函数中

            for (int i = 0; i < 4; i++)
            {
                Point p = new Point(i * 20, 0);
                string[] fonts = { "微软雅黑", "宋体", "黑体", "隶书", "仿宋" };
                Color[] colors = { Color.Blue, Color.Black, Color.Red, Color.Green };
                g.DrawString(code[i].ToString(), new Font(fonts[r.Next(0, 5)], 15, FontStyle.Bold), new SolidBrush(colors[r.Next(0, 4)]), p);
            }
            //画线，可以不要
            for (int i = 0; i < 5; i++)
            {
                Point p1 = new Point(r.Next(0, bmp.Width), r.Next(0, bmp.Height));
                Point p2 = new Point(r.Next(0, bmp.Width), r.Next(0, bmp.Height));
                g.DrawLine(new Pen(Brushes.Green), p1, p2);
            }
            return bmp;
        }
    }
}
