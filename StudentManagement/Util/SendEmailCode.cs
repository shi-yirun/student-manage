﻿using System;
using System.Net.Mail;
using System.Text;

namespace StudentManagement.Util
{
    /// <summary>
    /// 发送邮箱验证码
    /// </summary>
    public class SendEmailCode
    {
        static SendEmailCode()
        {
            Times = 59; //初始化times
        }

        /// <summary>
        /// 存储生成的验证码
        /// </summary>
        public static string Code { get; set; }
        /// <summary>
        /// 提供计时器做倒计时
        /// </summary>
        public static int Times { get; set; }


        /// <summary>
        /// 生成验证码并发送验证码
        /// </summary>
        /// <param name="sendMail">收件人</param>
        /// <returns></returns>
        public static bool GetEmail(string sendMail, string content = "", string name = "")
        {
            try
            {
                string element = "1234567890";
                string code = "";
                Random random = new Random();
                for (int i = 0; i < 6; i++)
                {
                    code += element[random.Next(0, element.Length)].ToString();
                }
                SendEmailCode.Code = code;
                if (content != "")
                {
                    content = name + "您正在修改密码，<br/><br/>" +
                     "您的验证码为：<span style='color:red;font-weight:bold;font-size:18px;'>"
                     + code + "</span><br/>(5分钟内有效)<br/><br/>工作人员不会向您索要密码、验证码等信息。" +
                     "如非本人操作，请联系客服或忽略本条信息。<br/><br/>";
                }
                else
                {
                    content = "嗨喽，终于等到您啦！<br/><br/>欢迎登录xx学生管理系统，" +
                    "您的验证码为：<span style='color:red;font-weight:bold;font-size:18px;'>"
                    + code + "</span><br/>(5分钟内有效)<br/><br/>工作人员不会向您索要密码、验证码等信息。" +
                    "如非本人操作，请联系客服或忽略本条信息。<br/><br/>";
                }
                SendEmail(sendMail, "xx学生管理系统", content);
                return true;
            }
            catch (SmtpException)
            {
                return false;
            }

        }

        /// <summary>
        ///   发送邮箱验证码
        /// </summary>
        /// <param name="mailTo">收件人</param>
        /// <param name="mailSubject">主题</param>
        /// <param name="mailContent">内容</param>
        /// <returns></returns>
        private static void SendEmail(string mailTo, string mailSubject, string mailContent)
        {
            // 设置发送方的邮件信息,例如使用腾讯的smtp
            string smtpServer = "smtp.qq.com"; //SMTP服务器
            string mailFrom = "1694511572@qq.com"; //登陆用户名
            string userPassword = "mpjfbaesykpkegbd";//登陆密码,如果使用的是腾讯的 用的是授权码

            // 邮件服务设置
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;//指定电子邮件发送方式
            smtpClient.Host = smtpServer; //指定SMTP服务器
            //指定是否使用SSL加密
            smtpClient.EnableSsl = true;
            //不使用默认凭证，使用自己的凭证
            smtpClient.UseDefaultCredentials = false;
            // 设置自己凭证（用户名和密码）
            smtpClient.Credentials = new System.Net.NetworkCredential(mailFrom, userPassword);
            // 发送邮件设置       
            MailMessage mailMessage = new MailMessage(mailFrom, mailTo); // 发送人和收件人
            mailMessage.Subject = mailSubject;//主题
            mailMessage.Body = mailContent;//内容
            mailMessage.BodyEncoding = Encoding.UTF8;//正文编码
            mailMessage.IsBodyHtml = true;//设置为HTML格式
            mailMessage.Priority = MailPriority.Low;//优先级
            smtpClient.Send(mailMessage); // 发送邮件
        }
    }
}
