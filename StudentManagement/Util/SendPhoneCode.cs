﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;

namespace StudentManagement.Util
{
    /// <summary>
    /// 给手机发送验证码
    /// </summary>
    public class SendPhoneCode
    {
        static SendPhoneCode()
        {
            Times = 59;
        }

        public static int Code { get; set; }
        public static int Times { get; set; }
        /// <summary>
        /// 请求地址
        /// </summary>
        private static string PostUrl = ConfigurationManager.AppSettings["WebReference.Service.PostUrl"];
        /// <summary>
        /// 给手机发送验证码
        /// </summary>
        /// <param name="mobilePhone">手机号</param>
        /// <returns></returns>
        public static int SendCode(string mobilePhone)
        {
            string account = "C37294730";//查看用户名 
            string password = "ec13d71341d25efa711ee4f77b3cc0be"; //查看密码                                             
            Random rad = new Random();
            int mobile_code = rad.Next(1000, 10000);
            Code = mobile_code;
            //发送的内容
            string content = "您的验证码是：" + mobile_code + " 。请不要把验证码泄露给其他人。";
            //用户名+密码+注册的手机号+验证码
            string postStrTpl = string.Format("account={0}&password={1}&mobile={2}&content={3}", account, password, mobilePhone, content);
            // 传输到后台的数据
            byte[] postData = Encoding.UTF8.GetBytes(postStrTpl);
            //http 类型发送请求到 PostUrl
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(PostUrl);
            //http 发送请求提交的方式
            myRequest.Method = "POST";
            // http请求响应头
            myRequest.ContentType = "application/x-www-form-urlencoded";
            // 发送数据的长度
            myRequest.ContentLength = postData.Length;
            //把数据发送到服务器
            Stream newStream = myRequest.GetRequestStream();
            newStream.Write(postData, 0, postData.Length);
            newStream.Flush();
            newStream.Close();
            //http响应
            HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
            //判断响应状态，是否成功
            if (myResponse.StatusCode == HttpStatusCode.OK)
            {
                StreamReader reader = new StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
                string res = reader.ReadToEnd();
                int len1 = res.IndexOf("</code>");
                int len2 = res.IndexOf("<code>");
                string code = res.Substring((len2 + 6), (len1 - len2 - 6));
                return Convert.ToInt32(code);
            }
            else
            {
                return 0;
            }
        }
    }
}
