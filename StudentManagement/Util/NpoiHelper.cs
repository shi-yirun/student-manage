﻿using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;

namespace StudentManagement.Util
{
    public class NpoiHelper
    {
        /// <summary>
        /// 自动设置列宽
        /// </summary>
        /// <param name="sheet"></param>
        static void AutoSizeColumns(ISheet sheet)
        {
            if (sheet.PhysicalNumberOfRows > 0)
            {
                IRow headerRow = sheet.GetRow(0);
                for (int i = 0, l = headerRow.LastCellNum; i < l; i++)
                {
                    sheet.AutoSizeColumn(i);
                }
            }
        }

        /// <summary>
        /// 导出DataTable到Excel中 
        /// </summary>
        /// <param name="table">数据表</param>
        /// <param name="headTextList">标题名</param>
        /// <param name="filePath">文件存储的路径</param>
        /// <param name="fileName">Excel.xslx文件名字</param>
        /// <returns></returns>

        public static int ExportExcel(DataTable table, List<string> headTextList, string filePath, string fileName)
        {
            // -1 表示数据为空
            if (table == null)
            {
                return -1;
            }
            else
            {
                //创建工作簿
                IWorkbook workbook;
                //单元格格式对象
                ICellStyle cellStyle, cellHeadStyle;
                // 字体对象
                IFont font;
                //单元格对象
                ICell cell;
                //创建Excel文件的对象
                workbook = new XSSFWorkbook();
                //创建一个Sheet对象
                ISheet sheet = workbook.CreateSheet("sheet1");
                //创建一个单元格样式对象
                cellStyle = workbook.CreateCellStyle();
                cellHeadStyle = workbook.CreateCellStyle();
                //创建一个字体对象
                font = workbook.CreateFont();
                font.FontName = "宋体";
                cellStyle.SetFont(font);

                //设置单元格头部样式
                cellHeadStyle.VerticalAlignment = VerticalAlignment.Center;
                cellHeadStyle.FillBackgroundColor = HSSFColor.SkyBlue.Index;
                cellHeadStyle.FillForegroundColor = HSSFColor.White.Index;
                cellHeadStyle.FillPattern = FillPattern.FineDots;
                font = workbook.CreateFont();
                font.FontName = "黑体";
                cellHeadStyle.SetFont(font);

                // 从sheet对象里面去创建一行
                IRow row = sheet.CreateRow(0);

                //读取列名,并给excel表头赋值
                for (int i = 0; i < headTextList.Count; i++)
                {
                    cell = row.CreateCell(i);
                    cell.SetCellValue(headTextList[i]);
                    cell.CellStyle = cellHeadStyle;
                }

                //从datatable中读取数据并写入Excel
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    row = sheet.CreateRow(i + 1);
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        cell = row.CreateCell(j);
                        cell.SetCellValue(table.Rows[i][j].ToString());
                        cell.CellStyle = cellStyle;
                    }
                }

                //自动计算列宽
                AutoSizeColumns(sheet);
                // 关闭Excel文件
                CloseExcelWorkbook(fileName);
                //使用文件流
                using (FileStream stream = new FileStream(filePath, FileMode.Create))
                {
                    try
                    {
                        workbook.Write(stream);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        workbook.Close();  //释放资源
                    }
                }
                return 1; //写入成功
            }

        }

        /// <summary>
        /// 判断名为workbookName的Excel文件是否被打开,如果被打开，就关闭
        /// </summary>
        /// <param name="workbookName"></param>
        internal static void CloseExcelWorkbook(string workbookName)
        {

            try
            {
                //Process[] plist = Process.GetProcessesByName("Excel", ".");
                //if (plist.Length > 1)
                //    throw new Exception("More than one Excel process running.");
                //else if (plist.Length == 0)
                //    throw new Exception("No Excel process running.");


                //从运行列表中获取正在运行的Excel对象
                Object obj = Marshal.GetActiveObject("Excel.Application");
                //如果没有excel文件在运行，就没有必要进行关闭excel
                Microsoft.Office.Interop.Excel.Application excelAppl = (Microsoft.Office.Interop.Excel.Application)obj;
                Microsoft.Office.Interop.Excel.Workbooks workbooks = excelAppl.Workbooks;
                foreach (Microsoft.Office.Interop.Excel.Workbook wkbk in workbooks)
                {
                    if (wkbk.Name == workbookName)
                        wkbk.Close();
                }
                //dispose
                //workbooks.Close(); //this would close all workbooks
                GC.Collect();
                GC.WaitForPendingFinalizers();
                if (workbooks != null)
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbooks);
                //excelAppl.Quit(); //would close the excel application
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excelAppl);
                GC.Collect();
            }
            // 当找不到对象时(也就是返回null),直接return
            catch (System.Runtime.InteropServices.COMException)
            {
                return; //就不往下面继续执行
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }




    }
}
