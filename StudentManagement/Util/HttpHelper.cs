﻿using System.IO;
using System.Net;

namespace StudentManagement.Util
{
    class HttpHelper
    {
        /// <summary>
        /// 获取http响应信息
        /// </summary>
        /// <param name="url">url地址</param>
        public static string GetHttpResponseInfo(string url)
        {
            try
            {
                //使用http协议发送一个get 请求
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
                myRequest.Method = "GET";
                myRequest.ContentType = "text/html; charset=utf-8";
                myRequest.Accept = "*/*";
                //http响应
                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                //判断响应状态，是否成功
                if (myResponse.StatusCode == HttpStatusCode.OK)
                {

                    StreamReader reader = new StreamReader(myResponse.GetResponseStream());
                    string res = reader.ReadToEnd();
                    return res;
                }
                else
                {

                    return "";
                }
            }
            catch (HttpListenerException)
            {
                return "";
            }

        }
    }
}
