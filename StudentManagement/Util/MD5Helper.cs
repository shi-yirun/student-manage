﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace StudentManagement.Util
{
    class MD5Helper
    {
        /// <summary>
        /// 用于将字符串进行md5加密
        /// </summary>
        /// <param name="str">要进行加密的字符串</param>
        /// <returns></returns>
        public static string GetMD5Str(string str)
        {
            MD5 md = new MD5CryptoServiceProvider();
            byte[] buff = md.ComputeHash(Encoding.UTF8.GetBytes(str));
            //将一个字节数组转为字符串
            return BitConverter.ToString(buff).Replace("-", "");
        }
    }
}
