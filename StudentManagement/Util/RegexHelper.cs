﻿using System.Text.RegularExpressions;

namespace StudentManagement.Util
{
    public class RegexHelper
    {

        /// <summary>
        /// 验证是不是手机号码
        /// </summary>
        /// <param name="input"></param>
        /// <returns>是返回true,不是返回false</returns>
        public static bool IsMobilePhone(string input)
        {
            return Regex.IsMatch(input, "^1[34578]\\d{9}$");
        }
        /// <summary>
        /// 验证是不是邮箱
        /// </summary>
        /// <param name="input">邮箱号</param>
        /// <returns>是返回true,不是返回false</returns>
        public static bool IsEmail(string input)
        {
            return Regex.IsMatch(input, @"^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$");
        }
        /// <summary>
        /// 匹配数字
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsNumber(string input)
        {
            return Regex.IsMatch(input, @"^[+]{0,1}(\d+)$");
        }

        /// <summary>
        /// 匹配数字
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsFlootNumber(string input)
        {
            return Regex.IsMatch(input, @"^\d+(\.\d+)?$");
        }

    }
}
