﻿
namespace StudentManagement.Student
{
    partial class FrmIndex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmIndex));
            this.btnSetting = new Sunny.UI.UIHeaderButton();
            this.btnTools = new Sunny.UI.UIHeaderButton();
            this.btnSelected = new Sunny.UI.UIHeaderButton();
            this.btnEvent = new Sunny.UI.UIHeaderButton();
            this.btnInfo = new Sunny.UI.UIHeaderButton();
            this.btnPower = new Sunny.UI.UIHeaderButton();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.SuspendLayout();
            // 
            // btnSetting
            // 
            this.btnSetting.BackColor = System.Drawing.Color.Transparent;
            this.btnSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSetting.CircleColor = System.Drawing.Color.RoyalBlue;
            this.btnSetting.CircleHoverColor = System.Drawing.Color.DarkGray;
            this.btnSetting.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSetting.FillColor = System.Drawing.Color.Transparent;
            this.btnSetting.FillHoverColor = System.Drawing.Color.Gray;
            this.btnSetting.FillPressColor = System.Drawing.Color.Gray;
            this.btnSetting.FillSelectedColor = System.Drawing.Color.Transparent;
            this.btnSetting.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnSetting.ForeColor = System.Drawing.Color.Black;
            this.btnSetting.ForeHoverColor = System.Drawing.Color.DarkGray;
            this.btnSetting.ForePressColor = System.Drawing.Color.DarkGray;
            this.btnSetting.ForeSelectedColor = System.Drawing.Color.DarkGray;
            this.btnSetting.Location = new System.Drawing.Point(631, 454);
            this.btnSetting.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Padding = new System.Windows.Forms.Padding(0, 8, 0, 3);
            this.btnSetting.Radius = 300;
            this.btnSetting.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnSetting.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.btnSetting.ShowCircleHoverColor = true;
            this.btnSetting.Size = new System.Drawing.Size(213, 94);
            this.btnSetting.Style = Sunny.UI.UIStyle.Custom;
            this.btnSetting.StyleCustomMode = true;
            this.btnSetting.Symbol = 61459;
            this.btnSetting.SymbolOffset = new System.Drawing.Point(2, 5);
            this.btnSetting.TabIndex = 25;
            this.btnSetting.Text = "系统设置";
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // btnTools
            // 
            this.btnTools.BackColor = System.Drawing.Color.Transparent;
            this.btnTools.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnTools.CircleColor = System.Drawing.Color.RoyalBlue;
            this.btnTools.CircleHoverColor = System.Drawing.Color.DarkGray;
            this.btnTools.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTools.FillColor = System.Drawing.Color.Transparent;
            this.btnTools.FillHoverColor = System.Drawing.Color.Gray;
            this.btnTools.FillPressColor = System.Drawing.Color.Gray;
            this.btnTools.FillSelectedColor = System.Drawing.Color.Transparent;
            this.btnTools.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnTools.ForeColor = System.Drawing.Color.Black;
            this.btnTools.ForeHoverColor = System.Drawing.Color.DarkGray;
            this.btnTools.ForePressColor = System.Drawing.Color.DarkGray;
            this.btnTools.ForeSelectedColor = System.Drawing.Color.DarkGray;
            this.btnTools.Location = new System.Drawing.Point(337, 454);
            this.btnTools.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnTools.Name = "btnTools";
            this.btnTools.Padding = new System.Windows.Forms.Padding(0, 8, 0, 3);
            this.btnTools.Radius = 300;
            this.btnTools.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnTools.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.btnTools.ShowCircleHoverColor = true;
            this.btnTools.Size = new System.Drawing.Size(213, 94);
            this.btnTools.Style = Sunny.UI.UIStyle.Custom;
            this.btnTools.StyleCustomMode = true;
            this.btnTools.Symbol = 361831;
            this.btnTools.SymbolOffset = new System.Drawing.Point(0, 5);
            this.btnTools.SymbolSize = 40;
            this.btnTools.TabIndex = 24;
            this.btnTools.Text = "工具箱";
            this.btnTools.Click += new System.EventHandler(this.btnTools_Click);
            // 
            // btnSelected
            // 
            this.btnSelected.BackColor = System.Drawing.Color.Transparent;
            this.btnSelected.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSelected.CircleColor = System.Drawing.Color.RoyalBlue;
            this.btnSelected.CircleHoverColor = System.Drawing.Color.DarkGray;
            this.btnSelected.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelected.FillColor = System.Drawing.Color.Transparent;
            this.btnSelected.FillHoverColor = System.Drawing.Color.Gray;
            this.btnSelected.FillPressColor = System.Drawing.Color.Gray;
            this.btnSelected.FillSelectedColor = System.Drawing.Color.Transparent;
            this.btnSelected.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnSelected.ForeColor = System.Drawing.Color.Black;
            this.btnSelected.ForeHoverColor = System.Drawing.Color.DarkGray;
            this.btnSelected.ForePressColor = System.Drawing.Color.DarkGray;
            this.btnSelected.ForeSelectedColor = System.Drawing.Color.DarkGray;
            this.btnSelected.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnSelected.Location = new System.Drawing.Point(25, 454);
            this.btnSelected.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSelected.Name = "btnSelected";
            this.btnSelected.Padding = new System.Windows.Forms.Padding(0, 8, 0, 3);
            this.btnSelected.Radius = 300;
            this.btnSelected.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnSelected.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.btnSelected.ShowCircleHoverColor = true;
            this.btnSelected.Size = new System.Drawing.Size(213, 94);
            this.btnSelected.Style = Sunny.UI.UIStyle.Custom;
            this.btnSelected.StyleCustomMode = true;
            this.btnSelected.Symbol = 61485;
            this.btnSelected.TabIndex = 23;
            this.btnSelected.Text = "选课信息";
            this.btnSelected.Click += new System.EventHandler(this.btnTeach_Click);
            // 
            // btnEvent
            // 
            this.btnEvent.BackColor = System.Drawing.Color.Transparent;
            this.btnEvent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnEvent.CircleColor = System.Drawing.Color.RoyalBlue;
            this.btnEvent.CircleHoverColor = System.Drawing.Color.DimGray;
            this.btnEvent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEvent.FillColor = System.Drawing.Color.Transparent;
            this.btnEvent.FillSelectedColor = System.Drawing.Color.Transparent;
            this.btnEvent.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnEvent.ForeColor = System.Drawing.Color.Black;
            this.btnEvent.ForeHoverColor = System.Drawing.Color.DarkGray;
            this.btnEvent.ForePressColor = System.Drawing.Color.DarkGray;
            this.btnEvent.ForeSelectedColor = System.Drawing.Color.DarkGray;
            this.btnEvent.Location = new System.Drawing.Point(631, 270);
            this.btnEvent.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnEvent.Name = "btnEvent";
            this.btnEvent.Padding = new System.Windows.Forms.Padding(0, 8, 0, 3);
            this.btnEvent.Radius = 300;
            this.btnEvent.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnEvent.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.btnEvent.ShowCircleHoverColor = true;
            this.btnEvent.Size = new System.Drawing.Size(213, 94);
            this.btnEvent.Style = Sunny.UI.UIStyle.Custom;
            this.btnEvent.StyleCustomMode = true;
            this.btnEvent.Symbol = 362809;
            this.btnEvent.SymbolOffset = new System.Drawing.Point(3, 2);
            this.btnEvent.TabIndex = 22;
            this.btnEvent.Text = "奖惩信息";
            this.btnEvent.Click += new System.EventHandler(this.btnMenber_Click);
            // 
            // btnInfo
            // 
            this.btnInfo.BackColor = System.Drawing.Color.Transparent;
            this.btnInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnInfo.CircleColor = System.Drawing.Color.RoyalBlue;
            this.btnInfo.CircleHoverColor = System.Drawing.Color.Gray;
            this.btnInfo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnInfo.FillColor = System.Drawing.Color.Transparent;
            this.btnInfo.FillSelectedColor = System.Drawing.Color.Transparent;
            this.btnInfo.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnInfo.ForeColor = System.Drawing.Color.Black;
            this.btnInfo.ForeHoverColor = System.Drawing.Color.DarkGray;
            this.btnInfo.ForePressColor = System.Drawing.Color.DarkGray;
            this.btnInfo.ForeSelectedColor = System.Drawing.Color.DarkGray;
            this.btnInfo.Location = new System.Drawing.Point(337, 270);
            this.btnInfo.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Padding = new System.Windows.Forms.Padding(0, 8, 0, 3);
            this.btnInfo.Radius = 300;
            this.btnInfo.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnInfo.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.btnInfo.ShowCircleHoverColor = true;
            this.btnInfo.Size = new System.Drawing.Size(213, 94);
            this.btnInfo.Style = Sunny.UI.UIStyle.Custom;
            this.btnInfo.StyleCustomMode = true;
            this.btnInfo.Symbol = 61720;
            this.btnInfo.SymbolOffset = new System.Drawing.Point(1, 2);
            this.btnInfo.TabIndex = 21;
            this.btnInfo.Text = "个人信息";
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // btnPower
            // 
            this.btnPower.BackColor = System.Drawing.Color.Transparent;
            this.btnPower.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPower.CircleColor = System.Drawing.Color.RoyalBlue;
            this.btnPower.CircleHoverColor = System.Drawing.Color.Gray;
            this.btnPower.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPower.FillColor = System.Drawing.Color.Transparent;
            this.btnPower.FillHoverColor = System.Drawing.Color.Gray;
            this.btnPower.FillPressColor = System.Drawing.Color.Gray;
            this.btnPower.FillSelectedColor = System.Drawing.Color.Transparent;
            this.btnPower.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnPower.ForeColor = System.Drawing.Color.Black;
            this.btnPower.ForeHoverColor = System.Drawing.Color.DarkGray;
            this.btnPower.ForePressColor = System.Drawing.Color.DarkGray;
            this.btnPower.ForeSelectedColor = System.Drawing.Color.DarkGray;
            this.btnPower.Location = new System.Drawing.Point(25, 270);
            this.btnPower.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnPower.Name = "btnPower";
            this.btnPower.Padding = new System.Windows.Forms.Padding(0, 8, 0, 3);
            this.btnPower.Radius = 300;
            this.btnPower.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnPower.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.btnPower.ShowCircleHoverColor = true;
            this.btnPower.Size = new System.Drawing.Size(213, 94);
            this.btnPower.Style = Sunny.UI.UIStyle.Custom;
            this.btnPower.StyleCustomMode = true;
            this.btnPower.Symbol = 61950;
            this.btnPower.SymbolOffset = new System.Drawing.Point(2, 3);
            this.btnPower.TabIndex = 19;
            this.btnPower.Text = "成绩信息";
            this.btnPower.Click += new System.EventHandler(this.btnPower_Click);
            // 
            // uiLabel1
            // 
            this.uiLabel1.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel1.Font = new System.Drawing.Font("隶书", 42F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel1.Location = new System.Drawing.Point(65, 69);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(779, 136);
            this.uiLabel1.TabIndex = 20;
            this.uiLabel1.Text = "欢迎使用学生管理系统";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmIndex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1315, 693);
            this.Controls.Add(this.btnSetting);
            this.Controls.Add(this.btnTools);
            this.Controls.Add(this.btnSelected);
            this.Controls.Add(this.btnEvent);
            this.Controls.Add(this.btnInfo);
            this.Controls.Add(this.btnPower);
            this.Controls.Add(this.uiLabel1);
            this.Name = "FrmIndex";
            this.Text = "首页";
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIHeaderButton btnSetting;
        private Sunny.UI.UIHeaderButton btnTools;
        private Sunny.UI.UIHeaderButton btnSelected;
        private Sunny.UI.UIHeaderButton btnEvent;
        private Sunny.UI.UIHeaderButton btnInfo;
        private Sunny.UI.UIHeaderButton btnPower;
        private Sunny.UI.UILabel uiLabel1;
    }
}