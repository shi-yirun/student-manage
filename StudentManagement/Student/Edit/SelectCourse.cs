﻿using BLL.Admin;
using Sunny.UI;
using System;
using System.Data;
using System.Windows.Forms;
namespace StudentManagement.Student.Edit
{
    public partial class SelectCourse : UIEditForm
    {
        public SelectCourse()
        {
            InitializeComponent();
        }

        private void SelectCourse_Load(object sender, EventArgs e)
        {

            CourseBind();
            this.uiPagination1.ActivePage = 1;

        }

        private void Initi()
        {
            //获取当前页面的index
            int index = this.uiPagination1.ActivePage - 1;
            int pageIndex = this.uiPagination1.ActivePage;
            int size = this.uiPagination1.PageSize;
            int begin = (index * size) + 1;
            int end = (index + 1) * size;
            int count;
            int id = Program.Student.Id;
            int course = (int)this.cmbCourse.SelectedValue;
            this.dgvCoureseList.DataSource = new CourseBLL().GetCourseList1(id, course, out count, begin, end);
            uiPagination1.TotalCount = count;
        }

        private void CourseBind()
        {
            this.cmbCourse.DisplayMember = "CourseName";
            this.cmbCourse.ValueMember = "Id";
            int id = Program.Student.Id;
            DataTable table = new CourseBLL().GetCourseList1(id);
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["CourseName"] = "请选择课程";
            table.Rows.InsertAt(row, 0);
            this.cmbCourse.DataSource = table;
        }

        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            Initi();
        }

        private void dgvCoureseList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgvCoureseList.Rows.Count <= 0)
            {
                ShowWarningDialog("没有可以选则的数据");
                return;
            }
            if (this.dgvCoureseList.CurrentRow == null)
            {
                ShowWarningDialog("没有选中任何数据");
                return;
            }
            Model.Score score = new Model.Score();
            score.StuId = Program.Student.Id;
            score.CourseId = (int)this.dgvCoureseList.SelectedRows[0].Cells["Id"].Value;
            new ScoreBLL().AddScore(score);
            ShowSuccessDialog("添加成功");

            CourseBind();
            Initi();

        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            Initi();
        }

        private void btnOK_Click_1(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            TagString = "添加成功";
        }
    }
}
