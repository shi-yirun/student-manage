﻿using BLL.Admin;
using Sunny.UI;
using System;
namespace StudentManagement.Student
{
    public partial class FrmEvent : UIPage
    {
        public FrmEvent()
        {
            InitializeComponent();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            EventBind();
        }

        private void FrmEvent_Load(object sender, EventArgs e)
        {
            this.cmbStudent.SelectedIndex = 0;
            this.uiPagination1.ActivePage = 1;

        }

        private void EventBind()
        {

            int index = this.uiPagination1.ActivePage - 1;
            int size = this.uiPagination1.PageSize;
            int begin = index * size + 1;
            int end = (index + 1) * size;
            //总行数
            int count;

            string type = this.cmbStudent.SelectedText;
            this.dgvStudentList.DataSource = new EventBLL().GetEventList(begin, end, out count, type, Program.Student.Id);
            //设置行数
            this.uiPagination1.TotalCount = count;
        }

        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            EventBind();


        }

        private void FrmEvent_Initialize(object sender, EventArgs e)
        {

        }
    }
}
