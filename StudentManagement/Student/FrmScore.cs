﻿using BLL.Admin;
using Sunny.UI;
using System;
using System.Data;
namespace StudentManagement.Student
{
    public partial class FrmScore : UIPage
    {
        public FrmScore()
        {
            InitializeComponent();
        }

        private void FrmScore_Load(object sender, EventArgs e)
        {
            CourseBind();
            this.uiPagination1.ActivePage = 1;
        }
        private void CourseBind()
        {
            this.cmbCourse.DisplayMember = "CourseName";
            this.cmbCourse.ValueMember = "Id";
            int id = Program.Student.Id;
            DataTable table = new CourseBLL().GetCourseList(id);
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["CourseName"] = "请选择课程";
            table.Rows.InsertAt(row, 0);
            this.cmbCourse.DataSource = table;
        }
        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            TableBind();
        }

        private void TableBind()
        {
            //获取当前页面的index
            int index = this.uiPagination1.ActivePage - 1;
            int pageIndex = this.uiPagination1.ActivePage;
            int size = this.uiPagination1.PageSize;
            int begin = (index * size) + 1;
            int end = (index + 1) * size;
            int count;
            int cId = (int)this.cmbCourse.SelectedValue;
            int sId = Program.Student.Id;
            this.dgvScoreList.DataSource = new ScoreBLL().GetScoreList(begin, end, out count, cId, sId);
            uiPagination1.TotalCount = count;
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            TableBind();
        }
    }
}
