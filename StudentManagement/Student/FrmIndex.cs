﻿using Sunny.UI;
using System;

namespace StudentManagement.Student
{
    public partial class FrmIndex : UIPage
    {
        public FrmIndex()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 成绩信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPower_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2010);
        }

        /// <summary>
        /// 个人信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnInfo_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2003);
        }

        /// <summary>
        /// 奖惩信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenber_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2009);
        }

        /// <summary>
        /// 选课信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTeach_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2011);
        }

        /// <summary>
        /// 工具箱
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTools_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2012);
        }

        /// <summary>
        /// 系统设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSetting_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2005);

        }
    }
}
