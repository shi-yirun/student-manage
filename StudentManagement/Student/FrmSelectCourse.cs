﻿using BLL.Admin;
using StudentManagement.Student.Edit;
using Sunny.UI;
using System;
using System.Data;
using System.Windows.Forms;
namespace StudentManagement.Student
{
    public partial class FrmSelectCourse : UIPage
    {
        public FrmSelectCourse()
        {
            InitializeComponent();
        }

        private void FrmSelectCourse_Load(object sender, EventArgs e)
        {
            CourseBind();
            this.uiPagination1.ActivePage = 1;
        }

        private void CourseBind()
        {
            this.cmbCourse.DisplayMember = "CourseName";
            this.cmbCourse.ValueMember = "Id";
            int id = Program.Student.Id;
            DataTable table = new CourseBLL().GetCourseList(id);
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["CourseName"] = "请选择课程";
            table.Rows.InsertAt(row, 0);
            this.cmbCourse.DataSource = table;
        }
        private void CourseTableBind()
        {
            //获取当前页面的index
            int index = this.uiPagination1.ActivePage - 1;
            int pageIndex = this.uiPagination1.ActivePage;
            int size = this.uiPagination1.PageSize;
            int begin = (index * size) + 1;
            int end = (index + 1) * size;
            int count;
            int id = Program.Student.Id;
            int course = (int)this.cmbCourse.SelectedValue;
            this.dgvCoureseList.DataSource = new StudentBLL().SelectStudent(id, course, out count, begin, end);
            uiPagination1.TotalCount = count;
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            CourseTableBind();
        }


        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            CourseTableBind();
        }

        private void btnCourseAdd_Click(object sender, EventArgs e)
        {
            //设置标题
            SelectCourse selectCourse = new SelectCourse();
            selectCourse.Text = "添加课程";
            DialogResult result = selectCourse.ShowDialog();
            //如果接收到的返回值为ok，那么就关闭窗体
            if (DialogResult.OK == result)
            {
                //关闭窗体 
                selectCourse.Dispose();
                //接受一下tagString 的值，如果为添加成功，那么就刷新
                if (selectCourse.TagString == "添加成功")
                {
                    CourseTableBind(); //刷新学生信息
                    CourseBind();
                }
            }
        }

        private void btnCourseDelete_Click(object sender, EventArgs e)
        {
            //获取选中行的数据
            if (this.dgvCoureseList.Rows.Count <= 0)
            {
                ShowWarningDialog("没有可以选则的数据");
                return;
            }
            if (this.dgvCoureseList.CurrentRow == null)
            {
                ShowWarningDialog("没有选中任何数据");
                return;
            }
            Model.Score score = new Model.Score();
            score.StuId = Program.Student.Id;
            score.CourseId = (int)this.dgvCoureseList.SelectedRows[0].Cells["Id"].Value;
            new ScoreBLL().Delete(score);
            CourseTableBind(); //刷新学生信息
            CourseBind();
        }
    }
}
