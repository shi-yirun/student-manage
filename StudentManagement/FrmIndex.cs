﻿using Sunny.UI;
using System;
namespace StudentManagement
{
    public partial class FrmIndex : UIPage
    {
        public FrmIndex()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 权限管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btnIndex_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2020);
        }
        /// <summary>
        /// 个人信息管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnInfo_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2003);
        }
        /// <summary>
        /// 人员信息管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenber_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2008);

        }
        /// <summary>
        /// 教学管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTeach_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2011);
        }
        /// <summary>
        /// 工具箱
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTools_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2022);
        }
        /// <summary>
        /// 系统设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSetting_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2005);

        }
    }
}
