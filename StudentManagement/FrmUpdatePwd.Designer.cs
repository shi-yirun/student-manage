﻿
namespace StudentManagement
{
    partial class FrmUpdatePwd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.btnUpdate = new Sunny.UI.UIButton();
            this.txtEmailCode = new Sunny.UI.UITextBox();
            this.txtpwd = new Sunny.UI.UITextBox();
            this.btnSendEmailCode = new Sunny.UI.UIButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.txtNewpwd = new Sunny.UI.UITextBox();
            this.SuspendLayout();
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(192, 106);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(100, 23);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "新密码";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(192, 162);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(100, 23);
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "确认密码";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(192, 221);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(100, 23);
            this.uiLabel3.TabIndex = 2;
            this.uiLabel3.Text = "验证码";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnUpdate.Location = new System.Drawing.Point(197, 275);
            this.btnUpdate.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(357, 35);
            this.btnUpdate.TabIndex = 5;
            this.btnUpdate.Text = "提交";
            this.btnUpdate.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // txtEmailCode
            // 
            this.txtEmailCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEmailCode.FillColor = System.Drawing.Color.White;
            this.txtEmailCode.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtEmailCode.Location = new System.Drawing.Point(304, 210);
            this.txtEmailCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEmailCode.Maximum = 2147483647D;
            this.txtEmailCode.Minimum = -2147483648D;
            this.txtEmailCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtEmailCode.Name = "txtEmailCode";
            this.txtEmailCode.Radius = 30;
            this.txtEmailCode.Size = new System.Drawing.Size(131, 34);
            this.txtEmailCode.Symbol = 61528;
            this.txtEmailCode.TabIndex = 3;
            this.txtEmailCode.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtEmailCode.Watermark = "验证码";
            // 
            // txtpwd
            // 
            this.txtpwd.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtpwd.FillColor = System.Drawing.Color.White;
            this.txtpwd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtpwd.Location = new System.Drawing.Point(304, 149);
            this.txtpwd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtpwd.Maximum = 2147483647D;
            this.txtpwd.Minimum = -2147483648D;
            this.txtpwd.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtpwd.Name = "txtpwd";
            this.txtpwd.PasswordChar = '*';
            this.txtpwd.Radius = 30;
            this.txtpwd.Size = new System.Drawing.Size(260, 36);
            this.txtpwd.Symbol = 61475;
            this.txtpwd.TabIndex = 2;
            this.txtpwd.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtpwd.Watermark = "请再次输入密码";
            // 
            // btnSendEmailCode
            // 
            this.btnSendEmailCode.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSendEmailCode.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSendEmailCode.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.btnSendEmailCode.ForeColor = System.Drawing.Color.Black;
            this.btnSendEmailCode.ForeHoverColor = System.Drawing.Color.Black;
            this.btnSendEmailCode.ForePressColor = System.Drawing.Color.Black;
            this.btnSendEmailCode.ForeSelectedColor = System.Drawing.Color.Black;
            this.btnSendEmailCode.Location = new System.Drawing.Point(449, 213);
            this.btnSendEmailCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSendEmailCode.Name = "btnSendEmailCode";
            this.btnSendEmailCode.Radius = 25;
            this.btnSendEmailCode.RectColor = System.Drawing.Color.White;
            this.btnSendEmailCode.RectSelectedColor = System.Drawing.Color.White;
            this.btnSendEmailCode.Size = new System.Drawing.Size(115, 31);
            this.btnSendEmailCode.Style = Sunny.UI.UIStyle.Custom;
            this.btnSendEmailCode.StyleCustomMode = true;
            this.btnSendEmailCode.TabIndex = 4;
            this.btnSendEmailCode.Text = "发送验证码";
            this.btnSendEmailCode.TipsColor = System.Drawing.Color.Black;
            this.btnSendEmailCode.Click += new System.EventHandler(this.btnSendEmailCode_Click);
            // 
            // txtNewpwd
            // 
            this.txtNewpwd.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtNewpwd.FillColor = System.Drawing.Color.White;
            this.txtNewpwd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtNewpwd.Location = new System.Drawing.Point(304, 93);
            this.txtNewpwd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNewpwd.Maximum = 2147483647D;
            this.txtNewpwd.Minimum = -2147483648D;
            this.txtNewpwd.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtNewpwd.Name = "txtNewpwd";
            this.txtNewpwd.PasswordChar = '*';
            this.txtNewpwd.Radius = 30;
            this.txtNewpwd.Size = new System.Drawing.Size(260, 36);
            this.txtNewpwd.Symbol = 61475;
            this.txtNewpwd.TabIndex = 1;
            this.txtNewpwd.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtNewpwd.Watermark = "请输入新密码";
            // 
            // FrmUpdatePwd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtNewpwd);
            this.Controls.Add(this.btnSendEmailCode);
            this.Controls.Add(this.txtpwd);
            this.Controls.Add(this.txtEmailCode);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.uiLabel3);
            this.Controls.Add(this.uiLabel2);
            this.Controls.Add(this.uiLabel1);
            this.Name = "FrmUpdatePwd";
            this.Text = "修改密码";
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UIButton btnUpdate;
        private Sunny.UI.UITextBox txtEmailCode;
        private Sunny.UI.UITextBox txtpwd;
        private Sunny.UI.UIButton btnSendEmailCode;
        private System.Windows.Forms.Timer timer1;
        private Sunny.UI.UITextBox txtNewpwd;
    }
}