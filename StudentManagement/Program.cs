﻿using Sunny.UI;
using System;
using System.Windows.Forms;
namespace StudentManagement
{
    static class Program
    {
        /// <summary>
        /// 学生
        /// </summary>
        public static Model.Student Student { get; set; }
        /// <summary>
        /// 教师
        /// </summary>
        public static Model.Teacher Teacher { get; set; }
        /// <summary>
        /// 管理员
        /// </summary>
        public static Model.Admin Admin { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public static string Type { get; set; }
        /// <summary>
        /// 存储导航栏对象，用于页面之间的跳转
        /// </summary>
        public static UINavMenu Aside;

        /// <summary>
        /// 存储Logo
        /// </summary>
        public static UIAvatar uIAvatar;

        /// <summary>
        /// 存储用户头像
        /// </summary>
        public static UIAvatar UserImage;


        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FrmLogin frmLogin = new FrmLogin();
            DialogResult result = frmLogin.ShowDialog();//显示窗体
            if (DialogResult.OK == result)
            {
                Application.Run(new FrmMain());
            }
            else
            {
                Application.Exit();
            }
        }
    }
}
