﻿
namespace StudentManagement
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.uiAvatar2 = new Sunny.UI.UIAvatar();
            this.uiAvatar5 = new Sunny.UI.UIAvatar();
            this.userImage = new Sunny.UI.UIAvatar();
            this.lblUserName = new Sunny.UI.UILabel();
            this.uiContextMenuStrip1 = new Sunny.UI.UIContextMenuStrip();
            this.tsmInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmUpdatePwd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSetting = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExit = new System.Windows.Forms.ToolStripMenuItem();
            this.timelable = new Sunny.UI.UILabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Header.SuspendLayout();
            this.uiContextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Aside
            // 
            this.Aside.LineColor = System.Drawing.Color.Black;
            this.Aside.Size = new System.Drawing.Size(250, 778);
            // 
            // Header
            // 
            this.Header.Controls.Add(this.userImage);
            this.Header.Controls.Add(this.timelable);
            this.Header.Controls.Add(this.lblUserName);
            this.Header.Controls.Add(this.uiAvatar5);
            this.Header.Controls.Add(this.uiAvatar2);
            this.Header.Size = new System.Drawing.Size(1225, 110);
            // 
            // uiAvatar2
            // 
            this.uiAvatar2.AvatarSize = 120;
            this.uiAvatar2.BackColor = System.Drawing.Color.Transparent;
            this.uiAvatar2.FillColor = System.Drawing.Color.Transparent;
            this.uiAvatar2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiAvatar2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(190)))), ((int)(((byte)(40)))));
            this.uiAvatar2.Icon = Sunny.UI.UIAvatar.UIIcon.Image;
            this.uiAvatar2.Image = global::StudentManagement.Properties.Resources.logo;
            this.uiAvatar2.Location = new System.Drawing.Point(3, 19);
            this.uiAvatar2.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiAvatar2.Name = "uiAvatar2";
            this.uiAvatar2.Radius = 0;
            this.uiAvatar2.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.uiAvatar2.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiAvatar2.Size = new System.Drawing.Size(126, 88);
            this.uiAvatar2.Style = Sunny.UI.UIStyle.Custom;
            this.uiAvatar2.TabIndex = 36;
            this.uiAvatar2.Text = "学生管理系统";
            // 
            // uiAvatar5
            // 
            this.uiAvatar5.AvatarSize = 300;
            this.uiAvatar5.BackColor = System.Drawing.Color.Transparent;
            this.uiAvatar5.FillColor = System.Drawing.Color.Transparent;
            this.uiAvatar5.Font = new System.Drawing.Font("华文行楷", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiAvatar5.Icon = Sunny.UI.UIAvatar.UIIcon.Text;
            this.uiAvatar5.Location = new System.Drawing.Point(125, 30);
            this.uiAvatar5.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiAvatar5.Name = "uiAvatar5";
            this.uiAvatar5.Radius = 20;
            this.uiAvatar5.Shape = Sunny.UI.UIShape.Square;
            this.uiAvatar5.Size = new System.Drawing.Size(243, 60);
            this.uiAvatar5.Style = Sunny.UI.UIStyle.Custom;
            this.uiAvatar5.StyleCustomMode = true;
            this.uiAvatar5.TabIndex = 37;
            this.uiAvatar5.Text = "学生管理系统";
            // 
            // userImage
            // 
            this.userImage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.userImage.AvatarSize = 85;
            this.userImage.FillColor = System.Drawing.Color.White;
            this.userImage.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.userImage.Icon = Sunny.UI.UIAvatar.UIIcon.Image;
            this.userImage.Location = new System.Drawing.Point(1045, 16);
            this.userImage.MinimumSize = new System.Drawing.Size(1, 1);
            this.userImage.Name = "userImage";
            this.userImage.Size = new System.Drawing.Size(91, 91);
            this.userImage.Style = Sunny.UI.UIStyle.Custom;
            this.userImage.TabIndex = 38;
            this.userImage.Click += new System.EventHandler(this.userImage_Click);
            // 
            // lblUserName
            // 
            this.lblUserName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblUserName.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblUserName.ForeColor = System.Drawing.Color.Gray;
            this.lblUserName.Location = new System.Drawing.Point(1142, 53);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(218, 37);
            this.lblUserName.StyleCustomMode = true;
            this.lblUserName.TabIndex = 39;
            this.lblUserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiContextMenuStrip1
            // 
            this.uiContextMenuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.uiContextMenuStrip1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiContextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.uiContextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmInfo,
            this.tsmUpdatePwd,
            this.tsmSetting,
            this.tsmExit});
            this.uiContextMenuStrip1.Name = "uiContextMenuStrip1";
            this.uiContextMenuStrip1.Size = new System.Drawing.Size(165, 132);
            this.uiContextMenuStrip1.Style = Sunny.UI.UIStyle.LightGray;
            this.uiContextMenuStrip1.StyleCustomMode = true;
            // 
            // tsmInfo
            // 
            this.tsmInfo.BackColor = System.Drawing.Color.Transparent;
            this.tsmInfo.ForeColor = System.Drawing.Color.Black;
            this.tsmInfo.Name = "tsmInfo";
            this.tsmInfo.Size = new System.Drawing.Size(164, 32);
            this.tsmInfo.Text = "基本资料";
            this.tsmInfo.Click += new System.EventHandler(this.tsmInfo_Click);
            // 
            // tsmUpdatePwd
            // 
            this.tsmUpdatePwd.BackColor = System.Drawing.Color.Transparent;
            this.tsmUpdatePwd.ForeColor = System.Drawing.Color.Black;
            this.tsmUpdatePwd.Name = "tsmUpdatePwd";
            this.tsmUpdatePwd.Size = new System.Drawing.Size(164, 32);
            this.tsmUpdatePwd.Text = "修改密码";
            this.tsmUpdatePwd.Click += new System.EventHandler(this.tsmUpdatePwd_Click);
            // 
            // tsmSetting
            // 
            this.tsmSetting.BackColor = System.Drawing.Color.Transparent;
            this.tsmSetting.ForeColor = System.Drawing.Color.Black;
            this.tsmSetting.Name = "tsmSetting";
            this.tsmSetting.Size = new System.Drawing.Size(164, 32);
            this.tsmSetting.Text = "系统设置";
            this.tsmSetting.Click += new System.EventHandler(this.tsmSetting_Click);
            // 
            // tsmExit
            // 
            this.tsmExit.BackColor = System.Drawing.Color.Transparent;
            this.tsmExit.ForeColor = System.Drawing.Color.Black;
            this.tsmExit.Name = "tsmExit";
            this.tsmExit.Size = new System.Drawing.Size(164, 32);
            this.tsmExit.Text = "退出登录";
            this.tsmExit.Click += new System.EventHandler(this.tsmExit_Click);
            // 
            // timelable
            // 
            this.timelable.Font = new System.Drawing.Font("微软雅黑", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.timelable.Location = new System.Drawing.Point(895, 59);
            this.timelable.Name = "timelable";
            this.timelable.Size = new System.Drawing.Size(281, 23);
            this.timelable.TabIndex = 40;
            this.timelable.Text = "uiLabel1";
            this.timelable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.timelable.UseCompatibleTextRendering = true;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1225, 923);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "主页";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.Controls.SetChildIndex(this.Header, 0);
            this.Controls.SetChildIndex(this.Aside, 0);
            this.Header.ResumeLayout(false);
            this.uiContextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Sunny.UI.UIAvatar uiAvatar2;
        private Sunny.UI.UIAvatar uiAvatar5;
        private Sunny.UI.UIAvatar userImage;
        private Sunny.UI.UILabel lblUserName;
        private Sunny.UI.UIContextMenuStrip uiContextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmUpdatePwd;
        private System.Windows.Forms.ToolStripMenuItem tsmSetting;
        private System.Windows.Forms.ToolStripMenuItem tsmExit;
        private System.Windows.Forms.ToolStripMenuItem tsmInfo;
        private Sunny.UI.UILabel timelable;
        private System.Windows.Forms.Timer timer1;
    }
}