﻿using BLL;
using BLL.Common;
using Model;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace StudentManagement
{

    public partial class FrmMain : UIHeaderAsideMainFrame
    {
        /// <summary>
        /// 存储角色的Id
        /// </summary>
        private int RoleId { get; set; }

        public FrmMain()
        {
            InitializeComponent();
            Program.Aside = this.Aside;

        }

        /// <summary>
        /// 初始化用户信息
        /// </summary>
        private void InitUserInfo()
        {
            if (Program.Type == "admin")
            {
                //ShowSuccessDialog(Application.StartupPath);
                this.RoleId = Program.Admin.RoleId;

                lblUserName.Text += string.Format("欢迎您，管理员：{0}", Program.Admin.AdminName);
                // userImage.Image = new Bitmap("../../images/" + Program.Admin.AdminImage);
                string str = Application.StartupPath + "/images/" + Program.Admin.AdminImage;
                userImage.Image = new Bitmap(Application.StartupPath + "/images/" + Program.Admin.AdminImage);
                //读取配置颜色
                ColorSetting colorSetting = new ColorSettingBLL().GetColor(Program.Admin.AdminAccount);
                if (colorSetting != null)
                {
                    //设置系统颜色
                    string[] rgbStr = colorSetting.UserColor.Split(",");
                    int[] rgb = Array.ConvertAll(rgbStr, int.Parse);
                    UIStyles.InitColorful(Color.FromArgb(rgb[0], rgb[1], rgb[2]), Color.White);
                    //修改Logo的填充颜色
                    uiAvatar2.FillColor = Color.Transparent;
                }
            }
            if (Program.Type == "student")
            {
                this.RoleId = Program.Student.RoleId;
                lblUserName.Text += string.Format("欢迎您,{0}", Program.Student.StudentName);

                //userImage.Image = new Bitmap("../../images/" + Program.Student.StudentImage);
                userImage.Image = new Bitmap(Application.StartupPath + "/images/" + Program.Student.StudentImage);
                ColorSetting colorSetting = new ColorSettingBLL().GetColor(Program.Student.StudentNo);
                if (colorSetting != null)
                {
                    //设置系统颜色
                    string[] rgbStr = colorSetting.UserColor.Split(",");
                    int[] rgb = Array.ConvertAll(rgbStr, int.Parse);
                    UIStyles.InitColorful(Color.FromArgb(rgb[0], rgb[1], rgb[2]), Color.White);
                    //修改Logo的填充颜色
                    uiAvatar2.FillColor = Color.Transparent;
                }
            }
            if (Program.Type == "teacher")
            {
                this.RoleId = Program.Teacher.RoleId;
                lblUserName.Text += string.Format("欢迎您，{0}", Program.Teacher.TeacherName);

                //Application.StartupPath
                //userImage.Image = new Bitmap("../../images/" + Program.Teacher.TeacherImage);
                userImage.Image = new Bitmap(Application.StartupPath + "/images/" + Program.Teacher.TeacherImage);
                ColorSetting colorSetting = new ColorSettingBLL().GetColor(Program.Teacher.TeacherNo);
                if (colorSetting != null)
                {
                    //设置系统颜色
                    string[] rgbStr = colorSetting.UserColor.Split(",");
                    int[] rgb = Array.ConvertAll(rgbStr, int.Parse);
                    UIStyles.InitColorful(Color.FromArgb(rgb[0], rgb[1], rgb[2]), Color.White);
                    //修改Logo的填充颜色
                    uiAvatar2.FillColor = Color.Transparent;
                }
            }
        }


        /// <summary>
        /// 节点索引
        /// </summary>
        int pageIndex = 2000;
        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmMain_Load(object sender, EventArgs e)
        {
            //初始化用户信息
            InitUserInfo();

            //1.从数据库读取根节点，并添加到导航栏
            Model.Menu menu = new MeunBLL().GetMeunRootInfo();
            //把根节点加入到导航栏
            TreeNode node = Aside.CreateNode(menu.MenuName, menu.MenuIcon, 24, pageIndex);
            //2.根据用户属性RoleId，从Sys_rolemenu读取对应的菜单信息,返回菜单信息应该是一个List
            List<Model.Menu> menus = new MeunBLL().GetMeunListInfo(this.RoleId);
            //3.递归出所有的子菜单
            CreateNode(menus, node, menu.MenuId);
            //4.存储导航栏对象用于页面之间跳转
            Program.Aside = this.Aside;
            // 5.存储Logo
            Program.uIAvatar = uiAvatar2;
            //6.存储头像
            Program.UserImage = userImage;
        }


        Dictionary<string, int> keyValues = new Dictionary<string, int>();

        /// <summary>
        /// 递归出所有子节点
        /// </summary>
        /// <param name="menus"></param>
        /// <param name="node"></param>
        /// <param name="v"></param>
        private void CreateNode(List<Model.Menu> menus, TreeNode parent, int parentId)
        {
            UIPage uI = null;
            TreeNode node = null;
            List<Model.Menu> childmenus = menus.Where(m => m.MenuParent == parentId).ToList();
            foreach (var item in childmenus)
            {
                if (item.MenuUrl == "")
                {
                    node = Aside.CreateNode(item.MenuName, item.MenuIcon, 24, ++pageIndex);

                }
                else
                {
                    if (item.MenuName == "首页")
                    {
                        ChangeIndex(item);
                    }
                    ChangeForm(item);
                    string name = this.GetType().Assembly.GetName().Name;
                    uI = (UIPage)Activator.CreateInstance(name, item.MenuUrl).Unwrap();
                    Aside.CreateChildNode(parent, item.MenuIcon, 24, AddPage(uI, ++pageIndex));
                }

                CreateNode(menus, node, item.MenuId);
            }

        }

        /// <summary>
        /// 根据不同的角色加载不同窗体
        /// </summary>
        /// <param name="item"></param>
        private void ChangeForm(Model.Menu item)
        {
            #region 加载班级管理
            if (item.MenuName == "班级信息管理")
            {
                if (Program.Type == "admin")
                {
                    item.MenuUrl = "StudentManagement.Admin.FrmClassManagement";
                }
                if (Program.Type == "teacher")
                {
                    item.MenuUrl = "StudentManagement.Teacher.FrmClassInfo";
                }
            }
            #endregion


            #region 加载课程管理
            if (item.MenuName == "课程信息管理")
            {
                if (Program.Type == "admin")
                {
                    item.MenuUrl = "StudentManagement.Admin.FrmCourseManagement";
                }
                if (Program.Type == "teacher")
                {
                    item.MenuUrl = "StudentManagement.Teacher.FrmCourseInfo";
                }
            }
            #endregion

            #region 加载奖惩管理
            if (item.MenuName == "奖惩信息管理")
            {
                if (Program.Type == "admin")
                {
                    item.MenuUrl = "StudentManagement.Admin.FrmEventManagement";
                }
                if (Program.Type == "student")
                {
                    item.MenuUrl = "StudentManagement.Student.FrmEvent";
                }
            }
            #endregion


            #region 加载成绩管理
            if (item.MenuName == "成绩信息管理")
            {
                if (Program.Type == "admin")
                {
                    item.MenuUrl = "StudentManagement.Admin.FrmScoreManagement";
                }
                if (Program.Type == "teacher")
                {
                    item.MenuUrl = "StudentManagement.Teacher.FrmScore";
                }
                if (Program.Type == "student")
                {
                    item.MenuUrl = "StudentManagement.Student.FrmScore";
                }
            }
            #endregion


            #region 加载学生管理
            if (item.MenuName == "学生信息管理")
            {
                if (Program.Type == "admin")
                {
                    item.MenuUrl = "StudentManagement.Admin.FrmStudentManager";
                }
                if (Program.Type == "teacher")
                {
                    item.MenuUrl = "StudentManagement.Teacher.FrmStudentInfo";
                }
            }
            #endregion
        }

        /// <summary>
        /// 根据不同的角色更换Url的值
        /// </summary>
        /// <param name="item"></param>
        private void ChangeIndex(Model.Menu item)
        {
            if (Program.Type == "admin")
            {
                item.MenuUrl = "StudentManagement.FrmIndex";
            }
            if (Program.Type == "student")
            {
                item.MenuUrl = "StudentManagement.Student.FrmIndex";
            }
            if (Program.Type == "teacher")
            {
                item.MenuUrl = "StudentManagement.Teacher.FrmIndex";
            }
        }

        /// <summary>
        /// 头像点击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void userImage_Click(object sender, EventArgs e)
        {
            userImage.ShowContextMenuStrip(uiContextMenuStrip1, -18, userImage.Height);
        }

        /// <summary>
        /// 退出登录
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmExit_Click(object sender, EventArgs e)
        {
            Application.Restart();//重写启动
        }

        /// <summary>
        /// 基本资料按钮 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmInfo_Click(object sender, EventArgs e)
        {

            if (Program.Type == "admin")
            {
                //跳转到管理员个人信息界面
                Aside.SelectPage(2004);
            }

            if (Program.Type == "student")
            {
                Aside.SelectPage(2003);

            }
            if (Program.Type == "teacher")
            {
                Aside.SelectPage(2004);
            }


        }


        /// <summary>
        /// 跳转到修改密码界面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmUpdatePwd_Click(object sender, EventArgs e)
        {

            if (Program.Type == "admin")
            {
                //跳转到修改密码界面
                Aside.SelectPage(2007);
            }

            if (Program.Type == "student")
            {
                //跳转到修改密码界面
                Aside.SelectPage(2007);

            }
            if (Program.Type == "teacher")
            {
                Aside.SelectPage(2007);
            }
        }
        /// <summary>
        /// 系统设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsmSetting_Click(object sender, EventArgs e)
        {
            //跳转到皮肤界面
            Aside.SelectPage(2006);


            if (Program.Type == "admin")
            {
                //跳转到皮肤界面
                Aside.SelectPage(2006);
            }

            if (Program.Type == "student")
            {
                //跳转到皮肤界面
                Aside.SelectPage(2005);
            }
            if (Program.Type == "teacher")
            {
                //跳转到皮肤界面
                Aside.SelectPage(2005);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timelable.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}
