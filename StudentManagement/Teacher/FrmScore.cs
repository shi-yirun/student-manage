﻿using Model;
using Sunny.UI;
using System;
using System.Data;
using System.Windows.Forms;
namespace StudentManagement.Teacher
{
    public partial class FrmScore : UIPage
    {
        bool isFirstLoad = true;
        public FrmScore()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCollegeEdit_Click(object sender, EventArgs e)
        {
            if (this.dgvScoreList.CurrentRow == null || this.dgvScoreList.RowCount == 0)
            {
                ShowWarningDialog("没有可编辑的行");
                return;

            }
            Score score = new Score();
            score.StuId = (int)this.dgvScoreList.SelectedRows[0].Cells["StuId"].Value;
            score.StudentName = this.dgvScoreList.SelectedRows[0].Cells["StudentName"].Value.ToString();
            score.CourseId = (int)this.dgvScoreList.SelectedRows[0].Cells["CourseId"].Value;
            score.CourseName = this.dgvScoreList.SelectedRows[0].Cells["CourseName"].Value.ToString();
            score.Grade = (decimal)this.dgvScoreList.SelectedRows[0].Cells["Grade"].Value;


            FrmAdd frmAdd = new FrmAdd();
            frmAdd.Text = "修改班级信息";
            frmAdd.Tag = score;
            DialogResult result = frmAdd.ShowDialog();
            if (result == DialogResult.OK)
            {
                frmAdd.Dispose();
                BindScore();
            }
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            //绑定成绩信息
            BindScore();
        }

        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            //绑定成绩信息
            BindScore();

        }

        private void FrmScore_Load(object sender, EventArgs e)
        {
            if (isFirstLoad == true)
            {
                // 绑定学生信息
                BindStudent();
                //绑定课程信息
                BindCourse();

                //设置页面会触发页面改变事件
                this.uiPagination1.ActivePage = 1;
                isFirstLoad = false;
            }
        }

        /// <summary>
        /// 绑定成绩信息
        /// </summary>
        private void BindScore()
        {
            int index = this.uiPagination1.ActivePage - 1;
            int size = this.uiPagination1.PageSize;
            int begin = index * size + 1;
            int end = (index + 1) * size;
            //总行数
            int count;
            int studentName = (int)this.cmbName.SelectedValue;
            int courseName = (int)this.cmbCourse.SelectedValue;
            this.dgvScoreList.DataSource = new BLL.Teacher.CourseBLL().GetScoreList(begin, end, out count, studentName, courseName, Program.Teacher.Id);

            //设置行数
            this.uiPagination1.TotalCount = count;
        }

        /// <summary>
        /// 绑定课程信息
        /// </summary>
        private void BindCourse()
        {
            //获取教师Id
            int id = Program.Teacher.Id;
            cmbCourse.DisplayMember = "CourseName";
            cmbCourse.ValueMember = "CourseId";
            DataTable dataTable = new BLL.Teacher.CourseBLL().GetCourseList(id);
            DataRow dataRow = dataTable.NewRow();
            dataRow["CourseId"] = 0;
            dataRow["CourseName"] = "请选择课程";
            dataTable.Rows.InsertAt(dataRow, 0);
            this.cmbCourse.DataSource = dataTable;

            cmbCourse.SelectedIndex = 0;
        }

        /// <summary>
        /// 绑定学生信息
        /// </summary>
        private void BindStudent()
        {
            //获取教师Id
            int id = Program.Teacher.Id;
            cmbName.DisplayMember = "studentName";
            cmbName.ValueMember = "stuId";
            DataTable dataTable = new BLL.Teacher.StudentBLL().GetStudentList(id);
            DataRow dataRow = dataTable.NewRow();
            dataRow["stuId"] = 0;
            dataRow["studentName"] = "请选择学生";
            dataTable.Rows.InsertAt(dataRow, 0);
            this.cmbName.DataSource = dataTable;
            cmbName.SelectedIndex = 0;
        }
    }
}
