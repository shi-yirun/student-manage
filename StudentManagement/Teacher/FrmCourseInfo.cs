﻿using BLL.Teacher;
using Sunny.UI;
using System;
using System.Data;
namespace StudentManagement.Teacher
{
    public partial class FrmCourseInfo : UIPage
    {
        public FrmCourseInfo()
        {
            InitializeComponent();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            CourseTableBind();
        }

        private void FrmCourseInfo_Load(object sender, EventArgs e)
        {
            CourseBind();
            this.uiPagination1.ActivePage = 1;
        }
        private void CourseBind()
        {
            this.cmbCourse.DisplayMember = "CourseName";
            this.cmbCourse.ValueMember = "Id";
            int id = Program.Teacher.Id;
            DataTable table = new TeacherBLL().GetTeacher(id);
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["CourseName"] = "请选择课程";
            table.Rows.InsertAt(row, 0);
            this.cmbCourse.DataSource = table;
        }
        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            CourseTableBind();
        }
        private void CourseTableBind()
        {
            //获取当前页面的index
            int index = this.uiPagination1.ActivePage - 1;
            int pageIndex = this.uiPagination1.ActivePage;
            int size = this.uiPagination1.PageSize;
            int begin = (index * size) + 1;
            int end = (index + 1) * size;
            int count;
            int id = Program.Teacher.Id;
            int course = (int)this.cmbCourse.SelectedValue;
            this.dgvCoureseList.DataSource = new TeacherBLL().SelectTeacher(id, course, out count, begin, end);
            uiPagination1.TotalCount = count;
        }
    }
}
