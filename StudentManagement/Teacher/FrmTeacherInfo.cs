﻿using BLL.Admin;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
namespace StudentManagement.Teacher
{
    public partial class FrmTeacherInfo : UIPage
    {
        string filename = "";
        public FrmTeacherInfo()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Model.Teacher teacher = new Model.Teacher();
            teacher.TeacherNo = txtTeacherNo.Text.Trim();
            teacher.TeacherName = txtName.Text.Trim();
            teacher.TeacherSex = radbBoy.Checked == true ? "男" : "女";
            teacher.TeacherNation = cmbNation.Text;
            teacher.TeacherCollegeId = (int)cmbCollege.SelectedValue;
            teacher.TeacherMajorId = (int)cmbMajor.SelectedValue;
            teacher.ClassId = (int)cmbClass.SelectedValue;
            teacher.TeacherDegree = cmbDegree.Text;
            teacher.TeacherTel = txtTel.Text.Trim();
            teacher.TeacherEmail = txtEmail.Text.Trim();
            teacher.TeacherBirthday = cmbDate.Value;
            teacher.TeacherStart_year = cmbStartYear.Text.Trim();
            teacher.TeacherImage = filename;
            if (string.IsNullOrEmpty(teacher.TeacherName))
            {
                ShowWarningDialog("姓名不能为空");
                return;
            }
            if (!string.IsNullOrEmpty(teacher.TeacherEmail) && !RegexHelper.IsEmail(teacher.TeacherEmail))
            {
                ShowWarningDialog("请输入正确的邮箱");
                return;
            }
            if (!string.IsNullOrEmpty(teacher.TeacherTel) && !RegexHelper.IsMobilePhone(teacher.TeacherTel))
            {
                ShowWarningDialog("请输入正确的电话");
                return;
            }
            // 判断学生邮箱是否存在
            if (!string.IsNullOrEmpty(teacher.TeacherEmail) &&
                 new BLL.Admin.TeacherBLL().IsExistTeacherEmail(teacher.TeacherEmail, teacher.TeacherNo))
            {
                ShowWarningDialog("邮箱已经存在了");
                return;
            }
            if (!string.IsNullOrEmpty(teacher.TeacherTel) &&
             new BLL.Admin.TeacherBLL().IsExistTeacherTel(teacher.TeacherTel, teacher.TeacherNo))
            {
                ShowWarningDialog("电话已经存在了");
                return;
            }


            if (cmbCollege.SelectedIndex == 0)
            {
                ShowWarningDialog("请选择学院");
                return;
            }
            if (cmbMajor.SelectedIndex == 0)
            {
                ShowWarningDialog("请选择专业");
                return;
            }
            if (cmbClass.SelectedIndex == 0)
            {
                ShowWarningDialog("请选择班级");
                return;
            }
            int result = new BLL.Teacher.TeacherBLL().TeacherUpdate(teacher);
            //Program.UserImage.Image = new Bitmap(@"../../images/" + filename);
            Program.UserImage.Image = new Bitmap(Application.StartupPath + "/images/" + filename);
            if (result >= 1)
            {
                ShowSuccessDialog("修改成功");
                //禁用修改按钮防止重复提交
                int Id = Program.Teacher.Id;
                Program.Teacher = teacher;
                Program.Teacher.Id = Id;

            }
            else
            {
                ShowSuccessDialog("修改失败");

            }

        }

        private void FrmTeacherInfo_Load(object sender, EventArgs e)
        {
            CollegeBind();
            //设置学院默认选中
            this.cmbCollege.SelectedIndex = 0;
            //绑定入学年份下拉框
            StartYearBind();
            //入学年份默认选中今年
            cmbStartYear.SelectedIndex = (DateTime.Now.Year - 1970);
            //绑定毕业年份下拉框

            //毕业年份默认选中今年

            //名族默认选中第一项
            cmbNation.SelectedIndex = 0;
            //学历默认选中第一项
            cmbDegree.SelectedIndex = 0;

            //判读tag对象是否为null,为null时就是添加，不为null就是修改

            string name = Program.Teacher.TeacherTel;
            Model.Teacher teacher = new BLL.Teacher.TeacherBLL().GetTeacherInfo(name);
            //为窗体赋值
            txtTeacherNo.Text = teacher.TeacherNo;
            txtName.Text = teacher.TeacherName;
            if (teacher.TeacherSex == "男") radbBoy.Checked = true;
            else radbGirl.Checked = true;
            cmbNation.Text = teacher.TeacherNation;
            cmbCollege.SelectedValue = teacher.TeacherCollegeId;
            cmbMajor.SelectedValue = teacher.TeacherMajorId;
            cmbClass.SelectedValue = teacher.ClassId;
            cmbDegree.Text = teacher.TeacherDegree;
            txtTel.Text = teacher.TeacherTel;
            txtEmail.Text = teacher.TeacherEmail;
            filename = teacher.TeacherImage;
            //userImage.Image = new Bitmap(@"../../images/" + teacher.TeacherImage);
            userImage.Image = new Bitmap(Application.StartupPath + "/images/" + teacher.TeacherImage);

            cmbDate.Value = teacher.TeacherBirthday;
            cmbStartYear.Text = teacher.TeacherStart_year;

        }

        private void StartYearBind()
        {
            for (int i = 1970; i <= 2099; i++)
            {
                cmbStartYear.Items.Add(i.ToString());
            }
        }
        private void CollegeBind()
        {
            this.cmbCollege.DisplayMember = "collegeName";
            this.cmbCollege.ValueMember = "Id";
            DataTable table = new CollegeBLL().GetCollegeList();
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["collegeName"] = "请选择学院";
            table.Rows.InsertAt(row, 0);
            this.cmbCollege.DataSource = table;
        }

        private void cmbCollege_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbCollege.SelectedIndex == 0)
            {
                //当学院选中的索引为0(为默认值),清空数据
                this.cmbMajor.DataSource = null;
                //清空items中数据，防止出现重复值
                this.cmbMajor.Items.Clear();
                //为专业下拉框添加一个默认值
                this.cmbMajor.Items.Insert(0, "请选择专业");
                this.cmbMajor.SelectedIndex = 0;
                return;
            }

            //获取对应学院下面专业信息
            int collegeId = Convert.ToInt32(this.cmbCollege.SelectedValue.ToString());
            //绑定专业信息
            MajorBind(collegeId);
        }
        private void MajorBind(int collegeId)
        {
            this.cmbMajor.DisplayMember = "majorName";
            this.cmbMajor.ValueMember = "Id";
            DataTable table = new MajorBLL().GetMajorList(collegeId);
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["majorName"] = "请选择专业";
            table.Rows.InsertAt(row, 0);
            this.cmbMajor.DataSource = table;
            this.cmbMajor.SelectedIndex = 0;

        }
        private void cmbMajor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cmbMajor.SelectedIndex == 0 || this.cmbMajor.SelectedIndex == -1)
            {
                //当专业选中的索引为0(为默认值),清空数据
                this.cmbClass.DataSource = null;
                //清空items中数据，防止出现重复值
                this.cmbClass.Items.Clear();
                //为班级下拉框添加一个默认值
                this.cmbClass.Items.Insert(0, "请选择班级");
                //默认选中第一项
                this.cmbClass.SelectedIndex = 0;
                return;
            }
            int majorId = Convert.ToInt32(this.cmbMajor.SelectedValue.ToString());
            //绑定专业信息
            ClassBind(majorId);
        }
        private void ClassBind(int majorId)
        {
            this.cmbClass.DisplayMember = "className";
            this.cmbClass.ValueMember = "Id";
            DataTable table = new ClassBLL().GetClassList(majorId);
            DataRow row = table.NewRow();
            row["Id"] = 0;
            row["className"] = "请选择班级";
            table.Rows.InsertAt(row, 0);
            this.cmbClass.DataSource = table;
            this.cmbClass.SelectedIndex = 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2002);
        }

        private void btnFileUpload_Click(object sender, EventArgs e)
        {

            openFileDialog1.Title = "选择上传头像";
            openFileDialog1.Filter = "图片类型|*.jpg;*.png;*.gif;|所有文件|*.*";
            openFileDialog1.Multiselect = false;
            //如果点了ok按钮就获取文件信息
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //获取文件所有在的位置
                string filePath = openFileDialog1.FileName;
                //获取文件名
                string fileName = openFileDialog1.SafeFileName;
                //目标路径
                //string destFileName = @"../../images/" + fileName;
                string destFileName = Application.StartupPath + "/images/" + fileName;

                this.filename = fileName;
                //判断文件是否存在
                if (File.Exists(destFileName) == false)
                {
                    File.Copy(filePath, destFileName);
                }
                else
                {
                    // 1.获取文件的拓展名
                    string extend = filename.Substring(filename.LastIndexOf("."));
                    //2.重新生成一个不重复的文件名
                    fileName = new SnowFlake(1, 1).GetId().ToString() + extend;
                    this.filename = fileName;
                    //3.copy文件
                    //destFileName = @"../../images/" + fileName;
                    destFileName = Application.StartupPath + "/images/" + fileName;

                    File.Copy(filePath, destFileName);
                }
                //设置用户头像的image属性
                userImage.Image = new Bitmap(destFileName);
            }
        }
    }
}
