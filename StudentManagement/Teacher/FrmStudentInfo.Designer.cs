﻿
namespace StudentManagement.Teacher
{
    partial class FrmStudentInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.btnFind = new Sunny.UI.UIButton();
            this.txtName = new Sunny.UI.UITextBox();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.txtStuNo = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.dgvStudentList = new Sunny.UI.UIDataGridView();
            this.uiPagination1 = new Sunny.UI.UIPagination();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentSex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentStart_year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentFinish_year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentNation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentBirthday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentDegree = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentImage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentTel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentPwd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collegeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.majorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.className = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentMajorId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoleId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.studentCollegeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudentList)).BeginInit();
            this.SuspendLayout();
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.btnFind);
            this.uiGroupBox1.Controls.Add(this.txtName);
            this.uiGroupBox1.Controls.Add(this.uiLabel3);
            this.uiGroupBox1.Controls.Add(this.txtStuNo);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(13, 14);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(1032, 81);
            this.uiGroupBox1.TabIndex = 110;
            this.uiGroupBox1.Text = "查询";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBox1.Click += new System.EventHandler(this.uiGroupBox1_Click);
            // 
            // btnFind
            // 
            this.btnFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFind.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnFind.Location = new System.Drawing.Point(901, 31);
            this.btnFind.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(100, 35);
            this.btnFind.TabIndex = 7;
            this.btnFind.Text = "查询";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // txtName
            // 
            this.txtName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtName.FillColor = System.Drawing.Color.White;
            this.txtName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtName.Location = new System.Drawing.Point(433, 32);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtName.Maximum = 2147483647D;
            this.txtName.Minimum = -2147483648D;
            this.txtName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(148, 35);
            this.txtName.TabIndex = 5;
            this.txtName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(328, 31);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(76, 35);
            this.uiLabel3.TabIndex = 4;
            this.uiLabel3.Text = "姓名：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtStuNo
            // 
            this.txtStuNo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtStuNo.FillColor = System.Drawing.Color.White;
            this.txtStuNo.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtStuNo.Location = new System.Drawing.Point(111, 32);
            this.txtStuNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtStuNo.Maximum = 2147483647D;
            this.txtStuNo.Minimum = -2147483648D;
            this.txtStuNo.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtStuNo.Name = "txtStuNo";
            this.txtStuNo.Size = new System.Drawing.Size(188, 35);
            this.txtStuNo.TabIndex = 1;
            this.txtStuNo.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(24, 31);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(80, 35);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "学号：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvStudentList
            // 
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.dgvStudentList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvStudentList.BackgroundColor = System.Drawing.Color.White;
            this.dgvStudentList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStudentList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvStudentList.ColumnHeadersHeight = 32;
            this.dgvStudentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvStudentList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.studentNo,
            this.studentName,
            this.studentSex,
            this.studentStart_year,
            this.studentFinish_year,
            this.studentNation,
            this.studentBirthday,
            this.studentDegree,
            this.studentImage,
            this.studentTel,
            this.studentEmail,
            this.studentPwd,
            this.collegeName,
            this.majorName,
            this.className,
            this.classId,
            this.studentMajorId,
            this.RoleId,
            this.studentCollegeId});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvStudentList.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvStudentList.EnableHeadersVisualStyles = false;
            this.dgvStudentList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.dgvStudentList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.dgvStudentList.Location = new System.Drawing.Point(12, 116);
            this.dgvStudentList.Name = "dgvStudentList";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStudentList.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvStudentList.RowHeadersWidth = 51;
            this.dgvStudentList.RowHeight = 27;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            this.dgvStudentList.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvStudentList.RowTemplate.Height = 27;
            this.dgvStudentList.SelectedIndex = -1;
            this.dgvStudentList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStudentList.ShowGridLine = true;
            this.dgvStudentList.Size = new System.Drawing.Size(1032, 404);
            this.dgvStudentList.TabIndex = 112;
            // 
            // uiPagination1
            // 
            this.uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPagination1.Location = new System.Drawing.Point(196, 544);
            this.uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPagination1.Name = "uiPagination1";
            this.uiPagination1.PageSize = 5;
            this.uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiPagination1.Size = new System.Drawing.Size(689, 43);
            this.uiPagination1.TabIndex = 111;
            this.uiPagination1.Text = "uiPagination1";
            this.uiPagination1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPagination1.PageChanged += new Sunny.UI.UIPagination.OnPageChangeEventHandler(this.uiPagination1_PageChanged);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "序号";
            this.Id.MinimumWidth = 6;
            this.Id.Name = "Id";
            this.Id.Visible = false;
            this.Id.Width = 125;
            // 
            // studentNo
            // 
            this.studentNo.DataPropertyName = "StudentNo";
            this.studentNo.HeaderText = "学号";
            this.studentNo.MinimumWidth = 6;
            this.studentNo.Name = "studentNo";
            this.studentNo.Width = 125;
            // 
            // studentName
            // 
            this.studentName.DataPropertyName = "StudentName";
            this.studentName.HeaderText = "名字";
            this.studentName.MinimumWidth = 6;
            this.studentName.Name = "studentName";
            this.studentName.Width = 125;
            // 
            // studentSex
            // 
            this.studentSex.DataPropertyName = "StudentSex";
            this.studentSex.HeaderText = "性别";
            this.studentSex.MinimumWidth = 6;
            this.studentSex.Name = "studentSex";
            this.studentSex.Width = 125;
            // 
            // studentStart_year
            // 
            this.studentStart_year.DataPropertyName = "StudentStart_year";
            this.studentStart_year.HeaderText = "入学年份";
            this.studentStart_year.MinimumWidth = 6;
            this.studentStart_year.Name = "studentStart_year";
            this.studentStart_year.Width = 125;
            // 
            // studentFinish_year
            // 
            this.studentFinish_year.DataPropertyName = "StudentFinish_year";
            this.studentFinish_year.HeaderText = "毕业年份";
            this.studentFinish_year.MinimumWidth = 6;
            this.studentFinish_year.Name = "studentFinish_year";
            this.studentFinish_year.Width = 125;
            // 
            // studentNation
            // 
            this.studentNation.DataPropertyName = "StudentNation";
            this.studentNation.HeaderText = "民族";
            this.studentNation.MinimumWidth = 6;
            this.studentNation.Name = "studentNation";
            this.studentNation.Width = 125;
            // 
            // studentBirthday
            // 
            this.studentBirthday.DataPropertyName = "StudentBirthday";
            this.studentBirthday.HeaderText = "出生日期";
            this.studentBirthday.MinimumWidth = 6;
            this.studentBirthday.Name = "studentBirthday";
            this.studentBirthday.Width = 125;
            // 
            // studentDegree
            // 
            this.studentDegree.DataPropertyName = "StudentDegree";
            this.studentDegree.HeaderText = "学历";
            this.studentDegree.MinimumWidth = 6;
            this.studentDegree.Name = "studentDegree";
            this.studentDegree.Width = 125;
            // 
            // studentImage
            // 
            this.studentImage.DataPropertyName = "StudentImage";
            this.studentImage.HeaderText = "学生照片";
            this.studentImage.MinimumWidth = 6;
            this.studentImage.Name = "studentImage";
            this.studentImage.Width = 125;
            // 
            // studentTel
            // 
            this.studentTel.DataPropertyName = "studentTel";
            this.studentTel.HeaderText = "电话号码";
            this.studentTel.MinimumWidth = 6;
            this.studentTel.Name = "studentTel";
            this.studentTel.Width = 125;
            // 
            // studentEmail
            // 
            this.studentEmail.DataPropertyName = "StudentEmail";
            this.studentEmail.HeaderText = "邮件";
            this.studentEmail.MinimumWidth = 6;
            this.studentEmail.Name = "studentEmail";
            this.studentEmail.Width = 125;
            // 
            // studentPwd
            // 
            this.studentPwd.DataPropertyName = "StudentPwd";
            this.studentPwd.HeaderText = "密码";
            this.studentPwd.MinimumWidth = 6;
            this.studentPwd.Name = "studentPwd";
            this.studentPwd.Visible = false;
            this.studentPwd.Width = 125;
            // 
            // collegeName
            // 
            this.collegeName.DataPropertyName = "CollegeName";
            this.collegeName.HeaderText = "学院";
            this.collegeName.MinimumWidth = 6;
            this.collegeName.Name = "collegeName";
            this.collegeName.Width = 125;
            // 
            // majorName
            // 
            this.majorName.DataPropertyName = "MajorName";
            this.majorName.HeaderText = "专业";
            this.majorName.MinimumWidth = 6;
            this.majorName.Name = "majorName";
            this.majorName.Width = 125;
            // 
            // className
            // 
            this.className.DataPropertyName = "ClassName";
            this.className.HeaderText = "班级";
            this.className.MinimumWidth = 6;
            this.className.Name = "className";
            this.className.Visible = false;
            this.className.Width = 125;
            // 
            // classId
            // 
            this.classId.DataPropertyName = "ClassId";
            this.classId.HeaderText = "班级号";
            this.classId.MinimumWidth = 6;
            this.classId.Name = "classId";
            this.classId.Visible = false;
            this.classId.Width = 125;
            // 
            // studentMajorId
            // 
            this.studentMajorId.DataPropertyName = "StudentMajorId";
            this.studentMajorId.HeaderText = "专业号";
            this.studentMajorId.MinimumWidth = 6;
            this.studentMajorId.Name = "studentMajorId";
            this.studentMajorId.Visible = false;
            this.studentMajorId.Width = 125;
            // 
            // RoleId
            // 
            this.RoleId.DataPropertyName = "RoleId";
            this.RoleId.HeaderText = "角色号";
            this.RoleId.MinimumWidth = 6;
            this.RoleId.Name = "RoleId";
            this.RoleId.Visible = false;
            this.RoleId.Width = 125;
            // 
            // studentCollegeId
            // 
            this.studentCollegeId.DataPropertyName = "StudentCollegeId";
            this.studentCollegeId.HeaderText = "学院号";
            this.studentCollegeId.MinimumWidth = 6;
            this.studentCollegeId.Name = "studentCollegeId";
            this.studentCollegeId.Visible = false;
            this.studentCollegeId.Width = 125;
            // 
            // FrmStudentInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1059, 609);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.dgvStudentList);
            this.Controls.Add(this.uiPagination1);
            this.Name = "FrmStudentInfo";
            this.Text = "学生信息";
            this.Load += new System.EventHandler(this.FrmStudentInfo_Load);
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudentList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UIButton btnFind;
        private Sunny.UI.UITextBox txtName;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UITextBox txtStuNo;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIDataGridView dgvStudentList;
        private Sunny.UI.UIPagination uiPagination1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentSex;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentStart_year;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentFinish_year;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentNation;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentBirthday;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentDegree;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentTel;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentPwd;
        private System.Windows.Forms.DataGridViewTextBoxColumn collegeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn majorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn className;
        private System.Windows.Forms.DataGridViewTextBoxColumn classId;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentMajorId;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoleId;
        private System.Windows.Forms.DataGridViewTextBoxColumn studentCollegeId;
    }
}