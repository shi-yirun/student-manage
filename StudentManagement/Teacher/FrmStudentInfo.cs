﻿using BLL.Admin;
using Sunny.UI;
using System;
namespace StudentManagement.Teacher
{
    public partial class FrmStudentInfo : UIPage
    {
        public FrmStudentInfo()
        {
            InitializeComponent();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            StudentBind();
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }
        private void StudentBind()
        {
            //获取当前页面的index
            int index = this.uiPagination1.ActivePage - 1;
            int pageIndex = this.uiPagination1.ActivePage;
            int size = this.uiPagination1.PageSize;
            int begin = (index * size) + 1;
            int end = (index + 1) * size;
            int count;
            //获取3个参数
            Model.Student student = new Model.Student();
            student.ClassId = Program.Teacher.ClassId;
            student.StudentNo = txtStuNo.Text;
            student.StudentName = txtName.Text;

            this.dgvStudentList.DataSource = new StudentBLL().GetStudentList(begin, end, out count, student);
            //设置分页控件总数
            uiPagination1.TotalCount = count;
        }

        private void uiPagination1_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            StudentBind();
        }

        private void FrmStudentInfo_Load(object sender, EventArgs e)
        {
            this.uiPagination1.ActivePage = 1;
        }
    }
}
