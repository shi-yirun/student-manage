﻿using BLL.Teacher;
using Sunny.UI;
using System;
namespace StudentManagement.Teacher
{
    public partial class FrmClassInfo : UIPage
    {
        public FrmClassInfo()
        {
            InitializeComponent();
        }

        private void FrmClassInfo_Initialize(object sender, EventArgs e)
        {
            this.dvgClassList.AutoGenerateColumns = false;
            int id = Program.Teacher.ClassId;
            this.dvgClassList.DataSource = new TeacherBLL().GetTeacherList(id);
        }
    }
}
