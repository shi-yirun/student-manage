﻿using Model;
using StudentManagement.Util;
using Sunny.UI;
using System;
using System.Windows.Forms;
namespace StudentManagement.Teacher
{
    public partial class FrmAdd : UIEditForm
    {
        public FrmAdd()
        {
            InitializeComponent();
        }

        private void Add_Load(object sender, EventArgs e)
        {
            // 获取成绩信息
            Score score = (Score)this.Tag;

            txtNane.Text = score.StudentName;
            txtCourseName.Text = score.CourseName;
            txtGrade.Text = score.Grade.ToString();

            ButtonOkClick += FrmAdd_ButtonOkClick;
            CheckedData += FrmAdd_CheckedData;


        }

        private bool FrmAdd_CheckedData(object sender, EditFormEventArgs e)
        {
            if (txtGrade.Text == "")
            {
                ShowWarningDialog("成绩不能为空");
                return false;
            }
            if (RegexHelper.IsFlootNumber(txtGrade.Text) == false)
            {
                ShowWarningDialog("成绩只能是正小数");
                return false;

            }

            return true;
        }

        /// <summary>
        /// 修改信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmAdd_ButtonOkClick(object sender, EventArgs e)
        {
            //获取成绩信息
            Score score = (Score)this.Tag;
            score.Grade = Convert.ToDecimal(txtGrade.Text);
            //修改信息
            int result = new BLL.Teacher.ScoreBLL().UpdateScore(score);
            if (result >= 1)
            {
                ShowSuccessDialog("修改成功");
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                ShowWarningDialog("修改成功");
            }



        }
    }
}
