﻿
namespace StudentManagement.Teacher
{
    partial class FrmCourseInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Find = new Sunny.UI.UIGroupBox();
            this.cmbCourse = new Sunny.UI.UIComboBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.btnFind = new Sunny.UI.UIButton();
            this.uiPagination1 = new Sunny.UI.UIPagination();
            this.dgvCoureseList = new Sunny.UI.UIDataGridView();
            this.CourseName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CourseGrade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.course_start_time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.course_end_time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.course_desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teacherName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TeacherId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Find.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCoureseList)).BeginInit();
            this.SuspendLayout();
            // 
            // Find
            // 
            this.Find.Controls.Add(this.cmbCourse);
            this.Find.Controls.Add(this.uiLabel2);
            this.Find.Controls.Add(this.btnFind);
            this.Find.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.Find.Location = new System.Drawing.Point(13, 14);
            this.Find.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Find.MinimumSize = new System.Drawing.Size(1, 1);
            this.Find.Name = "Find";
            this.Find.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.Find.Size = new System.Drawing.Size(1032, 81);
            this.Find.TabIndex = 144;
            this.Find.Text = "查询";
            this.Find.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbCourse
            // 
            this.cmbCourse.DataSource = null;
            this.cmbCourse.FillColor = System.Drawing.Color.White;
            this.cmbCourse.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbCourse.Location = new System.Drawing.Point(82, 37);
            this.cmbCourse.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCourse.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbCourse.Name = "cmbCourse";
            this.cmbCourse.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbCourse.Size = new System.Drawing.Size(150, 29);
            this.cmbCourse.TabIndex = 10;
            this.cmbCourse.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(11, 32);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(80, 35);
            this.uiLabel2.TabIndex = 9;
            this.uiLabel2.Text = "课程：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnFind
            // 
            this.btnFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFind.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnFind.Location = new System.Drawing.Point(901, 31);
            this.btnFind.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(100, 35);
            this.btnFind.TabIndex = 7;
            this.btnFind.Text = "查询";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // uiPagination1
            // 
            this.uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPagination1.Location = new System.Drawing.Point(186, 533);
            this.uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPagination1.Name = "uiPagination1";
            this.uiPagination1.PageSize = 5;
            this.uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiPagination1.Size = new System.Drawing.Size(689, 41);
            this.uiPagination1.TabIndex = 142;
            this.uiPagination1.Text = "uiPagination1";
            this.uiPagination1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPagination1.PageChanged += new Sunny.UI.UIPagination.OnPageChangeEventHandler(this.uiPagination1_PageChanged);
            // 
            // dgvCoureseList
            // 
            this.dgvCoureseList.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.dgvCoureseList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCoureseList.BackgroundColor = System.Drawing.Color.White;
            this.dgvCoureseList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCoureseList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCoureseList.ColumnHeadersHeight = 32;
            this.dgvCoureseList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvCoureseList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CourseName,
            this.CourseGrade,
            this.course_start_time,
            this.course_end_time,
            this.course_desc,
            this.teacherName,
            this.Id,
            this.TeacherId});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCoureseList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvCoureseList.EnableHeadersVisualStyles = false;
            this.dgvCoureseList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.dgvCoureseList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.dgvCoureseList.Location = new System.Drawing.Point(12, 113);
            this.dgvCoureseList.Name = "dgvCoureseList";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCoureseList.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCoureseList.RowHeadersWidth = 51;
            this.dgvCoureseList.RowHeight = 27;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.dgvCoureseList.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvCoureseList.RowTemplate.Height = 27;
            this.dgvCoureseList.SelectedIndex = -1;
            this.dgvCoureseList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCoureseList.ShowGridLine = true;
            this.dgvCoureseList.Size = new System.Drawing.Size(1032, 398);
            this.dgvCoureseList.TabIndex = 145;
            // 
            // CourseName
            // 
            this.CourseName.DataPropertyName = "CourseName";
            this.CourseName.HeaderText = "课程名";
            this.CourseName.MinimumWidth = 6;
            this.CourseName.Name = "CourseName";
            this.CourseName.Width = 125;
            // 
            // CourseGrade
            // 
            this.CourseGrade.DataPropertyName = "CourseGrade";
            this.CourseGrade.HeaderText = "学分";
            this.CourseGrade.MinimumWidth = 6;
            this.CourseGrade.Name = "CourseGrade";
            this.CourseGrade.Width = 125;
            // 
            // course_start_time
            // 
            this.course_start_time.DataPropertyName = "course_start_time";
            this.course_start_time.HeaderText = "开始时间";
            this.course_start_time.MinimumWidth = 6;
            this.course_start_time.Name = "course_start_time";
            this.course_start_time.Width = 125;
            // 
            // course_end_time
            // 
            this.course_end_time.DataPropertyName = "course_end_time";
            this.course_end_time.HeaderText = "结束时间";
            this.course_end_time.MinimumWidth = 6;
            this.course_end_time.Name = "course_end_time";
            this.course_end_time.Width = 125;
            // 
            // course_desc
            // 
            this.course_desc.DataPropertyName = "course_desc";
            this.course_desc.HeaderText = "描述";
            this.course_desc.MinimumWidth = 6;
            this.course_desc.Name = "course_desc";
            this.course_desc.Width = 125;
            // 
            // teacherName
            // 
            this.teacherName.DataPropertyName = "teacherName";
            this.teacherName.HeaderText = "教师";
            this.teacherName.MinimumWidth = 6;
            this.teacherName.Name = "teacherName";
            this.teacherName.Visible = false;
            this.teacherName.Width = 125;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "课程号";
            this.Id.MinimumWidth = 6;
            this.Id.Name = "Id";
            this.Id.Visible = false;
            this.Id.Width = 125;
            // 
            // TeacherId
            // 
            this.TeacherId.DataPropertyName = "TeacherId";
            this.TeacherId.HeaderText = "教师号";
            this.TeacherId.MinimumWidth = 6;
            this.TeacherId.Name = "TeacherId";
            this.TeacherId.Visible = false;
            this.TeacherId.Width = 125;
            // 
            // FrmCourseInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 612);
            this.Controls.Add(this.dgvCoureseList);
            this.Controls.Add(this.Find);
            this.Controls.Add(this.uiPagination1);
            this.Name = "FrmCourseInfo";
            this.Text = "课程信息";
            this.Load += new System.EventHandler(this.FrmCourseInfo_Load);
            this.Find.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCoureseList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIGroupBox Find;
        private Sunny.UI.UIComboBox cmbCourse;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UIButton btnFind;
        private Sunny.UI.UIPagination uiPagination1;
        private Sunny.UI.UIDataGridView dgvCoureseList;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseGrade;
        private System.Windows.Forms.DataGridViewTextBoxColumn course_start_time;
        private System.Windows.Forms.DataGridViewTextBoxColumn course_end_time;
        private System.Windows.Forms.DataGridViewTextBoxColumn course_desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn teacherName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn TeacherId;
    }
}