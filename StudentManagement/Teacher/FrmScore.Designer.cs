﻿
namespace StudentManagement.Teacher
{
    partial class FrmScore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvScoreList = new Sunny.UI.UIDataGridView();
            this.StuId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StudentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CourseName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CourseId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uiPagination1 = new Sunny.UI.UIPagination();
            this.Find = new Sunny.UI.UIGroupBox();
            this.cmbName = new Sunny.UI.UIComboBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.cmbCourse = new Sunny.UI.UIComboBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.btnFind = new Sunny.UI.UIButton();
            this.btnTeacherEdit = new Sunny.UI.UISymbolButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgvScoreList)).BeginInit();
            this.Find.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvScoreList
            // 
            this.dgvScoreList.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.dgvScoreList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvScoreList.BackgroundColor = System.Drawing.Color.White;
            this.dgvScoreList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvScoreList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvScoreList.ColumnHeadersHeight = 32;
            this.dgvScoreList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvScoreList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StuId,
            this.StudentName,
            this.CourseName,
            this.Grade,
            this.CourseId});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvScoreList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvScoreList.EnableHeadersVisualStyles = false;
            this.dgvScoreList.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.dgvScoreList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.dgvScoreList.Location = new System.Drawing.Point(17, 179);
            this.dgvScoreList.Name = "dgvScoreList";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvScoreList.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvScoreList.RowHeadersWidth = 51;
            this.dgvScoreList.RowHeight = 27;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.dgvScoreList.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvScoreList.RowTemplate.Height = 27;
            this.dgvScoreList.SelectedIndex = -1;
            this.dgvScoreList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvScoreList.ShowGridLine = true;
            this.dgvScoreList.Size = new System.Drawing.Size(1032, 445);
            this.dgvScoreList.TabIndex = 138;
            // 
            // StuId
            // 
            this.StuId.DataPropertyName = "StuId";
            this.StuId.HeaderText = "学号";
            this.StuId.MinimumWidth = 6;
            this.StuId.Name = "StuId";
            this.StuId.Visible = false;
            this.StuId.Width = 125;
            // 
            // StudentName
            // 
            this.StudentName.DataPropertyName = "StudentName";
            this.StudentName.HeaderText = "学生";
            this.StudentName.MinimumWidth = 6;
            this.StudentName.Name = "StudentName";
            this.StudentName.Width = 125;
            // 
            // CourseName
            // 
            this.CourseName.DataPropertyName = "CourseName";
            this.CourseName.HeaderText = "课程名";
            this.CourseName.MinimumWidth = 6;
            this.CourseName.Name = "CourseName";
            this.CourseName.Width = 125;
            // 
            // Grade
            // 
            this.Grade.DataPropertyName = "Grade";
            this.Grade.HeaderText = "成绩";
            this.Grade.MinimumWidth = 6;
            this.Grade.Name = "Grade";
            this.Grade.Width = 125;
            // 
            // CourseId
            // 
            this.CourseId.DataPropertyName = "CourseId";
            this.CourseId.HeaderText = "课程号";
            this.CourseId.MinimumWidth = 6;
            this.CourseId.Name = "CourseId";
            this.CourseId.Visible = false;
            this.CourseId.Width = 125;
            // 
            // uiPagination1
            // 
            this.uiPagination1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPagination1.Location = new System.Drawing.Point(143, 632);
            this.uiPagination1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPagination1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPagination1.Name = "uiPagination1";
            this.uiPagination1.PageSize = 5;
            this.uiPagination1.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiPagination1.Size = new System.Drawing.Size(689, 43);
            this.uiPagination1.TabIndex = 139;
            this.uiPagination1.Text = "uiPagination1";
            this.uiPagination1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPagination1.PageChanged += new Sunny.UI.UIPagination.OnPageChangeEventHandler(this.uiPagination1_PageChanged);
            // 
            // Find
            // 
            this.Find.Controls.Add(this.cmbName);
            this.Find.Controls.Add(this.uiLabel1);
            this.Find.Controls.Add(this.cmbCourse);
            this.Find.Controls.Add(this.uiLabel2);
            this.Find.Controls.Add(this.btnFind);
            this.Find.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.Find.Location = new System.Drawing.Point(17, 27);
            this.Find.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Find.MinimumSize = new System.Drawing.Size(1, 1);
            this.Find.Name = "Find";
            this.Find.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.Find.Size = new System.Drawing.Size(1032, 81);
            this.Find.TabIndex = 140;
            this.Find.Text = "查询";
            this.Find.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbName
            // 
            this.cmbName.DataSource = null;
            this.cmbName.FillColor = System.Drawing.Color.White;
            this.cmbName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbName.Location = new System.Drawing.Point(168, 32);
            this.cmbName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbName.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbName.Name = "cmbName";
            this.cmbName.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbName.Size = new System.Drawing.Size(150, 29);
            this.cmbName.TabIndex = 12;
            this.cmbName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(81, 32);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(80, 35);
            this.uiLabel1.TabIndex = 11;
            this.uiLabel1.Text = "学生：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbCourse
            // 
            this.cmbCourse.DataSource = null;
            this.cmbCourse.FillColor = System.Drawing.Color.White;
            this.cmbCourse.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.cmbCourse.Location = new System.Drawing.Point(642, 31);
            this.cmbCourse.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbCourse.MinimumSize = new System.Drawing.Size(63, 0);
            this.cmbCourse.Name = "cmbCourse";
            this.cmbCourse.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.cmbCourse.Size = new System.Drawing.Size(148, 35);
            this.cmbCourse.TabIndex = 10;
            this.cmbCourse.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(555, 31);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(80, 35);
            this.uiLabel2.TabIndex = 9;
            this.uiLabel2.Text = "课程：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnFind
            // 
            this.btnFind.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFind.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnFind.Location = new System.Drawing.Point(901, 31);
            this.btnFind.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(100, 35);
            this.btnFind.TabIndex = 7;
            this.btnFind.Text = "查询";
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // btnTeacherEdit
            // 
            this.btnTeacherEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTeacherEdit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnTeacherEdit.Location = new System.Drawing.Point(17, 138);
            this.btnTeacherEdit.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnTeacherEdit.Name = "btnTeacherEdit";
            this.btnTeacherEdit.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            this.btnTeacherEdit.RectSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)(((System.Windows.Forms.ToolStripStatusLabelBorderSides.Top | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.btnTeacherEdit.Size = new System.Drawing.Size(46, 35);
            this.btnTeacherEdit.Symbol = 61508;
            this.btnTeacherEdit.SymbolSize = 35;
            this.btnTeacherEdit.TabIndex = 141;
            this.btnTeacherEdit.Click += new System.EventHandler(this.btnCollegeEdit_Click);
            // 
            // FrmScore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1061, 697);
            this.Controls.Add(this.btnTeacherEdit);
            this.Controls.Add(this.Find);
            this.Controls.Add(this.uiPagination1);
            this.Controls.Add(this.dgvScoreList);
            this.Name = "FrmScore";
            this.Text = "成绩信息";
            this.Load += new System.EventHandler(this.FrmScore_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvScoreList)).EndInit();
            this.Find.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIDataGridView dgvScoreList;
        private Sunny.UI.UIPagination uiPagination1;
        private Sunny.UI.UIGroupBox Find;
        private Sunny.UI.UIComboBox cmbCourse;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UIButton btnFind;
        private Sunny.UI.UISymbolButton btnTeacherEdit;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIComboBox cmbName;
        private System.Windows.Forms.DataGridViewTextBoxColumn StuId;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grade;
        private System.Windows.Forms.DataGridViewTextBoxColumn CourseId;
    }
}