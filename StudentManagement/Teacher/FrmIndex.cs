﻿using Sunny.UI;
using System;

namespace StudentManagement.Teacher
{
    public partial class FrmIndex : UIPage
    {
        public FrmIndex()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 成绩信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPower_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2013);
        }

        /// <summary>
        /// 个人信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnInfo_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2003);
        }

        /// <summary>
        /// 人员信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenber_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2008);
        }

        /// <summary>
        /// 课程信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTeach_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2012);
        }

        /// <summary>
        /// 工具箱
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTools_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2014);
        }

        /// <summary>
        /// 系统设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSetting_Click(object sender, EventArgs e)
        {
            Program.Aside.SelectPage(2005);
        }
    }
}
