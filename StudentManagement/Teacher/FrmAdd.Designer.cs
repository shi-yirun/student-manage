﻿
namespace StudentManagement.Teacher
{
    partial class FrmAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.txtGrade = new Sunny.UI.UITextBox();
            this.txtCourseName = new Sunny.UI.UITextBox();
            this.txtNane = new Sunny.UI.UITextBox();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.pnlBtm.SuspendLayout();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBtm
            // 
            this.pnlBtm.Location = new System.Drawing.Point(1, 345);
            this.pnlBtm.Size = new System.Drawing.Size(395, 50);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(267, 12);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(152, 12);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.txtGrade);
            this.uiGroupBox1.Controls.Add(this.txtCourseName);
            this.uiGroupBox1.Controls.Add(this.txtNane);
            this.uiGroupBox1.Controls.Add(this.uiLabel3);
            this.uiGroupBox1.Controls.Add(this.uiLabel2);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiGroupBox1.Location = new System.Drawing.Point(5, 49);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(384, 296);
            this.uiGroupBox1.TabIndex = 8;
            this.uiGroupBox1.Text = "成绩信息";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGrade
            // 
            this.txtGrade.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtGrade.FillColor = System.Drawing.Color.White;
            this.txtGrade.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtGrade.Location = new System.Drawing.Point(147, 236);
            this.txtGrade.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtGrade.Maximum = 2147483647D;
            this.txtGrade.Minimum = -2147483648D;
            this.txtGrade.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtGrade.Name = "txtGrade";
            this.txtGrade.Size = new System.Drawing.Size(201, 34);
            this.txtGrade.TabIndex = 13;
            this.txtGrade.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCourseName
            // 
            this.txtCourseName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCourseName.FillColor = System.Drawing.Color.White;
            this.txtCourseName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtCourseName.Location = new System.Drawing.Point(147, 150);
            this.txtCourseName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCourseName.Maximum = 2147483647D;
            this.txtCourseName.Minimum = -2147483648D;
            this.txtCourseName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtCourseName.Name = "txtCourseName";
            this.txtCourseName.ReadOnly = true;
            this.txtCourseName.Size = new System.Drawing.Size(201, 34);
            this.txtCourseName.TabIndex = 12;
            this.txtCourseName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNane
            // 
            this.txtNane.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtNane.FillColor = System.Drawing.Color.White;
            this.txtNane.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtNane.Location = new System.Drawing.Point(147, 55);
            this.txtNane.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtNane.Maximum = 2147483647D;
            this.txtNane.Minimum = -2147483648D;
            this.txtNane.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtNane.Name = "txtNane";
            this.txtNane.ReadOnly = true;
            this.txtNane.Size = new System.Drawing.Size(201, 34);
            this.txtNane.TabIndex = 11;
            this.txtNane.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(21, 247);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(100, 23);
            this.uiLabel3.TabIndex = 10;
            this.uiLabel3.Text = "成绩：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(21, 161);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(100, 23);
            this.uiLabel2.TabIndex = 9;
            this.uiLabel2.Text = "课程名：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(21, 67);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(100, 23);
            this.uiLabel1.TabIndex = 8;
            this.uiLabel1.Text = "姓名：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 398);
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "FrmAdd";
            this.Text = "Add";
            this.Load += new System.EventHandler(this.Add_Load);
            this.Controls.SetChildIndex(this.pnlBtm, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            this.pnlBtm.ResumeLayout(false);
            this.uiGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UITextBox txtGrade;
        private Sunny.UI.UITextBox txtCourseName;
        private Sunny.UI.UITextBox txtNane;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel1;
    }
}