﻿
namespace StudentManagement
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogin));
            this.uiAvatar1 = new Sunny.UI.UIAvatar();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiPanel1 = new Sunny.UI.UIPanel();
            this.uiRadioButtonGroup1 = new Sunny.UI.UIRadioButtonGroup();
            this.uiRadioButton1 = new Sunny.UI.UIRadioButton();
            this.uiRadioButton3 = new Sunny.UI.UIRadioButton();
            this.uiRadioButton2 = new Sunny.UI.UIRadioButton();
            this.btnExit = new Sunny.UI.UISymbolButton();
            this.btnLogin = new Sunny.UI.UISymbolButton();
            this.uiTabControl1 = new Sunny.UI.UITabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.picCode = new System.Windows.Forms.PictureBox();
            this.txtUserCode = new Sunny.UI.UITextBox();
            this.txtUserPwd = new Sunny.UI.UITextBox();
            this.txtUserName = new Sunny.UI.UITextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnSendEmailCode = new Sunny.UI.UIButton();
            this.txtEmailCode = new Sunny.UI.UITextBox();
            this.txtUserEmail = new Sunny.UI.UITextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnSendPhoneCode = new Sunny.UI.UIButton();
            this.txtPhoneCode = new Sunny.UI.UITextBox();
            this.txtUserPhone = new Sunny.UI.UITextBox();
            this.uiLabel6 = new Sunny.UI.UILabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.uiPanel1.SuspendLayout();
            this.uiRadioButtonGroup1.SuspendLayout();
            this.uiTabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCode)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiAvatar1
            // 
            this.uiAvatar1.BackColor = System.Drawing.Color.Transparent;
            this.uiAvatar1.FillColor = System.Drawing.Color.SeaShell;
            this.uiAvatar1.Font = new System.Drawing.Font("微软雅黑", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiAvatar1.Location = new System.Drawing.Point(223, 68);
            this.uiAvatar1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiAvatar1.Name = "uiAvatar1";
            this.uiAvatar1.Size = new System.Drawing.Size(60, 60);
            this.uiAvatar1.Style = Sunny.UI.UIStyle.Custom;
            this.uiAvatar1.StyleCustomMode = true;
            this.uiAvatar1.TabIndex = 1;
            this.uiAvatar1.Text = "uiAvatar1";
            // 
            // uiLabel1
            // 
            this.uiLabel1.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel1.ForeColor = System.Drawing.Color.White;
            this.uiLabel1.Location = new System.Drawing.Point(134, 9);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(273, 56);
            this.uiLabel1.StyleCustomMode = true;
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "学生管理系统用户登录";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiPanel1
            // 
            this.uiPanel1.Controls.Add(this.uiRadioButtonGroup1);
            this.uiPanel1.Controls.Add(this.btnExit);
            this.uiPanel1.Controls.Add(this.btnLogin);
            this.uiPanel1.Controls.Add(this.uiTabControl1);
            this.uiPanel1.FillColor = System.Drawing.Color.White;
            this.uiPanel1.FillDisableColor = System.Drawing.Color.White;
            this.uiPanel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPanel1.Location = new System.Drawing.Point(20, 200);
            this.uiPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.RectColor = System.Drawing.Color.DimGray;
            this.uiPanel1.Size = new System.Drawing.Size(505, 396);
            this.uiPanel1.Style = Sunny.UI.UIStyle.Custom;
            this.uiPanel1.StyleCustomMode = true;
            this.uiPanel1.TabIndex = 3;
            this.uiPanel1.Text = null;
            this.uiPanel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uiRadioButtonGroup1
            // 
            this.uiRadioButtonGroup1.BackColor = System.Drawing.Color.White;
            this.uiRadioButtonGroup1.Controls.Add(this.uiRadioButton1);
            this.uiRadioButtonGroup1.Controls.Add(this.uiRadioButton3);
            this.uiRadioButtonGroup1.Controls.Add(this.uiRadioButton2);
            this.uiRadioButtonGroup1.FillColor = System.Drawing.Color.White;
            this.uiRadioButtonGroup1.FillDisableColor = System.Drawing.Color.White;
            this.uiRadioButtonGroup1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiRadioButtonGroup1.Location = new System.Drawing.Point(20, 267);
            this.uiRadioButtonGroup1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiRadioButtonGroup1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiRadioButtonGroup1.Name = "uiRadioButtonGroup1";
            this.uiRadioButtonGroup1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiRadioButtonGroup1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uiRadioButtonGroup1.Size = new System.Drawing.Size(456, 67);
            this.uiRadioButtonGroup1.Style = Sunny.UI.UIStyle.Custom;
            this.uiRadioButtonGroup1.StyleCustomMode = true;
            this.uiRadioButtonGroup1.TabIndex = 8;
            this.uiRadioButtonGroup1.Text = "选择身份登录";
            this.uiRadioButtonGroup1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uiRadioButton1
            // 
            this.uiRadioButton1.BackColor = System.Drawing.Color.White;
            this.uiRadioButton1.Checked = true;
            this.uiRadioButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiRadioButton1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiRadioButton1.Location = new System.Drawing.Point(56, 35);
            this.uiRadioButton1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiRadioButton1.Name = "uiRadioButton1";
            this.uiRadioButton1.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.uiRadioButton1.Radius = 20;
            this.uiRadioButton1.Size = new System.Drawing.Size(97, 29);
            this.uiRadioButton1.Style = Sunny.UI.UIStyle.Custom;
            this.uiRadioButton1.TabIndex = 9;
            this.uiRadioButton1.Text = "管理员";
            // 
            // uiRadioButton3
            // 
            this.uiRadioButton3.BackColor = System.Drawing.Color.White;
            this.uiRadioButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiRadioButton3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiRadioButton3.Location = new System.Drawing.Point(356, 35);
            this.uiRadioButton3.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiRadioButton3.Name = "uiRadioButton3";
            this.uiRadioButton3.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.uiRadioButton3.Size = new System.Drawing.Size(97, 29);
            this.uiRadioButton3.Style = Sunny.UI.UIStyle.Custom;
            this.uiRadioButton3.TabIndex = 11;
            this.uiRadioButton3.Text = "教师";
            // 
            // uiRadioButton2
            // 
            this.uiRadioButton2.BackColor = System.Drawing.Color.White;
            this.uiRadioButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiRadioButton2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiRadioButton2.Location = new System.Drawing.Point(206, 35);
            this.uiRadioButton2.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiRadioButton2.Name = "uiRadioButton2";
            this.uiRadioButton2.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.uiRadioButton2.Size = new System.Drawing.Size(97, 29);
            this.uiRadioButton2.Style = Sunny.UI.UIStyle.Custom;
            this.uiRadioButton2.TabIndex = 10;
            this.uiRadioButton2.Text = "学生";
            // 
            // btnExit
            // 
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnExit.Location = new System.Drawing.Point(352, 342);
            this.btnExit.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnExit.Name = "btnExit";
            this.btnExit.Radius = 25;
            this.btnExit.Size = new System.Drawing.Size(100, 35);
            this.btnExit.Style = Sunny.UI.UIStyle.Custom;
            this.btnExit.Symbol = 61453;
            this.btnExit.TabIndex = 13;
            this.btnExit.Text = "退出";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogin.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnLogin.Location = new System.Drawing.Point(59, 342);
            this.btnLogin.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Radius = 25;
            this.btnLogin.Size = new System.Drawing.Size(100, 35);
            this.btnLogin.Style = Sunny.UI.UIStyle.Custom;
            this.btnLogin.TabIndex = 12;
            this.btnLogin.Text = "登录";
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // uiTabControl1
            // 
            this.uiTabControl1.Controls.Add(this.tabPage1);
            this.uiTabControl1.Controls.Add(this.tabPage2);
            this.uiTabControl1.Controls.Add(this.tabPage3);
            this.uiTabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.uiTabControl1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTabControl1.ItemSize = new System.Drawing.Size(150, 40);
            this.uiTabControl1.Location = new System.Drawing.Point(20, 14);
            this.uiTabControl1.MainPage = "";
            this.uiTabControl1.MenuStyle = Sunny.UI.UIMenuStyle.Custom;
            this.uiTabControl1.Name = "uiTabControl1";
            this.uiTabControl1.SelectedIndex = 0;
            this.uiTabControl1.Size = new System.Drawing.Size(456, 245);
            this.uiTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.uiTabControl1.Style = Sunny.UI.UIStyle.Custom;
            this.uiTabControl1.TabBackColor = System.Drawing.Color.White;
            this.uiTabControl1.TabIndex = 8;
            this.uiTabControl1.TabSelectedColor = System.Drawing.Color.White;
            this.uiTabControl1.TabUnSelectedForeColor = System.Drawing.Color.DimGray;
            this.uiTabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.uiTabControl1_Selected);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.picCode);
            this.tabPage1.Controls.Add(this.txtUserCode);
            this.tabPage1.Controls.Add(this.txtUserPwd);
            this.tabPage1.Controls.Add(this.txtUserName);
            this.tabPage1.ForeColor = System.Drawing.Color.DimGray;
            this.tabPage1.Location = new System.Drawing.Point(0, 40);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(456, 205);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "账号密码登录";
            // 
            // picCode
            // 
            this.picCode.Location = new System.Drawing.Point(292, 155);
            this.picCode.Name = "picCode";
            this.picCode.Size = new System.Drawing.Size(117, 34);
            this.picCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCode.TabIndex = 17;
            this.picCode.TabStop = false;
            this.picCode.Click += new System.EventHandler(this.picCode_Click);
            // 
            // txtUserCode
            // 
            this.txtUserCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtUserCode.FillColor = System.Drawing.Color.White;
            this.txtUserCode.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtUserCode.Location = new System.Drawing.Point(39, 155);
            this.txtUserCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUserCode.Maximum = 2147483647D;
            this.txtUserCode.Minimum = -2147483648D;
            this.txtUserCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtUserCode.Name = "txtUserCode";
            this.txtUserCode.Radius = 30;
            this.txtUserCode.Size = new System.Drawing.Size(234, 34);
            this.txtUserCode.Style = Sunny.UI.UIStyle.Custom;
            this.txtUserCode.Symbol = 61528;
            this.txtUserCode.TabIndex = 7;
            this.txtUserCode.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtUserCode.Watermark = "请输入验证码";
            this.txtUserCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUserCode_KeyPress);
            // 
            // txtUserPwd
            // 
            this.txtUserPwd.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtUserPwd.FillColor = System.Drawing.Color.White;
            this.txtUserPwd.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtUserPwd.Location = new System.Drawing.Point(39, 92);
            this.txtUserPwd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUserPwd.Maximum = 2147483647D;
            this.txtUserPwd.Minimum = -2147483648D;
            this.txtUserPwd.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtUserPwd.Name = "txtUserPwd";
            this.txtUserPwd.PasswordChar = '*';
            this.txtUserPwd.Radius = 30;
            this.txtUserPwd.Size = new System.Drawing.Size(370, 34);
            this.txtUserPwd.Style = Sunny.UI.UIStyle.Custom;
            this.txtUserPwd.Symbol = 61475;
            this.txtUserPwd.TabIndex = 6;
            this.txtUserPwd.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtUserPwd.Watermark = "请输入密码";
            this.txtUserPwd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUserPwd_KeyPress);
            // 
            // txtUserName
            // 
            this.txtUserName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtUserName.FillColor = System.Drawing.Color.White;
            this.txtUserName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtUserName.Location = new System.Drawing.Point(39, 29);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUserName.Maximum = 2147483647D;
            this.txtUserName.Minimum = -2147483648D;
            this.txtUserName.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Radius = 30;
            this.txtUserName.Size = new System.Drawing.Size(370, 34);
            this.txtUserName.Style = Sunny.UI.UIStyle.Custom;
            this.txtUserName.Symbol = 62144;
            this.txtUserName.TabIndex = 5;
            this.txtUserName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtUserName.Watermark = "请输入用户名/邮箱/手机号";
            this.txtUserName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUserName_KeyPress);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Controls.Add(this.btnSendEmailCode);
            this.tabPage2.Controls.Add(this.txtEmailCode);
            this.tabPage2.Controls.Add(this.txtUserEmail);
            this.tabPage2.Location = new System.Drawing.Point(0, 40);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(450, 230);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "邮箱登录";
            // 
            // btnSendEmailCode
            // 
            this.btnSendEmailCode.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSendEmailCode.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSendEmailCode.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.btnSendEmailCode.ForeColor = System.Drawing.Color.Black;
            this.btnSendEmailCode.ForeHoverColor = System.Drawing.Color.Black;
            this.btnSendEmailCode.ForePressColor = System.Drawing.Color.Black;
            this.btnSendEmailCode.ForeSelectedColor = System.Drawing.Color.Black;
            this.btnSendEmailCode.Location = new System.Drawing.Point(281, 129);
            this.btnSendEmailCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSendEmailCode.Name = "btnSendEmailCode";
            this.btnSendEmailCode.Radius = 25;
            this.btnSendEmailCode.RectColor = System.Drawing.Color.White;
            this.btnSendEmailCode.RectSelectedColor = System.Drawing.Color.White;
            this.btnSendEmailCode.Size = new System.Drawing.Size(128, 31);
            this.btnSendEmailCode.Style = Sunny.UI.UIStyle.Custom;
            this.btnSendEmailCode.StyleCustomMode = true;
            this.btnSendEmailCode.TabIndex = 16;
            this.btnSendEmailCode.Text = "发送验证码";
            this.btnSendEmailCode.TipsColor = System.Drawing.Color.Black;
            this.btnSendEmailCode.Click += new System.EventHandler(this.btnSendEmailCode_Click);
            // 
            // txtEmailCode
            // 
            this.txtEmailCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEmailCode.FillColor = System.Drawing.Color.White;
            this.txtEmailCode.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtEmailCode.Location = new System.Drawing.Point(39, 129);
            this.txtEmailCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEmailCode.Maximum = 2147483647D;
            this.txtEmailCode.Minimum = -2147483648D;
            this.txtEmailCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtEmailCode.Name = "txtEmailCode";
            this.txtEmailCode.Radius = 30;
            this.txtEmailCode.Size = new System.Drawing.Size(235, 34);
            this.txtEmailCode.Style = Sunny.UI.UIStyle.Custom;
            this.txtEmailCode.Symbol = 61528;
            this.txtEmailCode.TabIndex = 15;
            this.txtEmailCode.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtEmailCode.Watermark = "验证码";
            this.txtEmailCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEmailCode_KeyPress);
            // 
            // txtUserEmail
            // 
            this.txtUserEmail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtUserEmail.FillColor = System.Drawing.Color.White;
            this.txtUserEmail.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtUserEmail.Location = new System.Drawing.Point(39, 63);
            this.txtUserEmail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUserEmail.Maximum = 2147483647D;
            this.txtUserEmail.Minimum = -2147483648D;
            this.txtUserEmail.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtUserEmail.Name = "txtUserEmail";
            this.txtUserEmail.Radius = 30;
            this.txtUserEmail.Size = new System.Drawing.Size(370, 34);
            this.txtUserEmail.Style = Sunny.UI.UIStyle.Custom;
            this.txtUserEmail.Symbol = 62144;
            this.txtUserEmail.TabIndex = 14;
            this.txtUserEmail.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtUserEmail.Watermark = "请输入邮箱";
            this.txtUserEmail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUserEmail_KeyPress);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.White;
            this.tabPage3.Controls.Add(this.btnSendPhoneCode);
            this.tabPage3.Controls.Add(this.txtPhoneCode);
            this.tabPage3.Controls.Add(this.txtUserPhone);
            this.tabPage3.Location = new System.Drawing.Point(0, 40);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(450, 230);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "手机号登录";
            // 
            // btnSendPhoneCode
            // 
            this.btnSendPhoneCode.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSendPhoneCode.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSendPhoneCode.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.btnSendPhoneCode.ForeColor = System.Drawing.Color.Black;
            this.btnSendPhoneCode.ForeHoverColor = System.Drawing.Color.Black;
            this.btnSendPhoneCode.ForePressColor = System.Drawing.Color.Black;
            this.btnSendPhoneCode.ForeSelectedColor = System.Drawing.Color.Black;
            this.btnSendPhoneCode.Location = new System.Drawing.Point(281, 114);
            this.btnSendPhoneCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSendPhoneCode.Name = "btnSendPhoneCode";
            this.btnSendPhoneCode.Radius = 25;
            this.btnSendPhoneCode.RectColor = System.Drawing.Color.White;
            this.btnSendPhoneCode.RectSelectedColor = System.Drawing.Color.White;
            this.btnSendPhoneCode.Size = new System.Drawing.Size(128, 31);
            this.btnSendPhoneCode.Style = Sunny.UI.UIStyle.Custom;
            this.btnSendPhoneCode.StyleCustomMode = true;
            this.btnSendPhoneCode.TabIndex = 19;
            this.btnSendPhoneCode.Text = "发送验证码";
            this.btnSendPhoneCode.TipsColor = System.Drawing.Color.Black;
            this.btnSendPhoneCode.Click += new System.EventHandler(this.txtSendPhoneCode_Click);
            // 
            // txtPhoneCode
            // 
            this.txtPhoneCode.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtPhoneCode.FillColor = System.Drawing.Color.White;
            this.txtPhoneCode.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtPhoneCode.Location = new System.Drawing.Point(39, 111);
            this.txtPhoneCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtPhoneCode.Maximum = 2147483647D;
            this.txtPhoneCode.Minimum = -2147483648D;
            this.txtPhoneCode.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtPhoneCode.Name = "txtPhoneCode";
            this.txtPhoneCode.Radius = 30;
            this.txtPhoneCode.Size = new System.Drawing.Size(235, 34);
            this.txtPhoneCode.Style = Sunny.UI.UIStyle.Custom;
            this.txtPhoneCode.Symbol = 61528;
            this.txtPhoneCode.TabIndex = 18;
            this.txtPhoneCode.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtPhoneCode.Watermark = "验证码";
            this.txtPhoneCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhoneCode_KeyPress);
            // 
            // txtUserPhone
            // 
            this.txtUserPhone.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtUserPhone.FillColor = System.Drawing.Color.White;
            this.txtUserPhone.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.txtUserPhone.Location = new System.Drawing.Point(39, 56);
            this.txtUserPhone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtUserPhone.Maximum = 2147483647D;
            this.txtUserPhone.Minimum = -2147483648D;
            this.txtUserPhone.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtUserPhone.Name = "txtUserPhone";
            this.txtUserPhone.Radius = 30;
            this.txtUserPhone.Size = new System.Drawing.Size(370, 34);
            this.txtUserPhone.Style = Sunny.UI.UIStyle.Custom;
            this.txtUserPhone.Symbol = 62144;
            this.txtUserPhone.TabIndex = 17;
            this.txtUserPhone.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.txtUserPhone.Watermark = "请输入用户名";
            this.txtUserPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUserPhone_KeyPress);
            // 
            // uiLabel6
            // 
            this.uiLabel6.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel6.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel6.ForeColor = System.Drawing.Color.Gainsboro;
            this.uiLabel6.Location = new System.Drawing.Point(190, 150);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Size = new System.Drawing.Size(143, 23);
            this.uiLabel6.StyleCustomMode = true;
            this.uiLabel6.TabIndex = 2;
            this.uiLabel6.Text = "学习改变世界";
            this.uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmLogin
            // 
            this.AllowShowTitle = false;
            this.BackgroundImage = global::StudentManagement.Properties.Resources.back;
            this.ClientSize = new System.Drawing.Size(547, 615);
            this.CloseAskString = "";
            this.Controls.Add(this.uiLabel6);
            this.Controls.Add(this.uiPanel1);
            this.Controls.Add(this.uiLabel1);
            this.Controls.Add(this.uiAvatar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmLogin";
            this.Padding = new System.Windows.Forms.Padding(0);
            this.ShowTitle = false;
            this.StyleCustomMode = true;
            this.Text = "登录";
            this.uiPanel1.ResumeLayout(false);
            this.uiRadioButtonGroup1.ResumeLayout(false);
            this.uiTabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picCode)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIAvatar uiAvatar1;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIPanel uiPanel1;
        private Sunny.UI.UITabControl uiTabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private Sunny.UI.UISymbolButton btnExit;
        private Sunny.UI.UISymbolButton btnLogin;
        private Sunny.UI.UITextBox txtUserPhone;
        private Sunny.UI.UITextBox txtUserCode;
        private Sunny.UI.UITextBox txtUserPwd;
        private Sunny.UI.UITextBox txtUserName;
        private Sunny.UI.UITextBox txtEmailCode;
        private Sunny.UI.UITextBox txtUserEmail;
        private Sunny.UI.UILabel uiLabel6;
        private Sunny.UI.UIButton btnSendPhoneCode;
        private Sunny.UI.UITextBox txtPhoneCode;
        private System.Windows.Forms.PictureBox picCode;
        private Sunny.UI.UIRadioButton uiRadioButton3;
        private Sunny.UI.UIRadioButton uiRadioButton2;
        private Sunny.UI.UIRadioButton uiRadioButton1;
        private Sunny.UI.UIRadioButtonGroup uiRadioButtonGroup1;
        private Sunny.UI.UIButton btnSendEmailCode;
        private System.Windows.Forms.Timer timer1;
    }
}