﻿using BLL;
using Model;
using Sunny.UI;
using System;
using System.Drawing;

namespace StudentManagement
{
    public partial class FrmSetting : UIPage
    {
        public FrmSetting()
        {
            InitializeComponent();
            uiPanel11.FillColor = uiPanel11.RectColor = RandomColor.GetColor(ColorScheme.Random, Luminosity.Bright);
            uiLabel2.Text = "RGB: " + uiPanel11.FillColor.R + ", " + uiPanel11.FillColor.G + ", " + uiPanel11.FillColor.B;
        }

        private void uiPanel1_Click(object sender, System.EventArgs e)
        {
            var panel = (UIPanel)sender;
            UIStyles.InitColorful(panel.FillColor, Color.White);
            //修改Logo的填充颜色
            Program.uIAvatar.FillColor = Color.Transparent;
            string color = panel.FillColor.R + ", " + panel.FillColor.G + ", " + panel.FillColor.B;
            GetColor(color);
        }

        private void GetColor(string color)
        {
            if (Program.Type == "admin")
            {
                string account = Program.Admin.AdminAccount;
                ColorSetting colorSetting = new ColorSettingBLL().GetColor(Program.Admin.AdminAccount);
                if (colorSetting == null)
                {
                    new ColorSettingBLL().AddColor(account, color);
                }
                else
                {
                    new ColorSettingBLL().UpdateColor(account, color);
                }

            }
            if (Program.Type == "student")
            {
                string account = Program.Student.StudentNo;
                ColorSetting colorSetting = new ColorSettingBLL().GetColor(Program.Student.StudentNo);
                if (colorSetting == null)
                {
                    new ColorSettingBLL().AddColor(account, color);
                }
                else
                {
                    new ColorSettingBLL().UpdateColor(account, color);
                }
            }
            if (Program.Type == "teacher")
            {
                string account = Program.Teacher.TeacherNo;
                ColorSetting colorSetting = new ColorSettingBLL().GetColor(Program.Teacher.TeacherNo);
                if (colorSetting == null)
                {
                    new ColorSettingBLL().AddColor(account, color);
                }
                else
                {
                    new ColorSettingBLL().UpdateColor(account, color);
                }
            }
        }

        private void uiPanel11_Click(object sender, System.EventArgs e)
        {
            uiPanel11.FillColor = uiPanel11.RectColor = RandomColor.GetColor(ColorScheme.Random, Luminosity.Bright);
            uiLabel2.Text = "RGB: " + uiPanel11.FillColor.R + ", " + uiPanel11.FillColor.G + ", " + uiPanel11.FillColor.B;
            UIStyles.InitColorful(uiPanel11.FillColor, Color.White);
            //修改Logo的填充颜色
            Program.uIAvatar.FillColor = Color.Transparent;
            string color = uiPanel11.FillColor.R + ", " + uiPanel11.FillColor.G + ", " + uiPanel11.FillColor.B;
            GetColor(color);
        }

        public static System.Drawing.Color GetRandomColor()
        {
            Random random = new Random(DateTime.Now.Millisecond);
            int int_Red = random.Next(255);
            int int_Green = random.Next(255);
            int int_Blue = random.Next(255);
            int_Blue = (int_Red + int_Green > 380) ? int_Red + int_Green - 380 : int_Blue;
            int_Blue = (int_Blue > 255) ? 255 : int_Blue;

            return GetDarkerColor(System.Drawing.Color.FromArgb(int_Red, int_Green, int_Blue));
        }

        public static Color GetDarkerColor(Color color)
        {
            const int max = 255;
            int increase = new Random(Guid.NewGuid().GetHashCode()).Next(30, 220); //还可以根据需要调整此处的值 
            int r = Math.Abs(Math.Min(color.R - increase, max));
            int g = Math.Abs(Math.Min(color.G - increase, max));
            int b = Math.Abs(Math.Min(color.B - increase, max));
            return Color.FromArgb(r, g, b);
        }

    }
}
